#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
import gc
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D
from ClassDef import MergeHists, SkipHist 

####################################################
#  #################################################
#  #
#  #        User defined variable 
#  #
#  #################################################
####################################################
SignalScale=10
SetLogY_1D = True
SetLogZ_2D = True 
SwitchBBToCC = True # switch name with BB to CC
UsingTop = True # use top quark process ttbar wt 
USETau = True #using taujet 
ZeroTagOnly = False #using 0tag only 
ZeroTagInfo="2tag"
DataOnPlot = True # plot data distribution on stack plots
ptv_cate_merge = False#
TarDir="Resolved_VHbbcc_cc_cp/"
SampleTYPE="cc"
#ROOTFILE_DIR  = "/home/chenye/work/CharmJet/VHCC/RootFile_VHcc/Merged_weight1/" # with flavor labal for VHbbcc2Lep VHcc 
#ROOTFILE_DIR  = "/home/chenye/work/CharmJet/VHCC/RootFile_VHbb/Merged/" # with flavor labal for VHbbcc2Lep  VHbb 0tag
ROOTFILE_DIR1  = "/home/chenye/work/CharmJet/VHCC/RootFile_VHcc/Resolved_3215_EMTopo/Hist/" # with flavor labal for VHbbcc2Lep VHcc 
ROOTFILE_DIR2  = "/home/chenye/work/CharmJet/VHCC/RootFile_VHcc/Resolved_3305_PFlow/Hist/" # with flavor labal for VHbbcc2Lep VHcc 
####################################################
#  #################################################
#  #
#  #        User defined drawing function
#  #
#  #################################################
####################################################
def CalCTagEff(histName,CTagEff):
    
    Eff = 1.0
    c = CTagEff["c"]
    b = CTagEff["b"]
    l = CTagEff["l"]
    t = CTagEff["tau"]
    if "H125_" in histName:
        Eff = Eff * b * b
    elif "H125cc_" in histName:
        Eff = Eff * c * c
    elif "cc_" in histName:
        Eff = Eff * c * c
    elif "bb_" in histName:
        Eff = Eff * b * b
    elif "ll_" in histName:
        Eff = Eff * l * l
    elif "bc_" in histName:
        Eff = Eff * b * c
    elif "bl_" in histName:
        Eff = Eff * b * l
    elif "cl_" in histName:
        Eff = Eff * c * l
    elif "tautau_" in histName:
        Eff = Eff * t * t
    elif "btau_" in histName:
        Eff = Eff * b * t
    elif "ctau_" in histName:
        Eff = Eff * c * t
    elif "ltau_" in histName:
        Eff = Eff * l * t
    elif "b_" in histName:
        Eff = Eff * b 
    elif "c_" in histName:
        Eff = Eff * c
    elif "l_" in histName:
        Eff = Eff * l
    print "CalCTagEff c-%f b-%f l-%f tau-%f Eff-%f"%(c,b,l,t,Eff)
        
    return Eff

def Hist2D_filllValue_phy_vs_ptv_simple(hist1DName, hist1D, weight, windowRange=[]):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    sumOfWeights = hist1D.GetSumOfWeights()
    sumOfWeights_window = sumOfWeights
    if "H" in hist1DName:
        print "??????????????   test ??????? %s "%hist1DName
        #print "Getintegral    : %f" %hist1D.GetIntegral()
        print "?? integral       : %f" %hist1D.Integral()
        print "?? sumOfWeights   : %f" %hist1D.GetSumOfWeights()
        print "?? Binwidth       : %f" %hist1D.GetBinWidth(1)
        print "?? Binwidth       : %f" %hist1D.GetBinWidth(2)
        print "?? integral/Binwidth : %f" % (hist1D.Integral()*1.0/hist1D.GetBinWidth(1))
        total = 0 
        for  x in range(0,hist1D.GetNbinsX()) :
            cot = hist1D.GetBinContent(x+1)
            center= hist1D.GetBinCenter(x+1)
            total += cot 
            print "?? x %d bincenter %f : %f"%(x,center,cot)
        print "?? total %f"    %total

    if len( windowRange )>0:
        #sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0/hist1D.GetBinWidth(2)
        sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0
    if ptv_cate_merge: 
        if "0_250ptv" in histName:
            ptv = 0.5
        elif "250_400ptv" in histName:
            ptv = 1.5
        elif "400ptv" in histName:
            ptv = 2.5
    else:
        if "75_150ptv" in histName:
            ptv = 0.5
        elif "150_250ptv" in histName:
            ptv = 1.5
        elif "250ptv" in histName:
            ptv = 2.5


    if "ggZllH125cccc_" in  histName: 
        phy=6.5
    elif "qqZllH125cccc_" in  histName: 
        phy=5.5
    elif "ggZllH125bb_" in  histName: 
        phy=4.5
    elif "qqZllH125bb_" in  histName: 
        phy=3.5
    elif "ggZZ" in  histName: 
        phy=2.5
    elif "ZZ" in  histName: 
        phy=1.5
    elif "WZ" in  histName: 
        phy=0.5
    elif "Z" in  histName: 
        phy=-0.5
    elif "ttbar" in  histName: 
        phy=-1.5
    elif "stopWt" in  histName: 
        phy=-2.5
    elif "stopt" in  histName: 
        phy=-3.5
    elif "stops" in  histName: 
        phy=-4.5
    
    x = ptv 
    if UsingTop :
        y = phy + 5
    else :
        y = phy

    return (x, y, sumOfWeights*weight, sumOfWeights_window*weight)

def Hist2D_filllValue_phy_vs_ptv_all(hist1DName, hist1D, weight, windowRange=[]):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    hist1D.Sumw2()
    sumOfWeights = hist1D.GetSumOfWeights()
    sumOfWeights_window = sumOfWeights
    if len( windowRange )>0:
        #sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0/hist1D.GetBinWidth(1)
        sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0
    
    if ptv_cate_merge:
        if "0_250ptv" in histName:
            ptv = 0.5
        elif "250_400ptv" in histName:
            ptv = 1.5
        elif "400ptv" in histName:
            ptv = 2.5
    else:
        if "75_150ptv" in histName:
            ptv = 0.5
        elif "150_250ptv" in histName:
            ptv = 1.5
        elif "250ptv" in histName:
            ptv = 2.5


    if "ggZllH125cccc_" in  histName: 
        phy=23.5
    elif "qqZllH125cccc_" in  histName: 
        phy=22.5
    elif "ggZllH125bb_" in  histName: 
        phy=21.5
    elif "qqZllH125bb_" in  histName: 
        phy=20.5
    elif "ggZZcc_" in  histName: 
        phy=19.5
    elif "ggZZbb_" in  histName: 
        phy=18.5
    elif "ggZZll_" in  histName: 
        phy=17.5
    elif "ggZZcl_" in  histName: 
        phy=16.5
    elif "ggZZbl_" in  histName: 
        phy=15.5
    elif "ggZZbc_" in  histName: 
        phy=14.5
    elif "ZZcc_" in  histName: 
        phy=13.5
    elif "ZZbb_" in  histName: 
        phy=12.5
    elif "ZZll_" in  histName: 
        phy=11.5
    elif "ZZcl_" in  histName: 
        phy=10.5
    elif "ZZbl_" in  histName: 
        phy=9.5
    elif "ZZbc_" in  histName: 
        phy=8.5
    elif "ZZbkg_" in  histName: 
        phy=7.5
    elif "Zcc_" in  histName: 
        phy=6.5
    elif "Zbb_" in  histName: 
        phy=5.5
    elif "Zll_" in  histName: 
        phy=4.5
    elif "Zcl_" in  histName: 
        phy=3.5
    elif "Zbl_" in  histName: 
        phy=2.5
    elif "Zbc_" in  histName: 
        phy=1.5
    elif "Z_" in  histName: 
        phy=0.5
    elif "ttbarcc_" in histName:
        phy = -0.5
    elif "ttbarbb_" in histName:
        phy = -1.5
    elif "ttbarll_" in histName:
        phy = -2.5
    elif "ttbarcl_" in histName:
        phy = -3.5
    elif "ttbarbl_" in histName:
        phy = -4.5
    elif "ttbarbc_" in histName:
        phy = -5.5
    elif "ttbar_" in histName:
        phy = -6.5
    elif "stopWtcc_" in histName:
        phy = -7.5
    elif "stopWtbb_" in histName:
        phy = -8.5
    elif "stopWtll_" in histName:
        phy = -9.5
    elif "stopWtcl_" in histName:
        phy = -10.5
    elif "stopWtbl_" in histName:
        phy = -11.5
    elif "stopWtbc_" in histName:
        phy = -12.5
    elif "stopWt_" in histName:
        phy = -13.5
    elif "stoptcc_" in histName:
        phy = -14.5
    elif "stoptbb_" in histName:
        phy = -15.5
    elif "stoptll_" in histName:
        phy = -16.5
    elif "stoptcl_" in histName:
        phy = -17.5
    elif "stoptbl_" in histName:
        phy = -18.5
    elif "stoptbc_" in histName:
        phy = -19.5
    elif "stopt_" in histName:
        phy = -20.5
    elif "stopscc_" in histName:
        phy = -21.5
    elif "stopsbb_" in histName:
        phy = -22.5
    elif "stopsll_" in histName:
        phy = -23.5
    elif "stopscl_" in histName:
        phy = -24.5
    elif "stopsbl_" in histName:
        phy = -25.5
    elif "stopsbc_" in histName:
        phy = -26.5
    elif "stops_" in histName:
        phy = -27.5
    else : ## tmp steeing
        phy = -27.5

    
    
    x = ptv 
    if UsingTop : 
        y = phy + 28
    else :
        y = phy

    return (x, y, sumOfWeights * weight, sumOfWeights_window * weight)

def Hist2D_filllValue_phy_vs_ntag(hist1DName, hist1D):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    integral = hist1D.GetSumOfWeights()
    
    if "0tag" in histName:
        tag = 0.5
    elif "1tag" in histName:
        tag = 1.5
    elif "2tag" in histName:
        tag = 2.5
    elif "3ptag" in histName:
        tag = 3.5

    if "ZZbkg" in  histName: 
        phy=0.5
    elif "ZZ" in histName:
        phy=1.5
    elif "Zcc" in histName:
        phy=2.5
    elif "Zcl" in histName:
        phy =3.5 
    elif "qqZll" in histName :
        phy=5.5
    elif "ggZll" in histName :
        phy=6.5
    elif "Zl" in histName :
        phy=4.5
    
    x = tag 
    y = phy
    return (tag, phy, integral)

def Hist2D_filllValue_njet_vs_ntag(hist1DName, hist1D):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    integral = hist1D.GetSumOfWeights()

    ## x value 
    if ptv_cate_merge:
        if "0_250ptv" in histName :
            ptv = 0
        elif "250_400ptv" in histName :
            ptv = 1
        else:
            ptv = 2
    else:
        if "75_150ptv" in histName :
            ptv = 0
        elif "150_250ptv" in histName :
            ptv = 1
        else:
            ptv = 2


    if "0tag" in histName:
        tag = 0.5
    elif "1tag" in histName:
        tag = 1.5
    elif "2tag" in histName:
        tag = 2.5
    elif "3ptag" in histName:
        tag = 3.5
    
    ## y value 
    if "2jet" in histName:
        jet = 0.5 
    elif "3jet" in histName:
        jet =1.5
    elif "4pjet" in histName:
        jet =2.5

    y = tag 
    x = ptv + 0.5  
    return (x,y,integral)
        
def ptvInfo_convert(ptvInfo):
    if ptv_cate_merge: 
        if ptvInfo == "400ptv":
            Info ="ZpT > 400 GeV"
        else:
            Info ="ZpT "+ (ptvInfo.replace("_","-")).replace("ptv"," GeV")
    else:
        if ptvInfo == "250ptv":
            Info ="ZpT > 250 GeV"
        else:
            Info ="ZpT "+ (ptvInfo.replace("_","-")).replace("ptv"," GeV")



    return Info
##########################################################
#######     Fill Hist Kinematics & Yields      
##########################################################

def FillHist_Kinematic_FatJet(SampleType, SampleTypeName, process_info, Physics_info, Hist2D_phy_vs_tag, InputRootFiles):
    '''
    Fill 1D & 2D Hists 
    2D Hist : Jet pTV vs. ntag 
    1D Hist : Physics variabloe Distribution (like mBB or pT)  of different pTV&ntag 

    Example : 
        SampleType     = "Z+C" 
        SampleTypeName = "ZC" 
        process_info   =  ZC_info
        Hist2D_phy_vs_tag     : 2D Jet Yeild Hist 
    '''
    if ZeroTagOnly :
        tag_info = [ZeroTagInfo]
    else:
        #tag_info = ["0tag", "1tag", "2tag", "3ptag"]
        tag_info = ["0tag", "1tag", "2tag"]


    if ptv_cate_merge:
        jet_info = [ "1pfat0pjet"]
        ptv_info = ["0_250ptv","250_400ptv","400ptv"]
    else:
        jet_info = [ "2jet","3pjet"]
        ptv_info = ["75_150ptv","150_250ptv","250ptv"]
    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]

    #SR_info="SR_2psubjet"  
    SR_info="SR"  

    for _phy in Physics_info :
        RootFile = InputRootFiles[SampleType]         

        for _process in process_info:
            ###  2D Hists ###
            if SwitchBBToCC :
                #_phytmp = _phy.replace("BB","CC")
                _phytmp = _phy.replace("B","C")
            else :
                _phytmp = _phy
            
            Hist2D_name = SampleTypeName+"_"+_process+"_"+_phytmp+"2D"
            Hist2D_title = SampleType+" : "+_process+" "+_phytmp+" Yields distribution"
            Hist2D = ROOT.TH2D(Hist2D_name, Hist2D_title,3,0,3,4,0,4)
            Hist2D.GetXaxis().SetBinLabel(1,"LPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(2,"MPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(3,"HPtV 1pfat+0jet")

            Hist2D.GetYaxis().SetBinLabel(1,"0 tag")
            Hist2D.GetYaxis().SetBinLabel(2,"1 tag")
            Hist2D.GetYaxis().SetBinLabel(3,"2 tag")
            Hist2D.GetYaxis().SetBinLabel(4,"3+ tag")
            Hist2D.GetZaxis().SetTitle("Number of evnets")

            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                drawHists = []
                legends   = []
                outputFileName = SampleTypeName+"_"+_process + "_" + _jet + "_"+ SR_info +"_"+_phytmp 
                #title = SampleType +" : " + _process + " " + _jet + " "+ SR_info 
                if "ZllHcc" in SampleType: 
                    title =  _process + " " + _jet 
                else :
                    title = SampleType +" : " + _process + " " + _jet 
                
                if SwitchBBToCC :
                    title = title.replace("B","C")
         
                for _tag in tag_info :
         
                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        ####_legendName= _process + " " + _tag+_jet + " " + _ptv 
                        _legendName=_tag + " " + ptvInfo_convert(_ptv)
                        
                        #skip blank hist 
                        print "------------------" 
                        tmpHist= RootFile.Get(_histName)
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 

                        copyHist=tmpHist.Clone("%s_copy"%_histName)
                        drawHists += [copyHist]
                        legends += [_legendName]
                        ## Fill 2D Hist 
                        FillValue_2D = Hist2D_filllValue_njet_vs_ntag(_histName , copyHist)
                        Hist2D.Fill(FillValue_2D[0], FillValue_2D[1], FillValue_2D[2])
                        FillValue_2D_phy_vs_ntag = Hist2D_filllValue_phy_vs_ntag(_histName, copyHist)
                        Hist2D_phy_vs_tag[_phy].Fill(FillValue_2D_phy_vs_ntag[0], FillValue_2D_phy_vs_ntag[1], FillValue_2D_phy_vs_ntag[2])
               
                ## Draw 1D Hist 
                HistPlot = HistMaker1D(drawHists,legends)
                if SwitchBBToCC :
                    _phytmp = _phy.replace("B","C")
                else :
                     _phytmp = _phy

                if ('pT' in _phy)  or ('Pt' in _phy) :
                    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
                    HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", legends, TarDir+outputFileName,[50,1500], SetLogY_1D)
                elif 'mBB' in _phy:
                    HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", legends, TarDir+outputFileName,[30,800], SetLogY_1D)
                else: 
                    HistPlot.DrawPlots_ATLAS(title, _phytmp,"Number of events", legends, TarDir+outputFileName,[30,800], SetLogY_1D)

            ## Draw 2D Hist tag vs jet 
            HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
            HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", TarDir+Hist2D_name,"COLZTEXT", SetLogZ_2D)

    h_cutflow_vhbb= RootFile.Get("CutFlow/PreselectionCutFlow")
    HistPlot_cutflow_vhbb = HistMaker1D([h_cutflow_vhbb],["Preselection cut flow"])
    HistPlot_cutflow_vhbb.DrawPlots_ATLAS(SampleTypeName+"Preselection cut flow", "","Number of events", ["Preselection cut flow"], TarDir+"CutFlow_Preselcetion"+"_"+SampleTypeName,[0,25], SetLogY_1D)

    h_cutflow_my= RootFile.Get("CutFlow/Nominal/CutsResolved")
    HistPlot_cutflow_my = HistMaker1D([h_cutflow_my],["Resolved cut flow"])
    HistPlot_cutflow_my.DrawPlots_ATLAS(SampleTypeName+"Resolved cut flow", "","Number of events", [ "Resolved cut flow"], TarDir+"CutFlow_Resolved"+"_"+SampleTypeName,[0,25], SetLogY_1D)
##########################################################
#######     Fill Hist Phy  
##########################################################

def FillHist_Phy_FatJet(SampleType_info, Sample_process_info, Physics_info, InputRootFiles, CTagEff={}):
    '''
    Fill 1D & 2D Hists 
    1D Hist : 
    2D Hist L 
    Example : 
        SampleType_info     = ["ZC","ZZ","ZB","ZL"        ]
        process_info   =  ZC_info                         
    '''                                                   
    applyTag = False 
    if len(CTagEff) > 0: 
        applyTag = True
                                                          

    if ZeroTagOnly:
        tag_info = [ZeroTagInfo]          
    else :
        #tag_info = ["0tag", "1tag", "2tag", "3ptag"]          
        tag_info = ["0tag", "1tag", "2tag"]          
                                                          

    if ptv_cate_merge:
        ptv_info = ["0_250ptv","250_400ptv","400ptv"]
        jet_info = [ "1pfat0pjet"]                            
    else:
        ptv_info = ["75_150ptv","150_250ptv","250ptv"]
        jet_info = [ "2jet","3pjet"]
    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]

    #SR_info="SR_2psubjet"  
    SR_info="SR"  

    Container_hists = {} # container with all hist Container_hists[_ptv][_tag]
    Container_hists_data = {} 
    Container_hists_sig = {} 
    Container_hists_bkg = {} 
    
    Container_legends = {} 
    Container_legends_sig = {} 
    Container_legends_bkg = {} 

    Container_MergeHistList = {} 
    _phy = Physics_info

    for _ptv in ptv_info :
        Container_hists[_ptv]       = []
        Container_hists_sig[_ptv]   = []
        Container_hists_bkg[_ptv]   = []
        Container_legends[_ptv]     = []
        Container_legends_sig[_ptv] = []
        Container_legends_bkg[_ptv] = []
        Container_MergeHistList[_ptv] = {}
        for _SampleType in SampleType_info :
            Container_MergeHistList[_ptv][_SampleType] = []
     
    #######################  
    ### Fill Containers 
    #               
    for _SampleType in SampleType_info :
        RootFile = InputRootFiles[_SampleType]         

        for _process in Sample_process_info[_SampleType]:
            ###  2D Hists ###
            if SwitchBBToCC :
                _phytmp = _phy.replace("B","C")
            else :
                _phytmp = _phy
            
            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                
                for _tag in tag_info :

                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        ####_legendName= _process + " " + _tag+_jet + " " + _ptv 
                        ####_legendName=_tag + " " + ptvInfo_convert(_ptv)

                        if "H" in  _SampleType : 
                            _legendName= _SampleType
                        elif "data" in _SampleType:
                            _legendName= "Data"  
                            #_histName = _histName.replace("tag","ptag")
                        else :
                            _legendName= _SampleType+" : "+_process+" "+_tag 
                        
                        #skip blank hist 
                        print "------------------" 
                        tmpHist= RootFile.Get(_histName)
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 
                        
                        #tmpHist.Sumw2()
                        #tmpHist.Rebin(2)
                        
                        copyHist=tmpHist.Clone("%s_copy"%_histName)
                        copyHist.Sumw2()
                        #copyHist.Rebin(4)
                        if applyTag: 
                            copyHist.Scale(CalCTagEff(_process+"_", CTagEff))
                        
                        Container_hists[_ptv] += [copyHist]
                        Container_legends[_ptv] += [_legendName]
                        #print "Container_MergeHistList ptv %s _SampleType %s"
                        #print Container_MergeHistList[_ptv][_SampleType] 
                        Container_MergeHistList[_ptv][_SampleType] += [copyHist]
                        if "stops" in _SampleType : 
                            print "COPYHISTT"
                            print Container_MergeHistList[_ptv][_SampleType]
                            

                        if "data" in _SampleType : 
                            Container_hists_data[_ptv] = [copyHist]
                        elif "H" in _SampleType : 
                            print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  signal %s  %s"%(_SampleType, _legendName)
                            Container_hists_sig[_ptv] += [copyHist]
                            Container_legends_sig[_ptv] += [_legendName]
                        else :
                            print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  Bkg  %s  %s"%(_SampleType, _legendName)
                            Container_hists_bkg[_ptv] += [copyHist]
                            Container_legends_bkg[_ptv] += [_legendName]

    #####################  
    ## Merge Hists initial
    #               
    
    MergedHists  = {}
    MergedHists_sig  = {}
    MergedHists_sig_cc  = {}
    MergedHists_sig_bb  = {}
    MergedHists_bkg  = {}
    MergedHists_data  = {}

    MergeLegends = {}
    MergeLegends_sig = {} 
    MergeLegends_bkg = {} 
    MergeLegends_data = {} 

    for _ptv in ptv_info : 
        MergedHists[_ptv]=[]
        MergedHists_sig[_ptv]=[]
        MergedHists_sig_cc[_ptv] = []
        MergedHists_sig_bb[_ptv] = []
        MergedHists_bkg[_ptv]=[]
        MergedHists_data[_ptv]=[]
        MergeLegends[_ptv]=[]
        MergeLegends_sig[_ptv]=[]
        MergeLegends_bkg[_ptv]=[]
        MergeLegends_data[_ptv]=[]

        for _SampleType in SampleType_info:
            print "%s %s"%(_SampleType,_ptv)
            print "Container_MergeHistList:"
         #   if _ptv == "400ptv" and _SampleType == "stops":
         #      continue
         #   if _ptv == "400ptv" and _SampleType == "stopt":
         #       continue
            print Container_MergeHistList[_ptv][_SampleType]
            if len( Container_MergeHistList[_ptv][_SampleType]) == 0:
                continue

            MergeLegends[_ptv] += [_SampleType] 
            if "data" in _SampleType: 
                MergeLegends_data[_ptv]+= [_SampleType] 
            elif "H" in _SampleType: 
                MergeLegends_sig[_ptv]+= [_SampleType] 
            else:
                MergeLegends_bkg[_ptv]+= [_SampleType] 

            h_merged = MergeHists(Container_MergeHistList[_ptv][_SampleType], _SampleType+_ptv ).Clone("h_%s_%s"%(_SampleType,_ptv))
            MergedHists[_ptv]+=[h_merged]

            if "data" in _SampleType: 
                MergedHists_data[_ptv] += [h_merged]
            elif "H" in _SampleType: 
                MergedHists_sig[_ptv] += [h_merged]
                if "cc" in _SampleType :
                    MergedHists_sig_cc[_ptv] += [h_merged]
                elif "bb" in _SampleType :
                    MergedHists_sig_bb[_ptv] += [h_merged]
            else :
                MergedHists_bkg[_ptv] += [h_merged]


    #####################  
    ##   Draw Plots   
    #               

    for _ptv in ptv_info : 
        
        ## 1D Hist 
        HistPlot = HistMaker1D(Container_hists[_ptv],Container_legends[_ptv])
        HistPlot.setHistStackSig(Container_hists_sig[_ptv],Container_legends_sig[_ptv])
        HistPlot.setHistStackBkg(Container_hists_bkg[_ptv],Container_legends_bkg[_ptv])

        if len(Container_hists_data) > 0 :
            HistPlot.setHistData(Container_hists_data[_ptv])

        HistPlot.setSignalScale(SignalScale)

        ## 1D Hist Merge
        HistPlot_Merge = HistMaker1D(MergedHists[_ptv],MergeLegends[_ptv])
        HistPlot_Merge.setHistStackSig(MergedHists_sig[_ptv],MergeLegends_sig[_ptv])
        HistPlot_Merge.setHistStackBkg(MergedHists_bkg[_ptv],MergeLegends_bkg[_ptv])

        if len(MergedHists_data[_ptv]) > 0 :
            HistPlot_Merge.setHistData(MergedHists_data[_ptv])
        
        HistPlot_Merge.setSignalScale(SignalScale)
        
        ## Set title and fileName
        if SwitchBBToCC :
            _phytmp = _phy.replace("B","C")
        else :
             _phytmp = _phy
        if applyTag :
            outputFileName = "CTag"+CTagEff["WP"]+"_"+_ptv+"_"+ SR_info +"_"+_phytmp 
        else:
            outputFileName = _ptv+"_"+ SR_info +"_"+_phytmp 
        if applyTag : 
            title =  "CTag"+CTagEff["WP"]+": "+ ptvInfo_convert(_ptv)    
        else:
            #title =  "BVeto: "+ ptvInfo_convert(_ptv)    
            title =  "BVeto+CTag: "+ ptvInfo_convert(_ptv)    
        
        if SwitchBBToCC :
            title = title.replace("B","C")
        print title
        print "outputFileName : %s"%outputFileName
        print "!!!!!!! VHCC-Ploter :  DrawStack !!!!! %s "%_ptv

        ## Draw Plots 
        if ('pT' in _phy)  or ('Pt' in _phy) :
            print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
            HistPlot.DrawStacks_ATLAS(title, _phytmp+" [GeV]","Number of events",TarDir+"stack_"+outputFileName, [50,1500], SetLogY_1D)
            HistPlot_Merge.DrawStacks_ATLAS(title, _phytmp+" [GeV]","Number of events",TarDir+"stack_Merge_"+outputFileName, [50,1500], SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", Container_legends[_ptv], TarDir+outputFileName+"_sig", [50,1500], "Sig", SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", Container_legends[_ptv], TarDir+outputFileName+"_all", [50,1500], "All", SetLogY_1D)
        elif ('mBB' in _phy) or ('mJ' in _phy):
            HistPlot.DrawStacks_ATLAS(title, _phytmp+" [GeV]","Number of events",TarDir+"stack_"+outputFileName, [30,800], SetLogY_1D)
            HistPlot_Merge.DrawStacks_ATLAS(title, _phytmp+" [GeV]","Number of events",TarDir+"stack_Merge_"+outputFileName, [30,800], SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", Container_legends[_ptv], TarDir+outputFileName+"_sig", [30,800], "Sig", SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", Container_legends[_ptv], TarDir+outputFileName+"_all", [30,800], "All", SetLogY_1D)
        else: 
            HistPlot.DrawStacks_ATLAS(title, _phytmp,"Number of events",TarDir+"stack_"+outputFileName, [30,800], SetLogY_1D)
            HistPlot_Merge.DrawStacks_ATLAS(title, _phytmp,"Number of events",TarDir+"stack_Merge_"+outputFileName, [30,800], SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp,"Number of events", Container_legends[_ptv], TarDir+outputFileName+"_sig", [30,800], "Sig", SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp,"Number of events", Container_legends[_ptv], TarDir+outputFileName+"_all", [30,800], "All", SetLogY_1D)
    
    #####################  
    ##   Save Hist  
    #               
    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    #
    # AllHist Save in 1 file 
    #

    if applyTag:
        File_Merged = TFile("output/Resolved_Hist_%s_%s.root"%(SampleTYPE,CTagEff["WP"]), "RECREATE");
    else:
        File_Merged = TFile("output/Resolved_Hist_%s.root"%(SampleTYPE), "RECREATE");
       
    h_merged_bkg={}
    h_merged_sig={}
    h_merged_sig_cc={}
    h_merged_sig_bb={}
    h_merged_bkg_sig={}
    h_merged_sig_bb_sig={}
    h_merged_sig_cc_sig={}

    for _ptv in ptv_info : 
        for hist_sig in MergedHists_sig[_ptv]:
            hist_sig.Write();
        for hist_bkg in MergedHists_bkg[_ptv]:
            hist_bkg.Write();
        for hist_data in MergedHists_data[_ptv]:
            hist_data.Write();

        h_merged_bkg[_ptv] = MergeHists(MergedHists_bkg[_ptv], _SampleType+_ptv ).Clone("h_bkg_%s"%(_ptv))
        h_merged_sig[_ptv] = MergeHists(MergedHists_sig[_ptv], _SampleType+_ptv ).Clone("h_sig_bbcc_%s"%(_ptv))
        h_merged_sig_cc[_ptv] = MergeHists(MergedHists_sig_cc[_ptv], _SampleType+_ptv ).Clone("h_sig_cc_%s"%(_ptv))
        h_merged_sig_bb[_ptv] = MergeHists(MergedHists_sig_bb[_ptv], _SampleType+_ptv ).Clone("h_sig_bb_%s"%(_ptv))
        h_merged_bkg[_ptv].Write()
        h_merged_sig[_ptv].Write()
        h_merged_sig_bb[_ptv].Write()
        h_merged_sig_cc[_ptv].Write()

    File_Merged.Close() 
    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    #
    # Save in VHcc file 
    #

    h_merged_sig_cc={}
    h_merged_sig_cc={}
    h_merged_sig_bb={}
    h_merged_bkg={}
    
    if applyTag:
        if ZeroTagOnly :                                                   
            File_Resolved_sigcc = TFile("output/Resolved_Hist_sigcc_%s_%s_%s.root"%(SampleTYPE,CTagEff["WP"],ZeroTagInfo), "RECREATE");
        else:
            File_Resolved_sigcc = TFile("output/Resolved_Hist_sigcc_%s_%s.root"%(SampleTYPE,CTagEff["WP"]), "RECREATE");
    else:
        if ZeroTagOnly :                                                   
            File_Resolved_sigcc = TFile("output/Resolved_Hist_sigcc_%s_%s.root"%(SampleTYPE,ZeroTagInfo), "RECREATE");
        else:
            File_Resolved_sigcc = TFile("output/Resolved_Hist_sigcc_%s.root"%(SampleTYPE), "RECREATE");
    
    for _ptv in ptv_info : 
        h_merged_sig_cc[_ptv] = MergeHists(MergedHists_sig_cc[_ptv], _SampleType+_ptv ).Clone("mcc_%s"%(_ptv))
        h_merged_sig_cc[_ptv].Write()
        h_merged_sig_cc_sig[_ptv]=MassWindowHist(h_merged_sig_cc[_ptv],"mcc_%s_sig"%(_ptv),75.1,144.9)
        h_merged_sig_cc_sig[_ptv].Write()
    
    print h_merged_sig_cc.values()
    h_merged_sig_cc_all = MergeHists(h_merged_sig_cc.values(), _SampleType+"mcc").Clone("mcc_all_ptv")
    h_merged_sig_cc_all.Write()
    h_merged_sig_cc_all_sig=MassWindowHist(h_merged_sig_cc_all,"mcc_all_ptv_sig",75.1,144.9)
    h_merged_sig_cc_all_sig.Write()
    File_Resolved_sigcc.Close() 

    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    #
    # Save in VHbb file 
    #

    if applyTag:
        if ZeroTagOnly :                                                   
            File_Resolved_sigbb = TFile("output/Resolved_Hist_sigbb_%s_%s_%s.root"%(SampleTYPE,CTagEff["WP"],ZeroTagInfo), "RECREATE");
        else:
            File_Resolved_sigbb = TFile("output/Resolved_Hist_sigbb_%s_%s.root"%(SampleTYPE,CTagEff["WP"]), "RECREATE");
    else:
        if ZeroTagOnly :                                                   
            File_Resolved_sigbb = TFile("output/Resolved_Hist_sigbb_%s_%s.root"%(SampleTYPE,ZeroTagInfo), "RECREATE");
        else:
            File_Resolved_sigbb = TFile("output/Resolved_Hist_sigbb_%s.root"%(SampleTYPE), "RECREATE");
    

    for _ptv in ptv_info : 
        h_merged_sig_bb[_ptv] = MergeHists(MergedHists_sig_bb[_ptv], _SampleType+_ptv ).Clone("mcc_%s"%(_ptv))
        h_merged_sig_bb[_ptv].Write()
        h_merged_sig_bb_sig[_ptv]=MassWindowHist(h_merged_sig_bb[_ptv],"mcc_%s_sig"%(_ptv),75.1,144.9)
        h_merged_sig_bb_sig[_ptv].Write()
    
    h_merged_sig_bb_all = MergeHists(h_merged_sig_bb.values(), _SampleType).Clone("mcc_all_ptv")
    h_merged_sig_bb_all.Write()
    h_merged_sig_bb_all_sig=MassWindowHist(h_merged_sig_bb_all,"mcc_all_ptv_sig",75.1,144.9)
    h_merged_sig_bb_all_sig.Write()
    File_Resolved_sigbb.Close() 

    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    #
    # Save in bkg file 
    #

    if applyTag:
        if ZeroTagOnly :                                                   
            File_Resolved_bkg = TFile("output/Resolved_Hist_bkg_%s_%s_%s.root"%(SampleTYPE,CTagEff["WP"],ZeroTagInfo), "RECREATE");
        else:
            File_Resolved_bkg = TFile("output/Resolved_Hist_bkg_%s_%s.root"%(SampleTYPE,CTagEff["WP"]), "RECREATE");
    else:
        if ZeroTagOnly :                                                   
            File_Resolved_bkg = TFile("output/Resolved_Hist_bkg_%s_%s.root"%(SampleTYPE,ZeroTagInfo), "RECREATE");
        else:
            File_Resolved_bkg = TFile("output/Resolved_Hist_bkg_%s.root"%(SampleTYPE), "RECREATE");
 
    for _ptv in ptv_info : 
        h_merged_bkg[_ptv] = MergeHists(MergedHists_bkg[_ptv], _SampleType+_ptv ).Clone("mcc_%s"%(_ptv))
        h_merged_bkg[_ptv].Write()
        h_merged_bkg_sig[_ptv]=MassWindowHist(h_merged_bkg[_ptv],"mcc_%s_sig"%(_ptv),75.1,144.9)
        h_merged_bkg_sig[_ptv].Write()

    h_merged_bkg_all = MergeHists(h_merged_bkg.values(), _SampleType).Clone("mcc_all_ptv")
    h_merged_bkg_all.Write()
    h_merged_bkg_all_sig=MassWindowHist(h_merged_bkg_all,"mcc_all_ptv_sig",75.1,144.9)
    h_merged_bkg_all_sig.Write()
    File_Resolved_bkg.Close() 
    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
    #
    # Save in VHcc x 100 file 
    #

    h_merged_sig_cc100={}
    
    if applyTag:
        if ZeroTagOnly :                                                   
            File_Resolved_sigcc100 = TFile("output/Resolved_Hist_sigcc100_%s_%s_%s.root"%(SampleTYPE,CTagEff["WP"],ZeroTagInfo), "RECREATE");
        else:
            File_Resolved_sigcc100 = TFile("output/Resolved_Hist_sigcc100_%s_%s.root"%(SampleTYPE,CTagEff["WP"]), "RECREATE");
    else:

        if ZeroTagOnly :                                                   
            File_Resolved_sigcc100 = TFile("output/Resolved_Hist_sigcc100_%s_%s.root"%(SampleTYPE,ZeroTagInfo), "RECREATE");
        else:
            File_Resolved_sigcc100 = TFile("output/Resolved_Hist_sigcc100_%s.root"%(SampleTYPE), "RECREATE");
    
    for _ptv in ptv_info : 
        h_merged_sig_cc100[_ptv] = MergeHists(MergedHists_sig_cc[_ptv], _SampleType+_ptv ).Clone("mcc_%s"%(_ptv))
        h_merged_sig_cc100[_ptv].Scale(100)
        h_merged_sig_cc100[_ptv].Write()
    
    h_merged_sig_cc100_all = MergeHists(h_merged_sig_cc100.values(), _SampleType).Clone("mcc_all_ptv")
    h_merged_sig_cc100_all.Write()

    File_Resolved_sigcc100.Close() 

#########################################################
#######     Fill hist phy Compare
##########################################################

def FillHist_Phy_Compare(SampleType_info, Sample_process_info, Physics_info, InputRootFiles_1, InputRootFiles_2, HistLabel_1,HistLabel_2):
    '''
    Fill 1D & 2D Hists 
    1D Hist : 
    2D Hist L 
    Example : 
        SampleType_info     = ["ZC","ZZ","ZB","ZL"        ]
        process_info   =  ZC_info                         
    '''                                                   

    print "!!!!!!!!!!!!!!!!!! FillHist_Phy_Compare !!!!!!!!!!!!!!" 
                                                          
    # Defalte Settings 
    gc.set_debug(gc.DEBUG_STATS|gc.DEBUG_LEAK)

    if ZeroTagOnly:
        tag_info = [ZeroTagInfo]          
    else :
        #tag_info = ["0tag", "1tag", "2tag", "3ptag"]          
        tag_info = ["0tag", "1tag", "2tag"]          
                                                          

    if ptv_cate_merge:
        ptv_info = ["0_250ptv","250_400ptv","400ptv"]
        jet_info = [ "1pfat0pjet"]                            
    else:
        ptv_info = ["75_150ptv","150_250ptv","250ptv"]
        jet_info = [ "2jet","3pjet"]
    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]

    #SR_info="SR_2psubjet"  
    SR_info="SR"  

    print "!!!!!!!!!!!!!!!!!! FillHist_Phy_Compare !!!!!!!!!!!!!! <<< 1 >>>" 
    ## Hist container 
    Container_hists_1_all = {} 
    Container_hists_2_all = {} 
    Container_hists_1_sig = {} 
    Container_hists_2_sig = {} 
    Container_hists_1_bkg = {} 
    Container_hists_2_bkg = {} 
    Container_Mergedhists_1_bkg = {} 
    Container_Mergedhists_2_bkg = {} 
    Container_Mergedhists_1_sig = {} 
    Container_Mergedhists_2_sig = {} 

    
    ## Legends container 
    '''
    Container_legends_1_all = {} 
    Container_legends_2_all = {} 
    Container_legends_1_sig = {} 
    Container_legends_2_sig = {} 
    Container_legends_1_bkg = {} 
    Container_legends_2_bkg = {} 
    '''
    Container_Mergedhists_1_sig_tag={}
    Container_Mergedhists_2_sig_tag={}
    Container_Mergedhists_1_bkg_tag={}
    Container_Mergedhists_2_bkg_tag={}
 
    print "!!!!!!!!!!!!!!!!!! FillHist_Phy_Compare !!!!!!!!!!!!!! <<< 2 >>>" 
    Container_hists_1_all.clear() 
    Container_hists_2_all.clear()
    Container_hists_1_sig.clear()
    Container_hists_2_sig.clear()
    Container_hists_1_bkg.clear()
    Container_hists_2_bkg.clear()
    Container_Mergedhists_1_bkg.clear() 
    Container_Mergedhists_2_bkg.clear()
    Container_Mergedhists_1_sig.clear()
    Container_Mergedhists_2_sig.clear()

    
    ## Legends container 
    ''' 
    Container_legends_1_all.clear()  
    Container_legends_2_all.clear()  
    Container_legends_1_sig.clear()  
    Container_legends_2_sig.clear()  
    Container_legends_1_bkg.clear()   
    Container_legends_2_bkg.clear()   
    '''
    Container_Mergedhists_1_sig_tag.clear() 
    Container_Mergedhists_2_sig_tag.clear()
    Container_Mergedhists_1_bkg_tag.clear()
    Container_Mergedhists_2_bkg_tag.clear()
 

    #Container_MergeHistList = {} 

    _phy = Physics_info

    for _ptv in ptv_info :

        print ">  %s "%_ptv
        Container_hists_1_sig[_ptv]   = {}
        Container_hists_1_bkg[_ptv]   = {}
        Container_hists_1_all[_ptv]   = {}
        Container_hists_2_sig[_ptv]   = {}
        Container_hists_2_bkg[_ptv]   = {}
        Container_hists_2_all[_ptv]   = {}
        '''
        Container_legends_1_sig[_ptv] = {}
        Container_legends_1_bkg[_ptv] = {}
        Container_legends_1_all[_ptv] = {}
        Container_legends_2_sig[_ptv] = {}
        Container_legends_2_bkg[_ptv] = {}
        Container_legends_2_all[_ptv] = {}
        '''
        Container_Mergedhists_1_bkg[_ptv] = {} 
        Container_Mergedhists_2_bkg[_ptv] = {} 
        Container_Mergedhists_1_sig[_ptv] = {} 
        Container_Mergedhists_2_sig[_ptv] = {} 

        for _tag in tag_info :
            print ">>>  %s "%_tag 
            Container_hists_1_sig[_ptv][_tag]   = {}
            Container_hists_1_bkg[_ptv][_tag]   = {}
            Container_hists_1_all[_ptv][_tag]   = {}
            Container_hists_2_sig[_ptv][_tag]   = {}
            Container_hists_2_bkg[_ptv][_tag]   = {}
            Container_hists_2_all[_ptv][_tag]   = {}
            '''
            Container_legends_1_sig[_ptv][_tag] = {}
            Container_legends_1_bkg[_ptv][_tag] = {}
            Container_legends_1_all[_ptv][_tag] = {}
            Container_legends_2_sig[_ptv][_tag] = {}
            Container_legends_2_bkg[_ptv][_tag] = {}
            Container_legends_2_all[_ptv][_tag] = {}
            '''
            Container_Mergedhists_1_bkg[_ptv][_tag] = {} 
            Container_Mergedhists_1_sig[_ptv][_tag] = {} 
            Container_Mergedhists_2_bkg[_ptv][_tag] = {} 
            Container_Mergedhists_2_sig[_ptv][_tag] = {} 
            for _jet in jet_info :
                print ">>>>>>  %s "%_jet 
                Container_hists_1_sig[_ptv][_tag][_jet]   = {}
                Container_hists_1_bkg[_ptv][_tag][_jet]   = {}
                Container_hists_1_all[_ptv][_tag][_jet]   = {}
                Container_hists_2_sig[_ptv][_tag][_jet]   = {}
                Container_hists_2_bkg[_ptv][_tag][_jet]   = {}
                Container_hists_2_all[_ptv][_tag][_jet]   = {}
                '''
                Container_legends_1_sig[_ptv][_tag][_jet] = {}
                Container_legends_1_bkg[_ptv][_tag][_jet] = {}
                Container_legends_1_all[_ptv][_tag][_jet] = {}
                Container_legends_2_sig[_ptv][_tag][_jet] = {}
                Container_legends_2_bkg[_ptv][_tag][_jet] = {}
                Container_legends_2_all[_ptv][_tag][_jet] = {}
                '''
                Container_Mergedhists_1_bkg[_ptv][_tag][_jet] = [] 
                Container_Mergedhists_1_sig[_ptv][_tag][_jet] = [] 
                Container_Mergedhists_2_bkg[_ptv][_tag][_jet] = [] 
                Container_Mergedhists_2_sig[_ptv][_tag][_jet] = [] 
                for _SampleType in SampleType_info :
                    print ">>>>>>>>>  %s "%_SampleType 
                    
                    print ">>>>> Container_hists_1_sig"
                    print Container_hists_1_sig[_ptv][_tag][_jet]
                    Container_hists_1_sig[_ptv][_tag][_jet][_SampleType]   = []
                    print Container_hists_1_sig[_ptv][_tag][_jet]

                    print ">>>>> Container_hists_1_all"
                    print Container_hists_1_all[_ptv][_tag][_jet]
                    Container_hists_1_all[_ptv][_tag][_jet][_SampleType]   = []
                    print Container_hists_1_all[_ptv][_tag][_jet]
                    
                                        
                    print ">>>>> Container_hists_1_bkg"
                    print Container_hists_1_bkg[_ptv][_tag][_jet]
                    Container_hists_1_bkg[_ptv][_tag][_jet][_SampleType]   = []
                    print Container_hists_1_bkg[_ptv][_tag][_jet]

                    print ">>>>> Container_hists_2_all"
                    print Container_hists_2_all[_ptv][_tag][_jet]
                    Container_hists_2_all[_ptv][_tag][_jet][_SampleType]   = []
                    print Container_hists_2_all[_ptv][_tag][_jet]

                    print ">>>>> Container_hists_2_sig"
                    print Container_hists_2_sig[_ptv][_tag][_jet]
                    Container_hists_2_sig[_ptv][_tag][_jet][_SampleType]   = []
                    print Container_hists_2_sig[_ptv][_tag][_jet]
                    
                    print ">>>>> Container_hists_2_bkg"
                    print Container_hists_2_bkg[_ptv][_tag][_jet]
                    Container_hists_2_bkg[_ptv][_tag][_jet][_SampleType]   = []
                    print Container_hists_2_bkg[_ptv][_tag][_jet]
                    
                    print ">>>>>>>>>  %s 1 "%_SampleType 
                    print Container_hists_1_all[_ptv][_tag][_jet][_SampleType] 
                    print Container_hists_1_sig[_ptv][_tag][_jet][_SampleType] 
                    print Container_hists_1_bkg[_ptv][_tag][_jet][_SampleType] 
                    print Container_hists_2_sig[_ptv][_tag][_jet][_SampleType] 
                    print Container_hists_2_bkg[_ptv][_tag][_jet][_SampleType] 
                    print Container_hists_2_all[_ptv][_tag][_jet][_SampleType] 

                    print ">>>>>>>>>  %s mid "%_SampleType 
                    '''
                    Container_legends_1_sig[_ptv][_tag][_jet][_SampleType] = []
                    Container_legends_1_bkg[_ptv][_tag][_jet][_SampleType] = []
                    Container_legends_1_all[_ptv][_tag][_jet][_SampleType] = []
                    Container_legends_2_sig[_ptv][_tag][_jet][_SampleType] = []
                    Container_legends_2_bkg[_ptv][_tag][_jet][_SampleType] = []
                    Container_legends_2_all[_ptv][_tag][_jet][_SampleType] = []
                    '''
                    print ">>>>>>>>>  %s ENd "%_SampleType 

    print "!!!!!!!!!!!!!!!!!! FillHist_Phy_Compare !!!!!!!!!!!!!! <<< 2 >>>" 
    #  ############################################ 
    #  Get Hists  
    #  ############################################ 
    print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" 
    for _SampleType in SampleType_info :
        RootFile_1 = InputRootFiles_1[_SampleType]         
        RootFile_2 = InputRootFiles_2[_SampleType]         

        for _process in Sample_process_info[_SampleType]:
            ###  2D Hists ###
            if SwitchBBToCC :
                _phytmp = _phy.replace("B","C")
            else :
                _phytmp = _phy
            
            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                
                for _tag in tag_info :

                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 

                        if "H" in  _SampleType : 
                            _legendName= _SampleType
                        elif "data" in _SampleType:
                            _legendName= "Data"  
                        else :
                            _legendName= _SampleType+" : "+_process+" "+_tag+_jet 
                        
                        #skip blank hist 
                        print "------------------" 
                        tmpHist_1= RootFile_1.Get(_histName)
                        tmpHist_2= RootFile_2.Get(_histName)
                        if SkipHist(tmpHist_1,_histName+"_1"): 
                            continue
                        if SkipHist(tmpHist_2,_histName+"_2"): 
                            continue
                        print "------------------" 
                        
                        copyHist_1=tmpHist_1.Clone("%s_copy_1"%_histName)
                        copyHist_2=tmpHist_2.Clone("%s_copy_2"%_histName)
                        copyHist_1.Sumw2()
                        copyHist_2.Sumw2()
                        if ("pT" in _phy) or ("mBB" in _phy ):
                            copyHist_1.Rebin(10)
                            copyHist_2.Rebin(10)
                        else:
                            copyHist_1.Rebin(1)
                            copyHist_2.Rebin(1)

                        Container_hists_1_all[_ptv][_tag][_jet][_SampleType] += [copyHist_1]
                        Container_hists_2_all[_ptv][_tag][_jet][_SampleType] += [copyHist_2]
                        '''
                        Container_legends_1_all[_ptv][_tag][_jet][_SampleType] += [_legendName+" "+HistLabel_1 ]
                        Container_legends_2_all[_ptv][_tag][_jet][_SampleType] += [_legendName+" "+HistLabel_2 ]
                        '''
                        #Container_MergeHistList[_ptv][_SampleType] += [copyHist]

                        if "data" in _SampleType : 
                            continue
                        elif "H" in _SampleType : 
                            Container_hists_1_sig[_ptv][_tag][_jet][_SampleType]    +=  [copyHist_1]
                            Container_hists_2_sig[_ptv][_tag][_jet][_SampleType]    +=  [copyHist_2]
                            '''
                            Container_legends_1_sig[_ptv][_tag][_jet][_SampleType]  +=  [_legendName+" "+HistLabel_1]
                            Container_legends_2_sig[_ptv][_tag][_jet][_SampleType]  +=  [_legendName+" "+HistLabel_2]
                            '''

                            if "cc" in _SampleType : 
                                Container_Mergedhists_1_sig[_ptv][_tag][_jet] +=[copyHist_1] 
                                Container_Mergedhists_2_sig[_ptv][_tag][_jet] +=[copyHist_2] 
                        else :
                            Container_hists_1_bkg[_ptv][_tag][_jet][_SampleType]    +=  [copyHist_1]
                            Container_hists_2_bkg[_ptv][_tag][_jet][_SampleType]    +=  [copyHist_2]
                            '''
                            Container_legends_1_bkg[_ptv][_tag][_jet][_SampleType]  +=  [_legendName+" "+HistLabel_1]
                            Container_legends_2_bkg[_ptv][_tag][_jet][_SampleType]  +=  [_legendName+" "+HistLabel_2]
                            '''
                            Container_Mergedhists_1_bkg[_ptv][_tag][_jet] +=[copyHist_1] 
                            Container_Mergedhists_2_bkg[_ptv][_tag][_jet] +=[copyHist_2] 
    
    #######################  
    ##   Draw Plots   
    #               
    for _tag in tag_info : 
        Container_Mergedhists_1_sig_tag[_tag]=[]
        Container_Mergedhists_2_sig_tag[_tag]=[]
        Container_Mergedhists_1_bkg_tag[_tag]=[]
        Container_Mergedhists_2_bkg_tag[_tag]=[]

    for _ptv in ptv_info : 
        for _tag in tag_info : 
            for _jet in jet_info : 
                for _SampleType in SampleType_info :

                    if "data" in _SampleType : 
                        continue
                    if len(Container_hists_1_all[_ptv][_tag][_jet][_SampleType]) == 0 :
                        continue

                    h_merged_1 = MergeHists(Container_hists_1_all[_ptv][_tag][_jet][_SampleType], _SampleType+_ptv+_tag+_jet ).Clone("h_%s_%s_%s_%s"%(_SampleType,_ptv,_tag,_jet))
                    h_merged_2 = MergeHists(Container_hists_2_all[_ptv][_tag][_jet][_SampleType], _SampleType+_ptv+_tag+_jet ).Clone("h_%s_%s_%s_%s"%(_SampleType,_ptv,_tag,_jet))
                    ## 1D Hist 
                    HistPlot = HistMaker1D([h_merged_1,h_merged_2],[HistLabel_1, HistLabel_2])
                 
                    ## Set title and fileName
                    title, outputFileName = GetOutputInfo(_phy,_SampleType, _ptv, _tag, _jet, SR_info, SwitchBBToCC)
                   
                    ## Draw Plots 
                    if ('pT' in _phy)  or ('Pt' in _phy) or ('pt' in _phy):
                        print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
                        HistPlot.DrawRatioPlots_ATLAS(title,_phytmp+"[GeV]","Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName,[50,1500], "All", SetLogY_1D)
                    elif ('mBB' in _phy) or ('mJ' in _phy):
                        HistPlot.DrawRatioPlots_ATLAS(title,_phytmp+"[GeV]","Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName,[30,800], "All", SetLogY_1D)
                    else: 
                        HistPlot.DrawRatioPlots_ATLAS(title,_phytmp,"Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName,[], "All", SetLogY_1D)

                # Sig/Bkg Hist Merged
                print " >>>>>>>>>>> TEST : Merged Hist SIG/BKG "
                h_merged_1_bkg = MergeHists(Container_Mergedhists_1_bkg[_ptv][_tag][_jet],"BKG "+_ptv+_tag+_jet ).Clone("h_bkg_%s_%s_%s"%(_ptv,_tag,_jet))
                h_merged_1_sig = MergeHists(Container_Mergedhists_1_sig[_ptv][_tag][_jet],"SIG "+_ptv+_tag+_jet ).Clone("h_sig_%s_%s_%s"%(_ptv,_tag,_jet))
                h_merged_2_bkg = MergeHists(Container_Mergedhists_2_bkg[_ptv][_tag][_jet],"BKG "+_ptv+_tag+_jet ).Clone("h_bkg_%s_%s_%s"%(_ptv,_tag,_jet))
                h_merged_2_sig = MergeHists(Container_Mergedhists_2_sig[_ptv][_tag][_jet],"SIG "+_ptv+_tag+_jet ).Clone("h_sig_%s_%s_%s"%(_ptv,_tag,_jet))
                Container_Mergedhists_1_sig_tag[_tag] += [h_merged_1_sig.Clone("h_sig_%s_%s_%s_tag"%(_ptv,_tag,_jet))]
                Container_Mergedhists_2_sig_tag[_tag] += [h_merged_2_sig.Clone("h_sig_%s_%s_%s_tag"%(_ptv,_tag,_jet))]
                Container_Mergedhists_1_bkg_tag[_tag] += [h_merged_1_bkg.Clone("h_bkg_%s_%s_%s_tag"%(_ptv,_tag,_jet))]
                Container_Mergedhists_2_bkg_tag[_tag] += [h_merged_2_bkg.Clone("h_bkg_%s_%s_%s_tag"%(_ptv,_tag,_jet))]

                ## 1D Hist 
                HistPlot_bkg = HistMaker1D([h_merged_1_bkg,h_merged_2_bkg],[HistLabel_1, HistLabel_2])
                HistPlot_sig = HistMaker1D([h_merged_1_sig,h_merged_2_sig],[HistLabel_1, HistLabel_2])
 
                ## Set title and fileName
                title_bkg, outputFileName_bkg = GetOutputInfo(_phy,"Background", _ptv, _tag, _jet, SR_info, SwitchBBToCC)
                title_sig, outputFileName_sig = GetOutputInfo(_phy,"Signal"    , _ptv, _tag, _jet, SR_info, SwitchBBToCC)
                
                ## Draw Plots 
                if ('pT' in _phy)  or ('Pt' in _phy) or ('pt' in _phy):
                    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
                    HistPlot_sig.DrawRatioPlots_ATLAS(title_bkg,_phytmp+"[GeV]","Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_bkg,[50,1500], "All", SetLogY_1D)
                    HistPlot_bkg.DrawRatioPlots_ATLAS(title_sig,_phytmp+"[GeV]","Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_sig,[50,1500], "All", SetLogY_1D)
                elif ('mBB' in _phy) or ('mJ' in _phy):                                                                                                  
                    HistPlot_bkg.DrawRatioPlots_ATLAS(title_bkg,_phytmp+"[GeV]","Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_bkg,[30,800], "All", SetLogY_1D)
                    HistPlot_sig.DrawRatioPlots_ATLAS(title_sig,_phytmp+"[GeV]","Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_sig,[30,800], "All", SetLogY_1D)
                else:                                                                                                                                    
                    HistPlot_bkg.DrawRatioPlots_ATLAS(title_bkg,_phytmp        ,"Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_bkg,[], "All", SetLogY_1D)
                    HistPlot_sig.DrawRatioPlots_ATLAS(title_sig,_phytmp        ,"Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_sig,[], "All", SetLogY_1D)    

                del HistPlot_bkg
                del HistPlot_sig
                del h_merged_1_bkg
                del h_merged_1_sig
                del h_merged_2_bkg
                del h_merged_2_sig
                print "gc "
                print gc.collect()
                print "gc -"

    for _tag in tag_info : 
        # Sig/Bkg Hist Merged
        print " >>>>>>>>>>> TEST : Merged Hist SIG/BKG _tag Merge"
        h_merged_1_bkg_tag = MergeHists(Container_Mergedhists_1_bkg_tag[_tag],"BKG "+_ptv+_tag+_jet ).Clone("h_bkg_%s"%(_tag))
        h_merged_1_sig_tag = MergeHists(Container_Mergedhists_1_sig_tag[_tag],"SIG "+_ptv+_tag+_jet ).Clone("h_sig_%s"%(_tag))
        h_merged_2_bkg_tag = MergeHists(Container_Mergedhists_2_bkg_tag[_tag],"BKG "+_ptv+_tag+_jet ).Clone("h_bkg_%s"%(_tag))
        h_merged_2_sig_tag = MergeHists(Container_Mergedhists_2_sig_tag[_tag],"SIG "+_ptv+_tag+_jet ).Clone("h_sig_%s"%(_tag))

        ## 1D Hist 
        print " >>>>>>>>>>> TEST : Merged Hist SIG/BKG _tag Hist"
        HistPlot_bkg_tag = HistMaker1D([h_merged_1_bkg_tag,h_merged_2_bkg_tag],[HistLabel_1, HistLabel_2])
        HistPlot_sig_tag = HistMaker1D([h_merged_1_sig_tag,h_merged_2_sig_tag],[HistLabel_1, HistLabel_2])
                                                                    
        ## Set title and fileName                                    
        print " >>>>>>>>>>> TEST : Merged Hist SIG/BKG _tag get output"
        title_bkg_tag, outputFileName_bkg_tag = GetOutputInfo(_phy,"Background", "AllpTV", _tag, "", SR_info, SwitchBBToCC)
        title_sig_tag, outputFileName_sig_tag = GetOutputInfo(_phy,"Signal"    , "AllpTV", _tag, "", SR_info, SwitchBBToCC)
        
        ## Draw Plots 
        print " >>>>>>>>>>> TEST : Merged Hist SIG/BKG _tag Draw "
        if ('pT' in _phy)  or ('Pt' in _phy) :
            print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
            HistPlot_sig_tag.DrawRatioPlots_ATLAS(title_bkg_tag,_phytmp+"[GeV]","Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_bkg_tag,[50,1500], "All", SetLogY_1D)
            HistPlot_bkg_tag.DrawRatioPlots_ATLAS(title_sig_tag,_phytmp+"[GeV]","Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_sig_tag,[50,1500], "All", SetLogY_1D)
        elif ('mBB' in _phy) or ('mJ' in _phy):                                                                                                  
            HistPlot_bkg_tag.DrawRatioPlots_ATLAS(title_bkg_tag,_phytmp+"[GeV]","Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_bkg_tag,[30,800], "All", SetLogY_1D)
            HistPlot_sig_tag.DrawRatioPlots_ATLAS(title_sig_tag,_phytmp+"[GeV]","Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_sig_tag,[30,800], "All", SetLogY_1D)
        else:                                                                                                                                    
            print " >>>>>>>>>>> TEST : Merged Hist SIG/BKG _tag Draw else bkg"
            print "title %s"%title_bkg_tag
            print "xtitle %s"%_phytmp
            print [HistLabel_1, HistLabel_2]
            print TarDir+outputFileName_bkg_tag
            print "LogY %s"%SetLogY_1D
            print HistPlot_bkg_tag
            HistPlot_bkg_tag.DrawRatioPlots_ATLAS(title_bkg_tag,_phytmp,"Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_bkg_tag,[], "All", SetLogY_1D)
            print " >>>>>>>>>>> TEST : Merged Hist SIG/BKG _tag Draw else sig"
            HistPlot_sig_tag.DrawRatioPlots_ATLAS(title_sig_tag,_phytmp,"Number of events", [HistLabel_1, HistLabel_2], TarDir+outputFileName_sig_tag,[], "All", SetLogY_1D)    
 
        del HistPlot_bkg_tag
        del HistPlot_sig_tag
        del h_merged_1_bkg_tag 
        del h_merged_1_sig_tag 
        del h_merged_2_bkg_tag 
        del h_merged_2_sig_tag 
        print "gc "
        gc.collect()
        print "gc -"

    print " >>>>>>>>>>> TEST : Clear "
    Container_hists_1_all.clear() 
    Container_hists_2_all.clear()
    Container_hists_1_sig.clear()
    Container_hists_2_sig.clear()
    Container_hists_1_bkg.clear()
    Container_hists_2_bkg.clear()
    Container_Mergedhists_1_bkg.clear() 
    Container_Mergedhists_2_bkg.clear()
    Container_Mergedhists_1_sig.clear()
    Container_Mergedhists_2_sig.clear()

    
    ## Legends container 
    '''
    Container_legends_1_all.clear()  
    Container_legends_2_all.clear()  
    Container_legends_1_sig.clear()  
    Container_legends_2_sig.clear()  
    Container_legends_1_bkg.clear()   
    Container_legends_2_bkg.clear()   
    '''
    Container_Mergedhists_1_sig_tag.clear() 
    Container_Mergedhists_2_sig_tag.clear()
    Container_Mergedhists_1_bkg_tag.clear()
    Container_Mergedhists_2_bkg_tag.clear()
 

    del Container_hists_1_all 
    del Container_hists_2_all 
    del Container_hists_1_sig 
    del Container_hists_2_sig 
    del Container_hists_1_bkg 
    del Container_hists_2_bkg 
    del Container_Mergedhists_1_bkg
    del Container_Mergedhists_2_bkg
    del Container_Mergedhists_1_sig
    del Container_Mergedhists_2_sig

    
    ## Legends container 
    '''
    del Container_legends_1_all 
    del Container_legends_2_all 
    del Container_legends_1_sig 
    del Container_legends_2_sig 
    del Container_legends_1_bkg  
    del Container_legends_2_bkg  
    '''
    del Container_Mergedhists_1_sig_tag
    del Container_Mergedhists_2_sig_tag
    del Container_Mergedhists_1_bkg_tag
    del Container_Mergedhists_2_bkg_tag
    print "gc "
    gc.collect()
    print "gc -"
##########################################################
#######     Save Mass window Hist 
##########################################################



def MassWindowHist(hist,histname,xlow,xmax):
    hist_window=hist.Clone(histname)
    xstart=hist_window.FindBin(xlow)
    xend  =hist_window.FindBin(xmax)
    nbins=hist_window.GetNbinsX()
    for ibin in range(1,nbins+1):
        if ibin < xstart or ibin > xend:
            hist_window.SetBinContent(ibin,0)
            hist_window.SetBinError(ibin,0)
    return hist_window


##########################################################
#######     Fill Hist Yield
##########################################################

def FillHist_Yield_FatJet(SampleType_info, Sample_process_info, Physics_info, InputRootFiles, CTagEff={}):
    '''
    Fill 2D Hists phy 
    '''                                                   
    applyTag = False 
    if len(CTagEff) > 0: 
        applyTag = True
    
    if ZeroTagOnly :                                                   
        tag_info = [ZeroTagInfo]          
    else :
        #tag_info = ["0tag", "1tag", "2tag", "3ptag"]          
        tag_info = ["0tag", "1tag", "2tag"]          
                                                          
    if ptv_cate_merge:
        jet_info = [ "1pfat0pjet"]                            
        ptv_info = ["0_250ptv","250_400ptv","400ptv"]
    else:
        jet_info = [ "2jet","3pjet"]                            
        ptv_info = ["75_150ptv","150_250ptv","250ptv"]



    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]

    #SR_info="SR_2psubjet"  
    SR_info="SR"  


    _phy = Physics_info

    Hist2D_phy_vs_ptv_name_all  = {} 
    Hist2D_phy_vs_ptv_name_simple  = {} 
    Hist2D_phy_vs_ptv_all       = {}
    Hist2D_phy_vs_ptv_simple       = {}
    Hist2D_phy_vs_ptv_title = {}
    Region_mBB = ["All","Sig"]

    if applyTag: 
        TagInfo="CTag"+CTagEff["WP"]
    else :
        #TagInfo="NoTag"
        TagInfo=" "

    for _ptv in Region_mBB:
        
        if SwitchBBToCC :
            ##_phytmp = _phy.replace("BB","CC")
            _phytmp = _phy.replace("B","C")
        else :
            _phytmp = _phy

               
        if _ptv =="Sig":
            Hist2D_phy_vs_ptv_title[_ptv] = "Yields distribution : signal region  "+TagInfo
        elif _ptv == "All":
            Hist2D_phy_vs_ptv_title[_ptv] = "Yields distribution : all region  "+TagInfo

        #### Yield 2D plot all detail (initial)####

        if UsingTop : 
            KK = 28
        else :
            KK = 0
        Hist2D_phy_vs_ptv_name_all[_ptv] = "phy_vs_ptv_Yield_"+_phytmp+"_2D_"+_ptv+"Window_"+TagInfo+"_all"
        Hist2D_phy_vs_ptv_all[_ptv] = ROOT.TH2D(Hist2D_phy_vs_ptv_name_all[_ptv], Hist2D_phy_vs_ptv_title[_ptv],4,0,4,24+KK,0,24+KK)
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(1,"low pTV" )
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(2,"medium pTV" )
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(3,"high pTV" )
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(4,"all pTV")

        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+24,"ggZH : ZllHcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+23,"qqZH : ZllHcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+22,"ggZH : ZllHbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+21,"qqZH : ZllHbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+20,"ggZZ : ZllZcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+19,"ggZZ : ZllZbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+18,"ggZZ : ZllZLL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+17,"ggZZ : ZllZcL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+16,"ggZZ : ZllZbL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+15,"ggZZ : ZllZbc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+14,"ZZ : ZllZcc")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+13,"ZZ : ZllZbb")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+12,"ZZ : ZllZLL")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+11,"ZZ : ZllZcL")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+10,"ZZ : ZllZbL")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+9,"ZZ : ZllZbc")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+8,"ZZ : ZZkg")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+7,"Z+Jet : Zcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+6,"Z+Jet : Zbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+5,"Z+Jet : ZLL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+4,"Z+Jet : ZcL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+3,"Z+Jet : ZbL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+2,"Z+Jet : Zbc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+1,"Z+Jet : Zbkg" )

        if UsingTop : 
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(28,"ttbar: cc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(27,"ttbar: bb" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(26,"ttbar: LL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(25,"ttbar: cL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(24,"ttbar: bL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(23,"ttbar: bc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(22,"ttbar: bkg" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(21,"stopWt: cc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(20,"stopWt: bb" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(19,"stopWt: LL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(18,"stopWt: cL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(17,"stopWt: bL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(16,"stopWt: bc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(15,"stopWt: bkg" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(14,"stopt: cc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(13,"stopt: bb" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(12,"stopt: LL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(11,"stopt: cL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(10,"stopt: bL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(9 ,"stopt: bc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(8 ,"stopt: bkg" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(7 ,"stops: cc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(6 ,"stops: bb" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(5 ,"stops: LL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(4 ,"stops: cL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(3 ,"stops: bL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(2 ,"stops: bc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(1 ,"stops: bkg" )
 




        Hist2D_phy_vs_ptv_all[_ptv].GetZaxis().SetTitle("Number of evnets")
                                                           
        #### Yield 2D plot - Simplify  (initial) ####
        if UsingTop : 
            KK = 4
        else :
            KK = 0
        Hist2D_phy_vs_ptv_name_simple[_ptv] = "phy_vs_ptv_Yield_"+_phytmp+"_2D_"+_ptv+"Window_"+TagInfo+"_simple"
        Hist2D_phy_vs_ptv_simple[_ptv] = ROOT.TH2D(Hist2D_phy_vs_ptv_name_simple[_ptv], Hist2D_phy_vs_ptv_title[_ptv],4,0,4,8+KK,0,8+KK)
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(1,"low pTV" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(2,"medium pTV" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(3,"high pTV" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(4,"all pTV")

        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+8,"ggZH: ZllHcc" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+7,"qqZH: ZllHcc" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+6,"ggZH: ZllHbb" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+5,"qqZH: ZllHbb" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+4,"ggZZ: ZllZqq" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+3,"ZZ: ZllZqq")
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+2,"WZ: ZllWqq " )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+1,"Z+Jet " )

        if UsingTop : 
            Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(4,"ttbar" )
            Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(3,"stopWt" )
            Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(2,"stopt" )
            Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(1,"stops" )

        Hist2D_phy_vs_ptv_simple[_ptv].GetZaxis().SetTitle("Number of evnets")

    #### =========================================
    #### Loop & Fill Yield 2D plot ####
    ####

    for _SampleType in SampleType_info :
        RootFile = InputRootFiles[_SampleType]         

        for _process in Sample_process_info[_SampleType]:
            ###  2D Hists ###
            if SwitchBBToCC :
                #_phytmp = _phy.replace("BB","CC")
                _phytmp = _phy.replace("B","C")
            else :
                _phytmp = _phy
            
            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                
                for _tag in tag_info :

                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        ####_legendName= _process + " " + _tag+_jet + " " + _ptv 
                        ####_legendName=_tag + " " + ptvInfo_convert(_ptv)
                        _legendName= _SampleType+" : "+_process  

                        if "data" in _legendName :
                            continue

                        #skip blank hist 
                        print "------------------" 
                        tmpHist = RootFile.Get(_histName)
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 
                        
                        copyHist=tmpHist.Clone("%s_copy"%_histName)
                        
                        xstart=copyHist.FindBin(75)
                        xend=copyHist.FindBin(145)
                        print ")))))))))))))))))))) test S: %d E:%d " %(xstart, xend)
                        xstart=copyHist.FindBin(75.1)
                        xend=copyHist.FindBin(144.9)

                        print ")))))))))))))))))))) S: %d E:%d " %(xstart, xend)

                        if applyTag: 
                            _CTagEff = CalCTagEff(_process+"_", CTagEff)
                        else :
                            _CTagEff = 1
                        
                        FillValue_all = Hist2D_filllValue_phy_vs_ptv_all(_histName, copyHist, _CTagEff, [xstart,xend])
                        #FillValue_simple = Hist2D_filllValue_phy_vs_ptv_simple(_histName, copyHist, [xstart, xend])
                        FillValue_simple = Hist2D_filllValue_phy_vs_ptv_simple(_histName, copyHist, _CTagEff, [xstart, xend])

                        Hist2D_phy_vs_ptv_all["All"].Fill(FillValue_all[0], FillValue_all[1],FillValue_all[2])
                        Hist2D_phy_vs_ptv_all["All"].Fill(3.5, FillValue_all[1],FillValue_all[2])
                        Hist2D_phy_vs_ptv_all["Sig"].Fill(FillValue_all[0], FillValue_all[1],FillValue_all[3])
                        Hist2D_phy_vs_ptv_all["Sig"].Fill(3.5, FillValue_all[1],FillValue_all[3])
                        Hist2D_phy_vs_ptv_simple["All"].Fill(FillValue_simple[0], FillValue_simple[1],FillValue_simple[2])
                        Hist2D_phy_vs_ptv_simple["All"].Fill(3.5, FillValue_simple[1],FillValue_simple[2])
                        Hist2D_phy_vs_ptv_simple["Sig"].Fill(FillValue_simple[0], FillValue_simple[1],FillValue_simple[3])
                        Hist2D_phy_vs_ptv_simple["Sig"].Fill(3.5, FillValue_simple[1],FillValue_simple[3])

    #### =========================================
    ####            S/B 1D plot               ####
    ####

    HistPlot2D_phy_vs_ptv_all={}
    HistPlot2D_phy_vs_ptv_simple={}
    Hist1D_SB={}
    Hist1D_SB_sqrt={}
    Hist1D_SB_title={}
    Hist1D_SB_name={}
    
   
    Hist1D_S={}
    Hist_cut_legend=[]
    Hist_cut={}
    for _region in Region_mBB : 
        
        HistPlot2D_phy_vs_ptv_simple[_region] = HistMaker2D(Hist2D_phy_vs_ptv_simple[_region], Hist2D_phy_vs_ptv_title[_region])
        HistPlot2D_phy_vs_ptv_simple[_region].DrawPlots_ATLAS(Hist2D_phy_vs_ptv_title[_region], "","", TarDir+Hist2D_phy_vs_ptv_name_simple[_region],"COLZTEXT", SetLogZ_2D)
        
        HistPlot2D_phy_vs_ptv_all[_region] = HistMaker2D(Hist2D_phy_vs_ptv_all[_region], Hist2D_phy_vs_ptv_title[_region])
        HistPlot2D_phy_vs_ptv_all[_region].DrawPlots_ATLAS(Hist2D_phy_vs_ptv_title[_region], "","", TarDir+Hist2D_phy_vs_ptv_name_all[_region],"COLZTEXT", SetLogZ_2D)

        
        if _region == "All":
            Hist1D_SB_title[_region] = "all region "+" "+TagInfo+" "
        elif _region == "Sig":
            Hist1D_SB_title[_region] = "signal region "+" "+TagInfo+" " 

        Hist1D_SB_name[_region] = "SB_phy_vs_ptv_Yield_"+_phytmp+"_2D_"+_region+"Window_"+TagInfo+"_all"
        Hist1D_SB[_region]= ROOT.TH1D(Hist1D_SB_name[_region], Hist1D_SB_title[_region], 4,0,4 )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(1,"low pTV" )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(2,"medium pTV" )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(3,"high pTV" )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(4,"all pTV")
        
        Hist1D_SB_sqrt[_region]= ROOT.TH1D(Hist1D_SB_name[_region]+"sqrt", Hist1D_SB_title[_region]+"sqrt", 3,0,3 )
        Hist1D_SB_sqrt[_region].GetXaxis().SetBinLabel(1,"low pTV" )
        Hist1D_SB_sqrt[_region].GetXaxis().SetBinLabel(2,"medium pTV" )
        Hist1D_SB_sqrt[_region].GetXaxis().SetBinLabel(3,"high pTV" )


        sig_int={}
        bkg_int={}
        bkg_int[1]=Hist2D_phy_vs_ptv_simple[_region].Integral(1,1,1,10)
        bkg_int[2]=Hist2D_phy_vs_ptv_simple[_region].Integral(2,2,1,10)
        bkg_int[3]=Hist2D_phy_vs_ptv_simple[_region].Integral(3,3,1,10)
        bkg_int[4]=Hist2D_phy_vs_ptv_simple[_region].Integral(4,4,1,10)

        sig_int[1]=Hist2D_phy_vs_ptv_simple[_region].Integral(1,1,11,12)
        sig_int[2]=Hist2D_phy_vs_ptv_simple[_region].Integral(2,2,11,12)
        sig_int[3]=Hist2D_phy_vs_ptv_simple[_region].Integral(3,3,11,12)
        sig_int[4]=Hist2D_phy_vs_ptv_simple[_region].Integral(4,4,11,12)
        print "-=-=-===-=-=-=check Integral -=-=-=-=-=--=-=-=-" 
        Hist_cut[_region]={}

        if len(Hist_cut_legend) == 0:
            for _y in range(1,12):
                Hist_cut_legend = Hist2D_phy_vs_ptv_simple[_region].GetYaxis().GetBinLabel(_y)

        for _x in range(1,5):
            inte=0
            inte_sig=0
            inte_bkg=0
            _ptv = _x
            Hist_cut[_region][_ptv]=[]
            print "-=-=-===-=-=-=check Integral -=-=-=-=-=--=-=-=-" 
            for _y in range(1,13):
                print _x
                print _y
                if len(CTagEff)>0:
                    print CTagEff["WP"]
                    nametmp="%d_%d_%s"%(_x,_y,CTagEff["WP"])
                else:
                    nametmp="%d_%d_NoTag"%(_x,_y)
                Hist_name = Hist2D_phy_vs_ptv_simple[_region].GetYaxis().GetBinLabel(_y)
                Hist_cut_tmp= ROOT.TH1D(Hist_name+nametmp, Hist_name, 5,0,5)
                Hist_cut_tmp.GetXaxis().SetBinLabel(1,"Notag" )
                Hist_cut_tmp.GetXaxis().SetBinLabel(2,"C-tag Loose" )
                Hist_cut_tmp.GetXaxis().SetBinLabel(3,"C-tag Tight" )

                Integral_tmp = Hist2D_phy_vs_ptv_simple[_region].GetBinContent(_ptv,_y)

                if len(CTagEff)==0:
                    Hist_cut_tmp.SetBinContent(1,Integral_tmp)
                elif CTagEff["WP"]=="Loose":
                    Hist_cut_tmp.SetBinContent(2,Integral_tmp)
                elif CTagEff["WP"]=="Tight":
                    Hist_cut_tmp.SetBinContent(3,Integral_tmp)
            
                Hist_cut[_region][_ptv]+=[Hist_cut_tmp.Clone()]

                # check integral
                print "x %d y %d Content %f"%(_x, _y, Hist2D_phy_vs_ptv_simple[_region].GetBinContent(_x,_y))
                _content=Hist2D_phy_vs_ptv_simple[_region].GetBinContent(_x,_y)
                inte += _content
                if _y <11 : 
                    inte_bkg+=_content
                else :
                    inte_sig+=_content

            print "x %d inte %f integral %f "%(_x, inte, Hist2D_phy_vs_ptv_simple[_region].Integral(_x,_x,1,12))    
            print "x %d Sig : inte %f integral %f "%(_x, inte_sig, math.sqrt(inte_sig))
            print "x %d Bkg : inte %f integral %f "%(_x, inte_bkg, math.sqrt(inte_bkg))
            print "x %d s/b  inte %f sqrt  %f "%( _x,inte_sig/inte_bkg , inte_sig/math.sqrt(inte_bkg))

            
        '''
        Hist1D_SB[_region].SetBinContent(1,sig_int[1]/math.sqrt(bkg_int[1]))
        Hist1D_SB[_region].SetBinContent(2,sig_int[2]/math.sqrt(bkg_int[2]))
        Hist1D_SB[_region].SetBinContent(3,sig_int[3]/math.sqrt(bkg_int[3]))
        Hist1D_SB[_region].SetBinContent(4,sig_int[4]/math.sqrt(bkg_int[4]))
        '''
        Hist1D_SB[_region].SetBinContent(1,sig_int[1]/(bkg_int[1]))
        Hist1D_SB[_region].SetBinContent(2,sig_int[2]/(bkg_int[2]))
        Hist1D_SB[_region].SetBinContent(3,sig_int[3]/(bkg_int[3]))
        Hist1D_SB[_region].SetBinContent(4,sig_int[4]/(bkg_int[4]))

        Hist1D_SB_sqrt[_region].SetBinContent(1,sig_int[1]/math.sqrt(bkg_int[1]))
        Hist1D_SB_sqrt[_region].SetBinContent(2,sig_int[2]/math.sqrt(bkg_int[2]))
        Hist1D_SB_sqrt[_region].SetBinContent(3,sig_int[3]/math.sqrt(bkg_int[3]))



    return [Hist1D_SB , Hist1D_SB_title, Hist_cut, Hist_cut_legend, Hist1D_SB_sqrt] 
    #HistPlot1D = HistMaker1D([Hist1D_SB["All"], Hist1D_SB["Sig"]],[Hist1D_SB_title["All"], Hist1D_SB_title["Sig"]])
    #HistPlot1D.DrawPlots_ATLAS("S/B distribution"+TagInfo, "","S/B", [Hist1D_SB_title["All"], Hist1D_SB_title["Sig"]], "Merged/SB"+TagInfo,[0,4],"All", False)





##########################################################
#######    Draw Hist : phy_vs_ntag 2D Plot     
##########################################################

def HistDraw_Plot2D_phy_vs_ntag(Physics_info, Hist2D_phy_vs_tag, Hist2D_phy_vs_tag_title, Hist2D_phy_vs_tag_name):
    '''
    Physics_info            : physics Info
    Hist2D_phy_vs_tag       : plot Hist 
    Hist2D_phy_vs_tag_title : plot title  
    Hist2D_phy_vs_tag_name  : output file name 

    '''
    HistPlot2D_phy_vs_ntag = {}
    for _phy in Physics_info :
        HistPlot2D_phy_vs_ntag[_phy] = HistMaker2D(Hist2D_phy_vs_tag[_phy], Hist2D_phy_vs_tag_title[_phy])
        HistPlot2D_phy_vs_ntag[_phy].DrawPlots_ATLAS(Hist2D_phy_vs_tag_title[_phy], "c-tag","", TarDir+Hist2D_phy_vs_tag_name[_phy],"COLZTEXT", SetLogZ_2D)
###########################################################
#######     Fill CutFlow
##########################################################

def FillHist_CutFlow(SampleType, SampleTypeName, InputRootFiles, OutputLabel=""):
    '''
    Fill CutFlow Hists 

    Example : 
        SampleType     = "Z+C" 
        SampleTypeName = "ZC" 
        process_info   =  ZC_info
        Hist2D_phy_vs_tag     : 2D Jet Yeild Hist 
    '''

    RootFile = InputRootFiles[SampleType]         
                
    h_cutflow_PreSelect= RootFile.Get("CutFlow/PreselectionCutFlow")
    HistPlot_cutflow_PreSelect = HistMaker1D([h_cutflow_PreSelect],["Preselection cut flow"])
    HistPlot_cutflow_PreSelect.setLocation_Legend_plot(0.7,0.7,0.93,0.78)
    HistPlot_cutflow_PreSelect.DrawPlots_ATLAS(SampleTypeName+" Preselection cut flow", "","Number of events", ["Preselection cut flow"], TarDir+"CutFlow_Preselcetion"+"_"+SampleType+OutputLabel,[0,13], "All", SetLogY_1D)

    h_cutflow_Merged= RootFile.Get("CutFlow/Nominal/CutsMerged")
    HistPlot_cutflow_Merged = HistMaker1D([h_cutflow_Merged],["Merged cut flow"])
    HistPlot_cutflow_Merged.setLocation_Legend_plot(0.7,0.7,0.93,0.78)
    HistPlot_cutflow_Merged.DrawPlots_ATLAS(SampleTypeName+" Merged cut flow", "","Number of events", [ "Merged cut flow"], TarDir+"CutFlow_Merged"+"_"+SampleType+OutputLabel,[0,16], "All",SetLogY_1D)

    h_cutflow_Resolved= RootFile.Get("CutFlow/Nominal/CutsResolved")
    HistPlot_cutflow_Resolved = HistMaker1D([h_cutflow_Resolved],["Resolved cut flow"])
    HistPlot_cutflow_Resolved.setLocation_Legend_plot(0.7,0.7,0.93,0.78)
    HistPlot_cutflow_Resolved.DrawPlots_ATLAS(SampleTypeName+" Resolved cut flow", "","Number of events", [ "Resolved cut flow"], TarDir+"CutFlow_Resolved"+"_"+SampleType+OutputLabel,[0,16], "All", SetLogY_1D)

def FillHist_CutFlow_Compare(SampleType, SampleTypeName, InputRootFiles_1, InputRootFiles_2, Legends):
    '''
    Fill CutFlow Hists 

    Example : 
        SampleType     = "Z+C" 
        SampleTypeName = "ZC" 
        process_info   =  ZC_info
        Hist2D_phy_vs_tag     : 2D Jet Yeild Hist 
    '''

    OutputLabel = "_Compare"
    RootFile_1  = InputRootFiles_1[SampleType]         
    RootFile_2  = InputRootFiles_2[SampleType]         
                
    h_cutflow_PreSelect_1= RootFile_1.Get("CutFlow/PreselectionCutFlow")
    h_cutflow_PreSelect_2= RootFile_2.Get("CutFlow/PreselectionCutFlow")
    HistPlot_cutflow_PreSelect = HistMaker1D([h_cutflow_PreSelect_1, h_cutflow_PreSelect_2],Legends)
    HistPlot_cutflow_PreSelect.setLocation_Legend_plot(0.7,0.7,0.93,0.78)
#    HistPlot_cutflow_PreSelect.DrawPlots_ATLAS(SampleTypeName+" Preselection cut flow", "","Number of events", Legends , TarDir+"CutFlow_Preselcetion"+"_"+SampleType+OutputLabel,[0,16], "All", SetLogY_1D)
    HistPlot_cutflow_PreSelect.DrawRatioPlots_ATLAS(SampleTypeName+" Preselection cut flow", "","Number of events", Legends , TarDir+"Ratio_CutFlow_Preselcetion"+"_"+SampleType+OutputLabel,[0,16], "All", SetLogY_1D,True)
    
    h_cutflow_Merged_1= RootFile_1.Get("CutFlow/Nominal/CutsMerged")
    h_cutflow_Merged_2= RootFile_2.Get("CutFlow/Nominal/CutsMerged")
    HistPlot_cutflow_Merged = HistMaker1D([h_cutflow_Merged_1, h_cutflow_Merged_2],Legends)
    HistPlot_cutflow_Merged.setLocation_Legend_plot(0.7,0.7,0.93,0.78)
#    HistPlot_cutflow_Merged.DrawPlots_ATLAS(SampleTypeName+" Merged cut flow", "","Number of events", Legends , TarDir+"CutFlow_Merged"+"_"+SampleType+OutputLabel,[0,16], "All",SetLogY_1D)
    HistPlot_cutflow_Merged.DrawRatioPlots_ATLAS(SampleTypeName+" Merged cut flow", "","Number of events", Legends , TarDir+"Ratio_CutFlow_Merged"+"_"+SampleType+OutputLabel,[0,20], "All",SetLogY_1D,True)

    h_cutflow_Resolved_1= RootFile_1.Get("CutFlow/Nominal/CutsResolved")
    h_cutflow_Resolved_2= RootFile_2.Get("CutFlow/Nominal/CutsResolved")
    HistPlot_cutflow_Resolved = HistMaker1D([h_cutflow_Resolved_1, h_cutflow_Resolved_2], Legends)
 #   HistPlot_cutflow_Resolved.setLocation_Legend_plot(0.7,0.7,0.93,0.78)
    HistPlot_cutflow_Resolved.DrawRatioPlots_ATLAS(SampleTypeName+" Resolved cut flow", "","Number of events", Legends, TarDir+"Ratio_CutFlow_Resolved"+"_"+SampleType+OutputLabel,[0,25], "All", SetLogY_1D, True)


#######
def GetOutputInfo(_phy,_SampleType, _ptv, _tag, _jet, SR_info, SwitchBBToCC):

    if SwitchBBToCC :
        _phytmp = _phy.replace("B","C")
    else :
         _phytmp = _phy
    outputFileName = _SampleType+"_"+_ptv+"_"+_tag+_jet+"_"+ SR_info +"_"+_phytmp

    ptvInfo = ""
    if _ptv == "AllpTV":
        ptvInfo = "ZpT All pTV"
    else:
        ptvInfo=ptvInfo_convert(_ptv)

    if "ZQ" in _SampleType:
        title =  "BVeto+CTag "+ "Z+Jets"+":"+ptvInfo+" "+_tag+" "+_jet    
    else:
        title =  "BVeto+CTag "+ _SampleType+":"+ptvInfo+" "+_tag+" "+_jet    
    #if SwitchBBToCC :
    #    title = title.replace("B","C")
    print "=========== GetOutputInfo ======"
    print "title : %s"%title
    print "outputFileName : %s"%outputFileName
    print "================================"
    return title, outputFileName
 
##########################################################################
# ########################################################################
# #
# #     main function
# #
# ########################################################################
##########################################################################

def main():
    atlas_style()

    #######################################################
    #   VHCC
    #######################################################

    #SetLogY_1D = True
    #SetLogZ_2D = True 
    #-----------------------
    # info
    #-----------------------
    rootfile_dir_1 = ROOTFILE_DIR1#"/home/chenye/work/CharmJet/VHCC/RootFile_VHbb/Merged/" # with flavor labal for VHbbcc2Lep
    rootfile_dir_2 = ROOTFILE_DIR2#"/home/chenye/work/CharmJet/VHCC/RootFile_VHbb/Merged/" # with flavor labal for VHbbcc2Lep
    
    rootfile_name = [
        "hist-ggZllHbb_PwPy8.root",
        "hist-ggZllHcc_PwPy8.root",
        "hist-qqZllHbbJ_PwPy8MINLO.root",
        "hist-qqZllHccJ_PwPy8MINLO.root",
        #"hist-ggZllZqq_Sh222.root",
        "hist-ggZqqZll_Sh222.root",
        "hist-ZccZll_Sh221.root",
        "hist-ZqqZll_Sh221.root",
        "hist-ZbbZll_Sh221.root"    
        "hist-ZC_Sh221.root",
        "hist-ZQ_Sh221.root",
        "hist-ZB_Sh221.root",
        "hist-ZL_Sh221.root",
        "hist-ZZ_Sh221.root",
        "hist-stopWt_PwPy8.root",
        "hist-ttbar_dilep_PwPy8.root",
        "hist-stops_PwPy8.root",
        "hist-stopt_PwPy8.root",
        "hist-WqqZll_Sh221.root",
        "hist-data.root"
    ]
        #"hist-stopWt_dilep_PwPy8.root",
    Physics_info = [
        "mBBTT", "mBB","pTV", "dRVH",
        "pTBB","dRBB","dEtaBB","dPhiBB",
        "pTB1","pTB2","EtaB1","EtaB2",
        "PtFatJ1","EtaFatJ1","NtrkjetsFatJ1"
    ]

    ##SR_info="topemucr_noaddbjetsr"  



    #---------------------------------------------
    # Get File 
    #

    InputFiles_1={}
    InputFiles_2={}
    
    for filename in rootfile_name :
        file_name_1 = rootfile_dir_1 + filename
        file_name_2 = rootfile_dir_2 + filename
        print file_name_1
        print file_name_2
        myFile_1 = TFile(file_name_1)
        myFile_2 = TFile(file_name_2)
        if "ggZllHcc" in filename:
            InputFiles_1["ggZllHcc"] = myFile_1
            InputFiles_2["ggZllHcc"] = myFile_2
        elif "qqZllHcc" in filename:
            InputFiles_1["qqZllHcc"] = myFile_1
            InputFiles_2["qqZllHcc"] = myFile_2
        elif "ggZllHbb" in filename:
            InputFiles_1["ggZllHbb"] = myFile_1
            InputFiles_2["ggZllHbb"] = myFile_2
        elif "qqZllHbb" in filename:
            InputFiles_1["qqZllHbb"] = myFile_1
            InputFiles_2["qqZllHbb"] = myFile_2
        elif ("ggZllZqq" in filename) or("ggZqqZll" in filename) :
            InputFiles_1["ggZllZqq"] = myFile_1
            InputFiles_2["ggZllZqq"] = myFile_2
        elif "ZC" in filename: 
            InputFiles_1["ZC"] = myFile_1
            InputFiles_2["ZC"] = myFile_2
        elif "ZB" in filename: 
            InputFiles_1["ZB"] = myFile_1
            InputFiles_2["ZB"] = myFile_2
        elif "ZL" in filename: 
            InputFiles_1["ZL"] = myFile_1
            InputFiles_2["ZL"] = myFile_2
        elif "ZQ" in filename: 
            InputFiles_1["ZQ"] = myFile_1
            InputFiles_2["ZQ"] = myFile_2
        elif "ZZ" in filename: 
            InputFiles_1["ZZ"] = myFile_1
            InputFiles_2["ZZ"] = myFile_2
        elif "ZccZll" in filename: 
            InputFiles_1["ZllZCC"] = myFile_1
            InputFiles_2["ZllZCC"] = myFile_2
        elif "ZbbZll" in filename: 
            InputFiles_1["ZllZBB"] = myFile_1
            InputFiles_2["ZllZBB"] = myFile_2
        elif "ZqqZll" in filename: 
            InputFiles_1["ZllZLL"] = myFile_1
            InputFiles_2["ZllZLL"] = myFile_2
        elif "ttbar" in filename: 
            InputFiles_1["ttbar"] = myFile_1
            InputFiles_2["ttbar"] = myFile_2
        elif "stopWt" in filename: 
            InputFiles_1["stopWt"] = myFile_1
            InputFiles_2["stopWt"] = myFile_2
        elif "stopt" in filename: 
            InputFiles_1["stopt"] = myFile_1
            InputFiles_2["stopt"] = myFile_2
        elif "stops" in filename: 
            InputFiles_1["stops"] = myFile_1
            InputFiles_2["stops"] = myFile_2
        elif "WqqZll" in filename: 
            InputFiles_1["WZ"] = myFile_1
            InputFiles_2["WZ"] = myFile_2
        elif "data" in filename: 
            InputFiles_1["data"] = myFile_1
            InputFiles_2["data"] = myFile_2

    #---------------------------------------------
    # Hist name & setting
    #

    Hist2D_phy_vs_tag_name  = {} 
    Hist2D_phy_vs_tag_title = {}
    Hist2D_phy_vs_tag       = {}
    for _phy in Physics_info :
        
        if SwitchBBToCC :
            ##_phytmp = _phy.replace("BB","CC")
            _phytmp = _phy.replace("B","C")
        else :
            _phytmp = _phy

        Hist2D_phy_vs_tag_name[_phy] = "phy_vs_ntag_"+_phytmp+"2D"
        Hist2D_phy_vs_tag_title[_phy] = " "+_phytmp+" Yields distribution"
        Hist2D_phy_vs_tag[_phy] = ROOT.TH2D(Hist2D_phy_vs_tag_name[_phy], Hist2D_phy_vs_tag_title[_phy],4,0,4,7,0,7)
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(1,"0 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(2,"1 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(3,"2 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(4,"3+ tag")

        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(7,"ggZH : ZllHcc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(6,"qqZH : ZllHcc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(5,"Z+C : Z+L" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(4,"Z+C : Z+cL" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(3,"Z+C : Z+cc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(2,"ZZ : ZZcc")
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(1,"ZZ : ZZbkg")
        Hist2D_phy_vs_tag[_phy].GetZaxis().SetTitle("Number of evnets")

    #---------------------------------------------
    # Hist name & setting
    #
    if DataOnPlot :
        SampleType_info = ["qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","stops","stopt","stopWt","ggZllZqq","ttbar","WZ","ZZ","ZQ","data"]
    else:
        SampleType_info = ["qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","stops","stopt","stopWt","ggZllZqq","ttbar","WZ","ZZ","ZQ"]
#    SampleType_info = ["qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","stops","stopt","stopWt","ggZllZqq","ttbar","WZ","ZZ","ZQ"]

    Sample_process_info = {}
    if USETau : 
        Sample_process_info["ZQ"] = ["Zcc", "Zbb", "Zll","Zcl","Zbl","Zbc","Zbtau","Zctau","Zltau","Ztautau","Zc","Zb","Zl","Z"] 
        Sample_process_info["ZZ"] = ["ZZcc", "ZZbc", "ZZcl","ZZbl","ZZbb","ZZll","ZZbtau","ZZctau","ZZltau","ZZtautau","ZZbkg"]
        Sample_process_info["WZ"] = ["WZcc", "WZbc", "WZcl","WZbl","WZbb","WZll","WZbtau","WZctau","WZltau","WZtautau","WZbkg"]
        Sample_process_info["ttbar"] = ["ttbarcc", "ttbarbc", "ttbarcl","ttbarbl","ttbarbb","ttbarll","ttbarbtau","ttbarctau","ttbarltau","ttbartautau","ttbar"]
        Sample_process_info["stopWt"] = ["stopWtcc", "stopWtbc", "stopWtcl","stopWtbl","stopWtbb","stopWtll","stopWtbtau","stopWtctau","stopWtltau","stopWttautau","stopWt"]
        Sample_process_info["stopt"] = ["stoptcc", "stoptbc", "stoptcl","stoptbl","stoptbb","stoptll","stoptbtau","stoptctau","stoptltau","stopttautau","stopt"]
        Sample_process_info["stops"] = ["stopscc", "stopsbc", "stopscl","stopsbl","stopsbb","stopsll","stopsbtau","stopsctau","stopsltau","stopstautau","stops"]
        Sample_process_info["qqZllHcc"] = ["qqZllH125cccc","qqZllH125ccbb","qqZllH125ccbc","qqZllH125cccl","qqZllH125ccll","qqZllH125cccbl"]
        Sample_process_info["ggZllHcc"] = ["ggZllH125cccc","ggZllH125ccbb","ggZllH125ccbc","ggZllH125cccl","ggZllH125ccll","ggZllH125cccbl"]
        #Sample_process_info["ggZllHcc"] = ["ggZllH125cccc"]
        Sample_process_info["qqZllHbb"] = ["qqZllH125bb"]
        Sample_process_info["ggZllHbb"] = ["ggZllH125bb"]
        Sample_process_info["ggZllZqq"] = ["ggZZcc","ggZZbc","ggZZcl","ggZZbb","ggZZbl","ggZZll"]
    else:
        Sample_process_info["ZQ"] = ["Zcc", "Zbb", "Zll","Zcl","Zbl","Zbc","Zc","Zb","Zl","Z"]
        Sample_process_info["ZZ"] = ["ZZcc", "ZZbc", "ZZcl","ZZbl","ZZbb","ZZll","ZZbkg"]
        Sample_process_info["WZ"] = ["WZcc", "WZbc", "WZcl","WZbl","WZbb","WZll","WZbkg"]
        #Sample_process_info["WZ"] = ["WZhadlepcc", "WZhadlepbc", "WZhadlepcl","WZhadlepbl","WZhadlepbb","WZhadlepll","WZhadlepbkg"]
        Sample_process_info["ttbar"] = ["ttbarcc", "ttbarbc", "ttbarcl","ttbarbl","ttbarbb","ttbarll","ttbar"]
        Sample_process_info["stopWt"] = ["stopWtcc", "stopWtbc", "stopWtcl","stopWtbl","stopWtbb","stopWtll","stopWt"]
        Sample_process_info["stopt"] = ["stoptcc", "stoptbc", "stoptcl","stoptbl","stoptbb","stoptll","stopt"]
        Sample_process_info["stops"] = ["stopscc", "stopsbc", "stopscl","stopsbl","stopsbb","stopsll","stops"]
        Sample_process_info["qqZllHcc"] = ["qqZllH125cc"]
        Sample_process_info["ggZllHcc"] = ["ggZllH125cc"]
        Sample_process_info["qqZllHbb"] = ["qqZllH125"]
        Sample_process_info["ggZllHbb"] = ["ggZllH125"]
        Sample_process_info["ggZllZqq"] = ["ggZZcc","ggZZbc","ggZZcl","ggZZbb","ggZZbl","ggZZll"]

    Sample_process_info["data"] = ["data"]

    #---------------------------------------------
    # Ctag
    #
    CTagEffTight={}
    CTagEffLoose={}
    '''
    CTagEffLoose["c"] = 0.41
    CTagEffLoose["b"] = 0.25
    CTagEffLoose["l"] = 0.05
    CTagEffLoose["tau"] = 0.41
    CTagEffLoose["WP"] = "Loose"

    CTagEffTight["c"] = 0.18
    CTagEffTight["b"] = 0.05
    CTagEffTight["l"] = 0.005
    CTagEffTight["tau"] = 0.18
    CTagEffTight["WP"] = "Tight"
    '''
    CTagEffLoose["c"] = 0.4
    CTagEffLoose["b"] = 0.2
    CTagEffLoose["l"] = 0.04
    CTagEffLoose["tau"] = 0.4
    CTagEffLoose["WP"] = "Loose"

    CTagEffTight["c"] = 0.17
    CTagEffTight["b"] = 0.05
    CTagEffTight["l"] = 0.0025
    CTagEffTight["tau"] = 0.17
    CTagEffTight["WP"] = "Tight"

    #---------------------------
    # Fill HIst Distribution 
    #---------------------------
    print "SAMPLETYPEINFO"
    print SampleType_info
    #physics_var="mJ"
    physics_var="mBB"

    #FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, physics_var, InputFiles_1,{})
    
    Physics_info = [
        #"mBB", 
        "dRVH","mBB","pTBB","dRBB","dEtaBB","dPhiBB",
        "pTB1","pTB2","EtaB1","EtaB2","sumPtJets","nJ"
#        "PtFatJ1","EtaFatJ1","NtrkjetsFatJ1"
    ]

    for phy_var in Physics_info:
        FillHist_Phy_Compare(SampleType_info, Sample_process_info, phy_var, InputFiles_1, InputFiles_2, "32-15 EMTopo","33-05 PFlow")
        
    #FillHist_Phy_Compare(SampleType_info, Sample_process_info, "dRVH", InputFiles_1, InputFiles_2, "32-15 EMTopo","33-05 PFlow")
    #FillHist_Phy_Compare(SampleType_info, Sample_process_info, "pTB1", InputFiles_1, InputFiles_2, "32-15 EMTopo","33-05 PFlow")
    #---------------------------
    # Fill Yield Distribution 
    #---------------------------

    #Hist_SB_NoTag = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, physics_var, InputFiles_1)
    #Hist_SB_NoTag = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, physics_var, InputFiles_2)
    
    #---------------------------
    # Fill CutFlow 
    #---------------------------
    '''
    FillHist_CutFlow_Compare( "ZZ",        "ZZ",         InputFiles_1, InputFiles_2, ["3215 EMTOPO","3305 PFLOW"])
    FillHist_CutFlow_Compare( "WZ",        "WZ",         InputFiles_1, InputFiles_2, ["3215 EMTOPO","3305 PFLOW"])
    FillHist_CutFlow_Compare( "ZQ",        "Z+Jets",     InputFiles_1, InputFiles_2, ["3215 EMTOPO","3305 PFLOW"])
    FillHist_CutFlow_Compare( "qqZllHcc",  "qqZllHcc",   InputFiles_1, InputFiles_2, ["3215 EMTOPO","3305 PFLOW"])
    FillHist_CutFlow_Compare( "ggZllHcc",  "ggZllHcc",   InputFiles_1, InputFiles_2, ["3215 EMTOPO","3305 PFLOW"])
    FillHist_CutFlow_Compare( "qqZllHbb",  "qqZllHbb",   InputFiles_1, InputFiles_2, ["3215 EMTOPO","3305 PFLOW"])
    FillHist_CutFlow_Compare( "ggZllHbb",  "ggZllHbb",   InputFiles_1, InputFiles_2, ["3215 EMTOPO","3305 PFLOW"])
    '''
    #---------------------------
    # Fill S/B  Distribution 
    #---------------------------
  
if __name__ == "__main__":
    main()
