#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D
from ClassDef import MergeHists 

####################################################
#  #################################################
#  #
#  #        User defined variable 
#  #
#  #################################################
####################################################
SignalScale=10
SetLogY_1D = True
SetLogZ_2D = True 
SwitchBBToCC = True # switch name with BB to CC
UsingTop = True # use top quark process ttbar wt 
USETau = False 
ZeroTagOnly = True
DataOnPlot = False 
####################################################
#  #################################################
#  #
#  #        User defined drawing function
#  #
#  #################################################
####################################################
def CalCTagEff(histName,CTagEff):
    
    Eff = 1.0
    c = CTagEff["c"]
    b = CTagEff["b"]
    l = CTagEff["l"]
    t = CTagEff["tau"]
    if "H125_" in histName:
        Eff = Eff * b * b
    elif "H125cc_" in histName:
        Eff = Eff * c * c
    elif "cc_" in histName:
        Eff = Eff * c * c
    elif "bb_" in histName:
        Eff = Eff * b * b
    elif "ll_" in histName:
        Eff = Eff * l * l
    elif "bc_" in histName:
        Eff = Eff * b * c
    elif "bl_" in histName:
        Eff = Eff * b * l
    elif "cl_" in histName:
        Eff = Eff * c * l
    elif "tautau_" in histName:
        Eff = Eff * t * t
    elif "btau_" in histName:
        Eff = Eff * b * t
    elif "ctau_" in histName:
        Eff = Eff * c * t
    elif "ltau_" in histName:
        Eff = Eff * l * t
    elif "b_" in histName:
        Eff = Eff * b 
    elif "c_" in histName:
        Eff = Eff * c
    elif "l_" in histName:
        Eff = Eff * l
    print "CalCTagEff c-%f b-%f l-%f tau-%f Eff-%f"%(c,b,l,t,Eff)
        
    return Eff

def Hist2D_filllValue_phy_vs_ptv_simple(hist1DName, hist1D, weight, windowRange=[]):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    sumOfWeights = hist1D.GetSumOfWeights()
    sumOfWeights_window = sumOfWeights
    if "H" in hist1DName:
        print "??????????????   test ??????? %s "%hist1DName
        #print "Getintegral    : %f" %hist1D.GetIntegral()
        print "?? integral       : %f" %hist1D.Integral()
        print "?? sumOfWeights   : %f" %hist1D.GetSumOfWeights()
        print "?? Binwidth       : %f" %hist1D.GetBinWidth(1)
        print "?? Binwidth       : %f" %hist1D.GetBinWidth(2)
        print "?? integral/Binwidth : %f" % (hist1D.Integral()*1.0/hist1D.GetBinWidth(1))
        total = 0 
        for  x in range(0,hist1D.GetNbinsX()) :
            cot = hist1D.GetBinContent(x+1)
            center= hist1D.GetBinCenter(x+1)
            total += cot 
            print "?? x %d bincenter %f : %f"%(x,center,cot)
        print "?? total %f"    %total

    if len( windowRange )>0:
        #sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0/hist1D.GetBinWidth(2)
        sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0
    
    if "0_250ptv" in histName:
        ptv = 0.5
    elif "250_400ptv" in histName:
        ptv = 1.5
    elif "400ptv" in histName:
        ptv = 2.5

    if "ggZllH125cc_" in  histName: 
        phy=6.5
    elif "qqZllH125cc_" in  histName: 
        phy=5.5
    elif "ggZllH125_" in  histName: 
        phy=4.5
    elif "qqZllH125_" in  histName: 
        phy=3.5
    elif "ggZZ" in  histName: 
        phy=2.5
    elif "ZZ" in  histName: 
        phy=1.5
    elif "WZ" in  histName: 
        phy=0.5
    elif "Z" in  histName: 
        phy=-0.5
    elif "ttbar" in  histName: 
        phy=-1.5
    elif "stopWt" in  histName: 
        phy=-2.5
    elif "stopt" in  histName: 
        phy=-3.5
    elif "stops" in  histName: 
        phy=-4.5
    
    x = ptv 
    if UsingTop :
        y = phy + 5
    else :
        y = phy

    return (x, y, sumOfWeights*weight, sumOfWeights_window*weight)

def Hist2D_filllValue_phy_vs_ptv_all(hist1DName, hist1D, weight, windowRange=[]):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    hist1D.Sumw2()
    sumOfWeights = hist1D.GetSumOfWeights()
    sumOfWeights_window = sumOfWeights
    if len( windowRange )>0:
        #sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0/hist1D.GetBinWidth(1)
        sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0
    
    if "0_250ptv" in histName:
        ptv = 0.5
    elif "250_400ptv" in histName:
        ptv = 1.5
    elif "400ptv" in histName:
        ptv = 2.5

    if "ggZllH125cc_" in  histName: 
        phy=23.5
    elif "qqZllH125cc_" in  histName: 
        phy=22.5
    elif "ggZllH125_" in  histName: 
        phy=21.5
    elif "qqZllH125_" in  histName: 
        phy=20.5
    elif "ggZZcc_" in  histName: 
        phy=19.5
    elif "ggZZbb_" in  histName: 
        phy=18.5
    elif "ggZZll_" in  histName: 
        phy=17.5
    elif "ggZZcl_" in  histName: 
        phy=16.5
    elif "ggZZbl_" in  histName: 
        phy=15.5
    elif "ggZZbc_" in  histName: 
        phy=14.5
    elif "ZZcc_" in  histName: 
        phy=13.5
    elif "ZZbb_" in  histName: 
        phy=12.5
    elif "ZZll_" in  histName: 
        phy=11.5
    elif "ZZcl_" in  histName: 
        phy=10.5
    elif "ZZbl_" in  histName: 
        phy=9.5
    elif "ZZbc_" in  histName: 
        phy=8.5
    elif "ZZbkg_" in  histName: 
        phy=7.5
    elif "Zcc_" in  histName: 
        phy=6.5
    elif "Zbb_" in  histName: 
        phy=5.5
    elif "Zll_" in  histName: 
        phy=4.5
    elif "Zcl_" in  histName: 
        phy=3.5
    elif "Zbl_" in  histName: 
        phy=2.5
    elif "Zbc_" in  histName: 
        phy=1.5
    elif "Z_" in  histName: 
        phy=0.5
    elif "ttbarcc_" in histName:
        phy = -0.5
    elif "ttbarbb_" in histName:
        phy = -1.5
    elif "ttbarll_" in histName:
        phy = -2.5
    elif "ttbarcl_" in histName:
        phy = -3.5
    elif "ttbarbl_" in histName:
        phy = -4.5
    elif "ttbarbc_" in histName:
        phy = -5.5
    elif "ttbar_" in histName:
        phy = -6.5
    elif "stopWtcc_" in histName:
        phy = -7.5
    elif "stopWtbb_" in histName:
        phy = -8.5
    elif "stopWtll_" in histName:
        phy = -9.5
    elif "stopWtcl_" in histName:
        phy = -10.5
    elif "stopWtbl_" in histName:
        phy = -11.5
    elif "stopWtbc_" in histName:
        phy = -12.5
    elif "stopWt_" in histName:
        phy = -13.5
    elif "stoptcc_" in histName:
        phy = -14.5
    elif "stoptbb_" in histName:
        phy = -15.5
    elif "stoptll_" in histName:
        phy = -16.5
    elif "stoptcl_" in histName:
        phy = -17.5
    elif "stoptbl_" in histName:
        phy = -18.5
    elif "stoptbc_" in histName:
        phy = -19.5
    elif "stopt_" in histName:
        phy = -20.5
    elif "stopscc_" in histName:
        phy = -21.5
    elif "stopsbb_" in histName:
        phy = -22.5
    elif "stopsll_" in histName:
        phy = -23.5
    elif "stopscl_" in histName:
        phy = -24.5
    elif "stopsbl_" in histName:
        phy = -25.5
    elif "stopsbc_" in histName:
        phy = -26.5
    elif "stops_" in histName:
        phy = -27.5
    else : ## tmp steeing
        phy = -27.5

    
    
    x = ptv 
    if UsingTop : 
        y = phy + 28
    else :
        y = phy

    return (x, y, sumOfWeights * weight, sumOfWeights_window * weight)

def Hist2D_filllValue_phy_vs_ntag(hist1DName, hist1D):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    integral = hist1D.GetSumOfWeights()
    
    if "0tag" in histName:
        tag = 0.5
    elif "1tag" in histName:
        tag = 1.5
    elif "2tag" in histName:
        tag = 2.5
    elif "3ptag" in histName:
        tag = 3.5

    if "ZZbkg" in  histName: 
        phy=0.5
    elif "ZZ" in histName:
        phy=1.5
    elif "Zcc" in histName:
        phy=2.5
    elif "Zcl" in histName:
        phy =3.5 
    elif "qqZll" in histName :
        phy=5.5
    elif "ggZll" in histName :
        phy=6.5
    elif "Zl" in histName :
        phy=4.5
    
    x = tag 
    y = phy
    return (tag, phy, integral)

def Hist2D_filllValue_njet_vs_ntag(hist1DName, hist1D):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    integral = hist1D.GetSumOfWeights()

    ## x value 
    if "0_250ptv" in histName :
        ptv = 0
    elif "250_400ptv" in histName :
       ptv = 1
    else:
        ptv = 2

    if "0tag" in histName:
        tag = 0.5
    elif "1tag" in histName:
        tag = 1.5
    elif "2tag" in histName:
        tag = 2.5
    elif "3ptag" in histName:
        tag = 3.5
    
    ## y value 
    if "2jet" in histName:
        jet = 0.5 
    elif "3jet" in histName:
        jet =1.5
    elif "4pjet" in histName:
        jet =2.5

    y = tag 
    x = ptv + 0.5  
    return (x,y,integral)
        
def ptvInfo_convert(ptvInfo):
    
    if ptvInfo == "400ptv":
        Info ="ZpT > 400 GeV"
    else:
        Info ="ZpT "+ (ptvInfo.replace("_","-")).replace("ptv"," GeV")

    return Info
##########################################################
#######     Fill Hist Kinematics & Yields      
##########################################################

def FillHist_Kinematic_FatJet(SampleType, SampleTypeName, process_info, Physics_info, Hist2D_phy_vs_tag, InputRootFiles):
    '''
    Fill 1D & 2D Hists 
    Example : 
        SampleType     = "Z+C" 
        SampleTypeName = "ZC" 
        process_info   =  ZC_info
        Hist2D_phy_vs_tag     : 2D Jet Yeild Hist 
    '''
    if ZeroTagOnly :
        tag_info = ["0tag"]
    else:
        tag_info = ["0tag", "1tag", "2tag", "3ptag"]

    jet_info = [ "1pfat0pjet"]

    ptv_info = ["0_250ptv","250_400ptv","400ptv"]
    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]

    SR_info="SR_2psubjet"  
    #SR_info="SR"  

    for _phy in Physics_info :
        RootFile = InputRootFiles[SampleType]         

        for _process in process_info:
            ###  2D Hists ###
            if SwitchBBToCC :
                #_phytmp = _phy.replace("BB","CC")
                _phytmp = _phy.replace("B","C")
            else :
                _phytmp = _phy
            
            Hist2D_name = SampleTypeName+"_"+_process+"_"+_phytmp+"2D"
            Hist2D_title = SampleType+" : "+_process+" "+_phytmp+" Yields distribution"
            Hist2D = ROOT.TH2D(Hist2D_name, Hist2D_title,3,0,3,4,0,4)
            Hist2D.GetXaxis().SetBinLabel(1,"LPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(2,"MPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(3,"HPtV 1pfat+0jet")

            Hist2D.GetYaxis().SetBinLabel(1,"0 tag")
            Hist2D.GetYaxis().SetBinLabel(2,"1 tag")
            Hist2D.GetYaxis().SetBinLabel(3,"2 tag")
            Hist2D.GetYaxis().SetBinLabel(4,"3+ tag")
            Hist2D.GetZaxis().SetTitle("Number of evnets")

            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                drawHists = []
                legends   = []
                outputFileName = SampleTypeName+"_"+_process + "_" + _jet + "_"+ SR_info +"_"+_phytmp 
                #title = SampleType +" : " + _process + " " + _jet + " "+ SR_info 
                if "ZllHcc" in SampleType: 
                    title =  _process + " " + _jet 
                else :
                    title = SampleType +" : " + _process + " " + _jet 
                
                if SwitchBBToCC :
                    title = title.replace("B","C")
         
                for _tag in tag_info :
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue
         
                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        ####_legendName= _process + " " + _tag+_jet + " " + _ptv 
                        _legendName=_tag + " " + ptvInfo_convert(_ptv)
                        
                        #skip blank hist 
                        print "------------------" 
                        tmpHist= RootFile.Get(_histName)
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 

                        copyHist=tmpHist.Clone("%s_copy"%_histName)
                        drawHists += [copyHist]
                        legends += [_legendName]
                        ## Fill 2D Hist 
                        FillValue_2D = Hist2D_filllValue_njet_vs_ntag(_histName , copyHist)
                        Hist2D.Fill(FillValue_2D[0], FillValue_2D[1], FillValue_2D[2])
                        FillValue_2D_phy_vs_ntag = Hist2D_filllValue_phy_vs_ntag(_histName, copyHist)
                        Hist2D_phy_vs_tag[_phy].Fill(FillValue_2D_phy_vs_ntag[0], FillValue_2D_phy_vs_ntag[1], FillValue_2D_phy_vs_ntag[2])
               
                ## Draw 1D Hist 
                HistPlot = HistMaker1D(drawHists,legends)
                if SwitchBBToCC :
                    _phytmp = _phy.replace("B","C")
                else :
                     _phytmp = _phy

                if ('pT' in _phy)  or ('Pt' in _phy) :
                    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
                    HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", legends, "Merged_VHcc/"+outputFileName,[50,1500], SetLogY_1D)
                elif 'mBB' in _phy:
                    HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", legends, "Merged_VHcc/"+outputFileName,[30,800], SetLogY_1D)
                else: 
                    HistPlot.DrawPlots_ATLAS(title, _phytmp,"Number of events", legends, "Merged_VHcc/"+outputFileName,[30,800], SetLogY_1D)

            ## Draw 2D Hist tag vs jet 
            HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
            HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", "Merged_VHcc/"+Hist2D_name,"COLZTEXT", SetLogZ_2D)

    h_cutflow_vhbb= RootFile.Get("CutFlow/PreselectionCutFlow")
    HistPlot_cutflow_vhbb = HistMaker1D([h_cutflow_vhbb],["Preselection cut flow"])
    HistPlot_cutflow_vhbb.DrawPlots_ATLAS(SampleTypeName+"Preselection cut flow", "","Number of events", ["Preselection cut flow"], "Merged_VHcc/CutFlow_Preselcetion"+"_"+SampleTypeName,[0,25], SetLogY_1D)

    h_cutflow_my= RootFile.Get("CutFlow/Nominal/CutsMerged")
    HistPlot_cutflow_my = HistMaker1D([h_cutflow_my],["Merged cut flow"])
    HistPlot_cutflow_my.DrawPlots_ATLAS(SampleTypeName+"Merged cut flow", "","Number of events", [ "Merged cut flow"], "Merged_VHcc/CutFlow_Merged"+"_"+SampleTypeName,[0,25], SetLogY_1D)
##########################################################
#######     Fill Hist Phy  
##########################################################

def FillHist_Phy_FatJet(SampleType_info, Sample_process_info, Physics_info, InputRootFiles, CTagEff={}):
    '''
    Fill 1D & 2D Hists 
    Example : 
        SampleType_info     = ["ZC","ZZ","ZB","ZL"        ]
        process_info   =  ZC_info                         
    '''                                                   
    applyTag = False 
    if len(CTagEff) > 0: 
        applyTag = True
                                                          

    if ZeroTagOnly:
        tag_info = ["0tag"]          
    else :
        tag_info = ["0tag", "1tag", "2tag", "3ptag"]          
                                                          
    jet_info = [ "1pfat0pjet"]                            

    ptv_info = ["0_250ptv","250_400ptv","400ptv"]
    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]

    SR_info="SR_2psubjet"  
    #SR_info="SR"  

    Container_hists = {} 
    Container_hists_data = {} 
    Container_hists_sig = {} 
    Container_hists_bkg = {} 
    
    Container_legends = {} 
    Container_legends_sig = {} 
    Container_legends_bkg = {} 

    Container_MergeHistList = {} 
    _phy = Physics_info

    for _ptv in ptv_info :
        Container_hists[_ptv]       = []
        Container_hists_sig[_ptv]   = []
        Container_hists_bkg[_ptv]   = []
        Container_legends[_ptv]     = []
        Container_legends_sig[_ptv] = []
        Container_legends_bkg[_ptv] = []
        Container_MergeHistList[_ptv] = {}
        for _SampleType in SampleType_info :
            Container_MergeHistList[_ptv][_SampleType] = []
     
    #######################  
    ### Fill Containers 
    #               
    for _SampleType in SampleType_info :
        RootFile = InputRootFiles[_SampleType]         

        for _process in Sample_process_info[_SampleType]:
            ###  2D Hists ###
            if SwitchBBToCC :
                #_phytmp = _phy.replace("BB","CC")
                _phytmp = _phy.replace("B","C")
            else :
                _phytmp = _phy
            
            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                
                for _tag in tag_info :
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue

                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        ####_legendName= _process + " " + _tag+_jet + " " + _ptv 
                        ####_legendName=_tag + " " + ptvInfo_convert(_ptv)

                        if "H" in  _SampleType : 
                            _legendName= _SampleType
                        elif "data" in _SampleType:
                            _legendName= "Data"  
                            _histName = _histName.replace("tag","ptag")
                        else :
                            _legendName= _SampleType+" : "+_process  
                        
                        #skip blank hist 
                        print "------------------" 
                        tmpHist= RootFile.Get(_histName)
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 
                        
                        #tmpHist.Sumw2()
                        #tmpHist.Rebin(2)
                        
                        copyHist=tmpHist.Clone("%s_copy"%_histName)
                        copyHist.Sumw2()
                        copyHist.Rebin(4)
                        if applyTag: 
                            copyHist.Scale(CalCTagEff(_process+"_", CTagEff))
                        
                        Container_hists[_ptv] += [copyHist]
                        Container_legends[_ptv] += [_legendName]
                        #print "Container_MergeHistList ptv %s _SampleType %s"
                        #print Container_MergeHistList[_ptv][_SampleType] 
                        Container_MergeHistList[_ptv][_SampleType] += [copyHist]
                        if "stops" in _SampleType : 
                            print "COPYHISTT"
                            print Container_MergeHistList[_ptv][_SampleType]
                            

                        if "data" in _SampleType : 
                            Container_hists_data[_ptv] = [copyHist]
                        elif "H" in _SampleType : 
                            print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  signal %s  %s"%(_SampleType, _legendName)
                            Container_hists_sig[_ptv] += [copyHist]
                            Container_legends_sig[_ptv] += [_legendName]
                        else :
                            print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  Bkg  %s  %s"%(_SampleType, _legendName)
                            Container_hists_bkg[_ptv] += [copyHist]
                            Container_legends_bkg[_ptv] += [_legendName]

    #####################  
    ## Merge Hists initial
    #               
    
    MergedHists  = {}
    MergedHists_sig  = {}
    MergedHists_bkg  = {}
    MergedHists_data  = {}
    
    MergeLegends = {}
    MergeLegends_sig = {} 
    MergeLegends_bkg = {} 
    MergeLegends_data = {} 

    for _ptv in ptv_info : 
        MergedHists[_ptv]=[]
        MergedHists_sig[_ptv]=[]
        MergedHists_bkg[_ptv]=[]
        MergedHists_data[_ptv]=[]
        
        MergeLegends[_ptv]=[]
        MergeLegends_sig[_ptv]=[]
        MergeLegends_bkg[_ptv]=[]
        MergeLegends_data[_ptv]=[]

        for _SampleType in SampleType_info:
            print "%s %s"%(_SampleType,_ptv)
            print "Container_MergeHistList:"
         #   if _ptv == "400ptv" and _SampleType == "stops":
         #      continue
         #   if _ptv == "400ptv" and _SampleType == "stopt":
         #       continue
            print Container_MergeHistList[_ptv][_SampleType]
            if len( Container_MergeHistList[_ptv][_SampleType]) == 0:
                continue

            MergeLegends[_ptv] += [_SampleType] 
            if "data" in _SampleType: 
                MergeLegends_data[_ptv]+= [_SampleType] 
            elif "H" in _SampleType: 
                MergeLegends_sig[_ptv]+= [_SampleType] 
            else:
                MergeLegends_bkg[_ptv]+= [_SampleType] 

            h_merged = MergeHists(Container_MergeHistList[_ptv][_SampleType], _SampleType+_ptv ).Clone("h_%s"%(_SampleType+_ptv))
            MergedHists[_ptv]+=[h_merged]

            if "data" in _SampleType: 
                MergedHists_data[_ptv] += [h_merged]
            elif "H" in _SampleType: 
                MergedHists_sig[_ptv] += [h_merged]
            else :
                MergedHists_bkg[_ptv] += [h_merged]

    #####################  
    ##   Draw Plots   
    #               

    for _ptv in ptv_info : 
        
        ## 1D Hist 
        HistPlot = HistMaker1D(Container_hists[_ptv],Container_legends[_ptv])
        HistPlot.setHistStackSig(Container_hists_sig[_ptv],Container_legends_sig[_ptv])
        HistPlot.setHistStackBkg(Container_hists_bkg[_ptv],Container_legends_bkg[_ptv])
        if len(Container_hists_data) > 0 :
            HistPlot.setHistData(Container_hists_data[_ptv])

        HistPlot.setSignalScale(SignalScale)

        ## 1D Hist Merge
        HistPlot_Merge = HistMaker1D(MergedHists[_ptv],MergeLegends[_ptv])
        HistPlot_Merge.setHistStackSig(MergedHists_sig[_ptv],MergeLegends_sig[_ptv])
        HistPlot_Merge.setHistStackBkg(MergedHists_bkg[_ptv],MergeLegends_bkg[_ptv])

        if len(MergedHists_data[_ptv]) > 0 :
            HistPlot_Merge.setHistData(MergedHists_data[_ptv])

        HistPlot_Merge.setSignalScale(SignalScale)
        
        ## Set title and fileName
        if SwitchBBToCC :
            _phytmp = _phy.replace("B","C")
        else :
             _phytmp = _phy
        if applyTag :
            outputFileName = "CTag"+CTagEff["WP"]+"_"+_ptv+"_"+ SR_info +"_"+_phytmp 
        else:
            outputFileName = _ptv+"_"+ SR_info +"_"+_phytmp 
        if applyTag : 
            title =  "CTag"+CTagEff["WP"]+": "+ ptvInfo_convert(_ptv)    
        else:
            title =  "NoTag: "+ ptvInfo_convert(_ptv)    
        
        if SwitchBBToCC :
            title = title.replace("B","C")
        print title
        print "outputFileName : %s"%outputFileName
        print "!!!!!!! VHCC-Ploter :  DrawStack !!!!! %s "%_ptv

        ## Draw Plots 
        if ('pT' in _phy)  or ('Pt' in _phy) :
            print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
            HistPlot.DrawStacks_ATLAS(title, _phytmp+" [GeV]","Number of events","Merged_VHcc/stack_"+outputFileName, [50,1500], SetLogY_1D)
            HistPlot_Merge.DrawStacks_ATLAS(title, _phytmp+" [GeV]","Number of events","Merged_VHcc/stack_Merge_"+outputFileName, [50,1500], SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", Container_legends[_ptv], "Merged_VHcc/"+outputFileName+"_sig", [50,1500], "Sig", SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", Container_legends[_ptv], "Merged_VHcc/"+outputFileName+"_all", [50,1500], "All", SetLogY_1D)
        elif ('mBB' in _phy) or ('mJ' in _phy):
            HistPlot.DrawStacks_ATLAS(title, _phytmp+" [GeV]","Number of events","Merged_VHcc/stack_"+outputFileName, [0,800], SetLogY_1D)
            HistPlot_Merge.DrawStacks_ATLAS(title, _phytmp+" [GeV]","Number of events","Merged_VHcc/stack_Merge_"+outputFileName, [0,800], SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", Container_legends[_ptv], "Merged_VHcc/"+outputFileName+"_sig", [0,800], "Sig", SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", Container_legends[_ptv], "Merged_VHcc/"+outputFileName+"_all", [0,800], "All", SetLogY_1D)
        else: 
            HistPlot.DrawStacks_ATLAS(title, _phytmp,"Number of events","Merged_VHcc/stack_"+outputFileName, [30,800], SetLogY_1D)
            HistPlot_Merge.DrawStacks_ATLAS(title, _phytmp,"Number of events","Merged_VHcc/stack_Merge_"+outputFileName, [30,800], SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp,"Number of events", Container_legends[_ptv], "Merged_VHcc/"+outputFileName+"_sig", [30,800], "Sig", SetLogY_1D)
            HistPlot.DrawPlots_ATLAS(title, _phytmp,"Number of events", Container_legends[_ptv], "Merged_VHcc/"+outputFileName+"_all", [30,800], "All", SetLogY_1D)
    
##########################################################
#######     Fill Hist Yield
##########################################################

def FillHist_Yield_FatJet(SampleType_info, Sample_process_info, Physics_info, InputRootFiles, CTagEff={}):
    '''
    Fill 2D Hists phy 
    '''                                                   
    applyTag = False 
    if len(CTagEff) > 0: 
        applyTag = True
    
    if ZeroTagOnly :                                                   
        tag_info = ["0tag"]          
    else :
        tag_info = ["0tag", "1tag", "2tag", "3ptag"]          
                                                          
    jet_info = [ "1pfat0pjet"]                            

    ptv_info = ["0_250ptv","250_400ptv","400ptv"]
    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]

    SR_info="SR_2psubjet"  
    #SR_info="SR"  


    _phy = Physics_info

    Hist2D_phy_vs_ptv_name_all  = {} 
    Hist2D_phy_vs_ptv_name_simple  = {} 
    Hist2D_phy_vs_ptv_all       = {}
    Hist2D_phy_vs_ptv_simple       = {}
    Hist2D_phy_vs_ptv_title = {}
    Region_mBB = ["All","Sig"]

    if applyTag: 
        TagInfo="CTag"+CTagEff["WP"]
    else :
        TagInfo="NoTag"

    for _ptv in Region_mBB:
        
        if SwitchBBToCC :
            ##_phytmp = _phy.replace("BB","CC")
            _phytmp = _phy.replace("B","C")
        else :
            _phytmp = _phy

               
        if _ptv =="Sig":
            Hist2D_phy_vs_ptv_title[_ptv] = "Yields distribution : signal region "+" ("+TagInfo+")"
        elif _ptv == "All":
            Hist2D_phy_vs_ptv_title[_ptv] = "Yields distribution : all region "+" ("+TagInfo+")"

        #### Yield 2D plot all detail (initial)####

        if UsingTop : 
            KK = 28
        else :
            KK = 0
        Hist2D_phy_vs_ptv_name_all[_ptv] = "phy_vs_ptv_Yield_"+_phytmp+"_2D_"+_ptv+"Window_"+TagInfo+"_all"
        Hist2D_phy_vs_ptv_all[_ptv] = ROOT.TH2D(Hist2D_phy_vs_ptv_name_all[_ptv], Hist2D_phy_vs_ptv_title[_ptv],4,0,4,24+KK,0,24+KK)
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(1,"low pTV" )
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(2,"medium pTV" )
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(3,"high pTV" )
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(4,"all pTV")

        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+24,"ggZH : ZllHcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+23,"qqZH : ZllHcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+22,"ggZH : ZllHbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+21,"qqZH : ZllHbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+20,"ggZZ : ZllZcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+19,"ggZZ : ZllZbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+18,"ggZZ : ZllZLL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+17,"ggZZ : ZllZcL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+16,"ggZZ : ZllZbL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+15,"ggZZ : ZllZbc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+14,"ZZ : ZllZcc")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+13,"ZZ : ZllZbb")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+12,"ZZ : ZllZLL")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+11,"ZZ : ZllZcL")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+10,"ZZ : ZllZbL")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+9,"ZZ : ZllZbc")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+8,"ZZ : ZZkg")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+7,"Z+Jet : Zcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+6,"Z+Jet : Zbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+5,"Z+Jet : ZLL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+4,"Z+Jet : ZcL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+3,"Z+Jet : ZbL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+2,"Z+Jet : Zbc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(KK+1,"Z+Jet : Zbkg" )

        if UsingTop : 
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(28,"ttbar: cc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(27,"ttbar: bb" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(26,"ttbar: LL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(25,"ttbar: cL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(24,"ttbar: bL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(23,"ttbar: bc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(22,"ttbar: bkg" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(21,"stopWt: cc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(20,"stopWt: bb" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(19,"stopWt: LL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(18,"stopWt: cL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(17,"stopWt: bL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(16,"stopWt: bc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(15,"stopWt: bkg" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(14,"stopt: cc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(13,"stopt: bb" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(12,"stopt: LL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(11,"stopt: cL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(10,"stopt: bL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(9 ,"stopt: bc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(8 ,"stopt: bkg" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(7 ,"stops: cc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(6 ,"stops: bb" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(5 ,"stops: LL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(4 ,"stops: cL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(3 ,"stops: bL" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(2 ,"stops: bc" )
            Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(1 ,"stops: bkg" )
 




        Hist2D_phy_vs_ptv_all[_ptv].GetZaxis().SetTitle("Number of evnets")
                                                           
        #### Yield 2D plot - Simplify  (initial) ####
        if UsingTop : 
            KK = 4
        else :
            KK = 0
        Hist2D_phy_vs_ptv_name_simple[_ptv] = "phy_vs_ptv_Yield_"+_phytmp+"_2D_"+_ptv+"Window_"+TagInfo+"_simple"
        Hist2D_phy_vs_ptv_simple[_ptv] = ROOT.TH2D(Hist2D_phy_vs_ptv_name_simple[_ptv], Hist2D_phy_vs_ptv_title[_ptv],4,0,4,8+KK,0,8+KK)
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(1,"low pTV" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(2,"medium pTV" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(3,"high pTV" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(4,"all pTV")

        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+8,"ggZH: ZllHcc" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+7,"qqZH: ZllHcc" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+6,"ggZH: ZllHbb" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+5,"qqZH: ZllHbb" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+4,"ggZZ: ZllZqq" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+3,"ZZ: ZllZqq")
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+2,"WZ: ZllWqq " )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(KK+1,"Z+Jet " )

        if UsingTop : 
            Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(4,"ttbar" )
            Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(3,"stopWt" )
            Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(2,"stopt" )
            Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(1,"stops" )

        Hist2D_phy_vs_ptv_simple[_ptv].GetZaxis().SetTitle("Number of evnets")

    #### =========================================
    #### Loop & Fill Yield 2D plot ####
    ####

    for _SampleType in SampleType_info :
        RootFile = InputRootFiles[_SampleType]         

        for _process in Sample_process_info[_SampleType]:
            ###  2D Hists ###
            if SwitchBBToCC :
                #_phytmp = _phy.replace("BB","CC")
                _phytmp = _phy.replace("B","C")
            else :
                _phytmp = _phy
            
            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                
                for _tag in tag_info :
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue

                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        ####_legendName= _process + " " + _tag+_jet + " " + _ptv 
                        ####_legendName=_tag + " " + ptvInfo_convert(_ptv)
                        _legendName= _SampleType+" : "+_process  

                        if "data" in _legendName :
                            continue

                        #skip blank hist 
                        print "------------------" 
                        tmpHist = RootFile.Get(_histName)
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 
                        
                        copyHist=tmpHist.Clone("%s_copy"%_histName)
                        
                        xstart=copyHist.FindBin(75)
                        xend=copyHist.FindBin(145)
                        print ")))))))))))))))))))) test S: %d E:%d " %(xstart, xend)
                        xstart=copyHist.FindBin(75.1)
                        xend=copyHist.FindBin(144.9)

                        print ")))))))))))))))))))) S: %d E:%d " %(xstart, xend)

                        if applyTag: 
                            _CTagEff = CalCTagEff(_process+"_", CTagEff)
                        else :
                            _CTagEff = 1
                        
                        FillValue_all = Hist2D_filllValue_phy_vs_ptv_all(_histName, copyHist, _CTagEff, [xstart,xend])
                        #FillValue_simple = Hist2D_filllValue_phy_vs_ptv_simple(_histName, copyHist, [xstart, xend])
                        FillValue_simple = Hist2D_filllValue_phy_vs_ptv_simple(_histName, copyHist, _CTagEff, [xstart, xend])

                        Hist2D_phy_vs_ptv_all["All"].Fill(FillValue_all[0], FillValue_all[1],FillValue_all[2])
                        Hist2D_phy_vs_ptv_all["All"].Fill(3.5, FillValue_all[1],FillValue_all[2])
                        Hist2D_phy_vs_ptv_all["Sig"].Fill(FillValue_all[0], FillValue_all[1],FillValue_all[3])
                        Hist2D_phy_vs_ptv_all["Sig"].Fill(3.5, FillValue_all[1],FillValue_all[3])
                        Hist2D_phy_vs_ptv_simple["All"].Fill(FillValue_simple[0], FillValue_simple[1],FillValue_simple[2])
                        Hist2D_phy_vs_ptv_simple["All"].Fill(3.5, FillValue_simple[1],FillValue_simple[2])
                        Hist2D_phy_vs_ptv_simple["Sig"].Fill(FillValue_simple[0], FillValue_simple[1],FillValue_simple[3])
                        Hist2D_phy_vs_ptv_simple["Sig"].Fill(3.5, FillValue_simple[1],FillValue_simple[3])

    #### =========================================
    ####            S/B 1D plot               ####
    ####

    HistPlot2D_phy_vs_ptv_all={}
    HistPlot2D_phy_vs_ptv_simple={}
    Hist1D_SB={}
    Hist1D_SB_sqrt={}
    Hist1D_SB_title={}
    Hist1D_SB_name={}
    
   
    Hist1D_S={}
    Hist_cut_legend=[]
    Hist_cut={}
    for _region in Region_mBB : 
        
        HistPlot2D_phy_vs_ptv_simple[_region] = HistMaker2D(Hist2D_phy_vs_ptv_simple[_region], Hist2D_phy_vs_ptv_title[_region])
        HistPlot2D_phy_vs_ptv_simple[_region].DrawPlots_ATLAS(Hist2D_phy_vs_ptv_title[_region], "","", "Merged_VHcc/"+Hist2D_phy_vs_ptv_name_simple[_region],"COLZTEXT", SetLogZ_2D)
        
        HistPlot2D_phy_vs_ptv_all[_region] = HistMaker2D(Hist2D_phy_vs_ptv_all[_region], Hist2D_phy_vs_ptv_title[_region])
        HistPlot2D_phy_vs_ptv_all[_region].DrawPlots_ATLAS(Hist2D_phy_vs_ptv_title[_region], "","", "Merged_VHcc/"+Hist2D_phy_vs_ptv_name_all[_region],"COLZTEXT", SetLogZ_2D)

        
        if _region == "All":
            Hist1D_SB_title[_region] = "all region "+" ("+TagInfo+")"
        elif _region == "Sig":
            Hist1D_SB_title[_region] = "signal region "+" ("+TagInfo+")"

        Hist1D_SB_name[_region] = "SB_phy_vs_ptv_Yield_"+_phytmp+"_2D_"+_region+"Window_"+TagInfo+"_all"
        Hist1D_SB[_region]= ROOT.TH1D(Hist1D_SB_name[_region], Hist1D_SB_title[_region], 4,0,4 )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(1,"low pTV" )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(2,"medium pTV" )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(3,"high pTV" )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(4,"all pTV")
        
        Hist1D_SB_sqrt[_region]= ROOT.TH1D(Hist1D_SB_name[_region]+"sqrt", Hist1D_SB_title[_region]+"sqrt", 3,0,3 )
        Hist1D_SB_sqrt[_region].GetXaxis().SetBinLabel(1,"low pTV" )
        Hist1D_SB_sqrt[_region].GetXaxis().SetBinLabel(2,"medium pTV" )
        Hist1D_SB_sqrt[_region].GetXaxis().SetBinLabel(3,"high pTV" )


        sig_int={}
        bkg_int={}
        bkg_int[1]=Hist2D_phy_vs_ptv_simple[_region].Integral(1,1,1,10)
        bkg_int[2]=Hist2D_phy_vs_ptv_simple[_region].Integral(2,2,1,10)
        bkg_int[3]=Hist2D_phy_vs_ptv_simple[_region].Integral(3,3,1,10)
        bkg_int[4]=Hist2D_phy_vs_ptv_simple[_region].Integral(4,4,1,10)

        sig_int[1]=Hist2D_phy_vs_ptv_simple[_region].Integral(1,1,11,12)
        sig_int[2]=Hist2D_phy_vs_ptv_simple[_region].Integral(2,2,11,12)
        sig_int[3]=Hist2D_phy_vs_ptv_simple[_region].Integral(3,3,11,12)
        sig_int[4]=Hist2D_phy_vs_ptv_simple[_region].Integral(4,4,11,12)
        print "-=-=-===-=-=-=check Integral -=-=-=-=-=--=-=-=-" 
        Hist_cut[_region]={}

        if len(Hist_cut_legend) == 0:
            for _y in range(1,12):
                Hist_cut_legend = Hist2D_phy_vs_ptv_simple[_region].GetYaxis().GetBinLabel(_y)

        for _x in range(1,5):
            inte=0
            inte_sig=0
            inte_bkg=0
            _ptv = _x
            Hist_cut[_region][_ptv]=[]
            print "-=-=-===-=-=-=check Integral -=-=-=-=-=--=-=-=-" 
            for _y in range(1,13):
                print _x
                print _y
                if len(CTagEff)>0:
                    print CTagEff["WP"]
                    nametmp="%d_%d_%s"%(_x,_y,CTagEff["WP"])
                else:
                    nametmp="%d_%d_NoTag"%(_x,_y)
                Hist_name = Hist2D_phy_vs_ptv_simple[_region].GetYaxis().GetBinLabel(_y)
                Hist_cut_tmp= ROOT.TH1D(Hist_name+nametmp, Hist_name, 5,0,5)
                Hist_cut_tmp.GetXaxis().SetBinLabel(1,"Notag" )
                Hist_cut_tmp.GetXaxis().SetBinLabel(2,"C-tag Loose" )
                Hist_cut_tmp.GetXaxis().SetBinLabel(3,"C-tag Tight" )

                Integral_tmp = Hist2D_phy_vs_ptv_simple[_region].GetBinContent(_ptv,_y)

                if len(CTagEff)==0:
                    Hist_cut_tmp.SetBinContent(1,Integral_tmp)
                elif CTagEff["WP"]=="Loose":
                    Hist_cut_tmp.SetBinContent(2,Integral_tmp)
                elif CTagEff["WP"]=="Tight":
                    Hist_cut_tmp.SetBinContent(3,Integral_tmp)
            
                Hist_cut[_region][_ptv]+=[Hist_cut_tmp.Clone()]

                # check integral
                print "x %d y %d Content %f"%(_x, _y, Hist2D_phy_vs_ptv_simple[_region].GetBinContent(_x,_y))
                _content=Hist2D_phy_vs_ptv_simple[_region].GetBinContent(_x,_y)
                inte += _content
                if _y <11 : 
                    inte_bkg+=_content
                else :
                    inte_sig+=_content

            print "x %d inte %f integral %f "%(_x, inte, Hist2D_phy_vs_ptv_simple[_region].Integral(_x,_x,1,12))    
            print "x %d Sig : inte %f integral %f "%(_x, inte_sig, math.sqrt(inte_sig))
            print "x %d Bkg : inte %f integral %f "%(_x, inte_bkg, math.sqrt(inte_bkg))
            print "x %d s/b  inte %f sqrt  %f "%( _x,inte_sig/inte_bkg , inte_sig/math.sqrt(inte_bkg))

            
        '''
        Hist1D_SB[_region].SetBinContent(1,sig_int[1]/math.sqrt(bkg_int[1]))
        Hist1D_SB[_region].SetBinContent(2,sig_int[2]/math.sqrt(bkg_int[2]))
        Hist1D_SB[_region].SetBinContent(3,sig_int[3]/math.sqrt(bkg_int[3]))
        Hist1D_SB[_region].SetBinContent(4,sig_int[4]/math.sqrt(bkg_int[4]))
        '''
        Hist1D_SB[_region].SetBinContent(1,sig_int[1]/(bkg_int[1]))
        Hist1D_SB[_region].SetBinContent(2,sig_int[2]/(bkg_int[2]))
        Hist1D_SB[_region].SetBinContent(3,sig_int[3]/(bkg_int[3]))
        Hist1D_SB[_region].SetBinContent(4,sig_int[4]/(bkg_int[4]))

        Hist1D_SB_sqrt[_region].SetBinContent(1,sig_int[1]/math.sqrt(bkg_int[1]))
        Hist1D_SB_sqrt[_region].SetBinContent(2,sig_int[2]/math.sqrt(bkg_int[2]))
        Hist1D_SB_sqrt[_region].SetBinContent(3,sig_int[3]/math.sqrt(bkg_int[3]))



    return [Hist1D_SB , Hist1D_SB_title, Hist_cut, Hist_cut_legend, Hist1D_SB_sqrt] 
    #HistPlot1D = HistMaker1D([Hist1D_SB["All"], Hist1D_SB["Sig"]],[Hist1D_SB_title["All"], Hist1D_SB_title["Sig"]])
    #HistPlot1D.DrawPlots_ATLAS("S/B distribution"+TagInfo, "","S/B", [Hist1D_SB_title["All"], Hist1D_SB_title["Sig"]], "Merged/SB"+TagInfo,[0,4],"All", False)





##########################################################
#######    Draw Hist : phy_vs_ntag 2D Plot     
##########################################################

def HistDraw_Plot2D_phy_vs_ntag(Physics_info, Hist2D_phy_vs_tag, Hist2D_phy_vs_tag_title, Hist2D_phy_vs_tag_name):
    '''
    Physics_info            : physics Info
    Hist2D_phy_vs_tag       : plot Hist 
    Hist2D_phy_vs_tag_title : plot title  
    Hist2D_phy_vs_tag_name  : output file name 

    '''
    HistPlot2D_phy_vs_ntag = {}
    for _phy in Physics_info :
        HistPlot2D_phy_vs_ntag[_phy] = HistMaker2D(Hist2D_phy_vs_tag[_phy], Hist2D_phy_vs_tag_title[_phy])
        HistPlot2D_phy_vs_ntag[_phy].DrawPlots_ATLAS(Hist2D_phy_vs_tag_title[_phy], "c-tag","", "Merged_VHcc/"+Hist2D_phy_vs_tag_name[_phy],"COLZTEXT", SetLogZ_2D)


##########################################################################
# ########################################################################
# #
# #     main function
# #
# ########################################################################
##########################################################################

def main():
    atlas_style()

    #######################################################
    #   VHCC
    #######################################################

    #SetLogY_1D = True
    #SetLogZ_2D = True 
    #-----------------------
    # info
    #-----------------------

    # rootfile_dir  = "/home/chenye/work/CharmJet/VHCC/Result_ade/Merged_2lcc/Result_ade/combin/"
    #rootfile_dir = "/home/chenye/work/CharmJet/VHCC/SimpleROOT_tree_and_hist/Merged/comb/"
    rootfile_dir = "/home/chenye/work/CharmJet/VHCC/SimpleROOT_FlavorLabel/Merged/comb/" # with flavor labal for VHcc2Lep
    rootfile_dir = "/hepustc/home/chenye/work/CharmJet/VHCC/SimpleROOT_FlavorLabel/NewMerged_samecut_VHbb/" # with flavor labal for VHcc2Lep
#    rootfile_dir = "/home/chenye/work/CharmJet/VHCC/RootFile_VHbb/Merged/" # with flavor labal for VHbbcc2Lep

    '''
    rootfile_name = [
        "hist-ZZ_Sh221.root",
        "hist-ZC_Sh221.root",
        "hist-ggZllHcc_PwPy8.root",
        "hist-qqZllHccJ_PwPy8MINLO.root"
    ]
    '''
    rootfile_name = [
        "hist-ggZllHbb_PwPy8.root",
        "hist-ggZllHcc_PwPy8.root",
        "hist-qqZllHbbJ_PwPy8MINLO.root",
        "hist-qqZllHccJ_PwPy8MINLO.root",
        #"hist-ggZllZqq_Sh222.root",
        "hist-ggZqqZll_Sh222.root",
        "hist-ZccZll_Sh221.root",
        "hist-ZqqZll_Sh221.root",
        "hist-ZbbZll_Sh221.root"    
        "hist-ZC_Sh221.root",
        "hist-ZQ_Sh221.root",
        "hist-ZB_Sh221.root",
        "hist-ZL_Sh221.root",
        "hist-ZZ_Sh221.root",
        "hist-stopWt_PwPy8.root",
        "hist-ttbar_dilep_PwPy8.root",
        "hist-stops_PwPy8.root",
        "hist-stopt_PwPy8.root",
        "hist-WqqZll_Sh221.root",
        "hist-data.root"
    ]
        #"hist-stopWt_dilep_PwPy8.root",
    Physics_info = [
        "mBBTT", "mBB","pTV", "dRVH",
        "pTBB","dRBB","dEtaBB","dPhiBB",
        "pTB1","pTB2","EtaB1","EtaB2",
        "PtFatJ1","EtaFatJ1","NtrkjetsFatJ1"
    ]

    ##SR_info="topemucr_noaddbjetsr"  



    #---------------------------------------------
    # Get File 
    #

    InputRootFiles={}
    
    for filename in rootfile_name :
        file_name = rootfile_dir + filename
        print file_name
        myFile = TFile(file_name)
        if "ggZllHcc" in filename:
            InputRootFiles["ggZllHcc"] = myFile
        elif "qqZllHcc" in filename:
            InputRootFiles["qqZllHcc"] = myFile
        elif "ggZllHbb" in filename:
            InputRootFiles["ggZllHbb"] = myFile
        elif "qqZllHbb" in filename:
            InputRootFiles["qqZllHbb"] = myFile
        elif ("ggZllZqq" in filename) or("ggZqqZll" in filename) :
            InputRootFiles["ggZllZqq"] = myFile
        elif "ZC" in filename: 
            InputRootFiles["ZC"] = myFile
        elif "ZB" in filename: 
            InputRootFiles["ZB"] = myFile
        elif "ZL" in filename: 
            InputRootFiles["ZL"] = myFile
        elif "ZQ" in filename: 
            InputRootFiles["ZQ"] = myFile
        elif "ZZ" in filename: 
            InputRootFiles["ZZ"] = myFile
        elif "ZccZll" in filename: 
            InputRootFiles["ZllZCC"] = myFile
        elif "ZbbZll" in filename: 
            InputRootFiles["ZllZBB"] = myFile
        elif "ZqqZll" in filename: 
            InputRootFiles["ZllZLL"] = myFile
        elif "ttbar" in filename: 
            InputRootFiles["ttbar"] = myFile
        elif "stopWt" in filename: 
            InputRootFiles["stopWt"] = myFile
        elif "stopt" in filename: 
            InputRootFiles["stopt"] = myFile
        elif "stops" in filename: 
            print "->>>>>>>>>>>>>>> stops  %s"%filename 
            InputRootFiles["stops"] = myFile
        elif "WqqZll" in filename: 
            InputRootFiles["WZ"] = myFile
        elif "data" in filename: 
            print "->>>>>>>>>>>>>>> data %s"%filename 
            InputRootFiles["data"] = myFile

    #---------------------------------------------
    # Hist name & setting
    #

    Hist2D_phy_vs_tag_name  = {} 
    Hist2D_phy_vs_tag_title = {}
    Hist2D_phy_vs_tag       = {}
    for _phy in Physics_info :
        
        if SwitchBBToCC :
            ##_phytmp = _phy.replace("BB","CC")
            _phytmp = _phy.replace("B","C")
        else :
            _phytmp = _phy

        Hist2D_phy_vs_tag_name[_phy] = "phy_vs_ntag_"+_phytmp+"2D"
        Hist2D_phy_vs_tag_title[_phy] = " "+_phytmp+" Yields distribution"
        Hist2D_phy_vs_tag[_phy] = ROOT.TH2D(Hist2D_phy_vs_tag_name[_phy], Hist2D_phy_vs_tag_title[_phy],4,0,4,7,0,7)
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(1,"0 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(2,"1 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(3,"2 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(4,"3+ tag")

        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(7,"ggZH : ZllHcc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(6,"qqZH : ZllHcc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(5,"Z+C : Z+L" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(4,"Z+C : Z+cL" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(3,"Z+C : Z+cc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(2,"ZZ : ZZcc")
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(1,"ZZ : ZZbkg")
        Hist2D_phy_vs_tag[_phy].GetZaxis().SetTitle("Number of evnets")

    #---------------------------------------------
    # Hist name & setting
    #
    #FillHist_Kinematic_FatJet("ZC",       "ZC",   ZC_info,    Physics_info, Hist2D_phy_vs_tag, InputRootFiles)
    #FillHist_Kinematic_FatJet("ZZ",        "ZZ",   ZZ_info,    Physics_info, Hist2D_phy_vs_tag, InputRootFiles)
    #FillHist_Kinematic_FatJet("qqZllHcc",  "qqZllHcc", qqZH_info,  Physics_info, Hist2D_phy_vs_tag, InputRootFiles)
    #FillHist_Kinematic_FatJet("ggZllHcc",  "ggZllHcc", ggZH_info,  Physics_info, Hist2D_phy_vs_tag, InputRootFiles)
    #HistDraw_Plot2D_phy_vs_ntag(["mBB"], Hist2D_phy_vs_tag, Hist2D_phy_vs_tag_title, Hist2D_phy_vs_tag_name)
    
    if DataOnPlot : 
        SampleType_info = ["qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","stops","stopt","stopWt","ggZllZqq","ttbar","WZ","ZZ","ZQ","data"]
    else :
        SampleType_info = ["qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","stops","stopt","stopWt","ggZllZqq","ttbar","WZ","ZZ","ZQ"]

    Sample_process_info = {}
    
    if USETau : 
        Sample_process_info["ZQ"] = ["Zcc", "Zbb", "Zll","Zcl","Zbl","Zbc","Zbtau","Zctau","Zltau","Ztautau","Zc","Zb","Zl","Z"] 
        Sample_process_info["ZZ"] = ["ZZcc", "ZZbc", "ZZcl","ZZbl","ZZbb","ZZll","ZZbtau","ZZctau","ZZltau","ZZtautau","ZZbkg"]
        Sample_process_info["WZ"] = ["WZcc", "WZbc", "WZcl","WZbl","WZbb","WZll","WZbtau","WZctau","WZltau","WZtautau","WZbkg"]
        Sample_process_info["ttbar"] = ["ttbarcc", "ttbarbc", "ttbarcl","ttbarbl","ttbarbb","ttbarll","ttbarbtau","ttbarctau","ttbarltau","ttbartautau","ttbar"]
        Sample_process_info["stopWt"] = ["stopWtcc", "stopWtbc", "stopWtcl","stopWtbl","stopWtbb","stopWtll","stopWtbtau","stopWtctau","stopWtltau","stopWttautau","stopWt"]
        Sample_process_info["stopt"] = ["stoptcc", "stoptbc", "stoptcl","stoptbl","stoptbb","stoptll","stoptbtau","stoptctau","stoptltau","stopttautau","stopt"]
        Sample_process_info["stops"] = ["stopscc", "stopsbc", "stopscl","stopsbl","stopsbb","stopsll","stopsbtau","stopsctau","stopsltau","stopstautau","stops"]
        #Sample_process_info["qqZllHcc"] = ["qqZllH125cc"]
        #Sample_process_info["ggZllHcc"] = ["ggZllH125cc"]
        #Sample_process_info["qqZllHbb"] = ["qqZllH125"]
        #Sample_process_info["ggZllHbb"] = ["ggZllH125"]
        Sample_process_info["qqZllHcc"] = ["qqZllH125cc"]
        Sample_process_info["ggZllHcc"] = ["ggZllH125cc"]
        Sample_process_info["qqZllHbb"] = ["qqZllH125bb"]
        Sample_process_info["ggZllHbb"] = ["ggZllH125bb"]
        Sample_process_info["ggZllZqq"] = ["ggZZcc","ggZZbc","ggZZcl","ggZZbb","ggZZbl","ggZZll"]
    else:
        Sample_process_info["ZQ"] = ["Zcc", "Zbb", "Zll","Zcl","Zbl","Zbc","Zc","Zb","Zl","Z"]
        Sample_process_info["ZZ"] = ["ZZcc", "ZZbc", "ZZcl","ZZbl","ZZbb","ZZll","ZZbkg"]
        #Sample_process_info["WZ"] = ["WZcc", "WZbc", "WZcl","WZbl","WZbb","WZll","WZbkg"]
        Sample_process_info["WZ"] = ["WZhadlepcc", "WZhadlepbc", "WZhadlepcl","WZhadlepbl","WZhadlepbb","WZhadlepll","WZhadlepbkg"]
        Sample_process_info["ttbar"] = ["ttbarcc", "ttbarbc", "ttbarcl","ttbarbl","ttbarbb","ttbarll","ttbar"]
        Sample_process_info["stopWt"] = ["stopWtcc", "stopWtbc", "stopWtcl","stopWtbl","stopWtbb","stopWtll","stopWt"]
        Sample_process_info["stopt"] = ["stoptcc", "stoptbc", "stoptcl","stoptbl","stoptbb","stoptll","stopt"]
        Sample_process_info["stops"] = ["stopscc", "stopsbc", "stopscl","stopsbl","stopsbb","stopsll","stops"]
        Sample_process_info["qqZllHcc"] = ["qqZllH125cc"]
        Sample_process_info["ggZllHcc"] = ["ggZllH125cc"]
        Sample_process_info["qqZllHbb"] = ["qqZllH125"]
        Sample_process_info["ggZllHbb"] = ["ggZllH125"]
        Sample_process_info["ggZllZqq"] = ["ggZZcc","ggZZbc","ggZZcl","ggZZbb","ggZZbl","ggZZll"]

    Sample_process_info["data"] = ["data"]
    CTagEffTight={}
    CTagEffLoose={}
    CTagEffLoose["c"] = 0.4
    CTagEffLoose["b"] = 0.2
    CTagEffLoose["l"] = 0.04
    CTagEffLoose["tau"] = 0.4
    CTagEffLoose["WP"] = "Loose"

    CTagEffTight["c"] = 0.17
    CTagEffTight["b"] = 0.05
    CTagEffTight["l"] = 0.0025
    CTagEffTight["tau"] = 0.17
    CTagEffTight["WP"] = "Tight"
    
    #---------------------------
    # Fill HIst Distribution 
    #---------------------------
    print "SAMPLETYPEINFO"
    print SampleType_info

    FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, "mBB", InputRootFiles, CTagEffLoose)
    FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, "mBB", InputRootFiles, CTagEffTight)
    FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, "mBB", InputRootFiles)
    
    #---------------------------
    # Fill Yield Distribution 
    #---------------------------

    Hist_SB_Loose = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mBB", InputRootFiles, CTagEffLoose)
    Hist_SB_Tight = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mBB", InputRootFiles, CTagEffTight)
    Hist_SB_NoTag = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mBB", InputRootFiles)
    
    #---------------------------
    # Fill S/B  Distribution 
    #---------------------------
    Hist1D_SB_sqrt = []
    Hist1D_SB_title = []
    
    Hist1D_SB_sqrt       = [Hist_SB_NoTag[4]["All"] , Hist_SB_Loose[4]["All"] , Hist_SB_Tight[4]["All"]]
    Hist1D_SB_title_sqrt = [Hist_SB_NoTag[1]["All"] , Hist_SB_Loose[1]["All"] , Hist_SB_Tight[1]["All"]]
    HistPlot1D = HistMaker1D(Hist1D_SB_sqrt,Hist1D_SB_title_sqrt)
    HistPlot1D.setLocation_ATLASTitle(0.2,0.8,0.48,0.88)
    #HistPlot1D.setLocation_Legend_plot(0.2,0.6,0.48,0.77)
    HistPlot1D.setLocation_Legend_plot(0.62,0.7,0.93,0.9)
    HistPlot1D.setColorType("NORM")
    HistPlot1D.DrawPlots_ATLAS("S/#sqrt{B} distribution: all region", "","S/#sqrt{B}", Hist1D_SB_title_sqrt, "Merged_VHcc/SB_sqrt_all",[0,3],"All", False)

    Hist1D_SB_sqrt       = [Hist_SB_NoTag[4]["Sig"] , Hist_SB_Loose[4]["Sig"] , Hist_SB_Tight[4]["Sig"]]
    Hist1D_SB_title_sqrt = [Hist_SB_NoTag[1]["Sig"] , Hist_SB_Loose[1]["Sig"] , Hist_SB_Tight[1]["Sig"]]
    HistPlot1D = HistMaker1D(Hist1D_SB_sqrt,Hist1D_SB_title_sqrt)
    HistPlot1D.setLocation_ATLASTitle(0.2,0.8,0.48,0.88)
    #HistPlot1D.setLocation_Legend_plot(0.2,0.6,0.48,0.77)
    HistPlot1D.setLocation_Legend_plot(0.62,0.7,0.93,0.9)
    HistPlot1D.setColorType("NORM")
    HistPlot1D.DrawPlots_ATLAS("S/#sqrt{B} distribution: signal region", "","S/#sqrt{B}", Hist1D_SB_title_sqrt, "Merged_VHcc/SB_sqrt_sig",[0,3],"All", False)

    #---------------------------

    Hist1D_SB = []
    Hist1D_SB_title = []
    
    Hist1D_SB       = [Hist_SB_NoTag[0]["All"] , Hist_SB_Loose[0]["All"] , Hist_SB_Tight[0]["All"]]
    Hist1D_SB_title = [Hist_SB_NoTag[1]["All"] , Hist_SB_Loose[1]["All"] , Hist_SB_Tight[1]["All"]]
    HistPlot1D = HistMaker1D(Hist1D_SB,Hist1D_SB_title)
    HistPlot1D.setLocation_ATLASTitle(0.2,0.8,0.48,0.88)
    HistPlot1D.setLocation_Legend_plot(0.2,0.6,0.48,0.77)
    HistPlot1D.setColorType("NORM")
    HistPlot1D.DrawPlots_ATLAS("S/B distribution: all region", "","S/B", Hist1D_SB_title, "Merged_VHcc/SB_all",[0,4],"All", False)

    Hist1D_SB       =   [Hist_SB_NoTag[0]["Sig"] , Hist_SB_Loose[0]["Sig"] , Hist_SB_Tight[0]["Sig"]]
    Hist1D_SB_title =   [ Hist_SB_NoTag[1]["Sig"] , Hist_SB_Loose[1]["Sig"] , Hist_SB_Tight[1]["Sig"]]
    HistPlot1D = HistMaker1D(Hist1D_SB,Hist1D_SB_title)
    HistPlot1D.setLocation_ATLASTitle(0.2,0.8,0.48,0.88)
    HistPlot1D.setLocation_Legend_plot(0.2,0.6,0.48,0.77)
    HistPlot1D.setColorType("NORM")
    HistPlot1D.DrawPlots_ATLAS("S/B distribution: signal region", "","S/B", Hist1D_SB_title, "Merged_VHcc/SB_sig",[0,4],"All", False)
    #---------------------------
    # Fill Eff cut   Distribution 
    #---------------------------
    
 #    sampleListName=["stops" ,"stopt","stopWt","ttbar","Z+jet","WZ","ZZ","ggZZ","qqZHbb","ggZHbb","qqZHcc","ggZHcc"]
 #    sampleListNum =[       0,      1,       2,       3,      4,      5,   6,   7,     8,       9,      10,      11]
    SampleListName=["stops" ,"stopt","stopWt", "ttbar","Z+jet",  "WZ", "ZZ", "ggZZ","qqZHbb","ggZHbb","qqZHcc","ggZHcc"]
    SampleListNum =[       0,      1,       2,       3,      4,     5,    6,      7,       8,       9,      10,      11]
    SampleMap={} 
    for _num in SampleListNum :
        SampleMap[_num]=SampleListName[_num]

    #SampleType_info=["ggZllHcc", "qqZllHcc","ggZllHbb","qqZllHbb","stops","stopt","stopWt","ggZllZqq","ttbar","WZ","ZZ","ZQ"]
    UsingNumList =   [        11,        10,         9 ,         8,     0 ,     1 ,      2 ,        7,     3 ,    5,   6,   4    ]
    Region_mBB = ["All","Sig"]
    Hists_Merge_Cut={}
    Hists_Merge_Cut_sig={}
    Hists_Merge_Cut_bkg={}
    #Hists_Merge_Cut_legend=Hist_SB_NoTag[3]
    Hists_Merge_Cut_legend=[]
    Hists_Merge_Cut_legend_sig=[]
    Hists_Merge_Cut_legend_bkg=[]
    for _process in UsingNumList:
        Hists_Merge_Cut_legend+=[SampleMap[_process]]
        if _process > 7: 
            Hists_Merge_Cut_legend_sig+=[SampleMap[_process]]
        else :
            Hists_Merge_Cut_legend_bkg+=[SampleMap[_process]]


    for _region in Region_mBB :
        Hists_Merge_Cut[_region]={}
        Hists_Merge_Cut_sig[_region]={}
        Hists_Merge_Cut_bkg[_region]={}
        for _ptv in range(1,5) :
            Hists_Merge_Cut[_region][_ptv]=[]
            Hists_Merge_Cut_sig[_region][_ptv]=[]
            Hists_Merge_Cut_bkg[_region][_ptv]=[]
            #for _process in range(0,len(Hist_SB_Loose[2][_region][_ptv])):
            for _process in UsingNumList:
                Hists_Loose = Hist_SB_Loose[2][_region][_ptv][_process]
                Hists_Tight = Hist_SB_Tight[2][_region][_ptv][_process]
                Hists_NoTag = Hist_SB_NoTag[2][_region][_ptv][_process]
                #Hists_Merge_tmp = MergeHists([Hists_Loose, Hists_Tight, Hists_NoTag],Hists_Merge_Cut_legend[_process])
                Hists_Merge_tmp = MergeHists([Hists_Loose, Hists_Tight, Hists_NoTag],_region+str(_ptv)+str(_process)+SampleMap[_process])
                Hists_Merge_Cut[_region][_ptv]+=[Hists_Merge_tmp.Clone()]
                if _process > 7: 
                    Hists_Merge_Cut_sig[_region][_ptv]+=[Hists_Merge_tmp.Clone()]
                else :
                    Hists_Merge_Cut_bkg[_region][_ptv]+=[Hists_Merge_tmp.Clone()]

            HistPlot_Cut = HistMaker1D(Hists_Merge_Cut[_region][_ptv],Hists_Merge_Cut_legend)
            HistPlot_Cut.setHistStackBkg(Hists_Merge_Cut_bkg[_region][_ptv],Hists_Merge_Cut_legend_bkg)
            HistPlot_Cut.setHistStackSig(Hists_Merge_Cut_sig[_region][_ptv],Hists_Merge_Cut_legend_sig)
            #HistPlot_Cut.setSignalScale(SignalScale)
            print "LENGTH Hists_Merge_Cut %d"%len(Hists_Merge_Cut[_region][_ptv])
            print "LENGTH Hists_Merge_Cut_legend %d"%len(Hists_Merge_Cut_legend)
            print Hists_Merge_Cut_legend
            if _region == "Sig":
                title = "Signal region" 
            else :
                title = "All region" 

            fileName  = _region+"Window" 
            if _ptv == 1 :
                title    +=": ZpT 0-250 GeV"
                fileName += "_LowPTV" 
            elif _ptv == 2 :
                title+=": ZpT 250-400 GeV"
                fileName += "_MediumPTV" 
            elif _ptv == 3 :
                title+=": ZpT >400 GeV"
                fileName += "_HighPTV" 
            elif _ptv == 4 :
                title+=" "
                fileName += "_AllPTV" 
            
            HistPlot_Cut.setColorType("AUTO")
            HistPlot1D.setLocation_Legend_plot(0.7,0.5,0.9,0.77)
            HistPlot_Cut.DrawStacks_ATLAS(title, "","Number of events","Merged_VHcc/stack_Cut_Merge_"+fileName,[0,5] ,SetLogY_1D)

if __name__ == "__main__":
    main()
