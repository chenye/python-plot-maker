#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText,TMath
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D
from ClassDef import MergeHists, SignificanceCalculatuion_All, SignificanceCalculatuion_Bin
from Function_VHcc_MVA import TMVAppResultEvaluation ,TMVAppResultEvaluation_SameFile, TMVATraining_output, TMVATraining_output_overtarin;

####################################################
#  #################################################
#  #
#  #        User defined variable 
#  #
#  #################################################
####################################################
SignalScale=10
SetLogY_1D = True
SetLogZ_2D = True 
SwitchBBToCC = True # switch name with BB to CC
UsingTop = True # use top quark process ttbar wt 
USETau = False 
ZeroTagOnly = True
DataOnPlot = False 
####################################################
#  #################################################
#  #
#  #        User defined drawing function
#  #
#  #################################################
####################################################
        
def ptvInfo_convert(ptvInfo):
    
    if ptvInfo == "400ptv":
        Info ="ZpT > 400 GeV"
    else:
        Info ="ZpT "+ (ptvInfo.replace("_","-")).replace("ptv"," GeV")

    return Info
##########################################################
#######     Fill Hist Kinematics & Yields      
##########################################################

##########################################################################
# ########################################################################
# #
# #     main function
# #
# ########################################################################
##########################################################################

def main():
    atlas_style()

    #######################################################
    #   VHCC
    #######################################################

    #SetLogY_1D = True
    #SetLogZ_2D = True 
    #-----------------------
    # info
    #-----------------------



    #---------------------------------------------
    # Get File TMVA Apply
    #

    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/Inclusive"
    # BDT Input 
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut_MVApp"
        #old variable VHcc SR
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut_MVApp/SigRegion"
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut_MVApp/SigRegion_HML_Seperate"
    InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/MVApp_with_pTV"
        #old variable VHbb
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut_MVApp_Old_Variable"


    # BDTG Input 
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDTG_nodR_noJetVetoCut_MVApp"
    # BDT Input with nJ Variable 
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut_MVApp_with_nJ/SR"
    FileNameLow="low.root"
    FileNameHM="hm.root"
    FileNameH="high.root"
    FileNameM="med.root"

    DataFileNameL="low_data.root"
    DataFileNameHM="hm_data.root"
    DataFileNameH="high_data.root"
    DataFileNameM="med_data.root"
    

    DataFileNameL=""
    DataFileNameHM=""
    DataFileNameH=""
    DataFileNameM=""
    SetName = "MVA_BDT"
    #SetName = "Method_BDT" # MVATraining_VHbb Setting
    '''
    TMVAppResultEvaluation_SameFile(InputDir, FileNameLow, SetName,"sig_2T_2pJ_75ptv_150ptv_mva","bg_2T_2pJ_75ptv_150ptv_mva","Resolved VHcc 2Lep;TT+TL Low pTV Region","Ntag2_PTV_Low" )
    TMVAppResultEvaluation_SameFile(InputDir, FileNameHM, SetName,"sig_2T_2pJ_150ptv_mva","bg_2T_2pJ_150ptv_mva","Resolved VHcc 2Lep;TT+TL Med+High pTV Region","Ntag2_PTV_MedHigh" )
    TMVAppResultEvaluation_SameFile(InputDir, FileNameH, SetName,"sig_2T_2pJ_250ptv_mva","bg_2T_2pJ_250ptv_mva","Resolved VHcc 2Lep;TT+TL High pTV Region","Ntag2_PTV_High" )
    TMVAppResultEvaluation_SameFile(InputDir, FileNameM, SetName,"sig_2T_2pJ_150ptv_250ptv_mva","bg_2T_2pJ_150ptv_250ptv_mva","Resolved VHcc 2Lep;TT+TL Medium pTV Region","Ntag2_PTV_Med" )
    '''

    TMVAppResultEvaluation_SameFile(InputDir, FileNameLow,  DataFileNameL,  SetName,"sig_2T_2pJ_75ptv_150ptv_mva","bg_2T_2pJ_75ptv_150ptv_mva","data_2T_2pJ_75ptv_150ptv_mva","Resolved VHcc 2Lep;TT+TL Low pTV Region","Ntag2_PTV_Low" )
    TMVAppResultEvaluation_SameFile(InputDir, FileNameHM,   DataFileNameHM, SetName,"sig_2T_2pJ_150ptv_mva","bg_2T_2pJ_150ptv_mva", "data_2T_2pJ_150ptv_mva",   "Resolved VHcc 2Lep;TT+TL Med+High pTV Region", "Ntag2_PTV_MedHigh" )
    #TMVAppResultEvaluation_SameFile(InputDir, FileNameH,    DataFileNameH,  SetName,"sig_2T_2pJ_250ptv_mva","bg_2T_2pJ_250ptv_mva", "data_2T_2pJ_250ptv_mva",   "Resolved VHcc 2Lep;TT+TL High pTV Region",     "Ntag2_PTV_High"    )
    #TMVAppResultEvaluation_SameFile(InputDir, FileNameM,    DataFileNameM,  SetName,"sig_2T_2pJ_150ptv_250ptv_mva","bg_2T_2pJ_150ptv_250ptv_mva","data_2T_2pJ_150ptv_250ptv_mva","Resolved VHcc 2Lep;TT+TL Medium pTV Region","Ntag2_PTV_Med" )

    #
    #---------------------------------------------
    # Get  TMVA Train out put 
    #
    dataloaders = [
        "dataloader_BDT_2L_2pJ_75ptv_150ptv_1of2",
        "dataloader_BDT_2L_2pJ_75ptv_150ptv_2of2",
     #   "dataloader_BDT_2L_2pJ_150ptv_250ptv_1of2",
     #   "dataloader_BDT_2L_2pJ_150ptv_250ptv_2of2",
     #   "dataloader_BDT_2L_2pJ_250ptv_1of2",
     #   "dataloader_BDT_2L_2pJ_250ptv_2of2",
        "dataloader_BDT_2L_2pJ_150ptv_1of2",    
        "dataloader_BDT_2L_2pJ_150ptv_2of2"
    ]
    
    InputFileNames = [
        "BDT_2L_2pJ_75ptv_150ptv_1of2",
        "BDT_2L_2pJ_75ptv_150ptv_2of2",
     #   "BDT_2L_2pJ_150ptv_250ptv_1of2",
     #   "BDT_2L_2pJ_150ptv_250ptv_2of2",
     #   "BDT_2L_2pJ_250ptv_1of2",
     #   "BDT_2L_2pJ_250ptv_2of2",
        "BDT_2L_2pJ_150ptv_1of2",
        "BDT_2L_2pJ_150ptv_2of2"
    ]
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/MVATrain"
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut_MVATrain"
    InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/MVATrain_2J3J_with_pTV"
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut_MVATrain_with_nJ"

    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut_MVATrain_Old_Variable"

    Method_Name = "Method_BDT" # MVATraining_VHbb Setting
    for datasetName , InputFileName in zip(dataloaders, InputFileNames):

        TagType = ""
        pTVRegion=""
        if "1of2" in datasetName : 
            TagType="_TTTL_1of2"
        elif "2of2" in datasetName : 
            TagType="_TTTL_2of2"
        
        if "75ptv_150ptv" in datasetName : 
            pTVRegion = "Low"
        elif "150ptv_250ptv" in datasetName : 
            pTVRegion = "Medium"
        elif "250ptv" in datasetName : 
            pTVRegion = "High"
        elif "150ptv" in datasetName : 
            pTVRegion = "Med+High"

        TMVATraining_output(InputDir,InputFileName, datasetName, pTVRegion, "2", "TT+TL", TagType, TMVA_Method=Method_Name)

    
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/CrossCheck"
    #TMVATraining_output_overtarin(InputDir, "Output_1of2",  datasetName, "High", "2", "TT+TL", TagType)
    # TMVATraining_output_overtarin(InputDir, "Output_2of2",  datasetName, "High", "2", "TT+TL", TagType)
               
if __name__ == "__main__":
    main()
