#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText,TMath
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D
from ClassDef import MergeHists, SignificanceCalculatuion_All, SignificanceCalculatuion_Bin
from Function_VHcc_MVA import TMVAppResultEvaluation ,TMVAppResultEvaluation_SameFile, TMVATraining_output, TMVATraining_output_overtarin;

####################################################
#  #################################################
#  #
#  #        User defined variable 
#  #
#  #################################################
####################################################
SignalScale=10
SetLogY_1D = True
SetLogZ_2D = True 
SwitchBBToCC = True # switch name with BB to CC
UsingTop = True # use top quark process ttbar wt 
USETau = False 
ZeroTagOnly = True
DataOnPlot = False 
####################################################
#  #################################################
#  #
#  #        User defined drawing function
#  #
#  #################################################
####################################################
        
def ptvInfo_convert(ptvInfo):
    
    if ptvInfo == "400ptv":
        Info ="ZpT > 400 GeV"
    else:
        Info ="ZpT "+ (ptvInfo.replace("_","-")).replace("ptv"," GeV")

    return Info
##########################################################
#######     Fill Hist Kinematics & Yields      
##########################################################

##########################################################################
# ########################################################################
# #
# #     main function
# #
# ########################################################################
##########################################################################

def main():
    #atlas_style()

    #######################################################
    #   VHCC
    #######################################################

    #SetLogY_1D = True
    #SetLogZ_2D = True 
    #-----------------------
    # info
    #-----------------------



    

    #
    #---------------------------------------------
    # Get  TMVA Train out put 
    #
    NTree = 1250
    Ndepth = 2  
    NCuts = 20

    NTree_list = [500,750,1000,1250.1500,1750,2000]
    Ndepth_list = [2,3,4]  
    NCuts_list = [20]
    
    InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/MVATrain_BDTG"

    for NTree in NTree_list:
        for Ndepth in Ndepth_list:
            for NCuts in NCuts_list:
                dataloaders = [
                    "dataset_BDTG_Node2p5_NTree%d_Ndepth%d_NCuts%d_Ntag2_PTV_Low_Nvar8_Bin_All"%(NTree,Ndepth,NCuts),
                    "dataset_BDTG_Node2p5_NTree%d_Ndepth%d_NCuts%d_Ntag2_PTV_MedHigh_Nvar8_Bin_All"%(NTree,Ndepth,NCuts)  
                ]
             
                InputFileNames = [
                    "TMVA_BDTG_Node2p5_NTree%d_Ndepth%d_NCuts%d_Ntag2_PTV_Low_Nvar8_Bin_All"%(NTree,Ndepth,NCuts),
                    "TMVA_BDTG_Node2p5_NTree%d_Ndepth%d_NCuts%d_Ntag2_PTV_MedHigh_Nvar8_Bin_All"%(NTree,Ndepth,NCuts)  
                ]
                for datasetName , InputFileName in zip(dataloaders, InputFileNames):
             
                    TagType = "_TTLL_BDTG"
                    pTVRegion=""
                    if "1of2" in datasetName : 
                        TagType="_TTTL_1of2"
                    elif "2of2" in datasetName : 
                        TagType="_TTTL_2of2"
                
                    if "Low" in datasetName : 
                        pTVRegion = "Low"
                    elif "Medium" in datasetName : 
                        pTVRegion = "Medium"
                    elif "MedHigh" in datasetName : 
                        pTVRegion = "Med+High"
                    elif "High" in datasetName : 
                        pTVRegion = "High"
             
                    TMVATraining_output(InputDir,InputFileName, datasetName, pTVRegion, "2", "TT+TL", TagType,"Method_BDT/BDTG", "_NTree%d_Ndepth%d_NCuts%d"%(NTree,Ndepth,NCuts))

    #---------------------------------------------
    # Get File TMVA Apply
    #

    InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/MVApp_BDTG"
    SetName = "MVA_BDTG"
   # NTree = 1250
   # Ndepth = 2  
   # NCuts = 20

    NTree_list = [500,750,1000,1250.1500,1750,2000]
    Ndepth_list = [2,3,4]  
    NCuts_list = [20]
    PTV_Region = ["Low","MedHigh"]
    for PTV in PTV_Region:
        for NTree in NTree_list:
            for Ndepth in Ndepth_list:
                for NCuts in NCuts_list:
                    SigFileName = "TMVApp_Sig_BDTG_Node2p5_NTree%d_Ndepth%d_NCuts%d_Ntag2_PTV_%s_Nvar8_Bin_All.root"%(NTree,Ndepth,NCuts,PTV)
                    BkgFileName = "TMVApp_Bkg_BDTG_Node2p5_NTree%d_Ndepth%d_NCuts%d_Ntag2_PTV_%s_Nvar8_Bin_All.root"%(NTree,Ndepth,NCuts,PTV)
                    Signif = TMVAppResultEvaluation(InputDir, SigFileName, BkgFileName, HistName="MVA_BDTG", Title ="Resolved VHcc 2Lep BDTG;TT+TL %s pTV Region"%(PTV), regionType="Ntag2_PTV_"+PTV, TagType=TagType,TLtype="TT+TL",MVAInfo="_NTree%d_Ndepth%d_NCuts%d"%(NTree,Ndepth,NCuts))

                    #Signif = TMVAppResultEvaluation(InputDir, SigFileName, BkgFileName, HistName="MVA_BDTG", Title ="Resolved VHcc 2Lep BDTG;TT+TL %s pTV Region"%(PTV), regionType="Ntag2_PTV_"+PTV, TagType=TagType,TLtype="TT+TL")
                    print "NTree %d Ndepth %d NCuts %d PTV %s Significance %f "%(NTree,Ndepth,NCuts,PTV,Signif)
                    txtfile = open ("output/txt_BDTG/BDTG_NTree%d_Ndepth%d_NCuts%d_PTV%s.txt"%(NTree,Ndepth,NCuts,PTV),'w')
                    txtfile.write("NTree %d Ndepth %d NCuts %d PTV %s Significance %f \n" %(NTree,Ndepth,NCuts,PTV,Signif))

    NTree_list = [750]
    Ndepth_list = [2]  
    NCuts_list = [20]
    PTV_Region = ["Low","MedHigh"]
    Norm_Region = ["None", "NumEvents","EqualNumEvents"]
    for PTV in PTV_Region:
        for NTree in NTree_list:
            for Ndepth in Ndepth_list:
                for NCuts in NCuts_list:
                    for Norm in Norm_Region:
                        SigFileName = "TMVApp_Sig_BDTG_Node2p5_NTree%d_Ndepth%d_NCuts%d_Ntag2_PTV_%s_Nvar8_Bin_All_NormMode_%s.root"%(NTree,Ndepth,NCuts,PTV,Norm)
                        BkgFileName = "TMVApp_Bkg_BDTG_Node2p5_NTree%d_Ndepth%d_NCuts%d_Ntag2_PTV_%s_Nvar8_Bin_All_NormMode_%s.root"%(NTree,Ndepth,NCuts,PTV,Norm)
                        Signif = TMVAppResultEvaluation(InputDir, SigFileName, BkgFileName, HistName="MVA_BDTG", Title ="Resolved VHcc 2Lep BDTG;TT+TL %s pTV Region"%(PTV), regionType="Ntag2_PTV_"+PTV, TagType=TagType,TLtype="TT+TL",MVAInfo="_NTree%d_Ndepth%d_NCuts%d_NormMode_%s"%(NTree,Ndepth,NCuts,Norm))

                    #Signif = TMVAppResultEvaluation(InputDir, SigFileName, BkgFileName, HistName="MVA_BDTG", Title ="Resolved VHcc 2Lep BDTG;TT+TL %s pTV Region"%(PTV), regionType="Ntag2_PTV_"+PTV, TagType=TagType,TLtype="TT+TL")
                        print "NTree %d Ndepth %d NCuts %d PTV %s Significance %f NormMode %s"%(NTree,Ndepth,NCuts,PTV,Signif,Norm)
                        txtfile = open ("output/txt_BDTG/BDTG_NTree%d_Ndepth%d_NCuts%d_PTV_%s_NormMode_%s.txt"%(NTree,Ndepth,NCuts,PTV,Norm),'w')
                        txtfile.write("NTree %d Ndepth %d NCuts %d PTV %s Significance %f NormMode %s\n" %(NTree,Ndepth,NCuts,PTV,Signif,Norm))


               
if __name__ == "__main__":
    main()
