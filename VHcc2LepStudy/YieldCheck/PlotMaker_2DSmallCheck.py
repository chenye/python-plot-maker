#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D
from ClassDef import MergeHists 
####################################################
#  #################################################
#  #
#  #        User defined variable 
#  #
#  #################################################
####################################################
SetLogZ_2D = False
#CalStatus="NoCal"
CalOption="Cal"
CalOption="NoCal"
#CutOption=""
CutOption="_Mass70_160"
#CutOption="_dRBBCut"
CalStatus=CalOption+CutOption
TarDir  = "DL1r_VHcc_DL1rcRankL_"+CalStatus+"/" # with flavor labal for VHbbcc2Lep VHcc 
InputFile = "/data/chenye/VHcc/Ntupple/code/result/hist_VHcc1_newDL1r_"+CalStatus+".root"
InputFile = "/data/chenye/VHcc/Ntupple/code/result/hist_VHcc_oldFlavOldTag_DL1rcRank_newDL1r_"+CalStatus+".root"
InputFile = "/data/chenye/VHcc/Ntupple/code/hist_VHcc1_newDL1r_"+CalStatus+".root"
InputFile = "/data/chenye/VHcc/Ntupple/code/hist_all1_newDL1r_"+CalStatus+".root"
#InputFile = "/data/chenye/VHcc/Ntupple/code/result/hist_sig_newDL1r_Cal.root"
TarDir= "Smalcheck/"
InputFile="/data/chenye/VHcc/Ntupple/code/vhcc-mva-tree-processor/MVATree_33-24_AllSignalJets_SF_VHcc_nsig_Slim.root"
InputFile="/data/chenye/VHcc/Ntupple/code/vhcc-mva-tree-processor/MVATree_33-24_AllSignalJets_SF_VHcc_nsig_new_Slim.root"
#InputFile="/data/chenye/VHcc/Ntupple/code/vhcc-mva-tree-processor/MVATree_33-24_AllSignalJets_SF_All_nsig_new_Slim.root"
####################################################
#  #################################################
#  #
#  #        User defined drawing function
#  #
#  #################################################
####################################################
def getTitle(start, sample, SRregion, ptv ):
    title=start
    if sample == "sig": 
        title += "Signal"
    elif sample == "bkg":
        title += "Background"
    else : 
        title += sample
    if sample!="": 
        title += " "
    title += ptv+" "

    if SRregion == "SR1":
        title += "2Jets "
    elif SRregion == "SR2":
        title += "#geq3Jets "
    elif SRregion == "SR":
        title += "#geq2Jets "
    
    return title


def main():
    #ctagFlav2D
    Sample_info = ["VHcc", "VHbb","ZJets","WJets","ZZ","WZ","stop","ttbar","sig","bkg"]
    Region_Info=["SR1","SR2","SR"]
    PTV_Info=["LowPTV","MedHighPTV"];
    
    print "================== Generate Ctag-Flavor 2D Hist  ==================" 
    myFile = TFile(InputFile)
    _histName = "h_CtagCheck_Num"
    Hist2D= myFile.Get(_histName)
    Hist2D.GetZaxis().SetTitle("Number of events")
    Hist2D.GetZaxis().SetTitleSize(0.04)
    Hist2D.GetXaxis().SetTitleSize(0.04)
    Hist2D.GetYaxis().SetTitleSize(0.04)
    Hist2D.GetXaxis().SetTitleOffset(1)
    Hist2D.GetYaxis().SetTitleOffset(1)
    Hist2D.SetMarkerSize(2.0)

    Hist2D_title = "VHcc2Lep Resolved: VHcc Signal PCFT Bin0"
    HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
    Hist2D_name = _histName
    HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "Jet Num.","CTag", TarDir+Hist2D_name,"COLZTEXT", SetLogZ_2D)
    
    _histName = "h_CtagCheck_JetpTJ3"
    Hist2D= myFile.Get(_histName)
    Hist2D.GetZaxis().SetTitle("Number of events")
    Hist2D.GetZaxis().SetTitleSize(0.04)
    Hist2D.GetXaxis().SetTitleSize(0.04)
    Hist2D.GetYaxis().SetTitleSize(0.04)
    Hist2D.GetXaxis().SetTitleOffset(1)
    Hist2D.GetYaxis().SetTitleOffset(1)
    Hist2D.SetMarkerSize(2.0)

    Hist2D_title = "VHcc2Lep Resolved:  VHcc Signal PCFT Bin0"
    HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
    Hist2D_name = _histName
    HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", TarDir+Hist2D_name,"COLZTEXT", SetLogZ_2D)
    HistPlot2D.DrawPlots_ATLAS(Hist2D_title,"pT_{J3}", "CTag", TarDir+Hist2D_name,"COLZTEXT", SetLogZ_2D)

    _histName = "h_CtagCheck_JetEtaJ3"
    Hist2D= myFile.Get(_histName)
    Hist2D.GetZaxis().SetTitle("Number of events")
    Hist2D.GetZaxis().SetTitleSize(0.04)
    Hist2D.GetXaxis().SetTitleSize(0.04)
    Hist2D.GetYaxis().SetTitleSize(0.04)
    Hist2D.GetXaxis().SetTitleOffset(1)
    Hist2D.GetYaxis().SetTitleOffset(1)
    Hist2D.SetMarkerSize(2.0)

    Hist2D_title = "VHcc2Lep Resolved:  VHcc Signal PCFT Bin0"
    HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
    Hist2D_name = _histName
    HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", TarDir+Hist2D_name,"COLZTEXT", SetLogZ_2D)
    HistPlot2D.DrawPlots_ATLAS(Hist2D_title,"Eta_{J3}", "CTag", TarDir+Hist2D_name,"COLZTEXT", SetLogZ_2D)

    _histName = "h_CtagCheck"
    Hist2D= myFile.Get(_histName)
    Hist2D.GetZaxis().SetTitle("Number of events")
    Hist2D.GetZaxis().SetTitleSize(0.04)
    Hist2D.GetXaxis().SetTitleSize(0.04)
    Hist2D.GetYaxis().SetTitleSize(0.04)
    Hist2D.GetXaxis().SetTitleOffset(1)
    Hist2D.GetYaxis().SetTitleOffset(1)
    Hist2D.SetMarkerSize(2.0)

    Hist2D_title = "VHcc2Lep Resolved:  VHcc Signal"
    HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
    Hist2D_name = _histName
    HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "CTag Calculate by PCFT", "CTag",TarDir+Hist2D_name,"COLZTEXT", SetLogZ_2D)


if __name__=="__main__":
    try:
        os.mkdir("output/"+TarDir)
    except OSError as error:
        print "OutputDir Exist"    

    main()
