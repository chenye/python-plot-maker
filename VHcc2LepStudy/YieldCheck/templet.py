#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D
from ClassDef import MergeHists 

####################################################
#  #################################################
#  #
#  #        User defined variable 
#  #
#  #################################################
####################################################
SignalScale=10
SetLogY_1D = True
SetLogZ_2D = True 
SwitchBBToCC = False # switch name with BB to CC
UsingTop = True # use top quark process ttbar wt 
USETau = True #using taujet 
ZeroTagOnly = True #using 0tag only 
ZeroTagInfo="2tag"
DataOnPlot = True # plot data distribution on stack plots
ptv_cate_merge = False#
TarDir="Resolved_VHbbcc_cc_2/"
SampleTYPE="cc"
SIGWIN_START=80.1 # 75.1
SIGWIN_END=339.9 # 74.9
#ROOTFILE_DIR  = "/home/chenye/work/CharmJet/VHCC/RootFile_VHcc/Merged_weight1/" # with flavor labal for VHbbcc2Lep VHcc 
#ROOTFILE_DIR  = "/home/chenye/work/CharmJet/VHCC/RootFile_VHbb/Merged/" # with flavor labal for VHbbcc2Lep  VHbb 0tag
ROOTFILE_DIR  = "/home/chenye/work/CharmJet/VHCC/RootFile_VHcc/Resolved/" # with flavor labal for VHbbcc2Lep VHcc 
ROOTFILE_DIR  = "/home/chenye/work/CharmJet/VHCC/RootFile_VHcc/Resolved_3305_PFlow/Hist/" 
#ROOTFILE_DIR  = "/home/chenye/work/CharmJet/VHCC/RootFile_VHcc/Resolved_3215_EMTopo/Hist/" 
####################################################
#  #################################################
#  #
#  #        User defined drawing function
#  #
#  #################################################
####################################################

##########################################################
#######     Fill Hist Kinematics & Yields      
##########################################################

            HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
            HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", TarDir+Hist2D_name,"COLZTEXT", SetLogZ_2D)

def FillHist_Kinematic_FatJet(SampleType, SampleTypeName, process_info, Physics_info, Hist2D_phy_vs_tag, InputRootFiles):
    '''
    Fill 1D & 2D Hists 
    2D Hist : Jet pTV vs. ntag 
    1D Hist : Physics variabloe Distribution (like mBB or pT)  of different pTV&ntag 

    Example : 
        SampleType     = "Z+C" 
        SampleTypeName = "ZC" 
        process_info   =  ZC_info
        Hist2D_phy_vs_tag     : 2D Jet Yeild Hist 
    '''
    if ZeroTagOnly :
        tag_info = [ZeroTagInfo]
    else:
        tag_info = ["0tag", "1tag", "2tag", "3ptag"]


    if ptv_cate_merge:
        jet_info = [ "1pfat0pjet"]
        ptv_info = ["0_250ptv","250_400ptv","400ptv"]
    else:
        jet_info = [ "2jet","3pjet"]
        ptv_info = ["75_150ptv","150_250ptv","250ptv"]
    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]

    #SR_info="SR_2psubjet"  
    SR_info="SR"  

    for _phy in Physics_info :
        RootFile = InputRootFiles[SampleType]         

        for _process in process_info:
            ###  2D Hists ###
            if SwitchBBToCC :
                #_phytmp = _phy.replace("BB","CC")
                _phytmp = _phy.replace("B","C")
            else :
                _phytmp = _phy
            
            Hist2D_name = SampleTypeName+"_"+_process+"_"+_phytmp+"2D"
            Hist2D_title = SampleType+" : "+_process+" "+_phytmp+" Yields distribution"
            Hist2D = ROOT.TH2D(Hist2D_name, Hist2D_title,3,0,3,4,0,4)
            Hist2D.GetXaxis().SetBinLabel(1,"LPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(2,"MPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(3,"HPtV 1pfat+0jet")

            Hist2D.GetYaxis().SetBinLabel(1,"0 tag")
            Hist2D.GetYaxis().SetBinLabel(2,"1 tag")
            Hist2D.GetYaxis().SetBinLabel(3,"2 tag")
            Hist2D.GetYaxis().SetBinLabel(4,"3+ tag")
            Hist2D.GetZaxis().SetTitle("Number of evnets")

            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                drawHists = []
                legends   = []
                outputFileName = SampleTypeName+"_"+_process + "_" + _jet + "_"+ SR_info +"_"+_phytmp 
                #title = SampleType +" : " + _process + " " + _jet + " "+ SR_info 
                if "ZllHcc" in SampleType: 
                    title =  _process + " " + _jet 
                else :
                    title = SampleType +" : " + _process + " " + _jet 
                
                if SwitchBBToCC :
                    title = title.replace("B","C")
         
                for _tag in tag_info :
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue
         
                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        ####_legendName= _process + " " + _tag+_jet + " " + _ptv 
                        _legendName=_tag + " " + ptvInfo_convert(_ptv)
                        
                        #skip blank hist 
                        print "------------------" 
                        tmpHist= RootFile.Get(_histName)
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 

                        copyHist=tmpHist.Clone("%s_copy"%_histName)
                        drawHists += [copyHist]
                        legends += [_legendName]
                        ## Fill 2D Hist 
                        FillValue_2D = Hist2D_filllValue_njet_vs_ntag(_histName , copyHist)
                        Hist2D.Fill(FillValue_2D[0], FillValue_2D[1], FillValue_2D[2])
                        FillValue_2D_phy_vs_ntag = Hist2D_filllValue_phy_vs_ntag(_histName, copyHist)
                        Hist2D_phy_vs_tag[_phy].Fill(FillValue_2D_phy_vs_ntag[0], FillValue_2D_phy_vs_ntag[1], FillValue_2D_phy_vs_ntag[2])
               
                ## Draw 1D Hist 
                HistPlot = HistMaker1D(drawHists,legends)
                if SwitchBBToCC :
                    _phytmp = _phy.replace("B","C")
                else :
                     _phytmp = _phy

                if ('pT' in _phy)  or ('Pt' in _phy) :
                    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
                    HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", legends, TarDir+outputFileName,[50,1500], SetLogY_1D)
                elif 'mBB' in _phy:
                    HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", legends, TarDir+outputFileName,[30,800], SetLogY_1D)
                else: 
                    HistPlot.DrawPlots_ATLAS(title, _phytmp,"Number of events", legends, TarDir+outputFileName,[30,800], SetLogY_1D)

            ## Draw 2D Hist tag vs jet 
            HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
            HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", TarDir+Hist2D_name,"COLZTEXT", SetLogZ_2D)

    h_cutflow_vhbb= RootFile.Get("CutFlow/PreselectionCutFlow")
    HistPlot_cutflow_vhbb = HistMaker1D([h_cutflow_vhbb],["Preselection cut flow"])
    HistPlot_cutflow_vhbb.DrawPlots_ATLAS(SampleTypeName+"Preselection cut flow", "","Number of events", ["Preselection cut flow"], TarDir+"CutFlow_Preselcetion"+"_"+SampleTypeName,[0,25], SetLogY_1D)

    h_cutflow_my= RootFile.Get("CutFlow/Nominal/CutsResolved")
    HistPlot_cutflow_my = HistMaker1D([h_cutflow_my],["Resolved cut flow"])
    HistPlot_cutflow_my.DrawPlots_ATLAS(SampleTypeName+"Resolved cut flow", "","Number of events", [ "Resolved cut flow"], TarDir+"CutFlow_Resolved"+"_"+SampleTypeName,[0,25], SetLogY_1D)
##########################################################
#######     Fill Hist Phy  
##########################################################

##########################################################
#######     Fill Hist Yield
##########################################################


##########################################################
#######    Draw Hist : phy_vs_ntag 2D Plot     
##########################################################

def HistDraw_Plot2D_phy_vs_ntag(Physics_info, Hist2D_phy_vs_tag, Hist2D_phy_vs_tag_title, Hist2D_phy_vs_tag_name):
    '''
    Physics_info            : physics Info
    Hist2D_phy_vs_tag       : plot Hist 
    Hist2D_phy_vs_tag_title : plot title  
    Hist2D_phy_vs_tag_name  : output file name 

    '''
    HistPlot2D_phy_vs_ntag = {}
    for _phy in Physics_info :
        HistPlot2D_phy_vs_ntag[_phy] = HistMaker2D(Hist2D_phy_vs_tag[_phy], Hist2D_phy_vs_tag_title[_phy])
        HistPlot2D_phy_vs_ntag[_phy].DrawPlots_ATLAS(Hist2D_phy_vs_tag_title[_phy], "c-tag","", TarDir+Hist2D_phy_vs_tag_name[_phy],"COLZTEXT", SetLogZ_2D)
###########################################################
#######     Fill CutFlow
##########################################################


##########################################################################
# ########################################################################
# #
# #     main function
# #
# ########################################################################
##########################################################################

def main():
    atlas_style()

    #######################################################
    #   VHCC
    #######################################################

    #SetLogY_1D = True
    #SetLogZ_2D = True 
    #-----------------------
    # info
    #-----------------------

    # rootfile_dir  = "/home/chenye/work/CharmJet/VHCC/Result_ade/Merged_2lcc/Result_ade/combin/"
    #rootfile_dir = "/home/chenye/work/CharmJet/VHCC/SimpleROOT_tree_and_hist/Merged/comb/"
#    rootfile_dir = "/home/chenye/work/CharmJet/VHCC/SimpleROOT_FlavorLabel/Merged/comb/" # with flavor labal for VHcc2Lep
#    rootfile_dir = "/home/chenye/work/CharmJet/VHCC/RootFile_VHbb/Merged/" # with flavor labal for VHbbcc2Lep
#    rootfile_dir = "/home/chenye/work/CharmJet/VHCC/RootFile_VHcc/Merged/" # with flavor labal for VHbbcc2Lep
#    rootfile_dir = "/home/chenye/work/CharmJet/VHCC/RootFile_VHcc/Merged_weight1/" # with flavor labal for VHbbcc2Lep
#    rootfile_dir = "/home/chenye/work/CharmJet/VHCC/RootFile_VHbb/Merged/" # with flavor labal for VHbbcc2Lep
    rootfile_dir = ROOTFILE_DIR#"/home/chenye/work/CharmJet/VHCC/RootFile_VHbb/Merged/" # with flavor labal for VHbbcc2Lep
    
    rootfile_name = [
        "hist-ggZllHbb_PwPy8.root",
        "hist-ggZllHcc_PwPy8.root",
        "hist-qqZllHbbJ_PwPy8MINLO.root",
        "hist-qqZllHccJ_PwPy8MINLO.root",
        #"hist-ggZllZqq_Sh222.root",
        "hist-ggZqqZll_Sh222.root",
        "hist-ZccZll_Sh221.root",
        "hist-ZqqZll_Sh221.root",
        "hist-ZbbZll_Sh221.root"    
        "hist-ZC_Sh221.root",
        "hist-ZQ_Sh221.root",
        "hist-ZB_Sh221.root",
        "hist-ZL_Sh221.root",
        "hist-ZZ_Sh221.root",
        "hist-stopWt_PwPy8.root",
        "hist-ttbar_dilep_PwPy8.root",
        "hist-stops_PwPy8.root",
        "hist-stopt_PwPy8.root",
        "hist-WqqZll_Sh221.root",
        "hist-data.root"
    ]
        #"hist-stopWt_dilep_PwPy8.root",
    Physics_info = [
        "mBBTT", "mBB","pTV", "dRVH",
        "pTBB","dRBB","dEtaBB","dPhiBB",
        "pTB1","pTB2","EtaB1","EtaB2",
        "PtFatJ1","EtaFatJ1","NtrkjetsFatJ1"
    ]

    ##SR_info="topemucr_noaddbjetsr"  



    #---------------------------------------------
    # Get File 
    #

    InputRootFiles={}
    
    for filename in rootfile_name :
        file_name = rootfile_dir + filename
        print file_name
        myFile = TFile(file_name)
        if "ggZllHcc" in filename:
            InputRootFiles["ggZllHcc"] = myFile
        
    #---------------------------------------------
    # Hist name & setting
    #

    Hist2D_phy_vs_tag_name  = {} 
    Hist2D_phy_vs_tag_title = {}
    Hist2D_phy_vs_tag       = {}
    for _phy in Physics_info :
        
        if SwitchBBToCC :
            ##_phytmp = _phy.replace("BB","CC")
            _phytmp = _phy.replace("B","C")
        else :
            _phytmp = _phy

    
    #SampleType_info = ["ZQ","ZZ","qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","ggZllZqq"]
#    SampleType_info = ["qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","ggZllZqq","ZZ","ZQ"]
    if DataOnPlot :
        SampleType_info = ["qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","stops","stopt","stopWt","ggZllZqq","ttbar","WZ","ZZ","ZQ","data"]
    else:
        SampleType_info = ["qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","stops","stopt","stopWt","ggZllZqq","ttbar","WZ","ZZ","ZQ"]
#    SampleType_info = ["qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","stops","stopt","stopWt","ggZllZqq","ttbar","WZ","ZZ","ZQ"]

    Sample_process_info = {}
# "XXbtau","XXctau","XXltau","XXtautau"
    if USETau : 
        Sample_process_info["ZQ"] = ["Zcc", "Zbb", "Zll","Zcl","Zbl","Zbc","Zbtau","Zctau","Zltau","Ztautau","Zc","Zb","Zl","Z"] 
        Sample_process_info["ZZ"] = ["ZZcc", "ZZbc", "ZZcl","ZZbl","ZZbb","ZZll","ZZbtau","ZZctau","ZZltau","ZZtautau","ZZbkg"]
        Sample_process_info["WZ"] = ["WZcc", "WZbc", "WZcl","WZbl","WZbb","WZll","WZbtau","WZctau","WZltau","WZtautau","WZbkg"]
        Sample_process_info["ttbar"] = ["ttbarcc", "ttbarbc", "ttbarcl","ttbarbl","ttbarbb","ttbarll","ttbarbtau","ttbarctau","ttbarltau","ttbartautau","ttbar"]
        Sample_process_info["stopWt"] = ["stopWtcc", "stopWtbc", "stopWtcl","stopWtbl","stopWtbb","stopWtll","stopWtbtau","stopWtctau","stopWtltau","stopWttautau","stopWt"]
        Sample_process_info["stopt"] = ["stoptcc", "stoptbc", "stoptcl","stoptbl","stoptbb","stoptll","stoptbtau","stoptctau","stoptltau","stopttautau","stopt"]
        Sample_process_info["stops"] = ["stopscc", "stopsbc", "stopscl","stopsbl","stopsbb","stopsll","stopsbtau","stopsctau","stopsltau","stopstautau","stops"]
        Sample_process_info["qqZllHcc"] = ["qqZllH125cccc","qqZllH125cccl","qqZllH125ccbc","qqZllH125ccbl","qqZllH125ccll","qqZllH125ccbb","qqZllH125ccbtau","qqZllH125ccctau","qqZllH125ccltau","qqZllH125cctautau","qqZllH125cc"]
        Sample_process_info["ggZllHcc"] = ["ggZllH125cccc","ggZllH125cccl","ggZllH125ccbc","ggZllH125ccbl","ggZllH125ccll", "ggZllH125ccbb","ggZllH125ccbtau","ggZllH125ccctau","ggZllH125ccltau","ggZllH125cctautau","ggZllH125cc"]
        Sample_process_info["qqZllHbb"] = ["qqZllH125bb","qqZllH125bc","qqZllH125bl","qqZllH125cl","qqZllH125cc","qqZllH125ll","qqZllH125ltau","qqZllH125ctau","qqZllH125btau","qqZllH125tautau","qqZllH125"]
        Sample_process_info["ggZllHbb"] = ["ggZllH125bb","ggZllH125bc","ggZllH125bl","ggZllH125cl","ggZllH125cc","ggZllH125ll","ggZllH125ltau","ggZllH125ctau","ggZllH125btau","ggZllH125tautau","ggZllH125"]
        Sample_process_info["ggZllZqq"] = ["ggZZcc","ggZZbc","ggZZcl","ggZZbb","ggZZbl","ggZZll","ggZZbtau","ggZZctau","ggZZltau","ggZZtautau","ggZZ"]
    else:
        Sample_process_info["ZQ"] = ["Zcc", "Zbb", "Zll","Zcl","Zbl","Zbc","Zc","Zb","Zl","Z"]
        Sample_process_info["ZZ"] = ["ZZcc", "ZZbc", "ZZcl","ZZbl","ZZbb","ZZll","ZZbkg"]
        Sample_process_info["WZ"] = ["WZcc", "WZbc", "WZcl","WZbl","WZbb","WZll","WZbkg"]
        #Sample_process_info["WZ"] = ["WZhadlepcc", "WZhadlepbc", "WZhadlepcl","WZhadlepbl","WZhadlepbb","WZhadlepll","WZhadlepbkg"]
        Sample_process_info["ttbar"] = ["ttbarcc", "ttbarbc", "ttbarcl","ttbarbl","ttbarbb","ttbarll","ttbar"]
        Sample_process_info["stopWt"] = ["stopWtcc", "stopWtbc", "stopWtcl","stopWtbl","stopWtbb","stopWtll","stopWt"]
        Sample_process_info["stopt"] = ["stoptcc", "stoptbc", "stoptcl","stoptbl","stoptbb","stoptll","stopt"]
        Sample_process_info["stops"] = ["stopscc", "stopsbc", "stopscl","stopsbl","stopsbb","stopsll","stops"]
        Sample_process_info["qqZllHcc"] = ["qqZllH125cc"]
        Sample_process_info["ggZllHcc"] = ["ggZllH125cc"]
        Sample_process_info["qqZllHbb"] = ["qqZllH125"]
        Sample_process_info["ggZllHbb"] = ["ggZllH125"]
        Sample_process_info["ggZllZqq"] = ["ggZZcc","ggZZbc","ggZZcl","ggZZbb","ggZZbl","ggZZll"]

    Sample_process_info["data"] = ["data"]
    CTagEffTight={}
    CTagEffLoose={}
    '''
    CTagEffLoose["c"] = 0.41
    CTagEffLoose["b"] = 0.25
    CTagEffLoose["l"] = 0.05
    CTagEffLoose["tau"] = 0.41
    CTagEffLoose["WP"] = "Loose"

    CTagEffTight["c"] = 0.18
    CTagEffTight["b"] = 0.05
    CTagEffTight["l"] = 0.005
    CTagEffTight["tau"] = 0.18
    CTagEffTight["WP"] = "Tight"
    '''
    CTagEffLoose["c"] = 0.4
    CTagEffLoose["b"] = 0.2
    CTagEffLoose["l"] = 0.04
    CTagEffLoose["tau"] = 0.4
    CTagEffLoose["WP"] = "Loose"

    CTagEffTight["c"] = 0.17
    CTagEffTight["b"] = 0.05
    CTagEffTight["l"] = 0.0025
    CTagEffTight["tau"] = 0.17
    CTagEffTight["WP"] = "Tight"

    #---------------------------
    # Fill HIst Distribution 
    #---------------------------
    print "SAMPLETYPEINFO"
    print SampleType_info
    #physics_var="mJ"
    physics_var="mBB"

   # FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, physics_var, InputRootFiles, CTagEffLoose)
   # FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, physics_var, InputRootFiles, CTagEffTight)
    FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, physics_var, InputRootFiles,{})
    
    #---------------------------
    # Fill Yield Distribution 
    #---------------------------

    Hist_SB_Loose = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, physics_var, InputRootFiles, CTagEffLoose)
    Hist_SB_Tight = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, physics_var, InputRootFiles, CTagEffTight)
    Hist_SB_NoTag = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, physics_var, InputRootFiles)
    #Hist_SB_Loose = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mJ", InputRootFiles, CTagEffLoose)
    #Hist_SB_Tight = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mJ", InputRootFiles, CTagEffTight)
    #Hist_SB_NoTag = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mJ", InputRootFiles)
    #Hist_SB_Loose = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mBB", InputRootFiles, CTagEffLoose)
    #Hist_SB_Tight = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mBB", InputRootFiles, CTagEffTight)
    #Hist_SB_NoTag = FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mBB", InputRootFiles)
    
    #---------------------------
    # Fill CutFlow 
    #---------------------------
    FillHist_CutFlow( "ZZ",        "ZZ",         InputRootFiles)
    FillHist_CutFlow( "ZQ",        "Z+Jets",         InputRootFiles)
    FillHist_CutFlow( "qqZllHcc",  "qqZllHcc",   InputRootFiles)
    FillHist_CutFlow( "ggZllHcc",  "ggZllHcc",   InputRootFiles)
    FillHist_CutFlow( "qqZllHbb",  "qqZllHbb",   InputRootFiles)
    FillHist_CutFlow( "ggZllHbb",  "ggZllHbb",   InputRootFiles)

    #---------------------------
    # Fill S/B  Distribution 
    #---------------------------
    Hist1D_SB_sqrt = []
    Hist1D_SB_title = []
    
    Hist1D_SB_sqrt       = [Hist_SB_NoTag[4]["All"]]
    Hist1D_SB_title_sqrt = [Hist_SB_NoTag[1]["All"]]
    HistPlot1D = HistMaker1D(Hist1D_SB_sqrt,Hist1D_SB_title_sqrt)
    HistPlot1D.setLocation_ATLASTitle(0.2,0.8,0.48,0.88)
    #HistPlot1D.setLocation_Legend_plot(0.2,0.6,0.48,0.77)
    HistPlot1D.setLocation_Legend_plot(0.62,0.7,0.93,0.9)
    HistPlot1D.setColorType("NORM")
    HistPlot1D.DrawPlots_ATLAS("S/#sqrt{B} distribution: all region", "","S/#sqrt{B}", Hist1D_SB_title_sqrt, TarDir+"SB_sqrt_all",[0,3],"All", False)

    Hist1D_SB_sqrt       = [Hist_SB_NoTag[4]["Sig"]] 
    Hist1D_SB_title_sqrt = [Hist_SB_NoTag[1]["Sig"] ]
    HistPlot1D = HistMaker1D(Hist1D_SB_sqrt,Hist1D_SB_title_sqrt)
    HistPlot1D.setLocation_ATLASTitle(0.2,0.8,0.48,0.88)
    #HistPlot1D.setLocation_Legend_plot(0.2,0.6,0.48,0.77)
    HistPlot1D.setLocation_Legend_plot(0.62,0.7,0.93,0.9)
    HistPlot1D.setColorType("NORM")
    HistPlot1D.DrawPlots_ATLAS("S/#sqrt{B} distribution: signal region", "","S/#sqrt{B}", Hist1D_SB_title_sqrt, TarDir+"SB_sqrt_sig",[0,3],"All", False)

    #---------------------------

    Hist1D_SB = []
    Hist1D_SB_title = []
    
    #Hist1D_SB       = [Hist_SB_NoTag[0]["All"] , Hist_SB_Loose[0]["All"] , Hist_SB_Tight[0]["All"]]
    #Hist1D_SB_title = [Hist_SB_NoTag[1]["All"] , Hist_SB_Loose[1]["All"] , Hist_SB_Tight[1]["All"]]
    Hist1D_SB       = [Hist_SB_NoTag[0]["All"]]
    Hist1D_SB_title = [Hist_SB_NoTag[1]["All"]]
    HistPlot1D = HistMaker1D(Hist1D_SB,Hist1D_SB_title)
    HistPlot1D.setLocation_ATLASTitle(0.2,0.8,0.48,0.88)
    HistPlot1D.setLocation_Legend_plot(0.2,0.6,0.48,0.77)
    HistPlot1D.setColorType("NORM")
    HistPlot1D.DrawPlots_ATLAS("S/B distribution: all region", "","S/B", Hist1D_SB_title, TarDir+"SB_all",[0,4],"All", False)

    Hist1D_SB       =   [Hist_SB_NoTag[0]["Sig"]]
    Hist1D_SB_title =   [ Hist_SB_NoTag[1]["Sig"]]
    HistPlot1D = HistMaker1D(Hist1D_SB,Hist1D_SB_title)
    HistPlot1D.setLocation_ATLASTitle(0.2,0.8,0.48,0.88)
    HistPlot1D.setLocation_Legend_plot(0.2,0.6,0.48,0.77)
    HistPlot1D.setColorType("NORM")
    HistPlot1D.DrawPlots_ATLAS("S/B distribution: signal region", "","S/B", Hist1D_SB_title, TarDir+"SB_sig",[0,4],"All", False)
    #---------------------------
    # Fill Eff cut   Distribution 
    #---------------------------
    
 #    sampleListName=["stops" ,"stopt","stopWt","ttbar","Z+jet","WZ","ZZ","ggZZ","qqZHbb","ggZHbb","qqZHcc","ggZHcc"]
 #    sampleListNum =[       0,      1,       2,       3,      4,      5,   6,   7,     8,       9,      10,      11]
    SampleListName=["stops" ,"stopt","stopWt", "ttbar","Z+jet",  "WZ", "ZZ", "ggZZ","qqZHbb","ggZHbb","qqZHcc","ggZHcc"]
    SampleListNum =[       0,      1,       2,       3,      4,     5,    6,      7,       8,       9,      10,      11]
    SampleMap={} 
    for _num in SampleListNum :
        SampleMap[_num]=SampleListName[_num]

    #SampleType_info=["ggZllHcc", "qqZllHcc","ggZllHbb","qqZllHbb","stops","stopt","stopWt","ggZllZqq","ttbar","WZ","ZZ","ZQ"]
    UsingNumList =   [        11,        10,         9 ,         8,     0 ,     1 ,      2 ,        7,     3 ,    5,   6,   4    ]
    Region_mBB = ["All","Sig"]
    Hists_Merge_Cut={}
    Hists_Merge_Cut_sig={}
    Hists_Merge_Cut_bkg={}
    #Hists_Merge_Cut_legend=Hist_SB_NoTag[3]
    Hists_Merge_Cut_legend=[]
    Hists_Merge_Cut_legend_sig=[]
    Hists_Merge_Cut_legend_bkg=[]
    for _process in UsingNumList:
        Hists_Merge_Cut_legend+=[SampleMap[_process]]
        if _process > 7: 
            Hists_Merge_Cut_legend_sig+=[SampleMap[_process]]
        else :
            Hists_Merge_Cut_legend_bkg+=[SampleMap[_process]]


    for _region in Region_mBB :
        Hists_Merge_Cut[_region]={}
        Hists_Merge_Cut_sig[_region]={}
        Hists_Merge_Cut_bkg[_region]={}
        for _ptv in range(1,5) :
            Hists_Merge_Cut[_region][_ptv]=[]
            Hists_Merge_Cut_sig[_region][_ptv]=[]
            Hists_Merge_Cut_bkg[_region][_ptv]=[]
            #for _process in range(0,len(Hist_SB_Loose[2][_region][_ptv])):
            for _process in UsingNumList:
                Hists_Loose = Hist_SB_Loose[2][_region][_ptv][_process]
                Hists_Tight = Hist_SB_Tight[2][_region][_ptv][_process]
                Hists_NoTag = Hist_SB_NoTag[2][_region][_ptv][_process]
                #Hists_Merge_tmp = MergeHists([Hists_Loose, Hists_Tight, Hists_NoTag],Hists_Merge_Cut_legend[_process])
                Hists_Merge_tmp = MergeHists([Hists_Loose, Hists_Tight, Hists_NoTag],_region+str(_ptv)+str(_process)+SampleMap[_process])
                Hists_Merge_Cut[_region][_ptv]+=[Hists_Merge_tmp.Clone()]
                if _process > 7: 
                    Hists_Merge_Cut_sig[_region][_ptv]+=[Hists_Merge_tmp.Clone()]
                else :
                    Hists_Merge_Cut_bkg[_region][_ptv]+=[Hists_Merge_tmp.Clone()]

            HistPlot_Cut = HistMaker1D(Hists_Merge_Cut[_region][_ptv],Hists_Merge_Cut_legend)
            HistPlot_Cut.setHistStackBkg(Hists_Merge_Cut_bkg[_region][_ptv],Hists_Merge_Cut_legend_bkg)
            HistPlot_Cut.setHistStackSig(Hists_Merge_Cut_sig[_region][_ptv],Hists_Merge_Cut_legend_sig)
            #HistPlot_Cut.setSignalScale(SignalScale)
            print "LENGTH Hists_Merge_Cut %d"%len(Hists_Merge_Cut[_region][_ptv])
            print "LENGTH Hists_Merge_Cut_legend %d"%len(Hists_Merge_Cut_legend)
            print Hists_Merge_Cut_legend
            if _region == "Sig":
                title = "Signal region" 
            else :
                title = "All region" 

            fileName  = _region+"Window" 
            if ptv_cate_merge:
                if _ptv == 1 :
                    title    +=": ZpT 0-250 GeV"
                    fileName += "_LowPTV" 
                elif _ptv == 2 :
                    title+=": ZpT 250-400 GeV"
                    fileName += "_MediumPTV" 
                elif _ptv == 3 :
                    title+=": ZpT >400 GeV"
                    fileName += "_HighPTV" 
                elif _ptv == 4 :
                    title+=" "
                    fileName += "_AllPTV" 
            else:
                if _ptv == 1 :
                    title    +=": ZpT 75-150 GeV"
                    fileName += "_LowPTV" 
                elif _ptv == 2 :
                    title+=": ZpT 150-250 GeV"
                    fileName += "_MediumPTV" 
                elif _ptv == 3 :
                    title+=": ZpT >250 GeV"
                    fileName += "_HighPTV" 
                elif _ptv == 4 :
                    title+=" "
                    fileName += "_AllPTV" 

            
            HistPlot_Cut.setColorType("AUTO")
            HistPlot1D.setLocation_Legend_plot(0.7,0.5,0.9,0.77)
            HistPlot_Cut.DrawStacks_ATLAS(title, "","Number of events",TarDir+"stack_Cut_Merge_"+fileName,[0,5] ,SetLogY_1D)

if __name__ == "__main__":
    main()
