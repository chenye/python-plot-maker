#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D
from ClassDef import MergeHists 
####################################################
#  #################################################
#  #
#  #        User defined variable 
#  #
#  #################################################
####################################################
SetLogZ_2D = True
#CalStatus="NoCal"
CalOption="Cal"
CalOption="NoCal"
#CutOption=""
CutOption="_Mass70_160"
#CutOption="_dRBBCut"
CalStatus=CalOption+CutOption
TarDir  = "DL1r_VHcc_DL1rcRankL_"+CalStatus+"/" # with flavor labal for VHbbcc2Lep VHcc 
InputFile = "/data/chenye/VHcc/Ntupple/code/result/hist_VHcc1_newDL1r_"+CalStatus+".root"
InputFile = "/data/chenye/VHcc/Ntupple/code/result/hist_VHcc_oldFlavOldTag_DL1rcRank_newDL1r_"+CalStatus+".root"
InputFile = "/data/chenye/VHcc/Ntupple/code/hist_VHcc1_newDL1r_"+CalStatus+".root"
InputFile = "/data/chenye/VHcc/Ntupple/code/hist_all1_newDL1r_"+CalStatus+".root"
#InputFile = "/data/chenye/VHcc/Ntupple/code/result/hist_sig_newDL1r_Cal.root"

####################################################
#  #################################################
#  #
#  #        User defined drawing function
#  #
#  #################################################
####################################################
def getTitle(start, sample, SRregion, ptv ):
    title=start
    if sample == "sig": 
        title += "Signal"
    elif sample == "bkg":
        title += "Background"
    else : 
        title += sample
    if sample!="": 
        title += " "
    title += ptv+" "

    if SRregion == "SR1":
        title += "2Jets "
    elif SRregion == "SR2":
        title += "#geq3Jets "
    elif SRregion == "SR":
        title += "#geq2Jets "
    
    return title


def main():
    #ctagFlav2D
    Sample_info = ["VHcc", "VHbb","ZJets","WJets","ZZ","WZ","stop","ttbar","sig","bkg"]
    Region_Info=["SR1","SR2","SR"]
    PTV_Info=["LowPTV","MedHighPTV"];
    
    print "================== Generate Ctag-Flavor 2D Hist  ==================" 
    myFile = TFile(InputFile)
    for _sample in Sample_info :
        for _region in Region_Info :
            for _ptv in PTV_Info :
                _histName = "hist2d_"+ _sample +"_"+ _region+"_"+_ptv+"_ctagFlav2D"
                print "------------------" 
                Hist2D= myFile.Get(_histName)
                if  type(Hist2D) == ROOT.TObject : 
                    print "Skip Hist  : %s"%_histName
                    continue 
                else :
                    print "Using Hist : %s"%_histName
                print "------------------" 
                Hist2D.GetZaxis().SetTitle("Number of events")
                Hist2D.GetZaxis().SetTitleSize(0.04)
                Hist2D.GetXaxis().SetTitleSize(0.04)
                Hist2D.GetYaxis().SetTitleSize(0.04)
                Hist2D.GetXaxis().SetTitleOffset(1)
                Hist2D.GetYaxis().SetTitleOffset(1)
                Hist2D.SetMarkerSize(2.0)

                Hist2D_title = getTitle("VHcc2Lep Resolved ",_sample, _region, _ptv)+" (new c-tag)"
                HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
                Hist2D_name = _histName
                HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "C-tagging label","Jet flavor", TarDir+Hist2D_name,"COLZTEXT", SetLogZ_2D)
                del HistPlot2D
                del Hist2D

    print "================== Generate Ctag-Yield 2D Hist  ==================" 
    for _region in Region_Info :
        for _ptv in PTV_Info :
                _histname = "hist2d_"+ _region+"_"+_ptv+"_ctagYield2D"
                print "------------------" 
                hist2d= myFile.Get(_histname)
                if  type(hist2d) == ROOT.TObject : 
                    print "skip hist  : %s"%_histname
                    continue 
                else :
                    print "using hist : %s"%_histname
                print "------------------" 
                hist2d.GetZaxis().SetTitle("number of events")
                hist2d.GetXaxis().SetTitleSize(0.04)
                hist2d.GetZaxis().SetTitleSize(0.04)
                hist2d.GetYaxis().SetTitleSize(0.04)
                hist2d.GetZaxis().SetTitleOffset(1)
                hist2d.GetXaxis().SetTitleOffset(1)
                hist2d.SetMarkerSize(1.6)
                
                hist2d_title = getTitle("VHcc2Lep Resolved","", _region, _ptv)+" (new c-tag)"
                histplot2d = HistMaker2D(hist2d, hist2d_title)
                hist2d_name = _histname
                histplot2d.DrawPlots_ATLAS(hist2d_title, "c-tagging label","", TarDir+hist2d_name,"colztext", SetLogZ_2D)
                ## S/B Claculatiuon 
                with open("output/SBInfo"+CalStatus+".txt","w") as f1:
                    f1.write("Tagging\t S/sqrt(B)\t Data/MC\t VHcc/VHbb\n") 
                    s_all=0
                    b_all=0
                    data_all=0
                    VHbb_all=0
                    for i in range(1,hist2d.GetNbinsX()+1):
                        tagginglabel = hist2d.GetXaxis().GetBinLabel(i)
                        s_n = hist2d.GetBinContent(i,4)
                        b_n = hist2d.GetBinContent(i,2)
                        data_n = hist2d.GetBinContent(i,1)
                        VHbb_n = hist2d.GetBinContent(i,3)
                        f1.write("%s\t %.3f\t %.3f\t %.3f\n"%(tagginglabel,s_n/math.sqrt(b_n),data_n/b_n,s_n/VHbb_n)) 
                        if "T" in tagginglabel:
                            s_all+=s_n
                            b_all+=b_n
                            data_all+=data_n
                            VHbb_all+=VHbb_n
                    
                    f1.write("%s\t %.3f\t %.3f\t %.3f\n"%("TT+TN+TT",s_all/math.sqrt(b_all),data_all/b_all,s_all/VHbb_all)) 

                del histplot2d
                del hist2d

    Flav_Info=["cc","cl"]
    Ctag_Info=["TT","TN","TL"]
    Region_Info=["SR1","SR2","SR"]
    print "================== Generate 3rd Jet Ctag 2D Hist  ==================" 
    for _region in Region_Info :
        for _ptv in PTV_Info :
            for _flav in Flav_Info :
                for _ctag in Ctag_Info :
                    # 3rd Jet 
                    _histname = "hist2d_sig_3rdJ_"+_flav+_ctag+"_"+ _region+"_"+_ptv+"_ctag2D"
                    #_histname = "hist2d_3rdJ_cl"+_ctag+"_"+ _region+"_"+_ptv+"_ctag2D"
                    print "------------------" 
                    hist2d= myFile.Get(_histname)
                    if  type(hist2d) == ROOT.TObject : 
                        print "skip hist  : %s"%_histname
                        continue 
                    else :
                        print "using hist : %s"%_histname
                    print "------------------" 
                    hist2d.GetZaxis().SetTitle("number of events")
                    hist2d.GetZaxis().SetTitleSize(0.04)
                    hist2d.GetXaxis().SetTitleSize(0.04)
                    hist2d.GetYaxis().SetTitleSize(0.04)
                    hist2d.GetXaxis().SetTitleOffset(1)
                    hist2d.GetYaxis().SetTitleOffset(1)
                    hist2d.SetMarkerSize(3.0)
                    J3Status="C-tagging & Flavor for J3, leading 2Jets: "+ _flav +" " +_ctag  
                    hist2d_title = getTitle("VHcc2Lep Resolved","", _region, _ptv)+" (new c-tag)" +";"+J3Status;
                    histplot2d = HistMaker2D(hist2d, hist2d_title)
                    hist2d_name = _histname
                    histplot2d.DrawPlots_ATLAS(hist2d_title, "c-tagging label","Jet flavor", TarDir+hist2d_name,"colztext", SetLogZ_2D)
                    del histplot2d
                    del hist2d
    print "================== Generate FlavFlav 2D Hist  ==================" 
    for _region in Region_Info :
        for _ptv in PTV_Info :
                    # 3rd Jet 
                    _histname = "hist2d_sig_"+ _region+"_"+_ptv+"_FlavFlav2D"
                    #_histname = "hist2d_3rdJ_cl"+_ctag+"_"+ _region+"_"+_ptv+"_ctag2D"
                    print "------------------" 
                    hist2d= myFile.Get(_histname)
                    if  type(hist2d) == ROOT.TObject : 
                        print "skip hist  : %s"%_histname
                        continue 
                    else :
                        print "using hist : %s"%_histname
                    print "------------------" 
                    hist2d.GetZaxis().SetTitle("number of events")
                    hist2d.GetZaxis().SetTitleSize(0.04)
                    #ZlabelSize = hist2d.GetLabelSize()
                    hist2d.SetMarkerSize(2.0)
                    hist2d.GetXaxis().SetTitleSize(0.04)
                    hist2d.GetYaxis().SetTitleSize(0.04)
                    hist2d.GetXaxis().SetTitleOffset(1)
                    hist2d.GetYaxis().SetTitleOffset(1)
                    SubTitle="Leading 2Jet flavor with different ranking method " 
                    hist2d_title = getTitle("VHcc2Lep Resolved","", _region, _ptv)+" (new c-tag)" +";"+SubTitle;
                    histplot2d = HistMaker2D(hist2d, hist2d_title)
                    hist2d_name = _histname
                    histplot2d.DrawPlots_ATLAS(hist2d_title, "leading 2 jet flavor (ranked by Jet pT)","leading 2 jet flavor (ranked by DL1r(c))", TarDir+hist2d_name,"colztext", SetLogZ_2D)
                    del histplot2d
                    del hist2d


    print "================== Generate mBB nJ dRBB Hist  ==================" 
    for _region in Region_Info :
        for _ptv in PTV_Info :
            hist1ds_mBB=[]
            hist1ds_dRBB=[]
            hist1ds_name_mBB=[]
            hist1ds_name_dRBB=[]
            hist1ds_nJ=[]
            hist1ds_name_nJ=[]
            for _flav in Flav_Info :
                for _ctag in Ctag_Info :
     
                    # mBB & dRBB 
                    # hist_VHcc_SR_mBB_clNN_MedHighPTV
                    print "--------mBB----------" 
                    _histname_mBB = "hist_VHcc_"+_region+"_mBB_"+_flav+_ctag+"_"+_ptv
                    hist1d= myFile.Get(_histname_mBB)
                    if  type(hist1d) == ROOT.TObject : 
                        print "skip hist  : %s"%_histname_mBB
                        continue 
                    else :
                        print "using hist : %s"%_histname_mBB
                    print "------------------" 
                    hist1d.Scale(1.0/hist1d.GetSumOfWeights())
                    hist1d.Rebin(5)
                    J3Status="leading pT 2Jets: "+_flav+"  " +_ctag  
                    hist1ds_name_mBB +=[J3Status]
                    hist1ds_mBB +=[hist1d]
                    #  dRBB 
                    # hist_VHcc_SR_mBB_clNN_MedHighPTV
                    _histname_dRBB = "hist_VHcc_"+_region+"_dRBB_"+_flav+_ctag+"_"+_ptv
                    print "---------dRBB---------" 
                    hist1d= myFile.Get(_histname_dRBB)
                    if  type(hist1d) == ROOT.TObject : 
                        print "skip hist  : %s"%_histname_dRBB
                        continue 
                    else :
                        print "using hist : %s"%_histname_dRBB
                    print "------------------" 
                    hist1d.Scale(1.0/hist1d.GetSumOfWeights())
                 
                    J3Status="leading pT 2Jets: "+_flav+"  " +_ctag  
                    hist1ds_name_dRBB +=[J3Status]
                    hist1ds_dRBB +=[hist1d]
                    '''
                    #  nJ 
                    print "--------nJ----------" 
                    _histname_nJ = "hist_VHcc_"+_region+"_nJ_"+_flav+_ctag+"_"+_ptv
                    hist1d= myFile.Get(_histname_nJ)
                    if  type(hist1d) == ROOT.TObject : 
                        print "skip hist  : %s"%_histname_nJ
                        continue 
                    else :
                        print "using hist : %s"%_histname_nJ
                    print "------------------" 
                    hist1d.Scale(1.0/hist1d.GetSumOfWeights())
                 
                    J3Status="leading pT 2Jets: "+_flav+"  " +_ctag  
                    hist1ds_name_nJ +=[J3Status]
                    hist1ds_nJ +=[hist1d]
                    '''
                    


                #hist1d_title = getTitle("", _region, _ptv)+"(new c-tag)" +";"+J3Status
                #hist2d_title = getTitle("VHcc2Lep Resolved","", _region, _ptv)+" (new c-tag)" +";"+J3Status;
                #del hist1d
            if len(hist1ds_mBB)>0:
                hist1d_title = getTitle("VHcc2Lep Resolved (New C-tag) ;","", _region, _ptv)+";mBB distribution"
                histplot1d = HistMaker1D(hist1ds_mBB, hist1ds_name_mBB)
                histplot1d.setLocation_Legend_plot(0.65,0.45,0.93,0.65)
                #histplot1d.setLocation_Legend_stack(0.7,0.2,0.9,0.77)
                hist1d_name = "hist_VHcc_"+_region+"_mBB_"+_ptv
                histplot1d.DrawPlots_ATLAS(hist1d_title, "mBB","Events",hist1ds_name_mBB ,TarDir+hist1d_name,[],"All" ,False)

            if len(hist1ds_nJ)>0:
                hist1d_title = getTitle("VHcc2Lep Resolved (New C-tag) ;","", _region, _ptv)+";nJ distribution"
                histplot1d = HistMaker1D(hist1ds_nJ, hist1ds_name_nJ)
                histplot1d.setLocation_Legend_plot(0.65,0.45,0.93,0.65)
                #histplot1d.setLocation_Legend_stack(0.7,0.2,0.9,0.77)
                hist1d_name = "hist_VHcc_"+_region+"_nJ_"+_ptv
                histplot1d.DrawPlots_ATLAS(hist1d_title, "nJ","Events",hist1ds_name_nJ ,TarDir+hist1d_name,[],"All" ,False)

            if len(hist1ds_dRBB)>0:
                hist1d_title = getTitle("VHcc2Lep Resolved (New C-tag) ;","", _region, _ptv)+";dRBB distribution"
                histplot1d = HistMaker1D(hist1ds_dRBB, hist1ds_name_dRBB)
                histplot1d.setLocation_Legend_plot(0.65,0.45,0.93,0.65)
                #histplot1d.setLocation_Legend_stack(0.7,0.2,0.9,0.77)
                hist1d_name = "hist_VHcc_"+_region+"_dRBB_"+_ptv
                histplot1d.DrawPlots_ATLAS(hist1d_title, "dRBB","Events",hist1ds_name_dRBB ,TarDir+hist1d_name,[],"All" ,False)
            

#    HistPlot_cutflow_vhbb = HistMaker1D([h_cutflow_vhbb],["Preselection cut flow"])
#    HistPlot_cutflow_vhbb.DrawPlots_ATLAS(SampleTypeName+"Preselection cut flow", "","Number of events", ["Preselection cut flow"], "Merged_VHcc/CutFlow_Preselcetion"+"_"+SampleTypeName,[0,25], SetLogY_1D)


if __name__=="__main__":
    try:
        os.mkdir("output/"+TarDir)
    except OSError as error:
        print "OutputDir Exist"    

    main()
