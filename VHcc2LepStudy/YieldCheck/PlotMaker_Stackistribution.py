#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D
from ClassDef import MergeHists 
####################################################
#  #################################################
#  #
#  #        User defined variable 
#  #
#  #################################################
####################################################
SetLogZ_2D = True
#CalStatus="NoCal"
CalOption="Cal"
CalOption="NoCal"
#CutOption=""
CutOption="_Mass70_160"
#CutOption="_dRBBCut"
CalStatus=CalOption+CutOption
TarDir  = "DL1r_VHcc_DL1rcRankL_"+CalStatus+"/" # with flavor labal for VHbbcc2Lep VHcc 
TarDir  = "Stacks/" # with flavor labal for VHbbcc2Lep VHcc 
InputFile = "/data/chenye/VHcc/Ntupple/code/hist_VHcc1_newDL1r_"+CalStatus+".root"
InputFile = "/data/chenye/VHcc/Ntupple/code/vhcc-mva-tree-processor/hist_all1_newDL1r_Cal.root"
#InputFile = "/data/chenye/VHcc/Ntupple/code/result/hist_sig_newDL1r_Cal.root"
SetLogY_1D=False

####################################################
#  #################################################
#  #
#  #        User defined drawing function
#  #
#  #################################################
####################################################
def getTitle(start, sample, SRregion, ptv ):
    title=start
    if sample == "sig": 
        title += "Signal"
    elif sample == "bkg":
        title += "Background"
    else : 
        title += sample
    if sample!="": 
        title += " "
    title += ptv+" "

    if SRregion == "SR1":
        title += "2Jets "
    elif SRregion == "SR2":
        title += "#geq3Jets "
    elif SRregion == "SR":
        title += "#geq2Jets "
    
    return title

def getPlotTitle(start, CTag,  SRregion):
    title = "#sqrt{s}=13 TeV,139 fb^{-1} (Run2);"+start+" "

    if SRregion == "SR1":
        title += "2Jets "
    elif SRregion == "SR2":
        title += "#geq3Jets "
    elif SRregion == "SR":
        title += "#geq2Jets "
    title += "CTag %s"%(CTag)
    return title

def getLegend(sample,  flavor):
    legend=""
    if "ZJets" in sample:
        legend+="Z+"
    elif "WJets" in sample:
        legend+="W+"
    else:
        legend+=sample+" "
    
    legend+=flavor

    return legend

def main():
    #ctagFlav2D
    Sample_info = ["VHcc", "VHbb","ZJets","WJets","ZZ","WZ","stop","ttbar"]
#    Sample_info = ["VHcc", "VHbb","stop","ttbar","WZ","ZZ","WJets","ZJets"]
    Flavor_info = ["hf","mf","lf"]
    Region_info = ["SR1","SR2","SR"]
    Region_info = ["SR1","SR2"]
    Tagging_info= ["NN","TN","TN","LL","TL","TT"]
    PTV_info=["LowPTV","MedHighPTV"];
    Phy_info=["mBB","dRBB","mBBJ"]
#              ,"mLL","pTV","MET","METSig","cosThetaLep"
#              ,"dPhiVBB","dEtaVBB","nJ"
#              ,"pTB1","pTB2","pTJ3"];
    #hist_VHcc_mf_SR2_Ctag_All
    print "================== Generate Stack Hist   ==================" 
    myFile = TFile(InputFile)
    for _phy in Phy_info :
        for _tagging in Tagging_info :
            for _region in Region_info : 
                ## Countainer 
                Container_hists_VHcc=[]
                Container_hists_VHbb=[]
                Container_hists_ttbar=[]
                Container_hists_stop=[]
                Container_hists_bkg=[]
                Container_hists_data=[]
                Legend_bkg = []
                for _sample in Sample_info :
                    for _flavor in Flavor_info :

                        # Hist initialize 
                        #hist_WJets_hf_SR1_dEtaBB_TN
                        _histName = "hist_"+ _sample +"_"+_flavor+"_" +_region+"_"+_phy+"_"+_tagging
                        print "------------------" 
                        tmpHist= myFile.Get(_histName)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 
                        copyHist=tmpHist.Clone("%s_copy"%_histName)
                        copyHist.Sumw2()
                        copyHist.Rebin(4)
                        if "VHcc" in _sample : 
                            Container_hists_VHcc += [copyHist]
                        elif "VHbb" in _sample : 
                            Container_hists_VHbb += [copyHist]
                        elif "data" in _sample:
                            Container_hists_data += [copyHist]
                        elif "ttbar" in _sample:
                            Container_hists_ttbar += [copyHist]
                        elif "stop" in _sample:
                            Container_hists_stop += [copyHist]
                        else:
                            Container_hists_bkg += [copyHist]
                            Legend_bkg += [getLegend(_sample,_flavor)]

                _histName = "hist_data_of_" +_region+"_"+_phy+"_"+_tagging
                print "------------------" 
                tmpHist= myFile.Get(_histName)
                if  type(tmpHist) == ROOT.TObject : 
                    print "Skip Hist  : %s"%_histName
                else :
                    hist_data=tmpHist.Clone("%s_copy"%_histName)
                    hist_data.Sumw2()
                    hist_data.Rebin(4)
                    Container_hists_data +=[hist_data]
                    print "Using Hist : %s"%_histName
                print "------------------" 

                if len(Container_hists_VHcc) < 1 or len(Container_hists_VHcc) < 1: 
                    continue

                hist_VHcc = MergeHists(Container_hists_VHcc,"hist_clone_VHcc_"+_region+"_"+_phy+"_"+_tagging).Clone("hist_VHcc_"+_region+"_"+_phy+"_"+_tagging)
                hist_VHbb = MergeHists(Container_hists_VHbb,"hist_clone_VHbb_"+_region+"_"+_phy+"_"+_tagging).Clone("hist_VHbb_"+_region+"_"+_phy+"_"+_tagging)
                hist_ttbar = MergeHists(Container_hists_ttbar,"hist_clone_ttbar_"+_region+"_"+_phy+"_"+_tagging).Clone("hist_ttbar_"+_region+"_"+_phy+"_"+_tagging)
                hist_stop = MergeHists(Container_hists_stop,"hist_clone_stop_"+_region+"_"+_phy+"_"+_tagging).Clone("hist_stop_"+_region+"_"+_phy+"_"+_tagging)
                #hist_data = MergeHists(Container_hists_data,"hist_clone_data_"+_region+"_"+_phy+"_"+_tagging).Clone("hist_data_"+_region+"_"+_phy+"_"+_tagging)
                Container_hists_bkg = Container_hists_bkg+[hist_stop,hist_ttbar]+[hist_VHbb]
                Container_hists = Container_hists_bkg+[hist_VHcc]
                Legend_bkg = Legend_bkg + ["stop","ttbar"]+["VHbb"]
                Legends = Legend_bkg + ["VHcc"]
                
                HistPlot = HistMaker1D(Container_hists,Legends)
                HistPlot.setHistStackSig([hist_VHcc],["VHcc"])
                HistPlot.setHistStackBkg(Container_hists_bkg,Legend_bkg)
                HistPlot.setLocation_Legend_stack(0.7,0.7,0.9,0.88)
                HistPlot.setLocation_ATLASTitle(0.2, 0.8,0.4,0.88)
                
                if len(Container_hists_data)>0:
                    HistPlot.setHistData([hist_data])
                HistPlot.setSignalScale(300)
                title = getPlotTitle("VHcc 2Lep",_tagging, _region)
                outputName = "hist_"+_region+"_"+_phy+"_"+_tagging
                if "mBB" in _phy or "pT" in _phy: 
                    HistPlot.DrawStacks_ATLAS(title, _phy+" [GeV]","Number of events",TarDir+"stack_"+outputName, [], SetLogY_1D)
                else:
                    HistPlot.DrawStacks_ATLAS(title, _phy, "Number of events",TarDir+"stack_"+outputName, [], SetLogY_1D)
                
                del HistPlot
                del Container_hists_bkg
                del Container_hists_VHcc
                del Container_hists_ttbar
                del Container_hists_stop
                del Container_hists_VHbb


if __name__=="__main__":
    try:
        os.mkdir("output/"+TarDir)
    except OSError as error:
        print "OutputDir Exist"    

    main()
