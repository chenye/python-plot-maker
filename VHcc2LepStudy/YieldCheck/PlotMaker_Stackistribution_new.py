#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend, TColor
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D
from ClassDef import MergeHists 
####################################################
#  #################################################
#  #
#  #        User defined variable 
#  #
#  #################################################
####################################################
SetLogZ_2D = True
#CalStatus="NoCal"
CalOption="Cal"
CalOption="NoCal"
#CutOption=""
CutOption="_Mass70_160"
#CutOption="_dRBBCut"
CalStatus=CalOption+CutOption
TarDir  = "DL1r_VHcc_DL1rcRankL_"+CalStatus+"/" # with flavor labal for VHbbcc2Lep VHcc 
TarDir  = "Stacks_AllSignalJets/" # with flavor labal for VHbbcc2Lep VHcc 
#InputFile = "/data/chenye/VHcc/Ntupple/code/hist_VHcc1_newDL1r_"+CalStatus+".root"
#InputFile = "/data/chenye/VHcc/Ntupple/code/vhcc-mva-tree-processor/hist_all1_newDL1r_Cal.root"
#InputFile = "/data/chenye/VHcc/Ntupple/code/result/hist_sig_newDL1r_Cal.root"
SetLogY_1D=False
JetStrategy="AllSignalJets"
#JetStrategy="Leading2Jets"
## Input 
InputFile = "/data/chenye/VHcc/Ntupple/code/vhcc-mva-tree-processor/output/"+JetStrategy+"Hist/hist_all1_newDL1r_NoCal.root"
InputFile="/data/chenye/VHcc/Ntupple/code/vhcc-mva-tree-processor/run/hist_all1_newDL1r_NoCal_Resolved_Boosted.root"
InputFile="/data/chenye/VHcc/Ntupple/code/vhcc-mva-tree-processor/run/hist_all1_newDL1r_NoCal.root"
## OutPut 
TarDir  = "Stacks_%s/"%JetStrategy # with flavor labal for VHbbcc2Lep VHcc 

####################################################
#  #################################################
#  #
#  #        User defined drawing function
#  #
#  #################################################
####################################################

def getSRCRString(SRregion):

    title=""
    if SRregion == "SR1":
        title += "SR 2Jets "
    elif SRregion == "SR2":
        title += "SR #geq3Jets "
    elif SRregion == "SR":
        title += "SR #geq2Jets "
    elif SRregion == "CR1":
        title += "CR 2Jets "
    elif SRregion == "CR2":
        title += "CR #geq3Jets "
    elif SRregion == "CR":
        title += "CR #geq2Jets "
    elif SRregion == "topCR1":
        title += "topCR 2Jets "
    elif SRregion == "topCR2":
        title += "topCR #geq3Jets "
    elif SRregion == "topCR":
        title += "topCR #geq2Jets "
    
    return title


def getTitle(start, sample, SRregion, ptv ):
    title=start
    if sample == "sig": 
        title += "Signal"
    elif sample == "bkg":
        title += "Background"
    else : 
        title += sample
    if sample!="": 
        title += " "
    title += ptv+" "

    if SRregion == "SR1":
        title += "SR 2Jets "
    elif SRregion == "SR2":
        title += "SR #geq3Jets "
    elif SRregion == "SR":
        title += "SR #geq2Jets "
    elif SRregion == "CR1":
        title += "CR 2Jets "
    elif SRregion == "CR2":
        title += "CR #geq3Jets "
    elif SRregion == "CR":
        title += "CR #geq2Jets "
    elif SRregion == "topCR1":
        title += "topCR 2Jets "
    elif SRregion == "topCR2":
        title += "topCR #geq3Jets "
    elif SRregion == "topCR":
        title += "topCR #geq2Jets "


    
    return title


def getPlotTitle(start, CTag,  SRregion):
    title = "#sqrt{s}=13 TeV,139 fb^{-1} (Run2);"+start+";"
    
    title += getSRCRString(SRregion)
    title += "CTag %s"%(CTag)
    return title

def getLegend(sample,  flavor):
    legend=""
    if "ZJets" in sample:
        legend+="Z+"
    elif "WJets" in sample:
        legend+="W+"
    else:
        legend+=sample+" "
    
    legend+=flavor

    return legend


def main():
    #ctagFlav2D
    Sample_info = ["VHcc","Zhf","Zmf","Zlf","Whf","Wmf","Wlf","VZcc","VWcq","VVbkg","ttbar","stop", "VHbb","data"]
    Sample_legend = ["VHcc", "Z+hf","Z+mf","Z+lf","W+hf","W+mf","W+lf","VZ(#rightarrow c#bar{c})","VW(#rightarrow cq)","VV(bkg.)","ttbar","stop","VHbb","data"]
#    Sample_info = ["VHcc", "VHbb","stop","ttbar","WZ","ZZ","WJets","ZJets"]
    Flavor_info = ["hf","mf","lf"]
    Region_info = ["SR1","SR2","CR1","CR2","topCR1","topCR2"]
    Tagging_info= ["NN","TN","LN","LL","TL","TT","inclusive"]
    PTV_info=["LowPTV","MedHighPTV"];
    Phy_info=[
               "mBB"
               "mLL"
              ,"dRBB","mBBJ"
              ,"mLL","pTV","MET","METSig","cosThetaLep"
              ,"dPhiVBB","dEtaVBB","nJ"
              ,"pTB1","pTB2","pTJ3"
              ,"etaB1","etaB2","etaJ3"
              ]
    #hist_VHcc_mf_SR2_Ctag_All
    print "================== Define VHcc Color Stack Hist   ==================" 
    ColorDict = {"Zhf"  :   TColor.GetColor("#3F65C5"),
                 "Zlf"  :   TColor.GetColor("#A4CBFA"),
                 "Zmf"  :   TColor.GetColor("#7197F8"),
                 "Wlf"  :   TColor.GetColor("#B1FCA3"),
                 "Wmf"  :   TColor.GetColor("#80C972"),
                 "Whf"  :   TColor.GetColor("#529742"),
                 "ttbar":   TColor.GetColor("#F7CE46"),
                 "stop" :   TColor.GetColor("#FEFE61"),
                 "VVbkg":   TColor.GetColor("#CCCCCC"),
                 "VWcq" :   TColor.GetColor("#999999"),
                 "VZcc" :   TColor.GetColor("#666666"),
                 "VHbb" :   TColor.GetColor("#5C0E93"),
                 "VHcc" :   TColor.GetColor("#E03A3E")
    }
    print "================== Generate Stack Hist   ==================" 
    myFile = TFile(InputFile)
    for _tagging in Tagging_info :
        for _region in Region_info : 
            FillPie = False
            for _phy in Phy_info :
                ## Countainer 
                ColorList_bkg = []
                ColorList_sig = []
                Container_hists_VHcc=[]
                Container_hists_VHbb=[]
                Container_hists_bkg=[]
                Container_hists_data=[]
                Legend_bkg = []
                for _i in range(0,len(Sample_info)) :
                        _sample = Sample_info[_i] 
                        _legend = Sample_legend[_i] 
                        # Hist initialize 
                        #hist_WJets_hf_SR1_dEtaBB_TN
                        _histName = "hist_"+ _sample +"_" +_region+"_"+_phy+"_"+_tagging
                        print "------------------" 
                        tmpHist= myFile.Get(_histName)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 
                        copyHist=tmpHist.Clone("%s_copy"%_histName)
                        #copyHist.Sumw2()
                        if _phy != "nJ":
                            copyHist.Rebin(4)
                        if "VHcc" in _sample : 
                            Container_hists_VHcc += [copyHist]
                            ColorList_sig += [ColorDict[_sample]]
                        elif "VHbb" in _sample : 
                            Container_hists_VHbb += [copyHist]
                        elif "data" in _sample:
                            Container_hists_data += [copyHist]
                        else:
                            Container_hists_bkg += [copyHist]
                            Legend_bkg += [_legend]
                            ColorList_bkg += [ColorDict[_sample]]
   
                if len(Container_hists_VHcc) < 1 or len(Container_hists_VHcc) < 1: 
                    continue

                Container_hists_bkg = Container_hists_bkg+Container_hists_VHbb
                Container_hists = Container_hists_bkg+Container_hists_VHcc
                Legend_bkg = Legend_bkg + ["VHbb"]
                ColorList_bkg = ColorList_bkg + [ColorDict["VHbb"]]
                Legends = Legend_bkg + ["VHcc"]
                
                HistPlot = HistMaker1D(Container_hists,Legends)
                HistPlot.setHistStackSig(Container_hists_VHcc,["VHcc"])
                HistPlot.setHistStackBkg(Container_hists_bkg,Legend_bkg)
                HistPlot.setColorListBkg(ColorList_bkg)
                HistPlot.setColorListSig(ColorList_sig)
                HistPlot.setColorType("DIY")
                HistPlot.setLocation_Legend_stack(0.6,0.7,0.9,0.89)
                HistPlot.setLocation_ATLASTitle(0.2, 0.8,0.4,0.88)
                
                if len(Container_hists_data)>0:
                    HistPlot.setHistData(Container_hists_data)
                HistPlot.setSignalScale(300)
                title = getPlotTitle("VHcc 2Lep "+JetStrategy,_tagging, _region)
                outputName = "hist_"+_region+"_"+_phy+"_"+_tagging+"_"+JetStrategy
                if "mBB" in _phy or "pT" in _phy: 
                    #HistPlot.DrawStacks_ATLAS(title, _phy+" [GeV]","Number of events",TarDir+"stack_"+outputName, [], SetLogY_1D)
                    HistPlot.DrawStacksRatio_ATLAS(title, _phy+" [GeV]","Number of events",TarDir+"stack_"+outputName, [], SetLogY_1D)
                else:
                    HistPlot.DrawStacksRatio_ATLAS(title, _phy, "Number of events",TarDir+"stack_"+outputName, [], SetLogY_1D)
                if FillPie == False :
#                    HistPlot.setLocation_Legend_stack(0.6,0.7,0.9,0.89)
                    HistPlot.setLocation_ATLASTitle(0.1, 0.87,0.4,0.95)
                    outputName = "pie"+_region+"_"+_tagging+"_"+JetStrategy
                    HistPlot.DrawPiePlot_ATLAS(title,TarDir+outputName)

                del HistPlot
                del Container_hists_bkg[:]
                del Container_hists_data[:]
                del Container_hists_VHcc[:]
                del Container_hists_VHbb[:]
                del Container_hists[:]
                del ColorList_bkg[:]
                del ColorList_sig[:]
                del Legend_bkg[:]

    # CTAG Strategy Yield Over view
    
    f = open("YieldInfo_%s.txt"%JetStrategy,"w")
    f.write("Yield Infomation")
    f.write("Region \t CTag \t Signal Yield \t Bkg Yield \t Data \t S/sqrt(B) \t Data/MC\n ")
    for _region in Region_info : 
        Yield_Info ={} 
        ## Countainer 
        ColorList_bkg = []
        ColorList_sig = []
        Container_hists_VHcc=[]
        Container_hists_VHbb=[]
        Container_hists_bkg=[]
        Container_hists_data=[]
        Legend_bkg = []
        # Loop Over sample : data, VHcc, VHbb, Zjets, ......
        for _i in range(0,len(Sample_info)) :
                _sample = Sample_info[_i] 
                _legend = Sample_legend[_i] 
                
                # Hist initialize 
                _histName = "hist_"+ _sample +"_" +_region+"_Ctag_All"
                print "------------------" 
                tmpHist= myFile.Get(_histName)
                if  type(tmpHist) == ROOT.TObject : 
                    print "Skip Hist  : %s"%_histName
                    continue 
                else :
                    print "Using Hist : %s"%_histName
                copyHist=tmpHist.Clone("%s_copy"%_histName)
                #copyHist.Sumw2()
                print "------------------" 

                # Fill Hists container 
                if "VHcc" in _sample : 
                    Container_hists_VHcc += [copyHist]
                    ColorList_sig += [ColorDict[_sample]]
                elif "VHbb" in _sample : 
                    Container_hists_VHbb += [copyHist]
                elif "data" in _sample:
                    Container_hists_data += [copyHist]
                else:
                    Container_hists_bkg += [copyHist]
                    Legend_bkg += [_legend]
                    ColorList_bkg += [ColorDict[_sample]]
   
        if len(Container_hists_VHcc) < 1 or len(Container_hists_VHcc) < 1: 
            continue

        Container_hists_bkg = Container_hists_bkg+Container_hists_VHbb
        Container_hists = Container_hists_bkg+Container_hists_VHcc
        Legend_bkg = Legend_bkg + ["VHbb"]
        ColorList_bkg = ColorList_bkg + [ColorDict["VHbb"]]
        Legends = Legend_bkg + ["VHcc"]
        
        # Setup for Stack Hist 
        HistPlot = HistMaker1D(Container_hists,Legends)
        HistPlot.setHistStackSig(Container_hists_VHcc,["VHcc"])
        HistPlot.setHistStackBkg(Container_hists_bkg,Legend_bkg)
        HistPlot.setColorListBkg(ColorList_bkg)
        HistPlot.setColorListSig(ColorList_sig)
        HistPlot.setColorType("DIY")
        HistPlot.setLocation_Legend_stack(0.6,0.7,0.9,0.9)
        HistPlot.setLocation_ATLASTitle(0.2,0.8,0.5,0.9)
        
        if len(Container_hists_data)>0:
            HistPlot.setHistData(Container_hists_data)
        HistPlot.setSignalScale(300)

        # Calculation

        hist_bkgMerge = MergeHists(Container_hists_bkg,"hist_bkg")
        hist_sigMerge = MergeHists(Container_hists_VHcc,"hist_sig")
        hist_allMerge = MergeHists(Container_hists,"hist_all")
        hist_data= Container_hists_data[0].Clone("hist_data")
        for bin_i in range(1,7):
            CTag = hist_data.GetXaxis().GetBinLabel(bin_i)
            Yield_data = hist_data.GetBinContent(bin_i)
            Yield_all = hist_allMerge.GetBinContent(bin_i)
            Yield_sig = hist_sigMerge.GetBinContent(bin_i)
            Yield_bkg = hist_bkgMerge.GetBinContent(bin_i)
            if  Yield_bkg > 0 :
                Signif = Yield_sig*1.0/math.sqrt(Yield_bkg)
            else:
                Signif = 0

            ratio = Yield_data*1.0/Yield_all 
            

            YieldInfo={"data": Yield_data, "sig": Yield_sig, "bkg": Yield_bkg, "Signif": Signif, "dataMC":ratio}
#            f = open("YieldInfo_%s.txt"%JetStrategy,"a")
            regionName=""
            if _region == "SR1":
                regionName = "SR 2Jet"
            elif _region == "CR1":
                regionName = "CR 2Jet"
            elif _region == "topCR1":
                regionName = "topCR 2Jet"
            elif _region == "SR2":
                regionName = "SR 3+jet"
            elif _region == "CR2":
                regionName = "CR 3+jet"
            elif _region == "topCR2":
                regionName = "topCR 3+jet"


            #f.write("Region \t CTag \t Signal Yield \t Bkg Yield \t Data \t S/sqrt(B) \t Data/MC\n ")
            f.write("%s     \t %s   \t %.3f         \t %.3f     \t %d    \t %.3f      \t %.3f   \n "%(regionName, CTag, YieldInfo["sig"],YieldInfo["bkg"],YieldInfo["data"], YieldInfo["Signif"],YieldInfo["dataMC"]))
        
        # Draw 
        title = getPlotTitle("VHcc 2Lep "+JetStrategy,_tagging, _region)
        outputName = "hist_"+_region+"_Ctag_All_"+JetStrategy
        HistPlot.DrawStacksRatio_ATLAS(title, "CTag", "Number of events",TarDir+"stack_"+outputName, [], True,True)
        
        del HistPlot
        del Container_hists_bkg[:]
        del Container_hists_data[:]
        del Container_hists_VHcc[:]
        del Container_hists_VHbb[:]
        del Container_hists[:]
        del ColorList_bkg[:]
        del ColorList_sig[:]
        del Legend_bkg[:]

    f.close()

if __name__=="__main__":
    try:
        os.mkdir("output/"+TarDir)
    except OSError as error:
        print "OutputDir Exist"    

    main()
