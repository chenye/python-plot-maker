#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText,TMath
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D
from ClassDef import MergeHists, SignificanceCalculatuion_All, SignificanceCalculatuion_Bin
from Function_VHcc_MVA import TMVAppResultEvaluation ,TMVAppResultEvaluation_SameFile, TMVATraining_output, TMVATraining_output_overtarin;

####################################################
#  #################################################
#  #
#  #        User defined variable 
#  #
#  #################################################
####################################################
SignalScale=10
SetLogY_1D = True
SetLogZ_2D = True 
SwitchBBToCC = True # switch name with BB to CC
UsingTop = True # use top quark process ttbar wt 
USETau = False 
ZeroTagOnly = True
DataOnPlot = False 
####################################################
#  #################################################
#  #
#  #        User defined drawing function
#  #
#  #################################################
####################################################
        
def ptvInfo_convert(ptvInfo):
    
    if ptvInfo == "400ptv":
        Info ="ZpT > 400 GeV"
    else:
        Info ="ZpT "+ (ptvInfo.replace("_","-")).replace("ptv"," GeV")

    return Info
##########################################################
#######     Fill Hist Kinematics & Yields      
##########################################################

##########################################################################
# ########################################################################
# #
# #     main function
# #
# ########################################################################
##########################################################################

def main():
    atlas_style()

    #######################################################
    #   VHCC
    #######################################################

    #SetLogY_1D = True
    #SetLogZ_2D = True 
    #-----------------------
    # info
    #-----------------------



    #---------------------------------------------
    # Get File TMVA Apply
    #

    JetRank = False
    JetStrategy = "AllSignalJets"
    InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/MVApp_with_pTV"
    if JetRank:
        InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/2022_3324_MVApp_oldVar_2J3J/AllJets_JetRank_TTTL"
        ## lead2
        if JetStrategy == "Leading2Jets":
            InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/2022_3324_MVApp_oldVar_2J3J/Lead2Jets_Origin_TX"
        elif JetStrategy == "AllSignalJets":
            InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/2022_3324_MVApp_oldVar_2J3J/AllJets_Origin_TX"

    else:
        InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/2022_3324_MVApp_oldVar_2J3J/AllJets_Origin_TTTL"
        if JetStrategy == "Leading2Jets":
            InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/2022_3324_MVApp_oldVar_2J3J/Lead2Jets_Origin_TX"
        elif JetStrategy == "AllSignalJets":
            InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/2022_3324_MVApp_oldVar_2J3J/AllJets_Origin_TX"
            InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/2022_3324_MVApp_oldVar_2J3J/AllJets_Origin_TTTL"

    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/MVApp_Old_Variable_2J3J"


#   FileNameL_2pJ="low.root"
#   FileNameHM_2pJ="hm.root"
    FileNameL_2J="low_2J.root"
    FileNameHM_2J="hm_2J.root"
    FileNameL_3pJ="low_3J.root"
    FileNameHM_3pJ="hm_3J.root"
    
    DataFileNameL_2pJ="data_2pJ.root"
    DataFileNameHM_2pJ="data_2pJ.root"
    DataFileNameL_2J="data_23J.root"
    DataFileNameHM_2J="data_23J.root"
    DataFileNameL_3pJ="data_23J.root"
    DataFileNameHM_3pJ="data_23J.root"

    DataFileNameL_2J=""
    DataFileNameHM_2J=""
    DataFileNameL_3pJ=""
    DataFileNameHM_3pJ=""

    SetName = "MVA_BDT"
    #SetName = "Method_BDT" # MVATraining_VHbb Setting
    '''
    TMVAppResultEvaluation_SameFile(InputDir, FileNameLow, SetName,"sig_2T_2pJ_75ptv_150ptv_mva","bg_2T_2pJ_75ptv_150ptv_mva","Resolved VHcc 2Lep;TT+TL Low pTV Region","Ntag2_PTV_Low" )
    TMVAppResultEvaluation_SameFile(InputDir, FileNameHM, SetName,"sig_2T_2pJ_150ptv_mva","bg_2T_2pJ_150ptv_mva","Resolved VHcc 2Lep;TT+TL Med+High pTV Region","Ntag2_PTV_MedHigh" )
    TMVAppResultEvaluation_SameFile(InputDir, FileNameH, SetName,"sig_2T_2pJ_250ptv_mva","bg_2T_2pJ_250ptv_mva","Resolved VHcc 2Lep;TT+TL High pTV Region","Ntag2_PTV_High" )
    TMVAppResultEvaluation_SameFile(InputDir, FileNameM, SetName,"sig_2T_2pJ_150ptv_250ptv_mva","bg_2T_2pJ_150ptv_250ptv_mva","Resolved VHcc 2Lep;TT+TL Medium pTV Region","Ntag2_PTV_Med" )
    '''
    # Old
#    TMVAppResultEvaluation_SameFile(InputDir, FileNameL_2pJ,  DataFileNameL_2pJ,  SetName,"sig_2T_2pJ_75ptv_150ptv_mva",  "bg_2T_2pJ_75ptv_150ptv_mva",   "data_2T_2pJ_75ptv_150ptv_mva", "Resolved VHcc 2Lep 2pJ;TT+TL Low pTV Region",      "Ntag2_PTV_Low_2pJ"     )
#    TMVAppResultEvaluation_SameFile(InputDir, FileNameHM_2pJ, DataFileNameHM_2pJ, SetName,"sig_2T_2pJ_150ptv_mva",        "bg_2T_2pJ_150ptv_mva",         "data_2T_2pJ_150ptv_mva",       "Resolved VHcc 2Lep 2pJ;TT+TL Med+High pTV Region", "Ntag2_PTV_MedHigh_2pJ" )

    if JetRank : 
        TMVAppResultEvaluation_SameFile(InputDir, FileNameL_2J,  DataFileNameL_2J,  SetName,"sig_2T_2pJ_75ptv_150ptv_mva",  "bg_2T_2pJ_75ptv_150ptv_mva",   "data_2T_2J_75ptv_150ptv_mva", "Resolved VHcc 2Lep 2J;TX Low pTV Region;Rank Jet by CTag",      "Ntag2_PTV_Low_2J"     )
        TMVAppResultEvaluation_SameFile(InputDir, FileNameHM_2J, DataFileNameHM_2J, SetName,"sig_2T_2pJ_150ptv_mva",        "bg_2T_2pJ_150ptv_mva",         "data_2T_2J_150ptv_mva",       "Resolved VHcc 2Lep 2J;TX Med+High pTV Region;Rank Jet by CTag", "Ntag2_PTV_MedHigh_2J" )
        TMVAppResultEvaluation_SameFile(InputDir, FileNameL_3pJ,  DataFileNameL_3pJ,  SetName,"sig_2T_2pJ_75ptv_150ptv_mva",  "bg_2T_2pJ_75ptv_150ptv_mva",   "data_2T_3pJ_75ptv_150ptv_mva", "Resolved VHcc 2Lep 3pJ;TX Low pTV Region;Rank Jet by CTag",      "Ntag2_PTV_Low_3pJ"     )
        TMVAppResultEvaluation_SameFile(InputDir, FileNameHM_3pJ, DataFileNameHM_3pJ, SetName,"sig_2T_2pJ_150ptv_mva",        "bg_2T_2pJ_150ptv_mva",         "data_2T_3pJ_150ptv_mva",       "Resolved VHcc 2Lep 3pJ;TX Med+High pTV Region;Rank Jet by CTag", "Ntag2_PTV_MedHigh_3pJ" )

    else:
        TMVAppResultEvaluation_SameFile(InputDir, FileNameL_2J,  DataFileNameL_2J,  SetName,"sig_2T_2pJ_75ptv_150ptv_mva",  "bg_2T_2pJ_75ptv_150ptv_mva",   "data_2T_2J_75ptv_150ptv_mva", "Resolved VHcc 2Lep 2J;TX Low pTV Region",      "Ntag2_PTV_Low_2J"     )
        TMVAppResultEvaluation_SameFile(InputDir, FileNameHM_2J, DataFileNameHM_2J, SetName,"sig_2T_2pJ_150ptv_mva",        "bg_2T_2pJ_150ptv_mva",         "data_2T_2J_150ptv_mva",       "Resolved VHcc 2Lep 2J;TX Med+High pTV Region", "Ntag2_PTV_MedHigh_2J" )
        TMVAppResultEvaluation_SameFile(InputDir, FileNameL_3pJ,  DataFileNameL_3pJ,  SetName,"sig_2T_2pJ_75ptv_150ptv_mva",  "bg_2T_2pJ_75ptv_150ptv_mva",   "data_2T_3pJ_75ptv_150ptv_mva", "Resolved VHcc 2Lep 3pJ;TX Low pTV Region",      "Ntag2_PTV_Low_3pJ"     )
        TMVAppResultEvaluation_SameFile(InputDir, FileNameHM_3pJ, DataFileNameHM_3pJ, SetName,"sig_2T_2pJ_150ptv_mva",        "bg_2T_2pJ_150ptv_mva",         "data_2T_3pJ_150ptv_mva",       "Resolved VHcc 2Lep 3pJ;TX Med+High pTV Region", "Ntag2_PTV_MedHigh_3pJ" )


    #
    #---------------------------------------------
    # Get  TMVA Train out put 
    #
    dataloaders_2pJ = [
        "dataloader_BDT_2L_2pJ_75ptv_150ptv_1of2",
        "dataloader_BDT_2L_2pJ_75ptv_150ptv_2of2",
        "dataloader_BDT_2L_2pJ_150ptv_1of2",    
        "dataloader_BDT_2L_2pJ_150ptv_2of2"
    ]

    dataloaders_2J3J= [
        "dataloader_BDT_2L_2J_75ptv_150ptv_1of2",
        "dataloader_BDT_2L_2J_75ptv_150ptv_2of2",
        "dataloader_BDT_2L_2J_150ptv_1of2",    
        "dataloader_BDT_2L_2J_150ptv_2of2",
        "dataloader_BDT_2L_3pJ_75ptv_150ptv_1of2",
        "dataloader_BDT_2L_3pJ_75ptv_150ptv_2of2",
        "dataloader_BDT_2L_3pJ_150ptv_1of2",    
        "dataloader_BDT_2L_3pJ_150ptv_2of2"
    ]
    
    InputFileNames_2pJ = [
        "BDT_2L_2pJ_75ptv_150ptv_1of2",
        "BDT_2L_2pJ_75ptv_150ptv_2of2",
        "BDT_2L_2pJ_150ptv_1of2",
        "BDT_2L_2pJ_150ptv_2of2"
    ]

    InputFileNames_2J3J = [
        "BDT_2L_2J_75ptv_150ptv_1of2",
        "BDT_2L_2J_75ptv_150ptv_2of2",
        "BDT_2L_2J_150ptv_1of2",
        "BDT_2L_2J_150ptv_2of2",
        "BDT_2L_3pJ_75ptv_150ptv_1of2",
        "BDT_2L_3pJ_75ptv_150ptv_2of2",
        "BDT_2L_3pJ_150ptv_1of2",
        "BDT_2L_3pJ_150ptv_2of2"
    ]
    InputFileNames = InputFileNames_2pJ + InputFileNames_2J3J
    dataloaders    = dataloaders_2pJ + dataloaders_2J3J

    InputFileNames = InputFileNames_2J3J
    dataloaders    = dataloaders_2J3J
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/MVATrain_2J3J_with_pTV"
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/MVATrain_Old_Variable_2J3J"
    ## lead2
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/2022_3324_MVApp_oldVar_2J3J/Lead2Jets_Origin_TX/BDT_Train"

    if JetStrategy == "Leading2Jets":
         InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/2022_3324_MVApp_oldVar_2J3J/Lead2Jets_Origin_TX/BDT_Train"
    elif JetStrategy == "AllSignalJets":
         InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/2022_3324_MVApp_oldVar_2J3J/AllJets_Origin_TX/BDT_Train"
         InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut/2022_3324_MVApp_oldVar_2J3J/AllJets_Origin_TTTL/BDT_Train"
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/BDT_nodR_noJetVetoCut_MVATrain_Old_Variable"

    Method_Name = "Method_BDT" # MVATraining_VHbb Setting
    for datasetName , InputFileName in zip(dataloaders, InputFileNames):

        TagType = ""
        pTVRegion=""
        JetType=""

        if "2J" in datasetName : 
            JetType="2J"
        elif "2pJ" in datasetName : 
            JetType="2pJ"
        elif "3pJ" in datasetName : 
            JetType="3pJ"

        if "1of2" in datasetName : 
            TagType="_TTTL_1of2"
        elif "2of2" in datasetName : 
            TagType="_TTTL_2of2"
        #TagType = TagType +"_"+ JetType 

        if "75ptv_150ptv" in datasetName : 
            pTVRegion = "Low"
        elif "150ptv_250ptv" in datasetName : 
            pTVRegion = "Medium"
        elif "250ptv" in datasetName : 
            pTVRegion = "High"
        elif "150ptv" in datasetName : 
            pTVRegion = "Med+High"
        
       # pTVRegion = pTVRegion+" "+JetType

        TMVATraining_output(InputDir,InputFileName, datasetName, pTVRegion, "2", "TX", TagType, JetType, TMVA_Method=Method_Name)

    
    #InputDir="/home/chenye/work/CharmJet/VHCC/MVAResult/CrossCheck"
    #TMVATraining_output_overtarin(InputDir, "Output_1of2",  datasetName, "High", "2", "TT+TL", TagType)
    # TMVATraining_output_overtarin(InputDir, "Output_2of2",  datasetName, "High", "2", "TT+TL", TagType)
              
              
if __name__ == "__main__":
    main()
