#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D


# #########################
# User defined drawing function
# #########################

def Hist2D_filllValue_phy_vs_ntag(hist1DName, hist1D):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    integral = hist1D.GetSumOfWeights()
    
    if "0tag" in histName:
        tag = 0.5
    elif "1tag" in histName:
        tag = 1.5
    elif "2tag" in histName:
        tag = 2.5
    elif "3ptag" in histName:
        tag = 3.5

    if "ZZbkg" in  histName: 
        phy=0.5
    elif "ZZ" in histName:
        phy=1.5
    elif "Zcc" in histName:
        phy=2.5
    elif "Zcl" in histName:
        phy =3.5 
    elif "qqZll" in histName :
        phy=5.5
    elif "ggZll" in histName :
        phy=6.5
    elif "Zl" in histName :
        phy=4.5
    
    x = tag 
    y = phy
    return (tag, phy, integral)

def Hist2D_filllValue_njet_vs_ntag(hist1DName, hist1D):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    integral = hist1D.GetSumOfWeights()

    ## x value 
    if "0_250ptv" in histName :
        ptv = 0
    elif "250_400ptv" in histName :
       ptv = 1
    else:
        ptv = 2

    if "0tag" in histName:
        tag = 0.5
    elif "1tag" in histName:
        tag = 1.5
    elif "2tag" in histName:
        tag = 2.5
    elif "3ptag" in histName:
        tag = 3.5
    
    ## y value 
    if "2jet" in histName:
        jet = 0.5 
    elif "3jet" in histName:
        jet =1.5
    elif "4pjet" in histName:
        jet =2.5

    y = tag 
    x = ptv + 0.5  
    return (x,y,integral)
        

##########################################################################
# ########################################################################
# #
# #     main function
# #
# ########################################################################
##########################################################################

def main():
    atlas_style()

    #######################################################
    #   VHCC
    #######################################################

    SetLogY_1D = True
    SetLogZ_2D = True 
    #-----------------------
    # info
    #-----------------------

    # rootfile_dir  = "/home/chenye/work/CharmJet/VHCC/Result_ade/Merged_2lcc/Result_ade/combin/"
    rootfile_dir = "/home/chenye/work/CharmJet/VHCC/SimpleROOT_tree_and_hist/Merged/comb/"

    rootfile_name = [
        "hist-ZZ_Sh221.root",
        "hist-ZC_Sh221.root",
        "hist-ggZllHcc_PwPy8.root",
        "hist-qqZllHccJ_PwPy8MINLO.root"
    ]

    tag_info = ["0tag", "1tag", "2tag", "3ptag"]
    #tag_info = ["0tag"]

    jet_info = [ "1pfat0pjet","1pfat1pjet"]

    Physics_info = ["mBBTT", "mBB","pTV", "dRVH",
                    "pTBB","dRBB","dEtaBB","dPhiBB",
                    "pTB1","pTB2","EtaB1","EtaB2",
                    "PtFatJ1","EtaFatJ1","NtrkjetsFatJ1"]

    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]
    ptv_info = ["0_250ptv","250_400ptv","400ptv"]
    #ptv_info = ["250ptv"]

    ZC_info = ["Zcc", "Zcl", "Zl"]
    ZZ_info = ["ZZ","ZZbkg"]
    qqZH_info = ["qqZllH125cc"]
    ggZH_info = ["ggZllH125cc"]
 

    ##SR_info="topemucr_noaddbjetsr"  

    SR_info="SR_2psubjet"  


    #---------------------------------------------
    # Get File 
    #

    InputRootFiles={}
    
    for filename in rootfile_name :
        file_name = rootfile_dir + filename
        print file_name
        myFile = TFile(file_name)
        if "ggZllHcc" in filename:
            InputRootFiles["ggZllHcc"] = myFile
        if "qqZllHcc" in filename:
            InputRootFiles["qqZllHcc"] = myFile
        if "ZC" in filename: 
            InputRootFiles["Z+C"] = myFile
        if "ZZ" in filename: 
            InputRootFiles["ZZ"] = myFile

    #---------------------------------------------
    # Hist name & Draw
    #

    Hist2D_phy_vs_tag_name  = {} 
    Hist2D_phy_vs_tag_title = {}
    Hist2D_phy_vs_tag       = {}
    for _phy in Physics_info :
        Hist2D_phy_vs_tag_name[_phy] = "phy_vs_ntag_"+_phy+"2D"
        Hist2D_phy_vs_tag_title[_phy] = " "+_phy+" Yields distribution"
        Hist2D_phy_vs_tag[_phy] = ROOT.TH2D(Hist2D_phy_vs_tag_name[_phy], Hist2D_phy_vs_tag_title[_phy],4,0,4,7,0,7)
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(1,"0 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(2,"1 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(3,"2 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(4,"3+ tag")

        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(7,"ggZH : ZllHcc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(6,"qqZH : ZllHcc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(5,"Z+C : Z+L" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(4,"Z+C : Z+cL" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(3,"Z+C : Z+cc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(2,"ZZ : ZZcc")
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(1,"ZZ : ZZbkg")
        Hist2D_phy_vs_tag[_phy].GetZaxis().SetTitle("Number of evnets")


    ###########  Z+C ###########

    SampleType="Z+C" ## type of sample 
    SampleTypeName="ZC" ## type of sample 

    for _phy in Physics_info :
        RootFile = InputRootFiles[SampleType]         

        for _zz in ZC_info:
            ###  2D Hists ###
            Hist2D_name = SampleTypeName+"_"+_zz+"_"+_phy+"2D"
            Hist2D_title = SampleType+" : "+_zz+" "+_phy+" Yields distribution"
            Hist2D = ROOT.TH2D(Hist2D_name, Hist2D_title,3,0,3,4,0,4)
            Hist2D.GetXaxis().SetBinLabel(1,"LPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(2,"MPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(3,"HPtV 1pfat+0jet")

            Hist2D.GetYaxis().SetBinLabel(1,"0 tag")
            Hist2D.GetYaxis().SetBinLabel(2,"1 tag")
            Hist2D.GetYaxis().SetBinLabel(3,"2 tag")
            Hist2D.GetYaxis().SetBinLabel(4,"3+ tag")
            Hist2D.GetZaxis().SetTitle("Number of evnets")

            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                drawHists = []
                legends   = []
                outputFileName = SampleTypeName+"_"+_zz + "_" + _jet + "_"+ SR_info +"_"+_phy 
                #title = SampleType +" : " + _zz + " " + _jet + " "+ SR_info 
                title = SampleType +" : " + _zz + " " + _jet 
         
                for _tag in tag_info :
                
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue
         
                    for _ptv in ptv_info :
                        _histName = _zz + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        _legendName= _zz + " " + _tag+_jet + " " + _ptv 
         
                        print _histName 
                        print RootFile
                        tmpHist= RootFile.Get(_histName)
                        print "------------------" 
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "No"
                            continue 
                        else :
                            print "Yes"
                        print "------------------" 
                        drawHists += [tmpHist]
                        legends += [_legendName]
                        ## Fill 2D Hist 
                        FillValue_2D = Hist2D_filllValue_njet_vs_ntag(_histName , tmpHist)
                        Hist2D.Fill(FillValue_2D[0], FillValue_2D[1], FillValue_2D[2])
                        FillValue_2D_phy_vs_ntag = Hist2D_filllValue_phy_vs_ntag(_histName, tmpHist)
                        Hist2D_phy_vs_tag[_phy].Fill(FillValue_2D_phy_vs_ntag[0], FillValue_2D_phy_vs_ntag[1], FillValue_2D_phy_vs_ntag[2])
               
                ## Draw 1D Hist 
                HistPlot = HistMaker1D(drawHists,legends)
                if ('pT' in _phy)  or ('Pt' in _phy) :
                    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
                    HistPlot.DrawPlots_ATLAS(title, _phy+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[50,1500], SetLogY_1D)
                elif 'mBB' in _phy:
                    HistPlot.DrawPlots_ATLAS(title, _phy+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)
                else: 
                    HistPlot.DrawPlots_ATLAS(title, _phy,"Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)

            ## Draw 2D Hist tag vs jet 
            HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
            HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", "Merged/"+Hist2D_name,"COLZTEXT", SetLogZ_2D)

    h_cutflow_vhbb= RootFile.Get("CutFlow/PreselectionCutFlow")
    HistPlot_cutflow_vhbb = HistMaker1D([h_cutflow_vhbb],["Preselection cut flow"])
    HistPlot_cutflow_vhbb.DrawPlots_ATLAS(SampleTypeName+"Preselection cut flow", "","Number of events", ["Preselection cut flow"], "Merged/CutFlow_Preselcetion"+"_"+SampleTypeName,[0,25], SetLogY_1D)

    h_cutflow_my= RootFile.Get("CutFlow/Nominal/CutsMerged")
    HistPlot_cutflow_my = HistMaker1D([h_cutflow_my],["Merged cut flow"])
    HistPlot_cutflow_my.DrawPlots_ATLAS(SampleTypeName+"Merged cut flow", "","Number of events", [ "Merged cut flow"], "Merged/CutFlow_Merged"+"_"+SampleTypeName,[0,25], SetLogY_1D)
    ########### ZZ ###########

    SampleType="ZZ" ## type of sample 
    SampleTypeName="ZZ" ## type of sample 
    for _phy in Physics_info :
        RootFile = InputRootFiles[SampleType]         

        for _zz in ZZ_info:
            ###  2D Hists ###
            Hist2D_name = SampleTypeName+"_"+_zz+"_"+_phy+"2D"
            Hist2D_title = SampleType+" "+_zz+" "+_phy+" 2D distribution"

            Hist2D = ROOT.TH2D(Hist2D_name, Hist2D_title,3,0,3,4,0,4)
            Hist2D.GetXaxis().SetBinLabel(1,"LPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(2,"MPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(3,"HPtV 1pfat+0jet")

            Hist2D.GetYaxis().SetBinLabel(1,"0 tag")
            Hist2D.GetYaxis().SetBinLabel(2,"1 tag")
            Hist2D.GetYaxis().SetBinLabel(3,"2 tag")
            Hist2D.GetYaxis().SetBinLabel(4,"3+ tag")

            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                drawHists = []
                legends   = []
                outputFileName = SampleTypeName+"_"+_zz + "_" + _jet + "_"+ SR_info +"_"+_phy 
                #title = SampleType +" : " + _zz + " " + _jet + " "+ SR_info 
                title = SampleType +" : " + _zz + " " + _jet  
         
                for _tag in tag_info :
                
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue
         
                    for _ptv in ptv_info :
                        _histName = _zz + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        _legendName= _zz + " " + _tag+_jet + " " + _ptv 
         
                        print _histName 
                        print RootFile
                        tmpHist= RootFile.Get(_histName)
                        print "------------------" 
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Yes"
                            continue 
                        else :
                            print "No"
                        print "------------------" 
 
                        drawHists += [tmpHist]
                        legends += [_legendName]
                        ## Fill 2D Hist 
                        FillValue_2D = Hist2D_filllValue_njet_vs_ntag(_histName , tmpHist)
                        Hist2D.Fill(FillValue_2D[0], FillValue_2D[1], FillValue_2D[2])
                        FillValue_2D_phy_vs_ntag = Hist2D_filllValue_phy_vs_ntag(_histName, tmpHist)
                        Hist2D_phy_vs_tag[_phy].Fill(FillValue_2D_phy_vs_ntag[0], FillValue_2D_phy_vs_ntag[1], FillValue_2D_phy_vs_ntag[2])
                
                ## Draw 1D Hist 
                HistPlot = HistMaker1D(drawHists,legends)
                if ('pT' in _phy)  or ('Pt' in _phy) :
                    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
                    HistPlot.DrawPlots_ATLAS(title, _phy+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[50,1500], SetLogY_1D)
                elif 'mBB' in _phy:
                    HistPlot.DrawPlots_ATLAS(title, _phy+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)
                else: 
                    HistPlot.DrawPlots_ATLAS(title, _phy,"Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)
                
            ## Draw 2D Hist tag vs jet 
            HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
            HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", "Merged/"+Hist2D_name,"COLZTEXT", SetLogZ_2D)
    
    ###########  Z+C & ZZ final 2D Hist  ###########

    h_cutflow_vhbb= RootFile.Get("CutFlow/PreselectionCutFlow")
    HistPlot_cutflow_vhbb = HistMaker1D([h_cutflow_vhbb],["Preselection cut flow"])
    HistPlot_cutflow_vhbb.DrawPlots_ATLAS(SampleTypeName+"Preselection cut flow", "","Number of events", ["Preselection cut flow"], "Merged/CutFlow_Preselcetion"+"_"+SampleTypeName,[0,25], SetLogY_1D)

    h_cutflow_my= RootFile.Get("CutFlow/Nominal/CutsMerged")
    HistPlot_cutflow_my = HistMaker1D([h_cutflow_my],["Merged cut flow"])
    HistPlot_cutflow_my.DrawPlots_ATLAS(SampleTypeName+"Merged cut flow", "","Number of events", [ "Merged cut flow"], "Merged/CutFlow_Merged"+"_"+SampleTypeName,[0,25], SetLogY_1D)
    
  ###########  qqZLL ###########

    SampleType="qqZllHcc" ## type of sample 
    SampleTypeName="qqZH" ## type of sample 

    for _phy in Physics_info :
        RootFile = InputRootFiles[SampleType]         

        for _zz in qqZH_info:
            ###  2D Hists ###
            Hist2D_name = SampleTypeName+"_"+_zz+"_"+_phy+"2D"
            Hist2D_title = SampleType+" : "+_zz+" "+_phy+" Yields distribution"
            Hist2D = ROOT.TH2D(Hist2D_name, Hist2D_title,3,0,3,4,0,4)
            Hist2D.GetXaxis().SetBinLabel(1,"LPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(2,"MPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(3,"HPtV 1pfat+0jet")

            Hist2D.GetYaxis().SetBinLabel(1,"0 tag")
            Hist2D.GetYaxis().SetBinLabel(2,"1 tag")
            Hist2D.GetYaxis().SetBinLabel(3,"2 tag")
            Hist2D.GetYaxis().SetBinLabel(4,"3+ tag")
            Hist2D.GetZaxis().SetTitle("Number of evnets")

            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                drawHists = []
                legends   = []
                outputFileName = SampleTypeName+"_"+_zz + "_" + _jet + "_"+ SR_info +"_"+_phy 
                #title = SampleType +" : " + _zz + " " + _jet + " "+ SR_info 
                title = SampleType +" : " + _zz + " " + _jet 
         
                for _tag in tag_info :
                
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue
         
                    for _ptv in ptv_info :
                        _histName = _zz + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        _legendName= _zz + " " + _tag+_jet + " " + _ptv 
         
                        print _histName 
                        print RootFile
                        tmpHist= RootFile.Get(_histName)
                        print "------------------" 
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "No"
                            continue 
                        else :
                            print "Yes"
                        print "------------------" 
                        drawHists += [tmpHist]
                        legends += [_legendName]
                        ## Fill 2D Hist 
                        FillValue_2D = Hist2D_filllValue_njet_vs_ntag(_histName , tmpHist)
                        Hist2D.Fill(FillValue_2D[0], FillValue_2D[1], FillValue_2D[2])
                        print "_+_+_+_+_+_+_+_+_+_+_+_+_+_+__+_+_+_ %s _+_+_+_+_+_+__+_+_"%_histName
                        FillValue_2D_phy_vs_ntag = Hist2D_filllValue_phy_vs_ntag(_histName, tmpHist)
                        print "tag %f phy %f int %f"%(FillValue_2D_phy_vs_ntag[0], FillValue_2D_phy_vs_ntag[1], FillValue_2D_phy_vs_ntag[2])
                        Hist2D_phy_vs_tag[_phy].Fill(FillValue_2D_phy_vs_ntag[0], FillValue_2D_phy_vs_ntag[1], FillValue_2D_phy_vs_ntag[2])
               
                ## Draw 1D Hist 
                HistPlot = HistMaker1D(drawHists,legends)
                if ('pT' in _phy)  or ('Pt' in _phy) :
                    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
                    HistPlot.DrawPlots_ATLAS(title, _phy+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[50,1500], SetLogY_1D)
                elif 'mBB' in _phy:
                    HistPlot.DrawPlots_ATLAS(title, _phy+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)
                else: 
                    HistPlot.DrawPlots_ATLAS(title, _phy,"Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)

            ## Draw 2D Hist tag vs jet 
            HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
            HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", "Merged/"+Hist2D_name,"COLZTEXT", SetLogZ_2D)

    h_cutflow_vhbb= RootFile.Get("CutFlow/PreselectionCutFlow")
    HistPlot_cutflow_vhbb = HistMaker1D([h_cutflow_vhbb],["Preselection cut flow"])
    HistPlot_cutflow_vhbb.DrawPlots_ATLAS(SampleTypeName+"Preselection cut flow", "","Number of events", ["Preselection cut flow"], "Merged/CutFlow_Preselcetion"+"_"+SampleTypeName,[0,25], SetLogY_1D)

    h_cutflow_my= RootFile.Get("CutFlow/Nominal/CutsMerged")
    HistPlot_cutflow_my = HistMaker1D([h_cutflow_my],["Merged cut flow"])
    HistPlot_cutflow_my.DrawPlots_ATLAS(SampleTypeName+"Merged cut flow", "","Number of events", [ "Merged cut flow"], "Merged/CutFlow_Merged"+"_"+SampleTypeName,[0,25], SetLogY_1D)

  ###########  qqZLL ###########

    SampleType="ggZllHcc" ## type of sample 
    SampleTypeName="ggZH" ## type of sample 

    for _phy in Physics_info :
        RootFile = InputRootFiles[SampleType]         

        for _zz in ggZH_info:
            ###  2D Hists ###
            Hist2D_name = SampleTypeName+"_"+_zz+"_"+_phy+"2D"
            Hist2D_title = SampleType+" : "+_zz+" "+_phy+" Yields distribution"
            Hist2D = ROOT.TH2D(Hist2D_name, Hist2D_title,3,0,3,4,0,4)
            Hist2D.GetXaxis().SetBinLabel(1,"LPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(2,"MPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(3,"HPtV 1pfat+0jet")

            Hist2D.GetYaxis().SetBinLabel(1,"0 tag")
            Hist2D.GetYaxis().SetBinLabel(2,"1 tag")
            Hist2D.GetYaxis().SetBinLabel(3,"2 tag")
            Hist2D.GetYaxis().SetBinLabel(4,"3+ tag")
            Hist2D.GetZaxis().SetTitle("Number of evnets")

            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                drawHists = []
                legends   = []
                outputFileName = SampleTypeName+"_"+_zz + "_" + _jet + "_"+ SR_info +"_"+_phy 
                #title = SampleType +" : " + _zz + " " + _jet + " "+ SR_info 
                title = SampleType +" : " + _zz + " " + _jet 
         
                for _tag in tag_info :
                
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue
         
                    for _ptv in ptv_info :
                        _histName = _zz + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        _legendName= _zz + " " + _tag+_jet + " " + _ptv 
         
                        print _histName 
                        print RootFile
                        tmpHist= RootFile.Get(_histName)
                        print "------------------" 
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "No"
                            continue 
                        else :
                            print "Yes"
                        print "------------------" 
                        drawHists += [tmpHist]
                        legends += [_legendName]
                        ## Fill 2D Hist 
                        FillValue_2D = Hist2D_filllValue_njet_vs_ntag(_histName , tmpHist)
                        Hist2D.Fill(FillValue_2D[0], FillValue_2D[1], FillValue_2D[2])
                        FillValue_2D_phy_vs_ntag = Hist2D_filllValue_phy_vs_ntag(_histName, tmpHist)
                        Hist2D_phy_vs_tag[_phy].Fill(FillValue_2D_phy_vs_ntag[0], FillValue_2D_phy_vs_ntag[1], FillValue_2D_phy_vs_ntag[2])
               
                ## Draw 1D Hist 
                HistPlot = HistMaker1D(drawHists,legends)
                if ('pT' in _phy)  or ('Pt' in _phy) :
                    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
                    HistPlot.DrawPlots_ATLAS(title, _phy+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[50,1500], SetLogY_1D)
                elif 'mBB' in _phy:
                    HistPlot.DrawPlots_ATLAS(title, _phy+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)
                else: 
                    HistPlot.DrawPlots_ATLAS(title, _phy,"Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)

            ## Draw 2D Hist tag vs jet 
            HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
            HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", "Merged/"+Hist2D_name,"COLZTEXT", SetLogZ_2D)

    h_cutflow_vhbb= RootFile.Get("CutFlow/PreselectionCutFlow")
    HistPlot_cutflow_vhbb = HistMaker1D([h_cutflow_vhbb],["Preselection cut flow"])
    HistPlot_cutflow_vhbb.DrawPlots_ATLAS(SampleTypeName+"Preselection cut flow", "","Number of events", ["Preselection cut flow"], "Merged/CutFlow_Preselcetion"+"_"+SampleTypeName,[0,25], SetLogY_1D)

    h_cutflow_my= RootFile.Get("CutFlow/Nominal/CutsMerged")
    HistPlot_cutflow_my = HistMaker1D([h_cutflow_my],["Merged cut flow"])
    HistPlot_cutflow_my.DrawPlots_ATLAS(SampleTypeName+"Merged cut flow", "","Number of events", [ "Merged cut flow"], "Merged/CutFlow_Merged"+"_"+SampleTypeName,[0,25], SetLogY_1D)

    HistPlot2D_phy_vs_ntag = {}
    for _phy in Physics_info :
        HistPlot2D_phy_vs_ntag[_phy] = HistMaker2D(Hist2D_phy_vs_tag[_phy], Hist2D_phy_vs_tag_title[_phy])
        HistPlot2D_phy_vs_ntag[_phy].DrawPlots_ATLAS(Hist2D_phy_vs_tag_title[_phy], "c-tag","", "Merged/"+Hist2D_phy_vs_tag_name[_phy],"COLZTEXT", SetLogZ_2D)

###########  qqZLL ###########

def FillHist_Kinematic(SampleType, SampleTypeName, process_info,Hist2D_phy_vs_tag)
    for _phy in Physics_info :
        RootFile = InputRootFiles[SampleType]         

        for _process in process_info:
            ###  2D Hists ###
            Hist2D_name = SampleTypeName+"_"+_process+"_"+_phy+"2D"
            Hist2D_title = SampleType+" : "+_process+" "+_phy+" Yields distribution"
            Hist2D = ROOT.TH2D(Hist2D_name, Hist2D_title,3,0,3,4,0,4)
            Hist2D.GetXaxis().SetBinLabel(1,"LPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(2,"MPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(3,"HPtV 1pfat+0jet")

            Hist2D.GetYaxis().SetBinLabel(1,"0 tag")
            Hist2D.GetYaxis().SetBinLabel(2,"1 tag")
            Hist2D.GetYaxis().SetBinLabel(3,"2 tag")
            Hist2D.GetYaxis().SetBinLabel(4,"3+ tag")
            Hist2D.GetZaxis().SetTitle("Number of evnets")

            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                drawHists = []
                legends   = []
                outputFileName = SampleTypeName+"_"+_process + "_" + _jet + "_"+ SR_info +"_"+_phy 
                #title = SampleType +" : " + _process + " " + _jet + " "+ SR_info 
                title = SampleType +" : " + _process + " " + _jet 
         
                for _tag in tag_info :
                
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue
         
                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        _legendName= _process + " " + _tag+_jet + " " + _ptv 
         
                        print _histName 
                        print RootFile
                        tmpHist= RootFile.Get(_histName)
                        print "------------------" 
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "No"
                            continue 
                        else :
                            print "Yes"
                        print "------------------" 
                        drawHists += [tmpHist]
                        legends += [_legendName]
                        ## Fill 2D Hist 
                        FillValue_2D = Hist2D_filllValue_njet_vs_ntag(_histName , tmpHist)
                        Hist2D.Fill(FillValue_2D[0], FillValue_2D[1], FillValue_2D[2])
                        FillValue_2D_phy_vs_ntag = Hist2D_filllValue_phy_vs_ntag(_histName, tmpHist)
                        Hist2D_phy_vs_tag[_phy].Fill(FillValue_2D_phy_vs_ntag[0], FillValue_2D_phy_vs_ntag[1], FillValue_2D_phy_vs_ntag[2])
               
                ## Draw 1D Hist 
                HistPlot = HistMaker1D(drawHists,legends)
                if ('pT' in _phy)  or ('Pt' in _phy) :
                    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
                    HistPlot.DrawPlots_ATLAS(title, _phy+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[50,1500], SetLogY_1D)
                elif 'mBB' in _phy:
                    HistPlot.DrawPlots_ATLAS(title, _phy+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)
                else: 
                    HistPlot.DrawPlots_ATLAS(title, _phy,"Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)

            ## Draw 2D Hist tag vs jet 
            HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
            HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", "Merged/"+Hist2D_name,"COLZTEXT", SetLogZ_2D)

    h_cutflow_vhbb= RootFile.Get("CutFlow/PreselectionCutFlow")
    HistPlot_cutflow_vhbb = HistMaker1D([h_cutflow_vhbb],["Preselection cut flow"])
    HistPlot_cutflow_vhbb.DrawPlots_ATLAS(SampleTypeName+"Preselection cut flow", "","Number of events", ["Preselection cut flow"], "Merged/CutFlow_Preselcetion"+"_"+SampleTypeName,[0,25], SetLogY_1D)

    h_cutflow_my= RootFile.Get("CutFlow/Nominal/CutsMerged")
    HistPlot_cutflow_my = HistMaker1D([h_cutflow_my],["Merged cut flow"])
    HistPlot_cutflow_my.DrawPlots_ATLAS(SampleTypeName+"Merged cut flow", "","Number of events", [ "Merged cut flow"], "Merged/CutFlow_Merged"+"_"+SampleTypeName,[0,25], SetLogY_1D)

    HistPlot2D_phy_vs_ntag = {}
    for _phy in Physics_info :
        HistPlot2D_phy_vs_ntag[_phy] = HistMaker2D(Hist2D_phy_vs_tag[_phy], Hist2D_phy_vs_tag_title[_phy])
        HistPlot2D_phy_vs_ntag[_phy].DrawPlots_ATLAS(Hist2D_phy_vs_tag_title[_phy], "c-tag","", "Merged/"+Hist2D_phy_vs_tag_name[_phy],"COLZTEXT", SetLogZ_2D)



if __name__ == "__main__":
    main()
