#!/bin/bash 
rm -r output/BDT_VHcc_TTTL_1of2
rm -r output/BDT_VHcc_TTTL_2of2
rm -r output/BDT_VHcc_TTTL
TarDir="output/Summary/Summary_BDT_nodR_noJetVetoCut_with_pTV/Blind"
#TarDir="output/Summary/Summary_BDT_nodR_noJetVetoCut_Old_variable/Blind"
python VHcc_BDT_2lep_Resolved_TL_MVATrainVHbb_2J_3J.py 
#python VHcc_BDT_2lep_Resolved_TL_MVATrainVHbb.py
mkdir -p  $TarDir
cp -r output/BDT_VHcc_TTTL_1of2 $TarDir 
cp  -r output/BDT_VHcc_TTTL_2of2 $TarDir 
cp  -r output/BDT_VHcc_TTTL $TarDir 

