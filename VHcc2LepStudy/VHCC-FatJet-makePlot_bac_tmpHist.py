#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D

####################################################
#  #################################################
#  #
#  #        User defined variable 
#  #
#  #################################################
####################################################
SignalScale=10
SetLogY_1D = True
SetLogZ_2D = True 
SwitchBBToCC = True # switch name with BB to CC
####################################################
#  #################################################
#  #
#  #        User defined drawing function
#  #
#  #################################################
####################################################
def CalCTagEff(histName,CTagEff):
    
    Eff = 1.0
    c = CTagEff["c"]
    b = CTagEff["b"]
    l = CTagEff["l"]
    if "H125_" in histName:
        Eff = Eff * b * b
    elif "H125cc_" in histName:
        Eff = Eff * c * c
    elif "cc_" in histName:
        Eff = Eff * c * c
    elif "bb_" in histName:
        Eff = Eff * b * b
    elif "ll_" in histName:
        Eff = Eff * l * l
    elif "bc_" in histName:
        Eff = Eff * b * c
    elif "bl_" in histName:
        Eff = Eff * b * l
    elif "cl_" in histName:
        Eff = Eff * c * l
    elif "b_" in histName:
        Eff = Eff * b 
    elif "c_" in histName:
        Eff = Eff * c
    elif "l_" in histName:
        Eff = Eff * l
    print "CalCTagEff c-%f b-%f l-%f  Eff-%f"%(c,b,l,Eff)
        
    return Eff
def Hist2D_filllValue_phy_vs_ptv_simple(hist1DName, hist1D, windowRange=[]):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    sumOfWeights = hist1D.GetSumOfWeights()
    sumOfWeights_window = sumOfWeights
    if "ggZllH" in hist1DName:
        print "??????????????   test ??????? %s "%hist1DName
        #print "Getintegral    : %f" %hist1D.GetIntegral()
        print "?? integral       : %f" %hist1D.Integral()
        print "?? sumOfWeights   : %f" %hist1D.GetSumOfWeights()
        print "?? Binwidth       : %f" %hist1D.GetBinWidth(1)
        print "?? Binwidth       : %f" %hist1D.GetBinWidth(2)
        print "?? integral/Binwidth : %f" % (hist1D.Integral()*1.0/hist1D.GetBinWidth(1))
        total = 0 
        for  x in range(0,hist1D.GetNbinsX()) :
            cot = hist1D.GetBinContent(x+1)
            center= hist1D.GetBinCenter(x+1)
            total += cot 
            print "?? x %d bincenter %f : %f"%(x,center,cot)
        print "?? total %f"    %total

    if len( windowRange )>0:
        #sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0/hist1D.GetBinWidth(2)
        sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0
    
    if "0_250ptv" in histName:
        ptv = 0.5
    elif "250_400ptv" in histName:
        ptv = 1.5
    elif "400ptv" in histName:
        ptv = 2.5

    if "ggZllH125cc_" in  histName: 
        phy=6.5
    elif "qqZllH125cc_" in  histName: 
        phy=5.5
    elif "ggZllH125_" in  histName: 
        phy=4.5
    elif "qqZllH125_" in  histName: 
        phy=3.5
    elif "ggZZ" in  histName: 
        phy=2.5
    elif "ZZ" in  histName: 
        phy=1.5
    elif "Z" in  histName: 
        phy=0.5
    
    x = ptv 
    y = phy
    return (x, y, sumOfWeights, sumOfWeights_window)

def Hist2D_filllValue_phy_vs_ptv_all(hist1DName, hist1D, windowRange=[]):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    hist1D.Sumw2()
    sumOfWeights = hist1D.GetSumOfWeights()
    sumOfWeights_window = sumOfWeights
    if len( windowRange )>0:
        #sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0/hist1D.GetBinWidth(1)
        sumOfWeights_window= hist1D.Integral(windowRange[0],windowRange[1])*1.0
    
    if "0_250ptv" in histName:
        ptv = 0.5
    elif "250_400ptv" in histName:
        ptv = 1.5
    elif "400ptv" in histName:
        ptv = 2.5

    if "ggZllH125cc_" in  histName: 
        phy=23.5
    elif "qqZllH125cc_" in  histName: 
        phy=22.5
    elif "ggZllH125_" in  histName: 
        phy=21.5
    elif "qqZllH125_" in  histName: 
        phy=20.5
    elif "ggZZcc_" in  histName: 
        phy=19.5
    elif "ggZZbb_" in  histName: 
        phy=18.5
    elif "ggZZll_" in  histName: 
        phy=17.5
    elif "ggZZcl_" in  histName: 
        phy=16.5
    elif "ggZZbl_" in  histName: 
        phy=15.5
    elif "ggZZbc_" in  histName: 
        phy=14.5
    elif "ZZcc_" in  histName: 
        phy=13.5
    elif "ZZbb_" in  histName: 
        phy=12.5
    elif "ZZll_" in  histName: 
        phy=11.5
    elif "ZZcl_" in  histName: 
        phy=10.5
    elif "ZZbl_" in  histName: 
        phy=9.5
    elif "ZZbc_" in  histName: 
        phy=8.5
    elif "ZZbkg_" in  histName: 
        phy=7.5
    elif "Zcc_" in  histName: 
        phy=6.5
    elif "Zbb_" in  histName: 
        phy=5.5
    elif "Zll_" in  histName: 
        phy=4.5
    elif "Zcl_" in  histName: 
        phy=3.5
    elif "Zbl_" in  histName: 
        phy=2.5
    elif "Zbc_" in  histName: 
        phy=1.5
    elif "Z_" in  histName: 
        phy=0.5
    
    x = ptv 
    y = phy
    return (x, y, sumOfWeights, sumOfWeights_window)

def Hist2D_filllValue_phy_vs_ntag(hist1DName, hist1D):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    integral = hist1D.GetSumOfWeights()
    
    if "0tag" in histName:
        tag = 0.5
    elif "1tag" in histName:
        tag = 1.5
    elif "2tag" in histName:
        tag = 2.5
    elif "3ptag" in histName:
        tag = 3.5

    if "ZZbkg" in  histName: 
        phy=0.5
    elif "ZZ" in histName:
        phy=1.5
    elif "Zcc" in histName:
        phy=2.5
    elif "Zcl" in histName:
        phy =3.5 
    elif "qqZll" in histName :
        phy=5.5
    elif "ggZll" in histName :
        phy=6.5
    elif "Zl" in histName :
        phy=4.5
    
    x = tag 
    y = phy
    return (tag, phy, integral)

def Hist2D_filllValue_njet_vs_ntag(hist1DName, hist1D):
    ''' fill n-jet(LptV MptV HptV) vs n-tag 2D Plot '''
    histName = hist1DName
    integral = hist1D.GetSumOfWeights()

    ## x value 
    if "0_250ptv" in histName :
        ptv = 0
    elif "250_400ptv" in histName :
       ptv = 1
    else:
        ptv = 2

    if "0tag" in histName:
        tag = 0.5
    elif "1tag" in histName:
        tag = 1.5
    elif "2tag" in histName:
        tag = 2.5
    elif "3ptag" in histName:
        tag = 3.5
    
    ## y value 
    if "2jet" in histName:
        jet = 0.5 
    elif "3jet" in histName:
        jet =1.5
    elif "4pjet" in histName:
        jet =2.5

    y = tag 
    x = ptv + 0.5  
    return (x,y,integral)
        
def ptvInfo_convert(ptvInfo):
    
    if ptvInfo == "400ptv":
        Info ="ZpT > 400 GeV"
    else:
        Info ="ZpT "+ (ptvInfo.replace("_","-")).replace("ptv"," GeV")

    return Info
##########################################################
#######     Fill Hist Kinematics & Yields      
##########################################################

def FillHist_Kinematic_FatJet(SampleType, SampleTypeName, process_info, Physics_info, Hist2D_phy_vs_tag, InputRootFiles):
    '''
    Fill 1D & 2D Hists 
    Example : 
        SampleType     = "Z+C" 
        SampleTypeName = "ZC" 
        process_info   =  ZC_info
        Hist2D_phy_vs_tag     : 2D Jet Yeild Hist 
    '''
    tag_info = ["0tag", "1tag", "2tag", "3ptag"]

    jet_info = [ "1pfat0pjet"]

    ptv_info = ["0_250ptv","250_400ptv","400ptv"]
    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]

    SR_info="SR_2psubjet"  

    for _phy in Physics_info :
        RootFile = InputRootFiles[SampleType]         

        for _process in process_info:
            ###  2D Hists ###
            if SwitchBBToCC :
                #_phytmp = _phy.replace("BB","CC")
                _phytmp = _phy.replace("B","C")
            else :
                _phytmp = _phy
            
            Hist2D_name = SampleTypeName+"_"+_process+"_"+_phytmp+"2D"
            Hist2D_title = SampleType+" : "+_process+" "+_phytmp+" Yields distribution"
            Hist2D = ROOT.TH2D(Hist2D_name, Hist2D_title,3,0,3,4,0,4)
            Hist2D.GetXaxis().SetBinLabel(1,"LPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(2,"MPtV 1pfat+0jet")
            Hist2D.GetXaxis().SetBinLabel(3,"HPtV 1pfat+0jet")

            Hist2D.GetYaxis().SetBinLabel(1,"0 tag")
            Hist2D.GetYaxis().SetBinLabel(2,"1 tag")
            Hist2D.GetYaxis().SetBinLabel(3,"2 tag")
            Hist2D.GetYaxis().SetBinLabel(4,"3+ tag")
            Hist2D.GetZaxis().SetTitle("Number of evnets")

            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                drawHists = []
                legends   = []
                outputFileName = SampleTypeName+"_"+_process + "_" + _jet + "_"+ SR_info +"_"+_phytmp 
                #title = SampleType +" : " + _process + " " + _jet + " "+ SR_info 
                if "ZllHcc" in SampleType: 
                    title =  _process + " " + _jet 
                else :
                    title = SampleType +" : " + _process + " " + _jet 
                
                if SwitchBBToCC :
                    title = title.replace("B","C")
         
                for _tag in tag_info :
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue
         
                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        ####_legendName= _process + " " + _tag+_jet + " " + _ptv 
                        _legendName=_tag + " " + ptvInfo_convert(_ptv)
                        
                        #skip blank hist 
                        print "------------------" 
                        tmpHist= RootFile.Get(_histName)
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 

                        drawHists += [tmpHist]
                        legends += [_legendName]
                        ## Fill 2D Hist 
                        FillValue_2D = Hist2D_filllValue_njet_vs_ntag(_histName , tmpHist)
                        Hist2D.Fill(FillValue_2D[0], FillValue_2D[1], FillValue_2D[2])
                        FillValue_2D_phy_vs_ntag = Hist2D_filllValue_phy_vs_ntag(_histName, tmpHist)
                        Hist2D_phy_vs_tag[_phy].Fill(FillValue_2D_phy_vs_ntag[0], FillValue_2D_phy_vs_ntag[1], FillValue_2D_phy_vs_ntag[2])
               
                ## Draw 1D Hist 
                HistPlot = HistMaker1D(drawHists,legends)
                if SwitchBBToCC :
                    _phytmp = _phy.replace("B","C")
                else :
                     _phytmp = _phy

                if ('pT' in _phy)  or ('Pt' in _phy) :
                    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
                    HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[50,1500], SetLogY_1D)
                elif 'mBB' in _phy:
                    #if SwitchBBToCC :
                    #    _phytmp = _phy.replace("BB","CC")
                    #else :
                    #    _phytmp = _phy

                    HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)
                else: 
                    HistPlot.DrawPlots_ATLAS(title, _phytmp,"Number of events", legends, "Merged/"+outputFileName,[30,500], SetLogY_1D)

            ## Draw 2D Hist tag vs jet 
            HistPlot2D = HistMaker2D(Hist2D, Hist2D_title)
            HistPlot2D.DrawPlots_ATLAS(Hist2D_title, "","", "Merged/"+Hist2D_name,"COLZTEXT", SetLogZ_2D)

    h_cutflow_vhbb= RootFile.Get("CutFlow/PreselectionCutFlow")
    HistPlot_cutflow_vhbb = HistMaker1D([h_cutflow_vhbb],["Preselection cut flow"])
    HistPlot_cutflow_vhbb.DrawPlots_ATLAS(SampleTypeName+"Preselection cut flow", "","Number of events", ["Preselection cut flow"], "Merged/CutFlow_Preselcetion"+"_"+SampleTypeName,[0,25], SetLogY_1D)

    h_cutflow_my= RootFile.Get("CutFlow/Nominal/CutsMerged")
    HistPlot_cutflow_my = HistMaker1D([h_cutflow_my],["Merged cut flow"])
    HistPlot_cutflow_my.DrawPlots_ATLAS(SampleTypeName+"Merged cut flow", "","Number of events", [ "Merged cut flow"], "Merged/CutFlow_Merged"+"_"+SampleTypeName,[0,25], SetLogY_1D)
##########################################################
#######     Fill Hist Phy  
##########################################################

def FillHist_Phy_FatJet(SampleType_info, Sample_process_info, Physics_info, InputRootFiles, CTagEff=[]):
    '''
    Fill 1D & 2D Hists 
    Example : 
        SampleType_info     = ["ZC","ZZ","ZB","ZL"        ]
        process_info   =  ZC_info                         
    '''                                                   
    applyTag = False 
    if len(CTagEff) > 0: 
        applyTag = True
                                                          
    tag_info = ["0tag", "1tag", "2tag", "3ptag"]          
                                                          
    jet_info = [ "1pfat0pjet"]                            

    ptv_info = ["0_250ptv","250_400ptv","400ptv"]
    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]

    SR_info="SR_2psubjet"  

    Container_hists = {} 
    Container_legends = {} 
    Container_hists_sig = {} 
    Container_legends_sig = {} 
    Container_hists_bkg = {} 
    Container_legends_bkg = {} 

    _phy = Physics_info

    for _ptv in ptv_info :
        Container_hists[_ptv]   = []
        Container_legends[_ptv] = []
        Container_hists_sig[_ptv]   = []
        Container_legends_sig[_ptv] = []
        Container_hists_bkg[_ptv]   = []
        Container_legends_bkg[_ptv] = []
    
    for _SampleType in SampleType_info :
        RootFile = InputRootFiles[_SampleType]         

        for _process in Sample_process_info[_SampleType]:
            ###  2D Hists ###
            if SwitchBBToCC :
                #_phytmp = _phy.replace("BB","CC")
                _phytmp = _phy.replace("B","C")
            else :
                _phytmp = _phy
            
            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                
                for _tag in tag_info :
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue

                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        ####_legendName= _process + " " + _tag+_jet + " " + _ptv 
                        ####_legendName=_tag + " " + ptvInfo_convert(_ptv)
                        _legendName= _SampleType+" : "+_process  
                        
                        #skip blank hist 
                        print "------------------" 
                        tmpHist= RootFile.Get(_histName)
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 
                        
                        #tmpHist.Sumw2()
                        #tmpHist.Rebin(2)
                        
                        copyHist=tmpHist.Clone("%s_copy"%_histName)
                        copyHist.Sumw2()
                        copyHist.Rebin(2)
                        if applyTag: 
                            #tmpHist.Scale(CalCTagEff(_process+"_", CTagEff))
                            copyHist.Scale(CalCTagEff(_process+"_", CTagEff))
                        
                        #Container_hists[_ptv] += [tmpHist]
                        Container_hists[_ptv] += [copyHist]
                        Container_legends[_ptv] += [_legendName]
                        if "H" in _SampleType : 
                            print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  signal %s  %s"%(_SampleType, _legendName)
                            #Container_hists_sig[_ptv] += [tmpHist]
                            Container_hists_sig[_ptv] += [copyHist]
                            Container_legends_sig[_ptv] += [_legendName]
                        else :
                            print "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<  Bkg  %s  %s"%(_SampleType, _legendName)
                            #Container_hists_bkg[_ptv] += [tmpHist]
                            Container_hists_bkg[_ptv] += [copyHist]
                            Container_legends_bkg[_ptv] += [_legendName]

    
    for _ptv in ptv_info : 
        
        ## Draw 1D Hist 
        HistPlot = HistMaker1D(Container_hists[_ptv],Container_legends[_ptv])
        HistPlot.setHistStackSig(Container_hists_sig[_ptv],Container_legends_sig[_ptv])
        HistPlot.setHistStackBkg(Container_hists_bkg[_ptv],Container_legends_bkg[_ptv])
        HistPlot.setSignalScale(SignalScale)
        if SwitchBBToCC :
            _phytmp = _phy.replace("B","C")
        else :
             _phytmp = _phy
        if applyTag :
            outputFileName = "CTag"+CTagEff["WP"]+"_"+_ptv+"_"+ SR_info +"_"+_phytmp 
        else:
            outputFileName = _ptv+"_"+ SR_info +"_"+_phytmp 
        if applyTag : 
            title =  "CTag"+CTagEff["WP"]+": "+ ptvInfo_convert(_ptv)    
        else:
            title =  "NoTag: "+ ptvInfo_convert(_ptv)    
        
        if SwitchBBToCC :
            title = title.replace("B","C")
        print title
        print "outputFileName : %s"%outputFileName
        print "!!!!!!! VHCC-Ploter :  DrawStack !!!!! %s "%_ptv
        if ('pT' in _phy)  or ('Pt' in _phy) :
            print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PT >>>>>>>>>>>>>>>>>>>>>> %s"%_phy
            HistPlot.DrawStacks_ATLAS(title, _phytmp+" [GeV]","Number of events","Merged/stack_"+outputFileName,[50,1500], SetLogY_1D)
            #HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", Container_legends[_ptv], "Merged/"+outputFileName,[50,1500], SetLogY_1D)
        elif 'mBB' in _phy:
            HistPlot.DrawStacks_ATLAS(title, _phytmp+" [GeV]","Number of events","Merged/stack_"+outputFileName,[30,500], SetLogY_1D)
            #HistPlot.DrawPlots_ATLAS(title, _phytmp+" [GeV]","Number of events", Container_legends[_ptv], "Merged/"+outputFileName,[30,500], SetLogY_1D)
        else: 
            HistPlot.DrawStacks_ATLAS(title, _phytmp,"Number of events","Merged/stack"+outputFileName,[30,500], SetLogY_1D)
            #HistPlot.DrawPlots_ATLAS(title, _phytmp,"Number of events", Container_legends[_ptv], "Merged/"+outputFileName,[30,500], SetLogY_1D)


##########################################################
#######     Fill Hist Yield
##########################################################

def FillHist_Yield_FatJet(SampleType_info, Sample_process_info, Physics_info, InputRootFiles, CTagEff=[]):
    '''
    Fill 1D & 2D Hists 
    Example : 
        SampleType_info     = ["ZC","ZZ","ZB","ZL"        ]
        process_info   =  ZC_info                         
    '''                                                   
    applyTag = False 
    if len(CTagEff) > 0: 
        applyTag = True
                                                          
    tag_info = ["0tag", "1tag", "2tag", "3ptag"]          
                                                          
    jet_info = [ "1pfat0pjet"]                            

    ptv_info = ["0_250ptv","250_400ptv","400ptv"]
    ##ptv_info = ["75_150ptv","150_250ptv","250ptv"]

    SR_info="SR_2psubjet"  


    _phy = Physics_info

    Hist2D_phy_vs_ptv_name_all  = {} 
    Hist2D_phy_vs_ptv_name_simple  = {} 
    Hist2D_phy_vs_ptv_all       = {}
    Hist2D_phy_vs_ptv_simple       = {}
    Hist2D_phy_vs_ptv_title = {}
    Region_mBB = ["Sig","All"]

    if applyTag: 
        TagInfo="CTag"+CTagEff["WP"]
    else :
        TagInfo="NoTag"

    for _ptv in Region_mBB:
        
        if SwitchBBToCC :
            ##_phytmp = _phy.replace("BB","CC")
            _phytmp = _phy.replace("B","C")
        else :
            _phytmp = _phy

               
        if _ptv =="Sig":
            Hist2D_phy_vs_ptv_title[_ptv] = "Yields distribution : signal region "+" ("+TagInfo+")"
        elif _ptv == "All":
            Hist2D_phy_vs_ptv_title[_ptv] = "Yields distribution : all region "+" ("+TagInfo+")"

        Hist2D_phy_vs_ptv_name_all[_ptv] = "phy_vs_ptv_Yield_"+_phytmp+"_2D_"+_ptv+"Window_"+TagInfo+"_all"
        Hist2D_phy_vs_ptv_all[_ptv] = ROOT.TH2D(Hist2D_phy_vs_ptv_name_all[_ptv], Hist2D_phy_vs_ptv_title[_ptv],4,0,4,24,0,24)
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(1,"low pTV" )
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(2,"medium pTV" )
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(3,"high pTV" )
        Hist2D_phy_vs_ptv_all[_ptv].GetXaxis().SetBinLabel(4,"all pTV")

        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(24,"ggZH : ZllHcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(23,"qqZH : ZllHcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(22,"qqZH : ZllHbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(21,"ggZH : ZllHbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(20,"ggZZ : ZllZcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(19,"ggZZ : ZllZbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(18,"ggZZ : ZllZLL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(17,"ggZZ : ZllZcL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(16,"ggZZ : ZllZbL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(15,"ggZZ : ZllZbc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(14,"ZZ : ZllZcc")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(13,"ZZ : ZllZbb")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(12,"ZZ : ZllZLL")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(11,"ZZ : ZllZcL")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(10,"ZZ : ZllZbL")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(9,"ZZ : ZllZbc")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(8,"ZZ : ZZkg")
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(7,"Z+Jet : Zcc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(6,"Z+Jet : Zbb" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(5,"Z+Jet : ZLL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(4,"Z+Jet : ZcL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(3,"Z+Jet : ZbL" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(2,"Z+Jet : Zbc" )
        Hist2D_phy_vs_ptv_all[_ptv].GetYaxis().SetBinLabel(1,"Z+Jet : Zbkg" )
        Hist2D_phy_vs_ptv_all[_ptv].GetZaxis().SetTitle("Number of evnets")
        #####

        Hist2D_phy_vs_ptv_name_simple[_ptv] = "phy_vs_ptv_Yield_"+_phytmp+"_2D_"+_ptv+"Window_"+TagInfo+"_simple"
        Hist2D_phy_vs_ptv_simple[_ptv] = ROOT.TH2D(Hist2D_phy_vs_ptv_name_simple[_ptv], Hist2D_phy_vs_ptv_title[_ptv],4,0,4,7,0,7)
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(1,"low pTV" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(2,"medium pTV" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(3,"high pTV" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetXaxis().SetBinLabel(4,"all pTV")

        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(7,"ggZH: ZllHcc" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(6,"qqZH: ZllHcc" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(5,"qqZH: ZllHbb" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(4,"ggZH: ZllHbb" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(3,"ggZZ: ZllZqq" )
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(2,"ZZ: ZllZqq")
        Hist2D_phy_vs_ptv_simple[_ptv].GetYaxis().SetBinLabel(1,"Z+Jet " )
        Hist2D_phy_vs_ptv_simple[_ptv].GetZaxis().SetTitle("Number of evnets")

    for _SampleType in SampleType_info :
        RootFile = InputRootFiles[_SampleType]         

        for _process in Sample_process_info[_SampleType]:
            ###  2D Hists ###
            if SwitchBBToCC :
                #_phytmp = _phy.replace("BB","CC")
                _phytmp = _phy.replace("B","C")
            else :
                _phytmp = _phy
            
            for _jet in jet_info :
                print " ============= Main : get Hist name - %s =========== "%_jet 
                
                for _tag in tag_info :
                    if "3ptag" == _tag and _phy  == "mBB":
                        continue

                    for _ptv in ptv_info :
                        _histName = _process + "_" + _tag+_jet + "_" + _ptv +"_"+ SR_info +"_"+_phy 
                        ####_legendName= _process + " " + _tag+_jet + " " + _ptv 
                        ####_legendName=_tag + " " + ptvInfo_convert(_ptv)
                        _legendName= _SampleType+" : "+_process  
                        
                        #skip blank hist 
                        print "------------------" 
                        tmpHist = RootFile.Get(_histName)
                        print type(tmpHist)
                        if  type(tmpHist) == ROOT.TObject : 
                            print "Skip Hist  : %s"%_histName
                            continue 
                        else :
                            print "Using Hist : %s"%_histName
                        print "------------------" 
                        
                        xstart=tmpHist.FindBin(75)
                        xend=tmpHist.FindBin(145)
                        print ")))))))))))))))))))) test S: %d E:%d " %(xstart, xend)
                        xstart=tmpHist.FindBin(75.1)
                        xend=tmpHist.FindBin(144.9)

                        print ")))))))))))))))))))) S: %d E:%d " %(xstart, xend)
                        
                        if applyTag: 
                            tmpHist.Scale(CalCTagEff(_process+"_", CTagEff))
                        
                        FillValue_all = Hist2D_filllValue_phy_vs_ptv_all(_histName, tmpHist, [xstart,xend])
                        #FillValue_simple = Hist2D_filllValue_phy_vs_ptv_simple(_histName, tmpHist, [xstart, xend])
                        FillValue_simple = Hist2D_filllValue_phy_vs_ptv_simple(_histName, tmpHist)

                        Hist2D_phy_vs_ptv_all["All"].Fill(FillValue_all[0], FillValue_all[1],FillValue_all[2])
                        Hist2D_phy_vs_ptv_all["All"].Fill(3.5, FillValue_all[1],FillValue_all[2])
                        Hist2D_phy_vs_ptv_all["Sig"].Fill(FillValue_all[0], FillValue_all[1],FillValue_all[3])
                        Hist2D_phy_vs_ptv_all["Sig"].Fill(3.5, FillValue_all[1],FillValue_all[3])
                        Hist2D_phy_vs_ptv_simple["All"].Fill(FillValue_simple[0], FillValue_simple[1],FillValue_simple[2])
                        Hist2D_phy_vs_ptv_simple["All"].Fill(3.5, FillValue_simple[1],FillValue_simple[2])
                        Hist2D_phy_vs_ptv_simple["Sig"].Fill(FillValue_simple[0], FillValue_simple[1],FillValue_simple[3])
                        Hist2D_phy_vs_ptv_simple["Sig"].Fill(3.5, FillValue_simple[1],FillValue_simple[3])

    HistPlot2D_phy_vs_ptv_all={}
    HistPlot2D_phy_vs_ptv_simple={}
    Hist1D_SB={}
    Hist1D_SB_title={}
    Hist1D_SB_name={}
    
    for _region in Region_mBB : 
        
        HistPlot2D_phy_vs_ptv_all[_region] = HistMaker2D(Hist2D_phy_vs_ptv_all[_region], Hist2D_phy_vs_ptv_title[_region])
        HistPlot2D_phy_vs_ptv_simple[_region] = HistMaker2D(Hist2D_phy_vs_ptv_simple[_region], Hist2D_phy_vs_ptv_title[_region])

        HistPlot2D_phy_vs_ptv_all[_region].DrawPlots_ATLAS(Hist2D_phy_vs_ptv_title[_region], "","", "Merged/"+Hist2D_phy_vs_ptv_name_all[_region],"COLZTEXT", SetLogZ_2D)
        HistPlot2D_phy_vs_ptv_simple[_region].DrawPlots_ATLAS(Hist2D_phy_vs_ptv_title[_region], "","", "Merged/"+Hist2D_phy_vs_ptv_name_simple[_region],"COLZTEXT", SetLogZ_2D)
        
        if _region == "All":
            Hist1D_SB_title[_region] = "all region "+" ("+TagInfo+")"
        elif _region == "Sig":
            Hist1D_SB_title[_region] = "signal region "+" ("+TagInfo+")"

        Hist1D_SB_name[_region] = "SB_phy_vs_ptv_Yield_"+_phytmp+"_2D_"+_region+"Window_"+TagInfo+"_all"
        Hist1D_SB[_region]= ROOT.TH1D(Hist1D_SB_name[_region], Hist1D_SB_title[_region], 4,0,4 )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(1,"low pTV" )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(2,"medium pTV" )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(3,"high pTV" )
        Hist1D_SB[_region].GetXaxis().SetBinLabel(4,"all pTV")

        sig_int={}
        bkg_int={}
        bkg_int[1]=Hist2D_phy_vs_ptv_simple[_region].Integral(1,1,1,5)
        bkg_int[2]=Hist2D_phy_vs_ptv_simple[_region].Integral(2,2,1,5)
        bkg_int[3]=Hist2D_phy_vs_ptv_simple[_region].Integral(3,3,1,5)
        bkg_int[4]=Hist2D_phy_vs_ptv_simple[_region].Integral(4,4,1,5)

        sig_int[1]=Hist2D_phy_vs_ptv_simple[_region].Integral(1,1,6,7)
        sig_int[2]=Hist2D_phy_vs_ptv_simple[_region].Integral(2,2,6,7)
        sig_int[3]=Hist2D_phy_vs_ptv_simple[_region].Integral(3,3,6,7)
        sig_int[4]=Hist2D_phy_vs_ptv_simple[_region].Integral(4,4,6,7)
        
        Hist1D_SB[_region].SetBinContent(1,sig_int[1]/bkg_int[1])
        Hist1D_SB[_region].SetBinContent(2,sig_int[2]/bkg_int[2])
        Hist1D_SB[_region].SetBinContent(3,sig_int[3]/bkg_int[3])
        Hist1D_SB[_region].SetBinContent(4,sig_int[4]/bkg_int[4])


    HistPlot1D = HistMaker1D([Hist1D_SB["All"], Hist1D_SB["Sig"]],[Hist1D_SB_title["All"], Hist1D_SB_title["Sig"]])
    HistPlot1D.DrawPlots_ATLAS("S/B distribution"+TagInfo, "","S/B", [Hist1D_SB_title["All"], Hist1D_SB_title["Sig"]], "Merged/SB"+TagInfo,[0,4], False)





##########################################################
#######    Draw Hist : phy_vs_ntag 2D Plot     
##########################################################

def HistDraw_Plot2D_phy_vs_ntag(Physics_info, Hist2D_phy_vs_tag, Hist2D_phy_vs_tag_title, Hist2D_phy_vs_tag_name):
    '''
    Physics_info            : physics Info
    Hist2D_phy_vs_tag       : plot Hist 
    Hist2D_phy_vs_tag_title : plot title  
    Hist2D_phy_vs_tag_name  : output file name 

    '''
    HistPlot2D_phy_vs_ntag = {}
    for _phy in Physics_info :
        HistPlot2D_phy_vs_ntag[_phy] = HistMaker2D(Hist2D_phy_vs_tag[_phy], Hist2D_phy_vs_tag_title[_phy])
        HistPlot2D_phy_vs_ntag[_phy].DrawPlots_ATLAS(Hist2D_phy_vs_tag_title[_phy], "c-tag","", "Merged/"+Hist2D_phy_vs_tag_name[_phy],"COLZTEXT", SetLogZ_2D)


##########################################################################
# ########################################################################
# #
# #     main function
# #
# ########################################################################
##########################################################################

def main():
    atlas_style()

    #######################################################
    #   VHCC
    #######################################################

    #SetLogY_1D = True
    #SetLogZ_2D = True 
    #-----------------------
    # info
    #-----------------------

    # rootfile_dir  = "/home/chenye/work/CharmJet/VHCC/Result_ade/Merged_2lcc/Result_ade/combin/"
    #rootfile_dir = "/home/chenye/work/CharmJet/VHCC/SimpleROOT_tree_and_hist/Merged/comb/"
    rootfile_dir = "/home/chenye/work/CharmJet/VHCC/SimpleROOT_FlavorLabel/Merged/comb/"
    '''
    rootfile_name = [
        "hist-ZZ_Sh221.root",
        "hist-ZC_Sh221.root",
        "hist-ggZllHcc_PwPy8.root",
        "hist-qqZllHccJ_PwPy8MINLO.root"
    ]
    '''
    rootfile_name = [
        "hist-ggZllHbb_PwPy8.root",
        "hist-ggZllHcc_PwPy8.root",
        "hist-qqZllHbbJ_PwPy8MINLO.root",
        "hist-qqZllHccJ_PwPy8MINLO.root",
        "hist-ggZllZqq_Sh222.root",
        "hist-ZccZll_Sh221.root",
        "hist-ZqqZll_Sh221.root",
        "hist-ZbbZll_Sh221.root"    
        "hist-ZC_Sh221.root",
        "hist-ZQ_Sh221.root",
        "hist-ZB_Sh221.root",
        "hist-ZL_Sh221.root",
        "hist-ZZ_Sh221.root"
    ]
    Physics_info = [
        "mBBTT", "mBB","pTV", "dRVH",
        "pTBB","dRBB","dEtaBB","dPhiBB",
        "pTB1","pTB2","EtaB1","EtaB2",
        "PtFatJ1","EtaFatJ1","NtrkjetsFatJ1"
    ]


    ZC_info = ["Zcc", "Zcl", "Zl"]
    ZZ_info = ["ZZ","ZZbkg"]
    qqZH_info = ["qqZllH125cc"]
    ggZH_info = ["ggZllH125cc"]
 

    ##SR_info="topemucr_noaddbjetsr"  



    #---------------------------------------------
    # Get File 
    #

    InputRootFiles={}
    
    for filename in rootfile_name :
        file_name = rootfile_dir + filename
        print file_name
        myFile = TFile(file_name)
        if "ggZllHcc" in filename:
            InputRootFiles["ggZllHcc"] = myFile
        elif "qqZllHcc" in filename:
            InputRootFiles["qqZllHcc"] = myFile
        elif "ggZllHbb" in filename:
            InputRootFiles["ggZllHbb"] = myFile
        elif "qqZllHbb" in filename:
            InputRootFiles["qqZllHbb"] = myFile
        elif "ggZllZqq" in filename:
            InputRootFiles["ggZllZqq"] = myFile
        elif "ZC" in filename: 
            InputRootFiles["ZC"] = myFile
        elif "ZB" in filename: 
            InputRootFiles["ZB"] = myFile
        elif "ZL" in filename: 
            InputRootFiles["ZL"] = myFile
        elif "ZQ" in filename: 
            InputRootFiles["ZQ"] = myFile
        elif "ZZ" in filename: 
            InputRootFiles["ZZ"] = myFile
        elif "ZccZll" in filename: 
            InputRootFiles["ZllZCC"] = myFile
        elif "ZbbZll" in filename: 
            InputRootFiles["ZllZBB"] = myFile
        elif "ZqqZll" in filename: 
            InputRootFiles["ZllZLL"] = myFile

    #---------------------------------------------
    # Hist name & setting
    #

    Hist2D_phy_vs_tag_name  = {} 
    Hist2D_phy_vs_tag_title = {}
    Hist2D_phy_vs_tag       = {}
    for _phy in Physics_info :
        
        if SwitchBBToCC :
            ##_phytmp = _phy.replace("BB","CC")
            _phytmp = _phy.replace("B","C")
        else :
            _phytmp = _phy

        Hist2D_phy_vs_tag_name[_phy] = "phy_vs_ntag_"+_phytmp+"2D"
        Hist2D_phy_vs_tag_title[_phy] = " "+_phytmp+" Yields distribution"
        Hist2D_phy_vs_tag[_phy] = ROOT.TH2D(Hist2D_phy_vs_tag_name[_phy], Hist2D_phy_vs_tag_title[_phy],4,0,4,7,0,7)
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(1,"0 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(2,"1 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(3,"2 tag" )
        Hist2D_phy_vs_tag[_phy].GetXaxis().SetBinLabel(4,"3+ tag")

        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(7,"ggZH : ZllHcc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(6,"qqZH : ZllHcc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(5,"Z+C : Z+L" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(4,"Z+C : Z+cL" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(3,"Z+C : Z+cc" )
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(2,"ZZ : ZZcc")
        Hist2D_phy_vs_tag[_phy].GetYaxis().SetBinLabel(1,"ZZ : ZZbkg")
        Hist2D_phy_vs_tag[_phy].GetZaxis().SetTitle("Number of evnets")

    #---------------------------------------------
    # Hist name & setting
    #
    #FillHist_Kinematic_FatJet("ZC",       "ZC",   ZC_info,    Physics_info, Hist2D_phy_vs_tag, InputRootFiles)
    #FillHist_Kinematic_FatJet("ZZ",        "ZZ",   ZZ_info,    Physics_info, Hist2D_phy_vs_tag, InputRootFiles)
    #FillHist_Kinematic_FatJet("qqZllHcc",  "qqZllHcc", qqZH_info,  Physics_info, Hist2D_phy_vs_tag, InputRootFiles)
    #FillHist_Kinematic_FatJet("ggZllHcc",  "ggZllHcc", ggZH_info,  Physics_info, Hist2D_phy_vs_tag, InputRootFiles)
    #HistDraw_Plot2D_phy_vs_ntag(["mBB"], Hist2D_phy_vs_tag, Hist2D_phy_vs_tag_title, Hist2D_phy_vs_tag_name)
    
    SampleType_info = ["ZQ","ZZ","qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","ggZllZqq"]
    SampleType_info = ["qqZllHcc", "ggZllHcc","qqZllHbb","ggZllHbb","ggZllZqq","ZZ","ZQ"]

    Sample_process_info = {}
    Sample_process_info["ZQ"] = ["Zcc", "Zbb", "Zll","Zcl","Zbl","Zbc","Zc","Zb","Zl","Z"]
    Sample_process_info["ZZ"] = ["ZZcc", "ZZbc", "ZZcl","ZZbl","ZZbb","ZZll","ZZbkg"]
    Sample_process_info["qqZllHcc"] = ["qqZllH125cc"]
    Sample_process_info["ggZllHcc"] = ["ggZllH125cc"]
    Sample_process_info["qqZllHbb"] = ["qqZllH125"]
    Sample_process_info["ggZllHbb"] = ["ggZllH125"]
    Sample_process_info["ggZllZqq"] = ["ggZZcc","ggZZbc","ggZZcl","ggZZbb","ggZZbl","ggZZll"]
    CTagEffTight={}
    CTagEffLoose={}
    CTagEffLoose["c"] = 0.41
    CTagEffLoose["b"] = 0.25
    CTagEffLoose["l"] = 0.05
    CTagEffLoose["WP"] = "Loose"

    CTagEffTight["c"] = 0.18
    CTagEffTight["b"] = 0.05
    CTagEffTight["l"] = 0.005
    CTagEffTight["WP"] = "Tight"

    #FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, "mBB", InputRootFiles,CTagEffLoose)
    #FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, "mBB", InputRootFiles,CTagEffTight)
    FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, "mBB", InputRootFiles)
    
    #FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mBB", InputRootFiles, CTagEffLoose)
    FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mBB", InputRootFiles)
    

#    FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, "pTBB", InputRootFiles,CTagEffLoose)
#    FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, "pTBB", InputRootFiles,CTagEffTight)
    #FillHist_Phy_FatJet(SampleType_info,  Sample_process_info, "pTBB", InputRootFiles)
    #FillHist_Yield_FatJet(SampleType_info, Sample_process_info, "mBB", InputRootFiles, CTagEffTight)

if __name__ == "__main__":
    main()
