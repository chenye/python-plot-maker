# Python Plot Maker

Draw ATLAS Plot by python

### Structure 
- atlas-style : atlas style included 
- data : input data 
- modules : python modules with functions and classes
- examples : example code 
- makePlot.py : main code
- setup.sh : setup ROOT & python path 

### How to 
```
source setup.sh
python makePlot.py
```
### VHcc Study 
dir : VHcc2LepStudy 
With python scripts and output inside

