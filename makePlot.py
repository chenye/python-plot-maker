#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker


# #########################
# User defined drawing function
# #########################

def ClassConvert( Info ):
    xvals=[]
    yvals=[]
    xerrs=[]
    yerrs=[]

    for ith in Info.keys():
        xvals += [Info[ith].day]
        xerrs += [Info[ith].dayerr]
        yvals += [Info[ith].effval]
        yerrs += [Info[ith].efferr]
    
    return GraphMaker(xvals, yvals, xerrs, yerrs)

def DrawPlots_ATLAS(self, title, xtitle, ytitle, outputName, hists, LegendLabels):

    ''' Draw Plots '''
    #
    # Canvas
    #
    can_Fin = ROOT.TCanvas("c1","c1",850,600) 
    atlas_style()
    frame = ROOT.TH2D("frame",title, 1, 110, 300, 1, 0.4 ,0.84)
    frame.GetXaxis().SetTitle(xtitle)
    frame.GetYaxis().SetTitle(ytitle)
    frame.SetLabelSize(0.055,"X")
    frame.SetLabelSize(0.055,"Y")
    frame.SetTitleSize(0.055,"X")
    frame.SetTitleSize(0.055,"Y")
    frame.SetStats(0)

    #
    # change label 
    #
    #frame.GetXaxis().ChangeLabel( 1, -1, 0.055,-1,-1,42, "30/4");

    frame.Draw()

    #
    #  ATLAS Logo & detail Bassed on TPaveText 
    # 

    logo = ROOT.TPaveText(0.22,0.2,0.6,0.5,"NDC");
    logo.AddText("#bf{#it{ATLAS}} Internal")
    logo.AddText("Data 2018, #sqrt{s} = 13 TeV, 60.8 fb^{-1}")
    logo.GetListOfLines().Last().SetTextFont(42)
    logo.GetListOfLines().Last().SetTextSize(0.05)
    logo.AddText("Z #rightarrow #mu#mu, p_{T}^{#mu} > 25 GeV" )
    logo.GetListOfLines().Last().SetTextFont(42)
    logo.GetListOfLines().Last().SetTextSize(0.05)
    logo.AddText("0.1 < #left|#eta^{#mu}#right| < 1.05")
    logo.GetListOfLines().Last().SetTextFont(42)
    logo.GetListOfLines().Last().SetTextSize(0.05)
    logo.SetBorderSize(0)
    logo.SetFillColor(10)
    logo.SetTextColor(1)
    logo.SetTextAlign(12)
    logo.Draw()

    #
    #  TLegend 
    #

    gStyle.SetLegendTextSize(0.045)
    legeff = ROOT.TLegend(0.756,0.2,0.93,0.5)
    legeff.SetFillColor(0)
    legeff.SetBorderSize(0)
    legeff.SetFillStyle(0)
    legeff.SetTextFont(42)
    ## Draw Option
    col = [1,2,3,4,6,7]
    mark   = [20,21,22,23,24,25]

    for ith in range(0,hists):

        hists[ith].SetLineColor(col[ith])
        hists[ith].SetLineWidth(2)
        hists[ith].SetLineStyle(42)
        hists[ith].SetMarkerColor(col[ith])
        hists[ith].SetMarkerStyle(mark[ith])
        hists[ith].SetMarkerSize(1.0)
        hists[ith].Draw("PLE")
        
        legeff.AddEntry(hists[ith],LegendLabels[ith],"PLE")       
            
    legeff.Draw()

    #
    # Save 
    #
    name = outputName
    can_Fin.SaveAs("%s.pdf"%name)
    can_Fin.SaveAs("%s.C"%name)

# #########################
# main function
# #########################

def main():
    atlas_style()

    ## read form file ###
    Infos = readtxt("data/Final_Info_ztp_20181210.txt")

    ## info DProcess ## 
    thrInfos = {}
    for ith in range (1,7):
        thrInfos[ith]=ThrInfo(ith)

    for ith in Infos.keys():
        for day in Infos[ith]:
            print "ith %s day %s"%(ith,day)
            #print Infos[ith][day] 
            thrInfos[ith].AddRunInfo(Infos[ith][day])

    
    graph = ClassConvert(thrInfos)

    graph.MakeGraphErrors()
    graph.DrawPlots_ATLAS("Effitiency vs time","day/month in 2018"," L1 muon barrel trigger efficiency", "Eff_vs_time")


if __name__ == "__main__":
    main()
