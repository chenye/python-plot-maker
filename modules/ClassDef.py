#!/usr/bin/python2.7

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow,log

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH1D,TLegend, TGaxis, TPaveText,TColor
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector, TMath
from ROOT import gROOT, gDirectory,gStyle,gPad
from ROOT import TPaveText, TLegend, TPie
from ROOT import kDeepSea, kGray 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
import numpy as np

#from readTxt import readtxt



###########################################################################
###########################################################################
### 
###      Class for RPC                                                      
### 
###########################################################################
###########################################################################

class RunInfo:
    ''' class to install Eff Info'''
    def __init__(self,run, day, ith, effval, efferr):

        self.run = run 
        self.day = day
        self.ith = ith
        self.effval = effval
        self.efferr = efferr


    def printInfo(self):
        print "ith %s day %s Run %s effval %s efferr %s " % (self.ith,self.day,self.run,self.effval,self.efferr)

class ThrInfo:
    '''class for threshols '''

    def __init__(self,ith):

        self.ith = ith
        self.day = []
        self.dayerr = []
        self.effval = []
        self.efferr = []
    
    def AddRunInfo( self, Info ):

        self.day += [Info.day]
        self.dayerr += [0]
        self.effval += [Info.effval]
        self.efferr += [Info.efferr]
    


###########################################################################
###########################################################################
###
###     Class for TGraphErrors 
###
###########################################################################
###########################################################################

class GraphMaker:

    '''make graph '''

    def __init__(self, xvals, yvals, xerrs,  yerrs):
        ## xval err are dictionary num(0~n)
        self.x_vals = xvals
        self.x_errs = xerrs
        self.y_vals = yvals
        self.y_errs = yerrs
        self.nGroup = len(xvals)
        self.nbins = {}
        self.graphErrs = {}
        
        for i in range(0,self.nGroup):
            self.nbins[i]=len(xvals[i])
        
        print "ngroup : %d"%self.nGroup 

    def setATLAS(self):

        ''' ATLAS Logo & detail Bassed on TPaveText '''
    
        logo = ROOT.TPaveText(0.22,0.2,0.6,0.5,"NDC");
        logo.AddText("#bf{#it{ATLAS}} Internal")
        logo.AddText("Data 2018, #sqrt{s} = 13 TeV, 60.8 fb^{-1}")
        logo.GetListOfLines().Last().SetTextFont(42)
        logo.GetListOfLines().Last().SetTextSize(0.05)
        logo.AddText("Z #rightarrow #mu#mu, p_{T}^{#mu} > 25 GeV" )
        logo.GetListOfLines().Last().SetTextFont(42)
        logo.GetListOfLines().Last().SetTextSize(0.05)
        logo.AddText("0.1 < #left|#eta^{#mu}#right| < 1.05")
        logo.GetListOfLines().Last().SetTextFont(42)
        logo.GetListOfLines().Last().SetTextSize(0.05)
        logo.SetBorderSize(0)
        logo.SetFillColor(10)
        logo.SetTextColor(1)
        logo.SetTextAlign(12)
        
        return logo  


    def MakeGraphErrors(self):

        ''' Use list info  to make TGrapErrors Plot '''

        can_Fin = ROOT.TCanvas("c1","c1",850,600) 
        MarkerStyle=[21,20,22]
        for i in range(0,self.nGroup):
            Style=i%3
            print "style %d" %Style
            gr = ROOT.TGraphErrors(self.nbins[i], array('d', self.x_vals[i]), array('d', self.y_vals[i]), array('d', self.x_errs[i]), array('d', self.y_errs[i]))
            gr.SetMarkerSize(1)
            gr.SetMarkerStyle(MarkerStyle[Style])

            self.graphErrs [i] = gr  

        
    def DrawPlots_ATLAS(self, title, xtitle, ytitle, outputName):
        ''' Draw Plots '''

        print '============ Graph Maker: Draw Plots ========'
        can_Fin = ROOT.TCanvas("c1","c1",850,600) 
        atlas_style()
        frame = ROOT.TH2D("frame",title, 1, 110, 300, 1, 0.4 ,0.84)
        frame.GetXaxis().SetTitle(xtitle)
        frame.GetYaxis().SetTitle(ytitle)
        frame.SetLabelSize(0.055,"X")
        frame.SetLabelSize(0.055,"Y")
        frame.SetTitleSize(0.055,"X")
        frame.SetTitleSize(0.055,"Y")

        frame.GetXaxis().ChangeLabel( 1, -1, 0.055,-1,-1,42, "30/4");
        frame.GetXaxis().ChangeLabel( 2, -1, 0.055,-1,-1,42, "20/5");
        frame.GetXaxis().ChangeLabel( 3, -1, 0.055,-1,-1,42, "9/6");
        frame.GetXaxis().ChangeLabel( 4, -1, 0.055,-1,-1,42, "29/6");
        frame.GetXaxis().ChangeLabel( 5, -1, 0.055,-1,-1,42, "19/7");
        frame.GetXaxis().ChangeLabel( 6, -1, 0.055,-1,-1,42, "8/8");
        frame.GetXaxis().ChangeLabel( 7, -1, 0.055,-1,-1,42, "28/8");
        frame.GetXaxis().ChangeLabel( 8, -1, 0.055,-1,-1,42, "18/9");
        frame.GetXaxis().ChangeLabel( 9, -1, 0.055,-1,-1,42, "8/10");
        frame.GetXaxis().ChangeLabel( 10, -1, 0.055,-1,-1,42, "28/10");
        frame.SetStats(0)
        frame.Draw()

        gStyle.SetLegendTextSize(0.045)
        legeff = ROOT.TLegend(0.756,0.2,0.93,0.5)
        legeff.SetFillColor(0)
        legeff.SetBorderSize(0)
        legeff.SetFillStyle(0)
        legeff.SetTextFont(42)
        #legeff.SetTextSize(0.055)
    
        ## Draw Option
        col    = [1,2,3,4,6,7]
        mark   = [20,21,22,23,24,25]
        MUi = [4,6,10,11,20,21]
 
        for ith in range(0,self.nGroup):

            self.graphErrs[ith].SetLineColor(col[ith])
            self.graphErrs[ith].SetLineWidth(2)
            self.graphErrs[ith].SetLineStyle(42)
            self.graphErrs[ith].SetMarkerStyle(mark[ith])
            self.graphErrs[ith].SetMarkerColor(col[ith])
            self.graphErrs[ith].SetMarkerSize(1.0)
            self.graphErrs[ith].Draw("eP")
            legeff.AddEntry(self.graphErrs[ith],'L1 MU%d'%(MUi[ith]),"P")       
                
        legeff.Draw()
        
        #
        # Save 
        #
        logo=self.setATLAS()
        logo.Draw()
        name = outputName
        can_Fin.SaveAs("%s.pdf"%name)
        can_Fin.SaveAs("%s.C"%name)
    
###########################################################################
###########################################################################
###
###     Class for Hist1D 
###
###########################################################################
###########################################################################

class HistMaker1D:

    ''' make Hist combine plot 1D '''

    def __init__(self, hists, labels):

        ## xval err are dictionary num(0~n)
        ## used for nornal hist Plot
        self.nGroup = len(hists)
        self.hists = hists
        self.histsLabel = labels #Lagens name
        print "Initial Hist1DMaker : Hists & histslabel"
        print self.hists
        print self.histsLabel

        ## Used for TPaveLatex Logo 
        self.ATLASTitle_x1 = 0.62
        self.ATLASTitle_y1 = 0.8
        self.ATLASTitle_x2 = 0.88
        self.ATLASTitle_y2 = 0.88
        
        ## Used for TLegend Location
        self.Legend_x1_plot = 0.7
        self.Legend_y1_plot = 0.4
        self.Legend_x2_plot = 0.93
        self.Legend_y2_plot = 0.77

        self.Legend_x1_stack = 0.7 
        self.Legend_y1_stack = 0.2
        self.Legend_x2_stack = 0.9
        self.Legend_y2_stack = 0.77

        ## color list 
        self.colorType = "AUTO" ##  AUTO or NORM
        self.colorList = []   
        self.colorListBkg = []   
        self.colorListSig = []   

        ## Used for Stack Plot 
        self.haveSig = False 
        self.haveBkg = False
        self.hists_sig = []
        self.hists_bkg = []
        self.histsLabel_sig = [] #Lagens name
        self.histsLabel_bkg = [] #Lagens name
        self.signalScale = 1.0  # DrawSignalHist = histSig x signalScale
        
        ## Use data comparison 
        self.haveData = False 
        self.hists_data = []
        self.Blind = True
        self.MassWindow = [74.9,144.9]
        self.canvas = TCanvas("can_self","can_self",800,800)
        print "ngroup : %d"%self.nGroup 
        
    def __del__(self):
        print "Delete Hist1DMaker"
        del self.hists[:]
        del self.histsLabel[:]#Lagens name

        del self.colorList[:]   
        del self.colorListBkg[:]   
        del self.colorListSig[:] 

        ## Used for Stack Plot 
        del self.hists_sig[:] 
        del self.hists_bkg[:] 
        del self.histsLabel_sig[:]  #Lagens name
        del self.histsLabel_bkg[:]  #Lagens name
        del self.hists_data[:]
        del self.canvas
        ## Use data comparison 

    
    def setColorType(self,type):

        self.colorType = type

    def setColorListBkg(self,colorlist):
        self.colorListBkg = colorlist

    def setColorListSig(self,colorlist):
        self.colorListSig = colorlist
    
    def setLocation_ATLASTitle(self, x1, y1, x2, y2):

        self.ATLASTitle_x1 = x1
        self.ATLASTitle_x2 = x2
        self.ATLASTitle_y1 = y1
        self.ATLASTitle_y2 = y2

    def setLocation_Legend_plot(self, x1, y1, x2, y2):

        self.Legend_x1_plot = x1  
        self.Legend_x2_plot = x2  
        self.Legend_y1_plot = y1  
        self.Legend_y2_plot = y2  
        
    def setLocation_Legend_stack(self, x1, y1, x2, y2):
        self.Legend_x1_stack = x1  
        self.Legend_x2_stack = x2  
        self.Legend_y1_stack = y1  
        self.Legend_y2_stack = y2  

    def setATLAS(self, title):

        ''' ATLAS Logo & detail Bassed on TPaveText '''
        ''' if using : in title means create a new row'''

        x1=self.ATLASTitle_x1
        x2=self.ATLASTitle_x2
        y1=self.ATLASTitle_y1
        y2=self.ATLASTitle_y2

        titles=[]
        if ";" in title: 
            titles = title.split(";")
            y1=y1-0.038*len(titles)

        logo = ROOT.TPaveText(x1,y1,x2,y2,"NDC");
       
        txt1=logo.AddText("#bf{#it{ATLAS}} Internal")
        txt1.SetTextSize(0.06)
        txt1.SetTextFont(42)
        #logo.AddText("Data 2018, #sqrt{s} = 13 TeV, 60.8 fb^{-1}")

        txt2=logo.AddText(" ")
        txt2.SetTextFont(42)
        txt2.SetTextSize(0.02)
        
        if ";" in title :
#            title_tmp = title.split(":",1)
#            title1 = title_tmp[0]
#            title2 = title_tmp[1]
 #           txt2=logo.AddText(title1)
 #           txt2.SetTextFont(42)
 #           txt2.SetTextSize(0.035)
 #           txt2=logo.AddText(title2)
 #           txt2.SetTextFont(42)
 #           txt2.SetTextSize(0.035)
            for title1 in titles:
                txt2=logo.AddText(title1)
                txt2.SetTextFont(42)
                txt2.SetTextSize(0.035)
                txt2=logo.AddText(" ")
                txt2.SetTextFont(42)
                txt2.SetTextSize(0.01)

        else :
            txt2=logo.AddText(title)
            txt2.SetTextFont(42)
            txt2.SetTextSize(0.035)

        '''
        logo.AddText("Z #rightarrow #mu#mu, p_{T}^{#mu} > 25 GeV" )
        logo.GetListOfLines().Last().SetTextFont(42)
        logo.GetListOfLines().Last().SetTextSize(0.05)
        logo.AddText("0.1 < #left|#eta^{#mu}#right| < 1.05")
        logo.GetListOfLines().Last().SetTextFont(42)
        logo.GetListOfLines().Last().SetTextSize(0.05)
        '''
        logo.SetBorderSize(0)
        logo.SetFillColor(10)
        logo.SetTextColor(1)
        logo.SetTextAlign(12)
        
        return logo
 
    def setMassWindow(self,Window):
        self.MassWindow = Window

    def setBlind(self,Blind):
        self.Blind = Blind

    def applyBlindRegion(self):        
        xmin=self.MassWindow[0]
        xmax=self.MassWindow[1]
        xstart =(self.hists_data[0]).FindBin(xmin)
        xend   =(self.hists_data[0]).FindBin(xmax)
        for ibin in range(xstart,xend+1):
            (self.hists_data[0]).SetBinContent(ibin, -100)

        
    def setHistData(self, hists_data):
        ''' set data hist stack '''
        self.hists_data = hists_data 
        self.haveData = True
   
    def setHistStackSig(self, hists_sig, histsLabel_sig):
        ''' set signal hist stack '''
        print " ==========  setHistStackSig ========== "
        if len(hists_sig) >0 :
            self.hists_sig = hists_sig
            self.histsLabel_sig = histsLabel_sig
            self.haveSig = True
            print self.hists_sig

    def setHistStackBkg(self, hists_bkg, histsLabel_bkg):
        ''' set bkg hist stack '''
        if len(hists_bkg)> 0:
            self.hists_bkg = hists_bkg
            self.histsLabel_bkg = histsLabel_bkg
            self.haveBkg = True

    def setSignalScale(self, scale):
        '''DrawSignalHist = histSig x signalScale'''
        self.signalScale = scale  # DrawSignalHist = histSig x signalScale

    def GetRangY(self, hists, times = 1, SetLogy = False):
        ''' get Minimum and Maximum of Y axis :  return [min, max]'''

        ### get min & max ###### 
        maxY = -100000000
        minY =  100000000
        for ith in range(0,len(hists)):
            print hists[ith]
            tmp_max = hists[ith].GetMaximum()
            tmp_min = hists[ith].GetMinimum()
            GetMinContent_Limit(hists[ith], 0)
            GetMaxContent_Limit(hists[ith], 100000000000)
            
            if maxY < tmp_max :
                maxY = tmp_max 
            if minY > tmp_min:
                minY = tmp_min
        
        ## range x times
        # SetLog Y 
        if maxY <= 0 or minY <= -5 :
            SetLogy = False
        
        if minY <0:
            print "!!!!!!! GetRange Y !!!!!!!!!! Attention LogY : Minimum Y Value < 0  : MinY = %f Max  = %f"%(minY,maxY)
        if SetLogy :
            gPad.SetLogy()
            if minY <= 0 : 
                if maxY >= 100 :
                    minY = 0.01
                elif maxY >= 10 :
                    minY = 0.001
                else :
                    minY = 0.0001
            else : 
                minY = 0.1 * minY

            maxY = times* 100000 * maxY
        else: 
            if maxY > 0 :
                maxY = times * maxY
            elif maxY < 0 :
                maxY = (1.0/times) * maxY

            if minY > 0 :
                minY = 0.3 *minY
            elif minY < 0 :
                minY = 5 * minY

        return [minY,maxY]

    def GetRangY_StackHist(self, hists, times = 1, SetLogy = False):
        ''' get Minimum and Maximum of Y axis :  return [min, max]'''

        ### get min & max ###### 
        maxY = -100000000
        minY =  100000000
        Hist_stack = MergeHists(hists,"h_merge_tmp")
        tmp_max = Hist_stack.GetMaximum()
        tmp_min = Hist_stack.GetMinimum()
        if maxY < tmp_max :
            maxY = tmp_max 
        if minY > tmp_min:
            minY = tmp_min
        
        ## range x times
        # SetLog Y 
        if maxY <= 0 or minY <= -5 :
            SetLogy = False
        
        if minY <0:
            print "!!!!!!! GetRange Y !!!!!!!!!! Attention LogY : Minimum Y Value < 0  : MinY = %f Max  = %f"%(minY,maxY)
        if SetLogy :
            gPad.SetLogy()
            if minY <= 0 : 
                if maxY >= 100 :
                    minY = 0.01
                elif maxY >= 10 :
                    minY = 0.001
                else :
                    minY = 0.0001
            else : 
                minY = 0.1 * minY

            maxY = times* 100000 * maxY
        else: 
            if maxY > 0 :
                maxY = times * maxY
            elif maxY < 0 :
                maxY = (1.0/times) * maxY

            if minY > 0 :
                minY = 0.3 *minY
            elif minY < 0 :
                minY = 5 * minY

        return [minY,maxY]


    def GetRangY_ratioPlot(self, hists, times = 1):
        ''' 
        Get Minimum and Maximum of Y axis :  return [min, max]
        Compare ratio Y range for ratio plots : hist[i]/hist[0]
        '''
        ### get min & max ###### 
        maxY = -100000
        minY =  100000
        for ith in range(0,len(hists)):
#            tmp_max = hists[ith].GetMaximum()
#            tmp_min = hists[ith].GetMinimum()
            tmp_min = GetMinContent_Limit(hists[ith],0)
            tmp_max = GetMaxContent_Limit(hists[ith],1000)
            if maxY < tmp_max :
                maxY = tmp_max 
            if minY > tmp_min and tmp_min > 0:
                minY = tmp_min
        
        ## range x times
        
        if maxY > 0 :
        #        maxY = times * maxY
                maxY =  maxY +0.05
        if minY > 0 :
                minY =  minY - 0.05
        if maxY > 2.5:
            maxY =2.5
        return [minY,maxY]

    def DrawPlots_ATLAS(self, title, xtitle, ytitle, legend_names, outputName, XRange, Content = "All", SetLogy=False):

        ''' Draw Plots '''
        '''
            title : plot title 
            legends_names : list of legend names 
            outputName : output plot file name "<outputName>.pdf"
            XRange : x axis Range 
            Content : All / Sig / Bkg
        '''

        print '============ Hist Maker: Draw Plots ========'
        
        
        ###  Canvas & Frame  
        can_Fin = ROOT.TCanvas("c1","c1",850,600) 
       
        hists = []
        if Content == "All" :
            hists = self.hists
        elif Content == "Sig":
            hists = self.hists_sig
        elif Content == "Bkg":
            hists = self.hists_bkg

        # SetLog Y 
        RangY = self.GetRangY(hists, 1.7, SetLogy)

        atlas_style()
        frame = ROOT.TH2D("frame",title, 1, 110, 300, 1, 0.4 ,0.84)
        frame.GetXaxis().SetTitle(xtitle)
        frame.GetYaxis().SetTitle(ytitle)
        frame.SetLabelSize(0.055,"X")
        frame.SetLabelSize(0.055,"Y")
        frame.SetTitleSize(0.055,"X")
        frame.SetTitleSize(0.055,"Y")
        frame.SetStats(0)

        if len(XRange) > 0:
            frame.GetXaxis().SetRangeUser(XRange[0],XRange[1])
            frame.GetYaxis().SetRangeUser(RangY[0],RangY[1])

        frame.Draw()


        gStyle.SetLegendTextSize(0.045)
        legeff = ROOT.TLegend(self.Legend_x1_plot, self.Legend_y1_plot, self.Legend_x2_plot, self.Legend_y2_plot)
        #logo = ROOT.TPaveText(0.7,0.8,0.88,0.93,"NDC");
        legeff.SetFillColor(0)
        legeff.SetBorderSize(0)
        legeff.SetFillStyle(0)
        legeff.SetTextFont(42)
        legeff.SetTextSize(0.03)
    
               
        ## Draw 

        # color List
        if self.colorType == "AUTO" :
            col = AutoColorList(len(hists))
        elif self.colorType == "NORM":
            col = NormColorList(len(hists))
        
        for ith in range(0,len(hists)):

            #hists[ith].SetLineColor(colorIndex_num_start + ith)
            hists[ith].SetLineColor(col[ith])
            hists[ith].SetLineWidth(2)
            hists[ith].SetFillColor(0)
            hists[ith].SetLineStyle(42)
            #self.hists[ith].SetMarkerStyle(20)
            hists[ith].SetMarkerColor(col[ith])
            #hists[ith].SetMarkerColor(colorIndex_num_start + ith)
            hists[ith].SetMarkerSize(1.0)
            hists[ith].GetXaxis().SetTitle(xtitle)
            hists[ith].GetYaxis().SetTitle(ytitle)

            if len(XRange) > 0:
                hists[ith].GetXaxis().SetRangeUser(XRange[0],XRange[1])

            hists[ith].GetYaxis().SetRangeUser(RangY[0],RangY[1])

            if ith == 0 :
                        hists[ith].Draw("HIST")
            else :
                hists[ith].Draw("SAMEHIST")
            
            legeff.AddEntry(hists[ith],legend_names[ith],"P")       
                
        if legeff.GetNRows() > 20: 
            legeff.SetTextSize(0.02)

        legeff.Draw()   
        logo=self.setATLAS(title)
        logo.Draw()
        name = outputName
        can_Fin.SaveAs("output/%s.pdf"%name)
        can_Fin.SaveAs("output/%s.C"%name)
        hists = []
        del  can_Fin
        del  pad1
        del  pad2

    def DrawRatioPlots_ATLAS(self, title, xtitle, ytitle, legend_names, outputName, XRange, Content = "All", SetLogy=False, CopyLabel=False):

        ''' Draw Plots Ratio'''
        '''
            title : plot title 
            legends_names : list of legend names 
            outputName : output plot file name "<outputName>.pdf"
            XRange : x axis Range 
            Content : All / Sig / Bkg
        '''

        print '============ Hist Maker: Draw Ratio Plots ========'
        
        ###-----------------------------
        ##  Canvas & Frame  
        #
        
        #can_Fin = ROOT.TCanvas("c1","c1",850,600) 
        
        can_Fin, pad1, pad2 = ceateCanvasPads()
       
        hists = []
        hists_ratio = []
        if Content == "All" :
            hists = self.hists
        elif Content == "Sig":
            hists = self.hists_sig
        elif Content == "Bkg":
            hists = self.hists_bkg

        ### SetLog Y 
        RangY = self.GetRangY(hists, 1.7, SetLogy)
        

        atlas_style()
        frame = ROOT.TH2D("frame",title, 1, 110, 300, 1, 0.4 ,0.84)
        frame.GetXaxis().SetTitle(xtitle)
        frame.GetYaxis().SetTitle(ytitle)
        frame.SetLabelSize(0.055,"X")
        frame.SetLabelSize(0.055,"Y")
        frame.SetTitleSize(0.055,"X")
        frame.SetTitleSize(0.055,"Y")
        frame.SetStats(0)

        if len(XRange) > 0:
            frame.GetXaxis().SetRangeUser(XRange[0],XRange[1])
        
        frame.GetYaxis().SetRangeUser(RangY[0],RangY[1])
        
        ### Legend 
        gStyle.SetLegendTextSize(0.045)
        legeff = ROOT.TLegend(self.Legend_x1_plot, self.Legend_y1_plot, self.Legend_x2_plot, self.Legend_y2_plot)
        #logo = ROOT.TPaveText(0.7,0.8,0.88,0.93,"NDC");
        legeff.SetFillColor(0)
        legeff.SetBorderSize(0)
        legeff.SetFillStyle(0)
        legeff.SetTextFont(42)
        legeff.SetTextSize(0.03)

        ### Color Setting
        if self.colorType == "AUTO" :
            col = AutoColorList(len(hists))
        elif self.colorType == "NORM":
            col = NormColorList(len(hists))

        ### ratio Hists 
        for ith in range(0,len(hists)):
            tmpHist = createRatioHist(hists[ith],hists[0],"ratio_%d"%ith)
            hists_ratio += [tmpHist]

        RangY_ratio = self.GetRangY_ratioPlot(hists_ratio, 1.05)
     
        ### Hist Setting 
        for ith in range(0,len(hists)):
            ### Hists 
            hists[ith].SetLineColor(col[ith])
            hists[ith].SetLineWidth(2)
            hists[ith].SetFillColor(0)
            hists[ith].SetLineStyle(42)
            hists[ith].SetMarkerColor(col[ith])
            hists[ith].SetMarkerSize(1.0)
            hists[ith].GetXaxis().SetTitle(xtitle)
            hists[ith].GetYaxis().SetTitle(ytitle)
            labelsize_Y=hists[ith].GetYaxis().GetLabelSize()
            labelsize_X=hists[ith].GetXaxis().GetLabelSize()
            OffSet_Y=hists[ith].GetYaxis().GetTitleOffset()


            ### Ratio hists 
            hists_ratio[ith].SetLineColor(col[ith])
            hists_ratio[ith].SetLineWidth(2)
            hists_ratio[ith].SetFillColor(0)
            hists_ratio[ith].SetLineStyle(42)
            hists_ratio[ith].SetMarkerColor(col[ith])
            hists_ratio[ith].SetMarkerSize(1.0)
            hists_ratio[ith].GetXaxis().SetTitle(xtitle)
            hists_ratio[ith].GetYaxis().SetTitle("Ratio")
            hists_ratio[ith].GetYaxis().SetLabelSize(labelsize_Y*0.6/0.4)
            hists_ratio[ith].GetYaxis().SetTitleSize(labelsize_Y*0.6/0.4)
            hists_ratio[ith].GetYaxis().SetTitleOffset(OffSet_Y*0.4/0.6)
            hists_ratio[ith].GetXaxis().SetLabelSize(labelsize_X*0.6/0.4)
            hists_ratio[ith].GetXaxis().SetTitleSize(labelsize_X*0.6/0.4)

            if CopyLabel :
                for xbin in range(1,hists[ith].GetNbinsX()+1):
                    binLabel = hists[ith].GetXaxis().GetBinLabel(xbin)
                    hists_ratio[ith].GetXaxis().SetBinLabel(xbin,binLabel)
                    hists_ratio[ith].GetXaxis().SetLabelSize(0.045*0.6/0.4)
                    hists_ratio[ith].GetXaxis().ChangeLabel(xbin,325)
               
            ### Range X & Y Setting    
            if len(XRange) > 0:
                hists[ith].GetXaxis().SetRangeUser(XRange[0],XRange[1])
                hists_ratio[ith].GetXaxis().SetRangeUser(XRange[0],XRange[1])
                
            hists[ith].GetYaxis().SetRangeUser(RangY[0],RangY[1])
            hists_ratio[ith].GetYaxis().SetRangeUser(RangY_ratio[0],RangY_ratio[1])

        ###-----------------------------
        ##  Pad 1 
        #
        pad1.cd()

        frame.Draw()
        
        if SetLogy :
            gPad.SetLogy()

        for ith in range(0,len(hists)):
            if ith == 0 :
                hists[ith].Draw("HIST")
            else :
                hists[ith].Draw("SAMEHIST")
            
            legeff.AddEntry(hists[ith],legend_names[ith],"P")       
                
        if legeff.GetNRows() > 20: 
            legeff.SetTextSize(0.02)

        legeff.Draw()   
        logo=self.setATLAS(title)
        logo.Draw()

        
        ###-----------------------------
        ##  Pad 2
        #
        pad2.cd()
        for ith in range(0,len(hists)):
            if ith == 0 :
                hists_ratio[ith].Draw("HISTE")
            else :
                hists_ratio[ith].Draw("SAME APE")
        ###-----------------------------
        ##  Save 
        #
        name = outputName
        can_Fin.SaveAs("output/%s.pdf"%name)
        can_Fin.SaveAs("output/%s.C"%name)
        hists = []

        del  can_Fin
        del  pad1
        del  pad2

    def DrawPlotsSB_ATLAS(self, title, xtitle, ytitle, outputName, XRange, Content = "All", MODE="Origin", SetLogy=False, CopyLabel=False):

        ''' Draw Plots Ratio'''
        '''
            title : plot title 
            legends_names : list of legend names 
            outputName : output plot file name "<outputName>.pdf"
            XRange : x axis Range 
            Content : All / Sig / Bkg
        '''

        print '============ Hist Maker: Draw Ratio Plots ========'
        
        ###-----------------------------
        ##  Canvas & Frame  
        #
        
        #can_Fin = ROOT.TCanvas("c1","c1",850,600) 
        
        can_Fin, pad1, pad2 = createCanvasPads()
       
        hists = []
        hists_ratio = []

        if self.haveSig:
            histsS = self.hists_sig[0].Clone()
            if MODE=="NORM":
                histsS.Scale(1.0/histsS.Integral())
            hists+=[histsS]
        if self.haveBkg:
            histsB = self.hists_bkg[0].Clone()
            if MODE=="NORM":
                histsB.Scale(1.0/histsB.Integral())
            hists+=[histsB]
        if self.haveData:
            histsD = self.hists_data[0].Clone()
            if MODE=="NORM":
                histsD.Scale(1.0/histsD.Integral())

        ### SetLog Y 

        if MODE=="NORM":
            RangY = self.GetRangY(hists, 1.5, False)
        else:
            RangY = self.GetRangY(hists, 1.5, SetLogy)
        
        atlas_style()
        frame = ROOT.TH2D("frame",title, 1, 110, 300, 1, 0.4 ,0.84)
        frame.GetXaxis().SetTitle(xtitle)
        frame.GetYaxis().SetTitle(ytitle)
        frame.SetLabelSize(0.055,"X")
        frame.SetLabelSize(0.055,"Y")
        frame.SetTitleSize(0.055,"X")
        frame.SetTitleSize(0.055,"Y")
        frame.SetStats(0)

        if len(XRange) > 0:
            frame.GetXaxis().SetRangeUser(XRange[0],XRange[1])
        
        frame.GetYaxis().SetRangeUser(RangY[0],RangY[1])
        
        ### Legend 
        gStyle.SetLegendTextSize(0.045)
        legeff = ROOT.TLegend(self.Legend_x1_plot, self.Legend_y1_plot, self.Legend_x2_plot, self.Legend_y2_plot)
        #logo = ROOT.TPaveText(0.7,0.8,0.88,0.93,"NDC");
        legeff.SetFillColor(0)
        legeff.SetBorderSize(0)
        legeff.SetFillStyle(0)
        legeff.SetTextFont(42)
        legeff.SetTextSize(0.03)

        ### Color Setting
        if self.colorType == "AUTO" :
            col = AutoColorList(2)
        elif self.colorType == "NORM":
            col = NormColorList(2)

        ### ratio Hists 
        ratioTitle="Ratio"
        if Content == "SB":
            tmpHist = createRatioHist(histsS,histsB,"ratio_SB")
            ratioTitle="S/B"
        elif Content == "SBsqrt":
            tmpHist = createSqrtRatioHist(histsS,histsB,"ratio_SB")
            ratioTitle="S/#sqrt{B}"
        elif Content == "DataMC" and self.haveData:
            tmpHist = createDataRatioHist(histsD,histsS,histsB,"ratio_DSB")
            ratioTitle="Data/MC"
        else :
            print "Something wrong in content mode : Please Use Norm, Sqrt, data"
            return 0
        
        hists_ratio = tmpHist

        RangY_ratio = self.GetRangY_ratioPlot([hists_ratio], 1.05)
     
        ### Hist Setting 
        ### Hists 
        histsS.SetLineColor(col[1])
        histsS.SetLineWidth(2)
        histsS.SetFillColor(0)
        histsS.SetLineStyle(42)
        histsS.SetMarkerColor(col[1])
        histsS.SetMarkerSize(1.0)
        histsS.GetXaxis().SetTitle(xtitle)
        histsS.GetYaxis().SetTitle(ytitle)
        
        histsB.SetLineColor(col[0])
        histsB.SetLineWidth(2)
        histsB.SetFillColor(col[0])
        histsB.SetLineStyle(42)
        histsB.SetMarkerColor(col[0])
        histsB.SetMarkerSize(1.0)
        histsB.GetXaxis().SetTitle(xtitle)
        histsB.GetYaxis().SetTitle(ytitle)

        if self.haveData:
            histsD.SetLineColor(1)
            histsD.SetLineWidth(2)
            histsD.SetLineStyle(42)
            histsD.SetMarkerColor(1)
            histsD.SetMarkerSize(1.0)

        labelsize_Y=histsS.GetYaxis().GetLabelSize()
        labelsize_X=histsS.GetXaxis().GetLabelSize()
        OffSet_Y=histsS.GetYaxis().GetTitleOffset()

        ### Ratio hists 
        hists_ratio.SetLineColor(1)
        hists_ratio.SetLineWidth(2)
        hists_ratio.SetFillColor(0)
        hists_ratio.SetLineStyle(42)
        hists_ratio.SetMarkerColor(1)
        hists_ratio.SetMarkerSize(1.0)
        hists_ratio.GetXaxis().SetTitle(xtitle)
        hists_ratio.GetYaxis().SetTitle(ratioTitle)
        hists_ratio.GetYaxis().SetLabelSize(labelsize_Y*0.6/0.4)
        hists_ratio.GetYaxis().SetTitleSize(labelsize_Y*0.6/0.4)
        hists_ratio.GetYaxis().SetTitleOffset(OffSet_Y*0.4/0.6)
        hists_ratio.GetXaxis().SetLabelSize(labelsize_X*0.6/0.4)
        hists_ratio.GetXaxis().SetTitleSize(labelsize_X*0.6/0.4)

        if CopyLabel :
            binLabel = histsS.GetXaxis().GetBinLabel(xbin)
            hists_ratio.GetXaxis().SetBinLabel(xbin,binLabel)
            hists_ratio.GetXaxis().SetLabelSize(0.045*0.6/0.4)
            hists_ratio.GetXaxis().ChangeLabel(xbin,325)
           
        ### Range X & Y Setting    
        if len(XRange) > 0:
            histsS.GetXaxis().SetRangeUser(XRange[0],XRange[1])
            histsB.GetXaxis().SetRangeUser(XRange[0],XRange[1])
            if self.haveData:
                histsD.GetXaxis().SetRangeUser(XRange[0],XRange[1])
            hists_ratio.GetXaxis().SetRangeUser(XRange[0],XRange[1])
            
        histsS.GetYaxis().SetRangeUser(RangY[0],RangY[1])
        histsB.GetYaxis().SetRangeUser(RangY[0],RangY[1])
        if self.haveData:
            histsD.GetYaxis().SetRangeUser(RangY[0],RangY[1])
        hists_ratio.GetYaxis().SetRangeUser(RangY_ratio[0],RangY_ratio[1])

        ###-----------------------------
        ##  Pad 1 
        #
        pad1.cd()

        frame.Draw()
        
        
        if SetLogy and MODE!="NORM":
            gPad.SetLogy()

        histsB.Draw("HIST")

        if not SetLogy : 
            histsS.Scale(1000)
        
        histsS.Draw("SAMEHIST")
        

        legeff.AddEntry(histsB,"Background","f")       
        if SetLogy : 
            legeff.AddEntry(histsS,"Signal","l")       
        else:
            legeff.AddEntry(histsS,"Signal x 1000","l")       
        if Content == "DataMC" and self.haveData:
            histsD.Draw("SAMELPE")
            legeff.AddEntry(histsD,"Data","lep")       

        if legeff.GetNRows() > 20: 
            legeff.SetTextSize(0.02)

        legeff.Draw()   
        logo=self.setATLAS(title)
        logo.Draw()

        
        ###-----------------------------
        ##  Pad 2
        #
        pad2.cd()
        if Content == "DataMC" and self.haveData:
            hists_ratio.Draw("PEL")
        else:
            hists_ratio.Draw("HISTE")
        
        ###-----------------------------
        ##  Save 
        #
        name = outputName
        can_Fin.SaveAs("output/%s_Type%s_%s.pdf"%(name,Content,MODE))
        can_Fin.SaveAs("output/%s_Type%s_%s.C"%(name,Content,MODE))
        del  hists[:]
        del  can_Fin
        del  pad1
        del  pad2

    def Draw2RatioPlots_ATLAS(self, title, xtitle, ytitle, outputName, XRange, SetLogy=False, CopyLabel=False):

        ''' Draw Double Ratio Plots'''
        '''
            Aim at BDT Trainning & test (signal/bkg) comparison
            title : plot title 
            legends_names : list of legend names 
            outputName : output plot file name "<outputName>.pdf"
            XRange : x axis Range 
            Content : All / Sig / Bkg
        '''

        print '============ Hist Maker: Draw Ratio Plots ========'
        
        ###-----------------------------
        ##  Canvas & Frame  
        #
        
        #can_Fin = ROOT.TCanvas("c1","c1",850,600) 
        
        can_Fin, pad1, pad2 = createCanvasPads()
       
        hists_ratio_sig = []
        hists_ratio_bkg = []
        hists_sig = self.hists_sig
        hists_bkg = self.hists_bkg
        hists = self.hists

        print '======hist_sig'
        print self.hists_sig

        ### SetLog Y 
        RangY = self.GetRangY(hists, 2, SetLogy)
#        RangY = self.GetRangY(hists_sig+hists_bkg, 1.5, SetLogy)
        atlas_style()
        frame = ROOT.TH2D("frame",title, 1, 110, 300, 1, 0.4 ,0.84)
        frame.GetXaxis().SetTitle(xtitle)
        frame.GetYaxis().SetTitle(ytitle)
        frame.SetLabelSize(0.055,"X")
        frame.SetLabelSize(0.055,"Y")
        frame.SetTitleSize(0.055,"X")
        frame.SetTitleSize(0.055,"Y")
        frame.SetStats(0)

        if len(XRange) > 0:
            frame.GetXaxis().SetRangeUser(XRange[0],XRange[1])
        
        frame.GetYaxis().SetRangeUser(RangY[0],RangY[1])
        
        ### Legend 
        gStyle.SetLegendTextSize(0.045)
        legeff = ROOT.TLegend(self.Legend_x1_plot, self.Legend_y1_plot, self.Legend_x2_plot, self.Legend_y2_plot)
        #logo = ROOT.TPaveText(0.7,0.8,0.88,0.93,"NDC");
        legeff.SetFillColor(0)
        legeff.SetBorderSize(0)
        legeff.SetFillStyle(0)
        legeff.SetTextFont(42)
        legeff.SetTextSize(0.03)

        ### Color Setting
        if self.colorType == "AUTO" :
            col = AutoColorList(len(hists_sig))
        elif self.colorType == "NORM":
            col = NormColorList(len(hists_sig))

        ### ratio Hists 
        for ith in range(0,len(hists_sig)):
            tmpHist = createRatioHist(hists_sig[ith],hists_sig[0],"ratio_%d"%ith)
            hists_ratio_sig += [tmpHist]
            #hists_ratio += [tmpHist]

        for ith in range(0,len(hists_bkg)):
            tmpHist = createRatioHist(hists_bkg[ith],hists_bkg[0],"ratio_%d"%ith)
            hists_ratio_bkg += [tmpHist]
            #hists_ratio += [tmpHist]

        #RangY_ratio = self.GetRangY_ratioPlot(hists_ratio, 1.05)
        RangY_ratio = self.GetRangY_ratioPlot(hists_ratio_sig+hists_ratio_bkg, 1.05)
     
        ### Hist Setting 
        for ith in range(0, len(hists_ratio_sig)):
            ### Hists 
            hists_sig[ith].SetLineColor(col[1])
            hists_sig[ith].SetLineWidth(2)
            hists_sig[ith].SetFillColor(0)
            hists_sig[ith].SetLineStyle(42)
            hists_sig[ith].SetMarkerColor(col[1])
            hists_sig[ith].SetMarkerSize(1.0)
            hists_sig[ith].GetXaxis().SetTitle(xtitle)
            hists_sig[ith].GetYaxis().SetTitle(ytitle)
            
            hists_bkg[ith].SetLineColor(col[0])
            hists_bkg[ith].SetLineWidth(2)
            hists_bkg[ith].SetFillColor(0)
            hists_bkg[ith].SetLineStyle(42)
            hists_bkg[ith].SetMarkerColor(col[0])
            hists_bkg[ith].SetMarkerSize(1.0)
            hists_bkg[ith].GetXaxis().SetTitle(xtitle)
            hists_bkg[ith].GetYaxis().SetTitle(ytitle)
            labelsize_Y=hists_sig[ith].GetYaxis().GetLabelSize()
            labelsize_X=hists_sig[ith].GetXaxis().GetLabelSize()
            OffSet_Y=hists_sig[ith].GetYaxis().GetTitleOffset()

            ### Ratio hists 
            hists_ratio_sig[ith].SetLineColor(col[1])
            hists_ratio_sig[ith].SetLineWidth(2)
            hists_ratio_sig[ith].SetFillColor(0)
            hists_ratio_sig[ith].SetLineStyle(42)
            hists_ratio_sig[ith].SetMarkerColor(col[1])
            hists_ratio_sig[ith].SetMarkerSize(1.0)
            hists_ratio_sig[ith].GetXaxis().SetTitle(xtitle)
            hists_ratio_sig[ith].GetYaxis().SetTitle("Test / Train")
            hists_ratio_sig[ith].GetYaxis().SetLabelSize(labelsize_Y*0.6/0.4)
            hists_ratio_sig[ith].GetYaxis().SetTitleSize(labelsize_Y*0.6/0.4)
            hists_ratio_sig[ith].GetYaxis().SetTitleOffset(OffSet_Y*0.4/0.6)
            hists_ratio_sig[ith].GetXaxis().SetLabelSize(labelsize_X*0.6/0.4)
            hists_ratio_sig[ith].GetXaxis().SetTitleSize(labelsize_X*0.6/0.4)
            
            hists_ratio_bkg[ith].SetLineColor(col[0])
            hists_ratio_bkg[ith].SetLineWidth(2)
            hists_ratio_bkg[ith].SetFillColor(0)
            hists_ratio_bkg[ith].SetLineStyle(42)
            hists_ratio_bkg[ith].SetMarkerColor(col[0])
            hists_ratio_bkg[ith].SetMarkerSize(1.0)
            hists_ratio_bkg[ith].GetXaxis().SetTitle(xtitle)
            hists_ratio_bkg[ith].GetYaxis().SetTitle("Test / Train")
            hists_ratio_bkg[ith].GetYaxis().SetLabelSize(labelsize_Y*0.6/0.4)
            hists_ratio_bkg[ith].GetYaxis().SetTitleSize(labelsize_Y*0.6/0.4)
            hists_ratio_bkg[ith].GetYaxis().SetTitleOffset(OffSet_Y*0.4/0.6)
            hists_ratio_bkg[ith].GetXaxis().SetLabelSize(labelsize_X*0.6/0.4)
            hists_ratio_bkg[ith].GetXaxis().SetTitleSize(labelsize_X*0.6/0.4)


            if CopyLabel :
                for xbin in range(1,hists_sig[ith].GetNbinsX()+1):
                    binLabel = hists_sig[ith].GetXaxis().GetBinLabel(xbin)
                    hists_ratio_sig[ith].GetXaxis().SetBinLabel(xbin,binLabel)
                    hists_ratio_sig[ith].GetXaxis().SetLabelSize(0.045*0.6/0.4)
                    hists_ratio_sig[ith].GetXaxis().ChangeLabel(xbin,325)
                for xbin in range(1,hists_bkg[ith].GetNbinsX()+1):
                    binLabel = hists_bkg[ith].GetXaxis().GetBinLabel(xbin)
                    hists_ratio_bkg[ith].GetXaxis().SetBinLabel(xbin,binLabel)
                    hists_ratio_bkg[ith].GetXaxis().SetLabelSize(0.045*0.6/0.4)
                    hists_ratio_bkg[ith].GetXaxis().ChangeLabel(xbin,325)

               
            ### Range X & Y Setting    
            if len(XRange) > 0:
                hists_sig[ith].GetXaxis().SetRangeUser(XRange[0],XRange[1])
                hists_sig[ith].GetXaxis().SetRangeUser(XRange[0],XRange[1])
                hists_bkg[ith].GetXaxis().SetRangeUser(XRange[0],XRange[1])
                hists_bkg[ith].GetXaxis().SetRangeUser(XRange[0],XRange[1])
                
            hists_sig[ith].GetYaxis().SetRangeUser(RangY[0],RangY[1])
            hists_ratio_sig[ith].GetYaxis().SetRangeUser(RangY_ratio[0],RangY_ratio[1])
            hists_bkg[ith].GetYaxis().SetRangeUser(RangY[0],RangY[1])
            hists_ratio_bkg[ith].GetYaxis().SetRangeUser(RangY_ratio[0],RangY_ratio[1])

        ###-----------------------------
        ##  Pad 1 
        #
        pad1.cd()

        frame.Draw()
        
        if SetLogy :
            gPad.SetLogy()

        hists_sig[0].Draw("HIST")
        hists_bkg[0].Draw("SAMEHIST")
        hists_sig[1].Draw("SAMEAPE")
        hists_bkg[1].Draw("SAMEAPE")

        legeff.AddEntry(hists_sig[0], self.histsLabel_sig[0],"f")       
        legeff.AddEntry(hists_bkg[0], self.histsLabel_bkg[0],"f")       
        legeff.AddEntry(hists_sig[1], self.histsLabel_sig[1],"ep")       
        legeff.AddEntry(hists_bkg[1], self.histsLabel_bkg[1],"ep")       
                

        legeff.Draw()   
        logo=self.setATLAS(title)
        logo.Draw()

        
        ###-----------------------------
        ##  Pad 2
        #
        pad2.cd()
        #hists_ratio_sig[0].Draw("HIST")
        hists_ratio_bkg[0].Draw("HIST")
        hists_ratio_sig[1].Draw("SAMEAPE")
        hists_ratio_bkg[1].Draw("SAMEAPE")
        
        ###-----------------------------
        ##  Save 
        #
        name = outputName
        can_Fin.SaveAs("output/%s.pdf"%name)
        can_Fin.SaveAs("output/%s.C"%name)
        
    def DrawStacks_ATLAS(self, title, xtitle, ytitle, outputName, XRange, SetLogy=False):

        ''' Draw Hist Stack Plots '''

        print '============ Hist Maker: Draw Stack Plots ========'
        
        if self.haveSig == False  and self.haveBkg == False :
            return 0 
        ###   
        ###  Canvas & Frame  
        ###------------------------------------------------- 

        can_Fin = ROOT.TCanvas("c1","c1",850,600) 
       
        # SetLog Y 
        #RangY = self.GetRangY(self.hists, 1.2, SetLogy)
        MaxY = -10000
        MinY= 10e8
        if self.haveData :
            RangY_data = self.GetRangY(self.hists_data, 1.7, SetLogy)
            if MaxY < RangY_data[1] : 
                MaxY = RangY_data[1]
            if MinY > RangY_data[0] : 
                MinY = RangY_data[0]

        if self.haveSig : 
            RangY_sig = self.GetRangY(self.hists_sig, 1.7, SetLogy)
            if MaxY < RangY_sig[1] : 
                MaxY = RangY_sig[1]
            if MinY > RangY_sig[0] : 
                MinY = RangY_sig[0]

        if self.haveBkg : 
            RangY_bkg = self.GetRangY(self.hists_bkg, 1.7, SetLogy)
            if MaxY < RangY_bkg[1] : 
                MaxY = RangY_bkg[1]
            if MinY > RangY_bkg[0] : 
                MinY = RangY_bkg[0]
        
        RangY = [MinY,MaxY]

        

        atlas_style()
        frame = ROOT.TH2D("frame",title, 1, 110, 300, 1, 0.4 ,0.84)
        frame.GetXaxis().SetTitle(xtitle)
        frame.GetYaxis().SetTitle(ytitle)
        frame.SetLabelSize(0.055,"X")
        frame.SetLabelSize(0.055,"Y")
        frame.SetTitleSize(0.055,"X")
        frame.SetTitleSize(0.055,"Y")
        frame.SetStats(0)

        if len(XRange) > 0:
            frame.GetXaxis().SetRangeUser(XRange[0],XRange[1])
            frame.GetYaxis().SetRangeUser(RangY[0],RangY[1])

        frame.Draw()


        gStyle.SetLegendTextSize(0.045)
        legeff = ROOT.TLegend(self.Legend_x1_stack,self.Legend_y1_stack,self.Legend_x2_stack,self.Legend_y2_stack)
        #logo = ROOT.TPaveText(0.7,0.8,0.88,0.93,"NDC");
        legeff.SetFillColor(0)
        legeff.SetBorderSize(0)
        legeff.SetFillStyle(0)
        legeff.SetTextFont(42)
        legeff.SetTextSize(0.03)
        legendCounter = 0 
        ## Draw Option
        
        kWhite   = 0   
        kBlack   = 1   
        kGray    = 920  
        kRed     = 632  
        kGreen   = 416
        kBlue    = 600
        kYellow  = 400 
        kMagenta = 616  
        kCyan    = 432  
        kOrange  = 800
        kSpring  = 820 
        kTeal    = 840 
        kAzure   =  860 
        kViolet  = 880  
        kPink    = 900
       
        col = [kBlack, kGray+1, kRed, kBlue, kGreen, kMagenta, kCyan+1, kOrange, kSpring-5, kTeal, kAzure-4, kViolet, kPink+7]
        col = [kGray+1, kRed, kGreen, kBlue, kMagenta, kCyan+1, kOrange, kSpring-5, kTeal, kAzure-4, kViolet, kPink+7]
        
        #col =  [ kGreen, kOrange, kSpring-5, kTeal, kAzure-4, kViolet, kPink+7]
        ###  Draw 
        ###------------------------------------------------- 
        
        ## BKG 
        #self.haveSig = False
        hists_bkg = []
        hists_sig = []
        hists_data = self.hists_data 
        #self.haveBkg= False
        histsLabel_bkg = self.histsLabel_bkg
        histsLabel_sig = self.histsLabel_sig

        for tmpHist in self.hists_bkg :
            hists_bkg +=[tmpHist.Clone("%s_tmp"%tmpHist.GetName())]
        for tmpHist in self.hists_sig :
            hists_sig +=[tmpHist.Clone("%s_tmp"%tmpHist.GetName())]

        ##--------------------------------
        ##     Background  
        ##
        if self.haveBkg :
            colorListBkg = []
            # color List
         
            if self.colorType == "AUTO" :
                colorListBkg = AutoColorList(len(hists_bkg))
            elif self.colorType == "NORM":
                colorListBkg = NormColorList(len(hists_bkg))
            elif self.colorType == "DIY":
                colorListBkg = self.colorListBkg
            
            
            #colorIndex_num_start = AutoColorList(len(hists_bkg)*3)

            histStack =  ROOT.THStack("histStack","histStack")

            print "colorListBkg %d "%len(colorListBkg) 
            print "hists_bkg %d "%len(hists_bkg) 

            if len(hists_bkg) != len(histsLabel_bkg) :
                print "!!!!!!!!!! ERROR len(histsLabel_bkg) != len(hists_bkg) !!!!!!!!!!!! "
                print "ColorList:"
                print colorListBkg
                print "bkg-Labels:"
                print histsLabel_bkg
                print "bkg-hists:"
                print hists_bkg
           
            for ith in range(0,len(histsLabel_bkg)):
                #print myColor.GetColorPalette(ith)
                #hists_bkg[ith].SetLineColor(colorIndex_num_start + 3*ith)
                #hists_bkg[ith].SetFillColor(colorIndex_num_start + 3*ith)
                print "ith %s " %(ith)
                print hists_bkg[ith]
                hists_bkg[ith].SetLineColor(colorListBkg[ith])
                hists_bkg[ith].SetFillColor(colorListBkg[ith])
                hists_bkg[ith].SetLineWidth(2)
                hists_bkg[ith].GetXaxis().SetTitle(xtitle)
                hists_bkg[ith].GetYaxis().SetTitle(ytitle)
                if len(XRange) > 0:
                    hists_bkg[ith].GetXaxis().SetRangeUser(XRange[0],XRange[1])
                
                hists_bkg[ith].GetYaxis().SetRangeUser(RangY[0],RangY[1])

                histStack.Add(hists_bkg[ith])
            ## Bkg Legends 2 Column
            if len(histsLabel_bkg)%2 ==0 : 
                N_counter =int(len(histsLabel_bkg))/2
            else:
                N_counter =int(len(histsLabel_bkg)+1)/2
            
            for ith in range(0,len(histsLabel_bkg)):
                if ith %2 == 0  : 
                    num = ith/2 +1 -1 
                    legeff.AddEntry(hists_bkg[num],histsLabel_bkg[num],"f")       
                else:
                    num = (ith+1)/2 + N_counter -1
                    legeff.AddEntry(hists_bkg[num],histsLabel_bkg[num],"f")       

            
            #if len(XRange) > 0:            
            #    histStack.GetHistogram().GetXaxis().SetRangeUser(XRange[0],XRange[1])
            
            histStack.Draw("hist")
            if len(XRange) > 0:
                histStack.GetXaxis().SetLimits(XRange[0],XRange[1])
            histStack.SetMinimum(RangY[0])
            histStack.SetMaximum(RangY[1])
            histStack.GetXaxis().SetTitle(xtitle)
            histStack.GetYaxis().SetTitle(ytitle)
            can_Fin.Modified()
        ##--------------------------------
        ##     Signal 
        ##
        if self.haveSig :
            colorListSig = col
            if self.colorType == "DIY":
                if len(self.colorListSig) == len(histsLabel_sig):
                    colorListSig = self.colorListSig
            
            for ith in range(0,len(histsLabel_sig)):
                integ1 = hists_sig[ith].GetSumOfWeights()
                ##### self.hists_bkg[ith].SetLineColor(col[ith])
                hists_sig[ith].SetLineWidth(3)
                hists_sig[ith].SetLineStyle(42)
                hists_sig[ith].SetMarkerColor(colorListSig[ith])
                hists_sig[ith].SetLineColor(colorListSig[ith])
                hists_sig[ith].SetMarkerSize(1.0)
                hists_sig[ith].Scale(self.signalScale) # Scale auto call Sumw2 
                hists_sig[ith].GetXaxis().SetTitle(xtitle)
                hists_sig[ith].GetYaxis().SetTitle(ytitle)
                integ2 = hists_sig[ith].GetSumOfWeights()
                print "!!!!!!!! In classDef Draw Stack !!!!!!!!!!! %s "% histsLabel_sig[ith]
                print "!!!!!!!! Scale %f - Integral1 %f - Integral2 %f "%(self.signalScale, integ1, integ2)
                sumInteg = 0 
                for binx in range(0,hists_sig[ith].GetNbinsX()):
                    #print "!!!! Binx : %d center: %f Content : %f  "%(binx,  hists_sig[ith].GetBinCenter(binx+1),hists_sig[ith].GetBinContent(binx+1))
                    sumInteg +=hists_sig[ith].GetBinContent(binx+1)
                print "Sum of Integral %f"% sumInteg

                hists_sig[ith].GetYaxis().SetRangeUser(RangY[0],RangY[1])
                if len(XRange) > 0:
                    hists_sig[ith].GetXaxis().SetRangeUser(XRange[0],XRange[1])

                if not self.haveBkg :
                    if ith == 0: 
                        hists_sig[ith].Draw("HIST")
                    else :
                        hists_sig[ith].Draw("HISTSAME")
                else :
                    hists_sig[ith].Draw("HISTSAME")
                
                legeff.AddEntry(hists_sig[ith],histsLabel_sig[ith]+  " x %d"%(self.signalScale),"pl")       
        ##--------------------------------
        ## MerggallHist
        ##
        hists_merge=hists_bkg
        hist_total = MergeHists(hists_merge, "h_merge_all_mc")
        hist_total.SetFillStyle(3354)
        hist_total.SetFillColor(1)
        hist_total.SetLineColor(1)
        hist_total.SetMarkerSize(0)
        hist_total.SetLineWidth(0)
        hist_total.Draw("E2 SAME")
        legeff.AddEntry(hist_total, "Uncertainty","f")       
        can_Fin.Modified()
        ##--------------------------------
        ##     Data 
        ##
        if self.haveData :

            hists_data = self.hists_data 
            h_data = hists_data[0].Clone("h_data_copy")

            if self.Blind :
                xmin=self.MassWindow[0]
                xmax=self.MassWindow[1]
                xstart = h_data.FindBin(xmin)
                xend   = h_data.FindBin(xmax)
                for ibin in range(xstart,xend+1):
                    h_data.SetBinContent(ibin, -100)

            h_data.SetLineWidth(2)
            h_data.SetLineStyle(42)
            h_data.SetMarkerColor(1)
            h_data.SetLineColor(1)
            h_data.SetMarkerSize(1.0)
            h_data.GetXaxis().SetTitle(xtitle)
            h_data.GetYaxis().SetTitle(ytitle)
            h_data.Draw("E SAME")
            h_data.GetYaxis().SetRangeUser(RangY[0],RangY[1])
            if len(XRange) > 0:
                h_data.GetXaxis().SetRangeUser(XRange[0],XRange[1])
            legeff.AddEntry(hists_data[0],"Data","ple")       
        can_Fin.Modified()
        ### Draw Total 
        legeff.SetNColumns(2)
        if legeff.GetNRows() > 20: 
            legeff.SetTextSize(0.02)
        if legeff.GetNRows() > 30: 
            legeff.SetTextSize(0.015)
        legeff.Draw()

        logo=self.setATLAS(title)
        logo.Draw()

        
        name = outputName
        can_Fin.SaveAs("output/%s.pdf"%name)
        can_Fin.SaveAs("output/%s.C"%name)
        del hists_sig 
        del hists_bkg 
        del can_Fin

        return 1

    def DrawStacksRatio_ATLAS(self, title, xtitle, ytitle, outputName, XRange, SetLogy=False, CopyLabel=False):

        ''' Draw Hist Stack Plots '''

        print '============ Hist Maker: Draw Stack Plots Ratio========'
        
        if self.haveSig == False  and self.haveBkg == False :
            return 0 
        ###   
        ###  Canvas & Frame  
        ###------------------------------------------------- 

        print '============ Hist Maker: Draw Stack Ratio Plots ========TEST1'
        can_Fin, pad1, pad2 = createCanvasPads()
        
        print '============ Hist Maker: Draw Stack Plots Ratio ========TEST2'
        
       
        # SetLog Y 
        #RangY = self.GetRangY(self.hists, 1.2, SetLogy)
        MaxY = -10000
        MinY= 10e8
        if self.haveData :
            RangY_data = self.GetRangY(self.hists_data, 1.7, SetLogy)
            if MaxY < RangY_data[1] : 
                MaxY = RangY_data[1]
            if MinY > RangY_data[0] : 
                MinY = RangY_data[0]

        if self.haveSig : 
            RangY_sig = self.GetRangY(self.hists_sig, 1.7, SetLogy)
            if MaxY < RangY_sig[1] : 
                MaxY = RangY_sig[1]
            if MinY > RangY_sig[0] : 
                MinY = RangY_sig[0]

        if self.haveBkg : 
            RangY_bkg = self.GetRangY_StackHist(self.hists_bkg, 1.7, SetLogy)
            if MaxY < RangY_bkg[1] : 
                MaxY = RangY_bkg[1]
            if MinY > RangY_bkg[0] : 
                MinY = RangY_bkg[0]
        
        RangY = [MinY,MaxY]

        
        print '============ Hist Maker: Draw Stack Plots Ratio ========TEST3'

        atlas_style()
        frame = ROOT.TH2D("frame",title, 1, 110, 300, 1, 0.4 ,0.84)
        frame.GetXaxis().SetTitle(xtitle)
        frame.GetYaxis().SetTitle(ytitle)
        frame.SetLabelSize(0.055,"X")
        frame.SetLabelSize(0.055,"Y")
        frame.SetTitleSize(0.055,"X")
        frame.SetTitleSize(0.055,"Y")
        frame.SetStats(0)

        if len(XRange) > 0:
            frame.GetXaxis().SetRangeUser(XRange[0],XRange[1])
            frame.GetYaxis().SetRangeUser(RangY[0],RangY[1])


        #frame.Draw()

        gStyle.SetLegendTextSize(0.045)
        legeff = ROOT.TLegend(self.Legend_x1_stack,self.Legend_y1_stack,self.Legend_x2_stack,self.Legend_y2_stack)
        #logo = ROOT.TPaveText(0.7,0.8,0.88,0.93,"NDC");
        legeff.SetFillColor(0)
        legeff.SetBorderSize(0)
        legeff.SetFillStyle(0)
        legeff.SetTextFont(42)
        legeff.SetTextSize(0.03)
        legendCounter = 0 
        ## Draw Option
        
        kWhite   = 0   
        kBlack   = 1   
        kGray    = 920  
        kRed     = 632  
        kGreen   = 416
        kBlue    = 600
        kYellow  = 400 
        kMagenta = 616  
        kCyan    = 432  
        kOrange  = 800
        kSpring  = 820 
        kTeal    = 840 
        kAzure   =  860 
        kViolet  = 880  
        kPink    = 900
       
        col = [kBlack, kGray+1, kRed, kBlue, kGreen, kMagenta, kCyan+1, kOrange, kSpring-5, kTeal, kAzure-4, kViolet, kPink+7]
        col = [kGray+1, kRed, kGreen, kBlue, kMagenta, kCyan+1, kOrange, kSpring-5, kTeal, kAzure-4, kViolet, kPink+7]
        
        #col =  [ kGreen, kOrange, kSpring-5, kTeal, kAzure-4, kViolet, kPink+7]
        ###  Draw 
        ###------------------------------------------------- 
        
        ## BKG 
        #self.haveSig = False
        hists_bkg = []
        hists_sig = []
        hists_data = self.hists_data 
        #self.haveBkg= False
        histsLabel_bkg = self.histsLabel_bkg
        histsLabel_sig = self.histsLabel_sig

        for tmpHist in self.hists_bkg :
            hists_bkg +=[tmpHist.Clone("%s_tmp"%tmpHist.GetName())]
        for tmpHist in self.hists_sig :
            hists_sig +=[tmpHist.Clone("%s_tmp"%tmpHist.GetName())]

        ##--------------------------------
        ##     Background  
        ##
        if self.haveBkg :
            # color List
            colorListBkg = []
            if self.colorType == "AUTO" :
                colorListBkg = AutoColorList(len(hists_bkg))
            elif self.colorType == "NORM":
                colorListBkg = NormColorList(len(hists_bkg))
            elif self.colorType == "DIY":
                colorListBkg = self.colorListBkg
            
            histStack =  ROOT.THStack("histStack","histStack")

            print "colorListBkg %d "%len(colorListBkg) 
            print "hists_bkg %d "%len(hists_bkg) 

            if len(hists_bkg) != len(histsLabel_bkg) :
                print "!!!!!!!!!! ERROR len(histsLabel_bkg) != len(hists_bkg) !!!!!!!!!!!! "
                print "ColorList:"
                print colorListBkg
                print "bkg-Labels:"
                print histsLabel_bkg
                print "bkg-hists:"
                print hists_bkg
           
            for ith in range(0,len(histsLabel_bkg)):
                #print myColor.GetColorPalette(ith)
                #hists_bkg[ith].SetLineColor(colorIndex_num_start + 3*ith)
                #hists_bkg[ith].SetFillColor(colorIndex_num_start + 3*ith)
                print "ith %s " %(ith)
                print hists_bkg[ith]
                hists_bkg[ith].SetLineColor(colorListBkg[ith])
                hists_bkg[ith].SetFillColor(colorListBkg[ith])
                hists_bkg[ith].SetLineWidth(2)
                hists_bkg[ith].GetXaxis().SetTitle(xtitle)
                hists_bkg[ith].GetYaxis().SetTitle(ytitle)
                if len(XRange) > 0:
                    hists_bkg[ith].GetXaxis().SetRangeUser(XRange[0],XRange[1])
                
                hists_bkg[ith].GetYaxis().SetRangeUser(RangY[0],RangY[1])

                histStack.Add(hists_bkg[ith])
            
            if len(histsLabel_bkg)%2 ==0 : 
                N_counter =int(len(histsLabel_bkg))/2
            else:
                N_counter =int(len(histsLabel_bkg)+1)/2

            for ith in range(0,len(histsLabel_bkg)):
                if ith %2 == 0  : 
                    num = ith/2 +1 -1 
                    legeff.AddEntry(hists_bkg[num],histsLabel_bkg[num],"f")       
                else:
                    num = (ith+1)/2 + N_counter -1
                    legeff.AddEntry(hists_bkg[num],histsLabel_bkg[num],"f")       

            
            #if len(XRange) > 0:            
            #    histStack.GetHistogram().GetXaxis().SetRangeUser(XRange[0],XRange[1])
            
        ##--------------------------------
        ##     Signal 
        ##
        if self.haveSig :
            # Color 
            colorListSig = col
            if self.colorType == "DIY":
                if len(self.colorListSig) == len(histsLabel_sig):
                    colorListSig = self.colorListSig
            # SetHist
            for ith in range(0,len(histsLabel_sig)):
                integ1 = hists_sig[ith].GetSumOfWeights()
                ##### self.hists_bkg[ith].SetLineColor(col[ith])
                hists_sig[ith].SetLineWidth(2)
                hists_sig[ith].SetLineStyle(42)
                hists_sig[ith].SetMarkerColor(colorListSig[ith])
                hists_sig[ith].SetLineColor(colorListSig[ith])
                hists_sig[ith].SetMarkerSize(1.0)
                #hists_sig[ith].Scale(self.signalScale) # Scale auto call Sumw2 
                hists_sig[ith].GetXaxis().SetTitle(xtitle)
                hists_sig[ith].GetYaxis().SetTitle(ytitle)
                integ2 = hists_sig[ith].GetSumOfWeights()
                print "!!!!!!!! In classDef Draw Stack !!!!!!!!!!! %s "% histsLabel_sig[ith]
                print "!!!!!!!! Scale %f - Integral1 %f - Integral2 %f "%(self.signalScale, integ1, integ2)
                sumInteg = 0 
                for binx in range(0,hists_sig[ith].GetNbinsX()):
                    #print "!!!! Binx : %d center: %f Content : %f  "%(binx,  hists_sig[ith].GetBinCenter(binx+1),hists_sig[ith].GetBinContent(binx+1))
                    sumInteg +=hists_sig[ith].GetBinContent(binx+1)
                print "Sum of Integral %f"% sumInteg

                hists_sig[ith].GetYaxis().SetRangeUser(RangY[0],RangY[1])
                if len(XRange) > 0:
                    hists_sig[ith].GetXaxis().SetRangeUser(XRange[0],XRange[1])
                
                legeff.AddEntry(hists_sig[ith],histsLabel_sig[ith]+  " x %d"%(self.signalScale),"pl")       

        ##--------------------------------
        ## Merge all Hist
        ##
        hists_merge=hists_bkg
        hist_total = MergeHists(hists_merge, "h_merge_all_mc")
        gStyle.SetHatchesSpacing(1.0)
        gStyle.SetHatchesLineWidth(2)
        hist_total.SetFillStyle(3354)
        hist_total.SetFillColor(1)
        hist_total.SetLineColor(1)
        hist_total.SetMarkerSize(0)
        hist_total.SetLineWidth(0)
        
        ##--------------------------------
        ##     Data 
        ##
        if self.haveData :

            hists_data = self.hists_data 
            h_data = hists_data[0].Clone("h_data_copy")

            if self.Blind :
                xmin=self.MassWindow[0]
                xmax=self.MassWindow[1]
                xstart = h_data.FindBin(xmin)
                xend   = h_data.FindBin(xmax)
                for ibin in range(xstart,xend+1):
                    h_data.SetBinContent(ibin, -100)

            h_data.SetLineWidth(2)
            h_data.SetLineStyle(42)
            h_data.SetMarkerColor(1)
            h_data.SetLineColor(1)
            h_data.SetMarkerSize(1.0)
            h_data.GetXaxis().SetTitle(xtitle)
            h_data.GetYaxis().SetTitle(ytitle)
            legeff.AddEntry(hists_data[0],"Data","ple")       

        legeff.AddEntry(hist_total,"Uncertainty","f")       
        ## Foe ratio plot  
        hist_dataMc = createDataRatioHist(h_data,hists_sig[0],hist_total,"ratio_DSB")
        hist_Err = createMCHistErr([hist_total]+hists_sig,"h_err")
        ################################ 
        ##Pad 1 
        print pad1
        pad1.cd()
        frame.Draw()
        if SetLogy :
            gPad.SetLogy()
        histStack.Draw("hist")
        if len(XRange) > 0:
            histStack.GetXaxis().SetLimits(XRange[0],XRange[1])
        histStack.SetMinimum(RangY[0])
        histStack.SetMaximum(RangY[1])
        histStack.GetXaxis().SetTitle(xtitle)
        histStack.GetYaxis().SetTitle(ytitle)
        #can_Fin.Modified()

        hist_total.Draw("E2 SAME")
        #can_Fin.Modified()
        
        if self.haveSig :
            for ith in range(0,len(histsLabel_sig)):
                hists_sig[ith].Scale(self.signalScale) # Scale auto call Sumw2 
                if not self.haveBkg :
                    if ith == 0: 
                        hists_sig[ith].Draw("HIST")
                    else :
                        hists_sig[ith].Draw("HISTSAME")
                else :
                    hists_sig[ith].Draw("HISTSAME")
             #   can_Fin.Modified()
        
        if self.haveData :
            h_data.Draw("E SAME")
            h_data.GetYaxis().SetRangeUser(RangY[0],RangY[1])
            if len(XRange) > 0:
                h_data.GetXaxis().SetRangeUser(XRange[0],XRange[1])

            #can_Fin.Modified()
        ### Draw Total Legend
        legeff.SetNColumns(2)
        if legeff.GetNRows() > 20: 
            legeff.SetTextSize(0.02)
        if legeff.GetNRows() > 30: 
            legeff.SetTextSize(0.015)
        legeff.Draw()
        
        # calculation  chi2 & significance 
        CalculationInfo=""
        if self.haveSig and self.haveBkg:
            significance  = SignificanceCalculatuion_Bin(hists_sig[0],hist_total,self.signalScale)
            CalculationInfo += ";Signal significance %.3f;"%(significance)
            if self.haveData: 
                Chi2 = GetChi2_DataVsMC(hists_sig[0],hist_total,h_data,self.signalScale)
                KS = GetKS_DataVsMC(hists_sig[0],hist_total,h_data, self.signalScale)
                CalculationInfo += "#chi^{2}/ndf: %.3f KS: %.3f;"%(Chi2,KS)
            title+=CalculationInfo

        logo=self.setATLAS(title)
        logo.Draw()


        
        pad2.cd()
        hist_Err.GetYaxis().SetRangeUser(0.4,1.6)

        labelsize_Y=h_data.GetYaxis().GetLabelSize()
        labelsize_X=h_data.GetXaxis().GetLabelSize()
        OffSet_Y=h_data.GetYaxis().GetTitleOffset()

        ### Ratio hists 
        ratioTitle="Data/Pred."
        hist_Err.SetLineColor(1)
        hist_Err.SetLineWidth(2)
        hist_Err.SetFillColor(0)
        hist_Err.SetLineStyle(42)
        hist_Err.SetMarkerColor(1)
        hist_Err.SetMarkerSize(1.0)
        hist_Err.GetXaxis().SetTitle(xtitle)
        hist_Err.GetYaxis().SetTitle(ratioTitle)
        hist_Err.GetYaxis().SetLabelSize(labelsize_Y*0.6/0.4)
        hist_Err.GetYaxis().SetTitleSize(labelsize_Y*0.6/0.4)
        hist_Err.GetYaxis().SetTitleOffset(OffSet_Y*0.4/0.6)
        hist_Err.GetXaxis().SetLabelSize(labelsize_X*0.6/0.4)
        hist_Err.GetXaxis().SetTitleSize(labelsize_X*0.6/0.4)
        '''
        if CopyLabel :
            binLabel = histsS.GetXaxis().GetBinLabel(xbin)
            hist_Err.GetXaxis().SetBinLabel(xbin,binLabel)
            hist_Err.GetXaxis().SetLabelSize(0.045*0.6/0.4)
            hist_Err.GetXaxis().ChangeLabel(xbin,325)
        '''     
        hist_Err.SetFillStyle(3354)
        #hist_Err.SetFillStyle(42)
        hist_Err.SetFillColor(1)
        #hist_Err.SetFillColor(TColor.GetColor("#3598DB"))
        hist_Err.SetLineColor(1)
        hist_Err.SetMarkerSize(0)
        hist_Err.SetLineWidth(0)
        if CopyLabel :
            for xbin in range(1,hists_bkg[0].GetNbinsX()+1):
                binLabel = hists_bkg[0].GetXaxis().GetBinLabel(xbin)
                hist_Err.GetXaxis().SetBinLabel(xbin,binLabel)
                #hist_Err.GetXaxis().SetLabelSize(0.045*0.6/0.4)
                #hist_Err.GetXaxis().ChangeLabel(xbin,325)

        hist_Err.Draw("e2")
        hist_dataMc.Draw("E SAME")


                
        name = outputName
        can_Fin.SaveAs("output/%s.pdf"%name)
        can_Fin.SaveAs("output/%s.C"%name)
        can_Fin.Close()
        del hists_sig[:] 
        del hists_bkg [:]
        del histStack
#        del can_Fin
        del pad1
        del pad2
        

    def DrawPiePlot_ATLAS(self, title, outputName):

        histsLabel_sig = self.histsLabel_sig
        histsLabel_bkg = self.histsLabel_bkg

        yield_pie = []
        colorList = []
        label_pie = []
        label_pie_tmp = self.histsLabel_bkg + histsLabel_sig
        hists_pie     = self.hists_bkg + self.hists_sig
        #-------------------------
        #    color setting 
        #
        colorDic = {    'kWhite'    : 0  ,  'kBlack'    : 1  ,'kGray'       : 920,  'kRed'      : 632,  'kGreen'   : 416,
                        'kBlue'     : 600,  'kYellow'   : 400,'kMagenta'    : 616,  'kCyan'     : 432,  'kOrange'  : 800,
                        'kSpring'   : 820,  'kTeal'     : 840, 'kAzure'     : 860,  'kViolet'   : 880,  'kPink' : 900
        } 
                
        col = [colorDic['kGray']+1, colorDic['kRed'], colorDic['kGreen'], colorDic['kBlue'], colorDic['kMagenta'], 
               colorDic['kCyan']+1, colorDic['kOrange'], colorDic['kSpring']-5, colorDic['kTeal'], 
               colorDic['kAzure']-4, colorDic['kViolet'], colorDic['kPink']+7]

        colorListBkg = []
        if self.colorType == "AUTO" :
            colorListBkg = AutoColorList(len(hists_bkg))
        elif self.colorType == "NORM":
            colorListBkg = NormColorList(len(hists_bkg))
        elif self.colorType == "DIY":
            colorListBkg = self.colorListBkg
 
        colorListSig = col
        if self.colorType == "DIY":
            if len(self.colorListSig) == len(histsLabel_sig):
                colorListSig = self.colorListSig
        
        colorList_tmp = colorListBkg + colorListSig
        
        #-------------------------
        #    hist 
        #
        sumYield=0
        for i  in range(0,len(hists_pie)):
           yield_tmp = hists_pie[i].GetSumOfWeights() 
           if yield_tmp > 0 : 
                yield_pie += [yield_tmp]
                colorList += [colorList_tmp[i]]
                label_pie +=[label_pie_tmp[i]]
                sumYield+=yield_tmp

        ### hists re rank for better legend ranking
        yield_pie_rerank = []
        colorList_rerank = []
        label_pie_rerank = []
        N_counter=0
        if len(yield_pie)%2 ==0 : 
            N_counter =int(len(yield_pie))/2
        else:
            N_counter =int(len(yield_pie)+1)/2

        for ith in range(0,len(yield_pie)):
            num = -1
            if ith %2 == 0  : 
                num = ith/2 +1 -1 
            else:
                num = (ith+1)/2 + N_counter -1
            yield_pie_rerank += [yield_pie[num]]
            colorList_rerank += [colorList[num]]
            label_pie_rerank += [label_pie[num]]
        #=============================
        # Canvas 
        #
        can_pie = TCanvas("can_pie","can_pie",800,800)
        atlas_style()
        yield_pie_array = array("d",yield_pie_rerank)
        colorList_array = array("i",colorList_rerank)

        plot_pie = TPie("Pie","Pie",len(yield_pie), yield_pie_array, colorList_array)
        for n in range(0,plot_pie.GetEntries()):
            plot_pie.SetEntryLabel(n,"%s   %.2f%%"%(label_pie_rerank[n],yield_pie_rerank[n]/sumYield*100))
            
        plot_pie.SetRadius(.35)
        plot_pie.SetY(.4)
        plot_pie.SetLabelFormat("")
#        plot_pie.SetLabelFormat("#splitline{%val (%perc)}{%txt}")
        plot_pie.Draw("t  nol <")
        #----------------------------
        # Legend 
        #
        pieleg = plot_pie.MakeLegend();
        pieleg.SetY1(.77); 
        pieleg.SetY2(.97);
        pieleg.SetX1(.51); 
        pieleg.SetX2(.95);

        pieleg.SetNColumns(2)
        if pieleg.GetNRows() > 20: 
            pieleg.SetTextSize(0.02)
        if pieleg.GetNRows() > 30: 
            pieleg.SetTextSize(0.015)

        gStyle.SetLegendTextSize(0.035)
        pieleg.SetFillColor(0)
        pieleg.SetBorderSize(0)
        pieleg.SetFillStyle(0)
        pieleg.SetTextFont(42)
        pieleg.SetTextSize(0.023)

        logo=self.setATLAS(title)
        logo.Draw()
        
        name = outputName
        can_pie.SaveAs("output/%s.pdf"%name)
        can_pie.SaveAs("output/%s.C"%name)
        can_pie.Close()
        del yield_pie[:] 
        del colorList[:]
        del label_pie[:]
        return 1


###########################################################################
###########################################################################
###
###     Class for Hist2D 
###
###########################################################################
###########################################################################

class HistMaker2D:

    ''' make Hist  2D'''

    def __init__(self, hists, labels):
        ## xval err are dictionary num(0~n)
        self.hists = hists
        self.histsLabel = labels #Lagens name

        

    def setATLAS(self,title,x1 = 0.18,y1=0.87,x2=0.8,y2=0.97):

        ''' ATLAS Logo & detail Bassed on TPaveText '''
    
        titles=[]
        if ";" in title: 
            titles = title.split(";")
        #    y1=y1-0.038*len(titles)

        #logo = ROOT.TPaveText(0.22,0.2,0.6,0.5,"NDC");
        logo = ROOT.TPaveText(0.18,0.87,0.8,0.97,"NDC");
        txt1=logo.AddText("#bf{#it{ATLAS}} Internal")
        txt1.SetTextSize(0.05)
        txt1.SetTextFont(42)
        txt3=logo.AddText(" ")
        txt3.SetTextFont(42)
        txt3.SetTextSize(0.02)

        #logo.AddText("Data 2018, #sqrt{s} = 13 TeV, 60.8 fb^{-1}")
        #txt2=logo.AddText(title)
        #txt2.SetTextFont(42)
        #txt2.SetTextSize(0.04)

        if ";" in title :
            count=0
            for title1 in titles:
                txt2=logo.AddText(title1)
                txt2.SetTextFont(42)
                txt2.SetTextSize(0.035)
                count+=1
                if count < len(titles):
                    txt2=logo.AddText(" ")
                    #txt2.SetTextFont(42)
                    txt2.SetTextSize(0.001)

        else :
            txt2=logo.AddText(title)
            txt2.SetTextFont(42)
            txt2.SetTextSize(0.04)

        '''
        logo.AddText("Z #rightarrow #mu#mu, p_{T}^{#mu} > 25 GeV" )
        logo.GetListOfLines().Last().SetTextFont(42)
        logo.GetListOfLines().Last().SetTextSize(0.05)
        logo.AddText("0.1 < #left|#eta^{#mu}#right| < 1.05")
        logo.GetListOfLines().Last().SetTextFont(42)
        logo.GetListOfLines().Last().SetTextSize(0.05)
        '''
        logo.SetBorderSize(0)
        logo.SetFillColor(10)
        logo.SetTextColor(1)
        logo.SetTextAlign(12)
        
        return logo

       
           
        
    def DrawPlots_ATLAS(self, title, xtitle, ytitle, outputName, DrawType="COLZ",SetLogz=False):

        ''' Draw Plots '''

        print '============ Hist Maker: Draw Plots ========'
        
        gStyle.SetPaintTextFormat("4.2f")
        ## Canvas & Frame  
        can_Fin = ROOT.TCanvas("c1","c1",850,600) 
        atlas_style()
        can_Fin.SetTopMargin(0.15) 
        can_Fin.SetBottomMargin(0.1) 
        can_Fin.SetLeftMargin(0.18) 
        can_Fin.SetRightMargin(0.17) 
        gStyle.SetPalette(57)
        gStyle.SetNumberContours(100)
        # SetLog Y 
        if SetLogz :
            gPad.SetLogz()

        frame = ROOT.TH2D("frame",title, 1, 110, 300, 1, 0.4 ,0.84)
        frame.GetXaxis().SetTitle(xtitle)
        frame.GetYaxis().SetTitle(ytitle)
        frame.SetLabelSize(0.055,"X")
        frame.SetLabelSize(0.055,"Y")
        frame.SetTitleSize(0.055,"X")
        frame.SetTitleSize(0.055,"Y")
        frame.SetStats(0)
        frame.Draw()


        #logo = ROOT.TPaveText(0.7,0.8,0.88,0.93,"NDC");
    
        ## Draw Option
        
        kWhite   = 0   
        kBlack   = 1   
        kGray    = 920  
        kRed     = 632  
        kGreen   = 416
        kBlue    = 600
        kYellow  = 400 
        kMagenta = 616  
        kCyan    = 432  
        kOrange  = 800
        kSpring  = 820 
        kTeal    = 840 
        kAzure   =  860 
        kViolet  = 880  
        kPink    = 900
       
        col = [kBlack, kGray+1, kRed, kGreen, kBlue, kMagenta, kCyan+1, kOrange, kSpring-5, kTeal, kAzure-4, kViolet, kPink+7]
        
        ## Draw 
        ''' 
        nbinx = self.hists.GetNbinsX()
        nbiny = self.hists.GetNbinsY()
        maxZ=self.hists.GetMaximum()
        minZ=self.hists.GetMinimum()
        
        if maxZ==0 and SetLogz : 
            print "Error for LogZ : maxZ = 0 ! " 
            return 0

        if minZ==0 and SetLogz : 
            minZ_tmp = maxZ
            for x in range(1, nbinx+1) :
                for y in range(1, nbiny+1) :
                    content = self.hists.GetBinContent(x,y)
                    if content > 0 and content < minZ_tmp : 
                        minZ_tmp = content
            minZ = minZ_tmp

        print ">>>>>>>>>>>>>> min Z %f maxZ %f"%(minZ,maxZ)
        self.hists.GetZaxis().SetRangeUser(maxZ,minZ)
        '''
        nbinx = self.hists.GetNbinsX()
        nbiny = self.hists.GetNbinsY()
        
        for x in range(1, nbinx+1) :
            for y in range(1, nbiny+1) :
                content = self.hists.GetBinContent(x,y)
                if content == 0 :
                    self.hists.SetBinContent(x,y,0.00001)


        self.hists.GetXaxis().SetTitle(xtitle)
        self.hists.GetYaxis().SetTitle(ytitle)
        self.hists.Draw(DrawType)

        logo=self.setATLAS(title)
        logo.Draw()
        name = outputName
        can_Fin.SaveAs("output/%s.pdf"%name)
        can_Fin.SaveAs("output/%s.C"%name)



###########################################################################
###########################################################################
###
###     Functions 
###
###########################################################################
###########################################################################
def MergeHists ( histList, hist_name): ## Problems do not use 
    
    ''' Merge hist -> Merge hists together : h=h1+h2+h3+....'''
    hist = histList[0].Clone(hist_name);
    hist.SetTitle(hist_name)

    list = ROOT.TList()
    for _hist in histList :
        list.Add(_hist.Clone("%s_MergeCopy"%_hist.GetName()))

    hist.Reset()
    hist.Merge(list)

    return hist
     
def AddHists ( histList, hist_name): ## Problems do not use 
    
    ''' Merge hist -> Merge hists together : h=h1+h2+h3+....'''
    hist = histList[0].Clone(hist_name);
    hist.SetTitle(hist_name)

    list = ROOT.TList()
    counter=0
    for _hist in histList :
        if counter == 0:
            continue
        hist.Add(_hist)

    return hist

def AutoColorList(ncolors):
        
    print " <<<<<<<     Auto Color List Generator   >>>>>>> "
    
    stops = [0.00, 0.34, 0.61, 0.84, 1.00]
    red   = [0.00, 0.00, 0.87, 1.00, 0.51]
    green = [0.00, 0.81, 1.00, 0.20, 0.00]
    blue  = [0.51, 1.00, 0.12, 0.00, 0.00]
    
    _S = array('d', stops)
    _R = array('d', red)
    _G = array('d', green)
    _B = array('d', blue)
    
    npoints = len(_S)
    colorIndex_num_start = TColor.GetFreeColorIndex();
    indexColor = TColor.CreateGradientColorTable(npoints, _S, _R, _G, _B, ncolors)
    gStyle.SetNumberContours(ncolors)
    
    colorIndex_num_end = TColor.GetFreeColorIndex()-1;
    print "AutoColorList -> Ncolor %s | start %d end  %d : "%(ncolors ,colorIndex_num_start, colorIndex_num_end )

    colorList=[]

    for _col in range(0,ncolors) : 
        _color = _col + colorIndex_num_start
        colorList += [ _color ]

    print "AutoColorList -> Colorlist:"
    print colorList
    return colorList
  
def NormColorList(ncolors):
    ## Draw Option
    
    kWhite   = 0   
    kBlack   = 1   
    kGray    = 920  
    kRed     = 632  
    kGreen   = 416
    kBlue    = 600
    kYellow  = 400 
    kMagenta = 616  
    kCyan    = 432  
    kOrange  = 800
    kSpring  = 820 
    kTeal    = 840 
    kAzure   =  860 
    kViolet  = 880  
    kPink    = 900
    
    colorList = [kBlack,  kRed, kGreen, kBlue, kGray+1,kMagenta, kCyan+1, kOrange, kSpring-5, kTeal, kAzure-4, kViolet, kPink+7]
    
    return colorList

def createRatioHist(h1, h2, h3_name):
    if h1.GetNbinsX() != h2.GetNbinsX() :
        h3 = RemakeHist(h1,h2.GetNbinsX(),0,h2.GetNbinsX())
    else:
        h3 = h1.Clone("h_"+h3_name)
    h3.Sumw2()
    h3.SetStats(0)
    h3.Divide(h2)

    return h3    

def createSqrtRatioHist(h1, h2, h3_name):
    if h1.GetNbinsX() != h2.GetNbinsX() :
        h3 = RemakeHist(h1,h2.GetNbinsX(),0,h2.GetNbinsX())
    else:
        h3 = h1.Clone("h_"+h3_name)
    h3.Sumw2()
    h3.SetStats(0)
    for i in range (1,h3.GetNbinsX()+1):
        s1 = h3.GetBinContent(i)
        b1 = h2.GetBinContent(i)
        s1_err = h3.GetBinError(i) 
        b1_err = h2.GetBinError(i) 
        if b1 <= 0 :   
            h3.SetBinContent(i,0)
            h3.SetBinError(i,0)
        else:
            h3.SetBinContent(i,s1/sqrt(b1))
            h3.SetBinError(i, sqrt( (s1_err*s1_err + s1*b1_err*b1_err/4)/b1) )

    return h3    

def createDataRatioHist(hd, hs, hb, h3_name):
    ''' data / (S+B) ratio'''
    nbins = hb.GetNbinsX()
    if hs.GetNbinsX() != hb.GetNbinsX() :
        hs1 = RemakeHist(hs,nbins,0,nbins)
    else:
        hs1 = hs.Clone("h_spb")
    
    if hd.GetNbinsX() != hb.GetNbinsX() :
        hd1 = RemakeHist(hd,nbins,0,nbins)
    else:
        hd1 = hd.Clone("h_"+h3_name)
    
    hd1.Sumw2()
    hd1.SetStats(0)
    hs1.Sumw2()
    hs1.SetStats(0)
    
    h3=ROOT.TH1D("h_"+h3_name, "h_"+h3_name, nbins, hb.GetBinLowEdge(1), hb.GetBinLowEdge(nbins+1))
    print ">>>>>>>>>>>>>>>>createDataRatioHist : hb.GetBinLowEdge[1] : %f hb.GetBinLowEdge(nbins+1): %f"%(hb.GetBinLowEdge(1), hb.GetBinLowEdge(nbins+1))
    for i in range (1,hd1.GetNbinsX()+1):
        s1 = hs1.GetBinContent(i)
        b1 = hb.GetBinContent(i)
        d1 = hd1.GetBinContent(i)
        s1_err = hs1.GetBinError(i)
        b1_err = hb.GetBinError(i)
        d1_err = hd1.GetBinError(i)
        mc1=s1+b1
        if (s1+b1) <= 0 :   
            h3.SetBinContent(i,0)
            h3.SetBinError(i,0)
        else:
            if d1 > 0:
                h3.SetBinContent(i,d1/(s1+b1))
                h3.SetBinError(i, 1/mc1 * sqrt( d1_err*d1_err + (d1*d1)/(mc1*mc1) * (s1_err*s1_err+b1_err*b1_err) ) )
    
    return h3  

def createMCHistErr(hists, h3_name):
    ''' Bkg  ratio'''
    nbins = hists[0].GetNbinsX()
    h_mc = MergeHists(hists,"h_mc") 
    h3=ROOT.TH1D("h_"+h3_name, "h_"+h3_name, nbins, h_mc.GetBinLowEdge(1), h_mc.GetBinLowEdge(nbins+1))
    print ">>>>>>>>>>>>>>>>createDataRatioHist : h_mc.GetBinLowEdge[1] : %f h_mc.GetBinLowEdge(nbins+1): %f"%(h_mc.GetBinLowEdge(1), h_mc.GetBinLowEdge(nbins+1))
    for i in range (1,h_mc.GetNbinsX()+1):
        b1 = h_mc.GetBinContent(i)
        b1_err = h_mc.GetBinError(i)
        if b1 <= 0 :   
            h3.SetBinContent(i,0)
            h3.SetBinError(i,0)
        else:
            h3.SetBinContent(i,1)
            h3.SetBinError(i, b1_err/b1 )
    
    return h3  

def createCanvasPads():
    c = TCanvas("c","cnavas",800,800)
    c.cd()
    pad1 = TPad("pad1","pad1",0,0.4,1,1.0)
    pad1.SetBottomMargin(0)
    pad1.SetRightMargin(0.1)
    pad1.SetGridx()
    pad1.Draw()

    c.cd()
    pad2 = TPad("pad2","pad2",0,0.002,1,0.39)
    pad2.SetBottomMargin(0.33)
    pad2.SetRightMargin(0.1)
    pad2.SetTopMargin(0.02)
    pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2  

def GetMinContent_Limit(hist,limit):
    min = 100000
    for ibin in range(1,hist.GetNbinsX()+1):
        tmp = hist.GetBinContent(ibin)
        if tmp < min and tmp > limit:
              min = tmp
        #print "------Get Min : ibin %d tmp %f  Min %f"%(ibin, tmp,min)

    return min

def GetMaxContent_Limit(hist,limit):
    max = -100000
    for ibin in range(1,hist.GetNbinsX()+1):
        tmp = hist.GetBinContent(ibin)
        if tmp > max and tmp < limit:
              max = tmp
        #print "------Get max : ibin %d tmp %f  Max %f"%(ibin, tmp,max)

    return max

def RemakeHist(hist,nbins,xstart,xend):

    h1=TH1D("h1","h1",nbins,xstart,xend)
    NBins = min (nbins, hist.GetNbinsX())
    for xbin in range(1,NBins+1):
        h1.SetBinContent(xbin, hist.GetBinContent(xbin))
        h1.GetXaxis().SetBinLabel(xbin, hist.GetXaxis().GetBinLabel(xbin))
    
    return h1

def SkipHist(tmpHist,_histName):
    print "====== Hist Status ======= "
    print type(tmpHist)
    
    if  type(tmpHist) == ROOT.TObject : 
        print "Not Exist >>>> Skip HistName: %s"%_histName
        return True
    else :
        print "Using Hist >>>> HistName: %s"%_histName
        return False

def SignificanceCalculatuion_Bin(hist_Signal,hist_Bkg,signalScale=1):
    nbins = hist_Bkg.GetNbinsX()+1
    Z2=0.0
    print "----------- SignificanceCalculatuion_Bin -----------"
    for ibin in range(1,nbins):
        si = hist_Signal.GetBinContent(ibin)*1.0/signalScale
        bi = hist_Bkg.GetBinContent(ibin)
        #print "Bini %d  Signal %f Bkg. %f"%(ibin,si,bi)
        ni = si+bi
        if bi>0 :
            Zi = ni*log(ni/bi)-si
            Z2 += 2 * Zi
 
    return sqrt(Z2)

    
def SignificanceCalculatuion_All(hist_Signal,hist_Bkg):
    si = hist_Signal.Integral()
    bi = hist_Bkg.Integral()
    ni = si+bi
    Zi = 2*(ni*log(ni/bi)-si)
 
    return sqrt(Zi)

def GetChi2_DataVsMC(h_sig,h_bkg,h_data,signalScale=1):
    h_sig_tmp = h_sig.Clone()
    h_sig_tmp.Sumw2()
    h_sig_tmp.Scale(1.0/signalScale)
    chi2 = 0.0 
    ndf = 0
    h_Total = MergeHists([h_bkg,h_sig_tmp],"h_total")
    for i in range(1,h_Total.GetNbinsX()+1):
        nd = h_data.GetBinContent(i)
        nt = h_Total.GetBinContent(i)
        if nd >0 and nt > 0:
            chi2 += (nd-nt)*(nd-nt)/nt
            ndf+=1
    if ndf >0 :
        chi2 = chi2/ndf 
#    chi2 = h_data.Chi2Test(h_Total,"CHI2/NDF")
    return chi2

def GetKS_DataVsMC(h_sig,h_bkg,h_data,signalScale=1):

    h_sig_tmp = h_sig.Clone()
    h_sig_tmp.Sumw2()
    h_sig_tmp.Scale(1.0/signalScale)
    h_Total = MergeHists([h_bkg,h_sig_tmp],"h_total")
    KS = h_data.KolmogorovTest(h_Total)
    return KS



