#!/usr/bin/python2.6

# ###########
# # Make Plots
# ###########

# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging

import array
# Python Paths
ROOTLIB, PATH_OnePlot_MODULE1, PATH_OnePlot_MODULE2 = "", "", ""

if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_OnePlot_MODULE1"): PATH_OnePlot_MODULE1=os.getenv("PATH_OnePlot_MODULE1")
if os.getenv("PATH_OnePlot_MODULE2"): PATH_OnePlot_MODULE2=os.getenv("PATH_OnePlot_MODULE2")

sys.path.append(ROOTLIB)
sys.path.append(PATH_OnePlot_MODULE1)
sys.path.append(PATH_OnePlot_MODULE2)

# Python Math Modules
import array
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
import ROOT

# user modules
from oneplot import oneplot
from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
    
# #########################
# User defined drawing function
# #########################

def createRatio(h1, h2, lineColor):
    h3 = h1.Clone("h3")
    h3.SetLineColor(lineColor)
    h3.SetMarkerStyle()
    #h3.SetTitle("")
    h3.SetMinimum(0.95)
    h3.SetMaximum(1.05)
    # Set up plot for markers and errors
    h3.Sumw2()
    h3.SetStats(0)
    h3.Divide(h2)
    h3.SetLineWidth(2)
    # Adjust y-axis settings
    y = h3.GetYaxis()
    y.SetTitle(" Systematic / Nominal ")
    #y.SetNdivisions(505)
    y.SetTitleSize(20)
    y.SetTitleFont(43)
    y.SetTitleOffset(1.55)
    y.SetLabelFont(43)
    y.SetLabelSize(15)

    # Adjust x-axis settings
    x = h3.GetXaxis()
    x.SetTitleSize(20)
    x.SetTitleFont(43)
    x.SetTitleOffset(4.0)
    x.SetLabelFont(43)
    x.SetLabelSize(15)

    return h3

#------------------------------------
#       Get max/min axis 
#------------------------------------
def GetMinX(hist):
    c1=hist.GetBinCenter(1)
    c2=hist.GetBinCenter(2)
    min = c1 - ( c2 - c1 )/2
    return min

def GetMaxX(hist):
    maxbin = hist.GetNbinsX()
    c1=hist.GetBinCenter(maxbin)
    c2=hist.GetBinCenter(maxbin-1)
    max = c1 + ( c1 - c2 )/2
    return max

def GetMaxY(hists):
    max = 0
    for hist in hists : 
        for i in range(hist.GetNbinsX()):
            if hist.GetBinContent(i+1) > max : 
                max = hist.GetBinContent(i+1)
    return max 

#------------------------------------
#       Get Canvas  Pad 
#------------------------------------
def createCanvasPads():
    c = TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.2)
    pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2

#------------------------------------
#       ratio plot 
#------------------------------------

def ratioplot(hists, lineColors, Legends, xtitle, outputPlot):
    
    denom_hist = hists[0]
    nom_hists   = hists[1:]
    nom_lineColors = lineColors[1:]
    max_y = GetMaxY(hists)
    min_x = GetMinX(denom_hist)
    max_x = GetMaxX(denom_hist)
    c, pad1, pad2 = createCanvasPads()
    
    ''' Draw Pad1 '''
    
    pad1.cd()
    Count = 0
    logo = TPaveText(0.15,0.79,0.41,0.85,"NDC");
    logo.AddText("ATLAS Internal")
    logo.SetBorderSize(0)
    logo.SetFillColor(10)
    logo.SetTextColor(1)
    logo.SetTextSize(0.06)
    logo.SetTextAlign(12)
    leg=TLegend(0.53, 0.3, 0.9, 0.87)
    leg.SetTextFont(42)
    leg.SetTextSize(0.023)
    leg.SetBorderSize(0)
    leg.SetFillColor(10)

    for hist,lineColor,legend in zip(hists, lineColors, Legends):
        
        hist.SetLineColor(lineColor)
        hist.SetLineWidth(1)
        #hist.SetLineWidth(1)
        hist.SetStats(0)
        hist.SetTitle("") 
        if Count == 0 :
            hist.GetYaxis().SetRangeUser(0,1.3*max_y)
            hist.GetXaxis().SetRangeUser(min_x, 1.3*max_x)
            hist.GetYaxis().SetTitle("Events")
            #to avoid clipping the bottom zero, redraw a small axis
            hist.GetYaxis().SetLabelSize(0.0)
            hist.Draw()
        else : 
            hist.GetYaxis().SetTitle("Events")
            hist.Draw("same")
        
        leg.AddEntry(hist, legend, "L")
        Count += 1

    leg.Draw()
    logo.Draw()

   
    ''' Draw axis'''
    
    axis = TGaxis(min_x, 0, min_x, 1.3*max_y, 0, 1.3*max_y, 520, "")
    axis.SetLabelFont(43)
    axis.SetLabelSize(15)
    axis.SetTitle("Events")
    axis.Draw()
    
    ''' Draw Pad2 '''

    pad2.cd()
    Count=0
    ratio_hists=[]

    for nom_hist, lineColor in zip(nom_hists, nom_lineColors):
        ratioHist = createRatio(nom_hist, denom_hist, lineColor)
        ratio_hists += [ratioHist]

    for ratio_hist, lineColor in zip(ratio_hists, nom_lineColors):
        if Count == 0 :
            ratio_hist.GetXaxis().SetRangeUser(min_x, 1.3*max_x)
            ratio_hist.Draw("HIST")
            ratio_hist.GetXaxis().SetTitle(xtitle)
        else:
            ratio_hist.Draw("HIST SAME")
            #ratioHist.Draw("PL SAME","","SAME")
        Count += 1 
    c.SaveAs(outputPlot)



