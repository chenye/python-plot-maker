import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH2D,TH1D,TLegend, TGaxis, TPaveText,TMath
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
#from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo, GraphMaker, HistMaker1D, HistMaker2D
from ClassDef import MergeHists, SignificanceCalculatuion_All, SignificanceCalculatuion_Bin

SetLogY_1D=True

#def TMVAppResultEvaluation(InputDir, SigFileName, BkgFileName, HistName="MVA_BDT", Title ="", regionType="Ntag2_PTV_High", TagType="_Src",TLtype="TT", MVAInfo="NTree_Ndepth_NCuts"):
def TMVAppResultEvaluation(InputDir, SigFileName, BkgFileName, HistName="MVA_BDT", Title ="", regionType="Ntag2_PTV_High", TagType="_Src",TLtype="TT", MVAInfo=""):

    myFile_Bkg = TFile(InputDir+"/"+BkgFileName)
    myFile_Sig = TFile(InputDir+"/"+SigFileName)
    
    Hist_Bkg = myFile_Bkg.Get(HistName)
    Hist_Sig = myFile_Sig.Get(HistName)
    print Hist_Bkg

    #Hist_Sig.Rebin(5)
    #Hist_Bkg.Rebin(5)
    Hist_Sig.Rebin(10)
    Hist_Bkg.Rebin(10)

    ## Significance Calculation 
    Z_bin = SignificanceCalculatuion_Bin(Hist_Sig, Hist_Bkg)
    Z_All = SignificanceCalculatuion_All(Hist_Sig, Hist_Bkg)

    # Draw Plots 
    HistPlot=HistMaker1D([Hist_Bkg,Hist_Sig],["Background","Signal"]);
    #HistPlot.setLocation_Legend_plot(0.6,0.55,0.93,0.7) 
    HistPlot.setLocation_Legend_plot(0.2,0.8,0.6,0.9) 
    HistPlot.setLocation_ATLASTitle(0.62, 0.82,0.88, 0.9)
    #Title = Title + ";Significance (Bin) %.3f ;Significance(All) %.3f"%(Z_bin,Z_All)
    Title = Title + ";Significance Z = %.3f "%(Z_bin)
    HistPlot.DrawPlots_ATLAS(Title, "BDT response","Number of events", ["Background","Signal"], "BDT_VHcc"+TagType+"/BDT_"+regionType+TLtype+MVAInfo, [], "All", SetLogY_1D)

    #
    # Generate New Signal TMVApp Hist 
    #
    print "___________%s___NewSigHist_________"%regionType+TLtype
    tmvapp_subDir=InputDir+"/New/"+"ExampleInputs_BDT_VHcc2Lep"+TagType  

    try:
        os.mkdir(InputDir+"/New/")
    except OSError as error:
       print InputDir+"/New"+" Exist"    

    try:
        os.mkdir(tmvapp_subDir)
    except OSError as error:
       print tmvapp_subDir+" Exist"    
    myFile_Sig_new = TFile(tmvapp_subDir+"/"+SigFileName,"recreate")
    Hist_Sig_new=Hist_Sig.Clone()
    
    # Get rid of <0 bins >> signal 
    print "NBinsX rebin  :%f _"%Hist_Sig_new.GetNbinsX();
    for ibinx in range(1, Hist_Sig_new.GetNbinsX()+1):
        if Hist_Sig_new.GetBinContent(ibinx)<0:
        #    print "Bin %d Content %f"%(ibinx,Hist_Sig_new.GetBinContent(ibinx))
            Hist_Sig_new.SetBinContent(ibinx,0)
        if Hist_Sig_new.GetBinContent(ibinx)<10:
            print "Sig Bin %d Content %f"%(ibinx,Hist_Sig_new.GetBinContent(ibinx))
    
    print "NewSigHist intregral %f "%(Hist_Sig_new.Integral())
    #Hist_Sig_new.Scale(100)
    Hist_Sig_new.Write()
    myFile_Sig_new.Close()

    #
    # Generate New BKG TMVApp Hist 
    #
    print "___________%s___NewBkgHist_________"%regionType+TLtype
    myFile_Bkg_new = TFile(tmvapp_subDir+"/"+BkgFileName,"recreate")
    Hist_Bkg_new=Hist_Bkg.Clone()

    # Get rid of <0 bins >> sbackground 
    print "NBinsX rebin  :%f _"%Hist_Bkg_new.GetNbinsX();
    for ibinx in range(1, Hist_Bkg_new.GetNbinsX()+1):
        if Hist_Bkg_new.GetBinContent(ibinx)<0:
            #print "Bin %d Content %f"%(ibinx,Hist_Bkg_new.GetBinContent(ibinx))
            Hist_Bkg_new.SetBinContent(ibinx,0)
        if Hist_Bkg_new.GetBinContent(ibinx)<10:
            print "Bkg Bin %d Content %f"%(ibinx,Hist_Bkg_new.GetBinContent(ibinx))
    
    print "NewBkgHist intregral %f "%(Hist_Bkg_new.Integral())
 
    Hist_Bkg_new.Write()
    myFile_Bkg_new.Close()
    return Z_bin

def TMVAppResultEvaluation_SameFile(InputDir, FileName, FileNameData, SetName, SigHist,  BkgHist, DataHist,  Title ="", regionType="Ntag2_PTV_High"):

    myFile= TFile(InputDir+"/"+FileName)
    Hist_Sig = myFile.Get(SigHist)
    Hist_Bkg = myFile.Get(BkgHist)
    
   
   # print Hist_Bkg
    Hist_Sig.Rebin(50)
    Hist_Bkg.Rebin(50)

    UsingData = False
    if FileNameData != "" and DataHist != "": 
        UsingData = True
        myFiledata= TFile(InputDir+"/"+FileNameData)
        Hist_Data = myFiledata.Get(DataHist)
        Hist_Data.Rebin(50)
 
    ## Significance Calculation 
    Z_bin = SignificanceCalculatuion_Bin(Hist_Sig, Hist_Bkg)
    Z_All = SignificanceCalculatuion_All(Hist_Sig, Hist_Bkg)

    # Draw Plots 
    HistPlot=HistMaker1D([Hist_Bkg,Hist_Sig],["Background","Signal"]);
    HistPlot.setHistStackSig([Hist_Sig],["Signal"])
    HistPlot.setHistStackBkg([Hist_Bkg],["Background"])
    if UsingData:
        HistPlot.setHistData([Hist_Data])
    #HistPlot.setLocation_Legend_plot(0.6,0.55,0.93,0.7) 
    HistPlot.setLocation_Legend_plot(0.2,0.8,0.6,0.9) 
    HistPlot.setLocation_ATLASTitle(0.62, 0.82,0.88, 0.9)
    Title = Title + ";Significance Z = %.3f "%(Z_bin)

    try:
        os.mkdir("output/BDT_VHcc_TTTL")
    except OSError as error:
       print "output/BDT_VHcc_TTTL" +" Exist"    

    #HistPlot.DrawPlots_ATLAS(Title, "BDT response","Number of events", ["Background","Signal"], "BDT_VHcc_TTTL"+"/BDT_"+regionType, [], "All", SetLogY_1D)
#    HistPlot.DrawPlotsSB_ATLAS(Title, "BDT response","Number of events",  "BDT_VHcc_TTTL"+"/BDT_SB_"+regionType, [], "SB", "Origin",SetLogY_1D)
    HistPlot.DrawPlotsSB_ATLAS(Title, "BDT response","Number of events",  "BDT_VHcc_TTTL"+"/BDT_SB_"+regionType+"_LogY", [], "SBsqrt", "Origin",SetLogY_1D)
    HistPlot.DrawPlotsSB_ATLAS(Title, "BDT response","Number of events",  "BDT_VHcc_TTTL"+"/BDT_SB_"+regionType, [], "SBsqrt", "Origin",False)
    HistPlot.DrawPlotsSB_ATLAS(Title, "BDT response","Number of events",  "BDT_VHcc_TTTL"+"/BDT_SB_"+regionType, [], "SBsqrt", "NORM",SetLogY_1D)
    if UsingData : 
        HistPlot.setMassWindow([0.2,1])
        HistPlot.applyBlindRegion()
        HistPlot.DrawPlotsSB_ATLAS(Title, "BDT response","Number of events",  "BDT_VHcc_TTTL"+"/BDT_SB_"+regionType, [], "DataMC", "Origin",SetLogY_1D)
        HistPlot.DrawPlotsSB_ATLAS(Title, "BDT response","Number of events",  "BDT_VHcc_TTTL"+"/BDT_SB_"+regionType, [], "DataMC", "NORM",SetLogY_1D)

    #
    # Generate New Signal TMVApp Hist 
    #
    print "___________%s___GenerateNewSigHist_________"%regionType+"TTTL"
    tmvapp_subDir=InputDir+"/New/"+"ExampleInputs_BDT_VHcc2Lep" 

    try:
        os.mkdir(InputDir+"/New/")
    except OSError as error:
       print InputDir+"/New"+" Exist"    
    try:
        os.mkdir(tmvapp_subDir)
    except OSError as error:
       print tmvapp_subDir+" Exist"    
    
    myFile_Sig_new = TFile(tmvapp_subDir+"/sig"+FileName,"recreate")
    Hist_Sig_new=Hist_Sig.Clone(SetName)
    
    # Get rid of <0 bins >> signal 
    print "NBinsX rebin  :%f _"%Hist_Sig_new.GetNbinsX();
    for ibinx in range(1, Hist_Sig_new.GetNbinsX()+1):
        if Hist_Sig_new.GetBinContent(ibinx)<0:
        #    print "Bin %d Content %f"%(ibinx,Hist_Sig_new.GetBinContent(ibinx))
            Hist_Sig_new.SetBinContent(ibinx,0)
        if Hist_Sig_new.GetBinContent(ibinx)<10:
            print "Sig Bin %d Content %f"%(ibinx,Hist_Sig_new.GetBinContent(ibinx))
    
    print "NewSigHist intregral %f "%(Hist_Sig_new.Integral())
    #Hist_Sig_new.Scale(100)
    Hist_Sig_new.Write()
    myFile_Sig_new.Close()

    #
    # Generate New BKG TMVApp Hist 
    #
    print "___________%s___GenerateNewBkgHist_________"%regionType+"TTTL"
    myFile_Bkg_new = TFile(tmvapp_subDir+"/bkg"+FileName,"recreate")
    Hist_Bkg_new=Hist_Bkg.Clone(SetName)

    # Get rid of <0 bins >> sbackground 
    print "NBinsX rebin  :%f _"%Hist_Bkg_new.GetNbinsX();
    for ibinx in range(1, Hist_Bkg_new.GetNbinsX()+1):
        if Hist_Bkg_new.GetBinContent(ibinx)<0:
            #print "Bin %d Content %f"%(ibinx,Hist_Bkg_new.GetBinContent(ibinx))
            Hist_Bkg_new.SetBinContent(ibinx,0)
        if Hist_Bkg_new.GetBinContent(ibinx)<10:
            print "Bkg Bin %d Content %f"%(ibinx,Hist_Bkg_new.GetBinContent(ibinx))
    
    print "NewBkgHist intregral %f "%(Hist_Bkg_new.Integral())

    Hist_Bkg_new.Write()
    myFile_Bkg_new.Close()
 
def TMVATraining_output(InputDir, InputFileName, datasetName, pTVRegion, TagRegion, TLtype, TagType, JetType,TMVA_Method="MVA_BDT",MVAInfo = ""):
    '''
    Get Correlation Matrix / BDT Distribution / ROC Curve
    MVAInfo = "NTree_Ndepth_NCuts"
    '''
    DirName="BDT_VHcc"+TagType
    try:
        os.mkdir("output/"+DirName)
    except OSError : 
        print "output/"+DirName+"  Exists "
    
    ##### ROC Curve #######
    myFile = TFile(InputDir+"/"+InputFileName+".root")

    ## BDT Origin Setting
    #HistName_Pre = datasetName +"/"+TMVA_Method+"/"+InputFileName+"/MVA_"+InputFileName 
    ## BDT TMVATraining_VHbb
    HistName_Pre = datasetName +"/"+TMVA_Method+"/"+InputFileName+"/MVA_"+InputFileName 
    # BDTG Setting
    #HistName_Pre = datasetName +"/"+TMVA_Method+"/MVA_BDTG" 
    # HistName_Pre = datasetName +"/Method_BDT/BDT/MVA_BDT"
    Hist_train = myFile.Get(HistName_Pre+"_trainingRejBvsS")
    Hist_test  = myFile.Get(HistName_Pre+"_rejBvsS")
    print InputFileName
    print datasetName
    print HistName_Pre
    pvalueKS_ROC = Hist_test.KolmogorovTest(Hist_train,"X")
    legend=["Train - ROCIntegral: %.3f"%Hist_train.Integral("width"),"Test - ROCIntegral: %.3f"%Hist_test.Integral("width")] 
    HistPlot=HistMaker1D([Hist_train,Hist_test],legend);
    HistPlot.setLocation_Legend_plot(0.2,0.75,0.5,0.85) 
    #HistPlot.DrawRatioPlots_ATLAS("Resolved VHcc ;%sTag %s pTV Region (%s)"%(TagRegion,pTVRegion,TLtype), "Bkgnal efficiency","Background rejection", legend, DirName+"/BDT_ROC%stag%s%s"%(TagRegion,pTVRegion,TLtype), [], "All", False)
    HistPlot.DrawRatioPlots_ATLAS("Resolved VHcc 2Lep %s;%s pTV Region (%s)"%(JetType, pTVRegion,TLtype), "Bkgnal efficiency","Background rejection", legend, DirName+"/BDT_ROC%stag%s%s%s%s"%(TagRegion,JetType,pTVRegion,TLtype,MVAInfo), [], "All", False)
    
    ##### BDT overtrain #######
    Hist_BDT_S_train = myFile.Get(HistName_Pre+"_Train_S")
    Hist_BDT_S_test  = myFile.Get(HistName_Pre+"_S")
    Hist_BDT_B_train = myFile.Get(HistName_Pre+"_Train_B")
    Hist_BDT_B_test  = myFile.Get(HistName_Pre+"_B")
    print ">>>>>>>>>>>> Integral B test: no width %f  with width %f"%(Hist_BDT_B_test.Integral(),Hist_BDT_B_test.Integral("width"))
    print ">>>>>>>>>>>> Integral S test: no width %f  with width %f"%(Hist_BDT_S_test.Integral(),Hist_BDT_S_test.Integral("width"))
    
    Hist_BDT_S_train.Rebin(4) 
    Hist_BDT_S_test.Rebin(4) 
    Hist_BDT_B_train.Rebin(4)
    Hist_BDT_B_test.Rebin(4) 
    
    pvalueKS_S = Hist_BDT_S_train.KolmogorovTest(Hist_BDT_S_test,"X")
    pvalueKS_S1 = Hist_BDT_S_test.KolmogorovTest(Hist_BDT_S_train,"X")
    #pvalueKS_S1 = Hist_BDT_S_train.KolmogorovTest(Hist_BDT_B_train,"X")
    pvalueKS_B1 = Hist_BDT_B_test.KolmogorovTest(Hist_BDT_B_train,"X")
    pvalueKS_B = Hist_BDT_B_train.KolmogorovTest(Hist_BDT_B_test,"X")
    print ">>>>>>>>>>>> Signal : KS test     :  p-value %f"%(pvalueKS_S)
    print ">>>>>>>>>>>> Signal test: KS test :  p-value %f"%(pvalueKS_S1)
    print ">>>>>>>>>>>> BKG: KS test         :  p-value %f"%(pvalueKS_B)
    print ">>>>>>>>>>>> BKG test: KS test    :  p-value %f"%(pvalueKS_B1)
     
    legend=["siganl train","siganl test","bkg train","bkg test"] 
    HistPlot_BDT = HistMaker1D([Hist_BDT_S_train,Hist_BDT_S_test,Hist_BDT_B_train,Hist_BDT_B_test],legend)
    
    HistPlot_BDT.setHistStackBkg([Hist_BDT_B_train,Hist_BDT_B_test],["Background (training sample)","Background (test Sample)"])
    HistPlot_BDT.setHistStackSig([Hist_BDT_S_train,Hist_BDT_S_test],["Signal (training sample)","Signal (test Sample)"])
    #BDT_PlotTitle = "Resolved VHcc ;%sTag %s pTV Region (%s);K-S test: P_{Sig. } = %.3f ;K-S test: P_{Bkg.} = %.3f "%(TagRegion,pTVRegion,TLtype,pvalueKS_S1,pvalueKS_B1)
    BDT_PlotTitle = "Resolved VHcc 2Lep %s;%s pTV Region (%s);K-S test: P_{Sig. } = %.3f ;K-S test: P_{Bkg.} = %.3f "%(JetType, pTVRegion,TLtype,pvalueKS_S1,pvalueKS_B1)
    HistPlot_BDT.setLocation_Legend_plot(0.2,0.7,0.6,0.9) 
    #HistPlot_BDT.Draw2RatioPlots_ATLAS(BDT_PlotTitle, "BDT response","(1/N)dN/dx", DirName+"/BDT_Overtrain_%stag%s%s"%(TagRegion,pTVRegion,TLtype), [], False)
    HistPlot_BDT.Draw2RatioPlots_ATLAS(BDT_PlotTitle, "BDT response","(1/N)dN/dx", DirName+"/BDT_Overtrain_%stag%s%s%s%s"%(TagRegion,JetType,pTVRegion,TLtype,MVAInfo), [], False)

    
    ##### Correlation Matrics #######
    Hist_Correlation_S = myFile.Get(datasetName +"/CorrelationMatrixS")
    Hist_Correlation_B = myFile.Get(datasetName +"/CorrelationMatrixB")
    HistPlot_2D_Corelation_S = HistMaker2D(Hist_Correlation_S,"CorrelationMatrixS") 
    #HistPlot_2D_Corelation_S.DrawPlots_ATLAS("CorrelationMatrix(Signal) : %sTag %s pTV Region (%s)"%(TagRegion,pTVRegion,TLtype),"","",DirName+"/BDT_CorrelationMatrixS_%stag%s%s"%(TagRegion,pTVRegion,TLtype),"COLZTEXT");
    HistPlot_2D_Corelation_S.DrawPlots_ATLAS("CorrelationMatrix(Signal) : %s %s pTV Region (%s)"%(JetType,pTVRegion,TLtype),"","",DirName+"/BDT_CorrelationMatrixS_%stag%s%s%s%s"%(TagRegion,JetType,pTVRegion,TLtype,MVAInfo),"COLZTEXT");
    
    HistPlot_2D_Corelation_B = HistMaker2D(Hist_Correlation_B,"CorrelationMatrixB") 
    #HistPlot_2D_Corelation_B.DrawPlots_ATLAS("CorrelationMatrix(Background) : %sTag %s pTV Region (%s)"%(TagRegion,pTVRegion,TLtype),"","",DirName+"/BDT_CorrelationMatrixB_%stag%s%s"%(TagRegion,pTVRegion,TLtype),"COLZTEXT");
    HistPlot_2D_Corelation_B.DrawPlots_ATLAS("CorrelationMatrix(Background) : %s %s pTV Region (%s)"%(JetType,pTVRegion,TLtype),"","",DirName+"/BDT_CorrelationMatrixB_%stag%s%s%s%s"%(TagRegion,JetType,pTVRegion,TLtype,MVAInfo),"COLZTEXT");

def TMVATraining_output_overtarin(InputDir, InputFileName,  datasetName, pTVRegion, TagRegion, TLtype, TagType):
    '''
    Get Correlation Matrix / BDT Distribution / ROC Curve
    '''
    DirName="BDT_VHcc_HighPTV_CrossCheck"
    try:
        os.mkdir("output/"+DirName)
    except OSError : 
        print "output/"+DirName+"  Exists "
    
    ##### ROC Curve #######
    myFile = TFile(InputDir+"/"+InputFileName+".root")
       
    ##### BDT overtrain #######
    Hist_BDT_S_train = myFile.Get("Train_Sig")
    Hist_BDT_S_test  = myFile.Get("Test_Sig")
    Hist_BDT_B_train = myFile.Get("Train_Bkg")
    Hist_BDT_B_test  = myFile.Get("Test_Bkg")
    print ">>>>>>>>>>>> Integral B test: no width %f  with width %f"%(Hist_BDT_B_test.Integral(),Hist_BDT_B_test.Integral("width"))
    print ">>>>>>>>>>>> Integral S test: no width %f  with width %f"%(Hist_BDT_S_test.Integral(),Hist_BDT_S_test.Integral("width"))
    
    Hist_BDT_S_train.Scale(1.0/Hist_BDT_S_train.GetSumOfWeights()); 
    Hist_BDT_S_test.Scale(1.0/Hist_BDT_S_test.GetSumOfWeights()); 
    Hist_BDT_B_train.Scale(1.0/Hist_BDT_B_train.GetSumOfWeights()); 
    Hist_BDT_B_test.Scale(1.0/Hist_BDT_B_test.GetSumOfWeights()); 
    #   Hist_BDT_B_train.Rebin(4)
    #   Hist_BDT_B_test.Rebin(4) 
    #   Hist_BDT_S_train.Rebin(4) 
    #   Hist_BDT_S_test.Rebin(4) 
    #   Hist_BDT_B_train.Rebin(4)
    #   Hist_BDT_B_test.Rebin(4) 
    
    pvalueKS_S = Hist_BDT_S_train.KolmogorovTest(Hist_BDT_S_test,"X")
    pvalueKS_S1 = Hist_BDT_S_test.KolmogorovTest(Hist_BDT_S_train,"X")
    #pvalueKS_S1 = Hist_BDT_S_train.KolmogorovTest(Hist_BDT_B_train,"X")
    pvalueKS_B1 = Hist_BDT_B_test.KolmogorovTest(Hist_BDT_B_train,"X")
    pvalueKS_B = Hist_BDT_B_train.KolmogorovTest(Hist_BDT_B_test,"X")
    print ">>>>>>>>>>>> Signal : KS test     :  p-value %f"%(pvalueKS_S)
    print ">>>>>>>>>>>> Signal test: KS test :  p-value %f"%(pvalueKS_S1)
    print ">>>>>>>>>>>> BKG: KS test         :  p-value %f"%(pvalueKS_B)
    print ">>>>>>>>>>>> BKG test: KS test    :  p-value %f"%(pvalueKS_B1)
     
    legend=["siganl train","siganl test","bkg train","bkg test"] 
    HistPlot_BDT = HistMaker1D([Hist_BDT_S_train,Hist_BDT_S_test,Hist_BDT_B_train,Hist_BDT_B_test],legend)
    
    HistPlot_BDT.setHistStackBkg([Hist_BDT_B_train,Hist_BDT_B_test],["Background (training sample)","Background (test Sample)"])
    HistPlot_BDT.setHistStackSig([Hist_BDT_S_train,Hist_BDT_S_test],["Signal (training sample)","Signal (test Sample)"])
    BDT_PlotTitle = "Resolved VHcc 2Lep;%s pTV Region (%s);Med+High pTV Training;K-S test: P_{Sig. } = %.3f ;K-S test: P_{Bkg.} = %.3f "%(pTVRegion,TLtype,pvalueKS_S1,pvalueKS_B1)
    HistPlot_BDT.setLocation_Legend_plot(0.2,0.7,0.6,0.9) 
    HistPlot_BDT.Draw2RatioPlots_ATLAS(BDT_PlotTitle, "BDT response","(1/N)dN/dx", DirName+"/BDT_Overtrain_%stag%s%s"%(TagRegion,pTVRegion,TLtype), [], False)
    
    
