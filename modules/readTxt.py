#!/usr/bin/python2.7

# Y.Chen <chenye94@mial.ustc.edu>
######################################################
# Python module to read TXT file info  
######################################################

########################
# Libraries and Includes
########################

import math
import os, sys 
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")

sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

from ClassDef import ThrInfo, RunInfo
#########################
# The list of functions #
#########################



def readtxt( fileName):
    
    thresholds_final = {}
    thresholds = {}
    for i in range(1,7):
        thresholds[i] = {}
        thresholds_final[i] = {}

    with open(fileName) as f:
        num = 0
        for line in f:
            num += 1
            fileEmpty = 0
            if num <= 10 :
                continue
            lineInfo = line.split(",")
            if len(lineInfo) >  5:
                Info = RunInfo(int(lineInfo[0]), int(lineInfo[1]), int(lineInfo[2]), float(lineInfo[3]), float(lineInfo[4]))
                # print Info

                try:
                    tmp = thresholds[int(lineInfo[2])][int(lineInfo[1])] 
                except KeyError:
                    thresholds[int(lineInfo[2])][int(lineInfo[1])] = []

                thresholds[int(lineInfo[2])][int(lineInfo[1])] += [Info]

    #print len(thresholds)
    for ith in thresholds.keys():
        for day in thresholds[ith].keys():
            avgCal = CalAverage(thresholds[ith][day])
            thresholds_final[ith][day] = avgCal

    return thresholds_final

def CalAverage(Infos):

    run = 0  
    day = 0
    ith = 0
    avgVal = 0
    avgErr = 0
    count = 0
    
    for Info in Infos:

        if count == 0 :
            run = Info.run
            day = Info.day
            ith = Info.ith

        Err = Info.efferr
        Val = Info.effval
        avgVal += Val
        avgErr += Err*Err
        count+=1

    Val = avgVal / len(Infos)
    Err = math.sqrt(avgErr) / len(Infos)
    Final = RunInfo(run, day, ith, Val, Err)
    return Final 
