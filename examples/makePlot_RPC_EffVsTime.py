#!/usr/bin/python2.7
# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging
import array
from array import array

# Python Paths
ROOTLIB, PATH_MyPlot, PATH_MyPlot_Modules = "", "", ""
if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_MyPlot"): PATH_MyPlot=os.getenv("PATH_MyPlot")
if os.getenv("PATH_MyPlot_Modules"): PATH_MyPlot_Modules=os.getenv("PATH_MyPlot_Modules")
sys.path.append(ROOTLIB)
sys.path.append(PATH_MyPlot)
sys.path.append(PATH_MyPlot_Modules)

# Python Math Modules
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad, TGraphErrors
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
from readTxt import readtxt
from ClassDef import ThrInfo, RunInfo


# #########################
# User defined drawing function
# #########################

def ClassConvert( Info ):
    xvals=[]
    yvals=[]
    xerrs=[]
    yerrs=[]

    for ith in Info.keys():
        xvals += [Info[ith].day]
        xerrs += [Info[ith].dayerr]
        yvals += [Info[ith].effval]
        yerrs += [Info[ith].efferr]
    
    return GraphMaker(xvals, yvals, xerrs, yerrs)

class GraphMaker:

    '''make graph '''

    def __init__(self, xvals, yvals, xerrs,  yerrs):
        ## xval err are dictionary num(0~n)
        self.x_vals = xvals
        self.x_errs = xerrs
        self.y_vals = yvals
        self.y_errs = yerrs
        self.nGroup = len(xvals)
        self.nbins = {}
        self.graphErrs = {}
        
        for i in range(0,self.nGroup):
            self.nbins[i]=len(xvals[i])
        
        print "ngroup : %d"%self.nGroup 

    def setATLAS(self):

        ''' ATLAS Logo & detail Bassed on TPaveText '''
    
        logo = ROOT.TPaveText(0.22,0.2,0.6,0.5,"NDC");
        logo.AddText("#bf{#it{ATLAS}} Internal")
        logo.AddText("Data 2018, #sqrt{s} = 13 TeV, 60.8 fb^{-1}")
        logo.GetListOfLines().Last().SetTextFont(42)
        logo.GetListOfLines().Last().SetTextSize(0.05)
        logo.AddText("Z #rightarrow #mu#mu, p_{T}^{#mu} > 25 GeV" )
        logo.GetListOfLines().Last().SetTextFont(42)
        logo.GetListOfLines().Last().SetTextSize(0.05)
        logo.AddText("0.1 < #left|#eta^{#mu}#right| < 1.05")
        logo.GetListOfLines().Last().SetTextFont(42)
        logo.GetListOfLines().Last().SetTextSize(0.05)
        logo.SetBorderSize(0)
        logo.SetFillColor(10)
        logo.SetTextColor(1)
        logo.SetTextAlign(12)
        
        return logo  


    def MakeGraphErrors(self):

        ''' Use list info  to make TGrapErrors Plot '''

        can_Fin = ROOT.TCanvas("c1","c1",850,600) 
        MarkerStyle=[21,20,22]
        for i in range(0,self.nGroup):
            Style=i%3
            print "style %d" %Style
            gr = ROOT.TGraphErrors(self.nbins[i], array('d', self.x_vals[i]), array('d', self.y_vals[i]), array('d', self.x_errs[i]), array('d', self.y_errs[i]))
            gr.SetMarkerSize(1)
            gr.SetMarkerStyle(MarkerStyle[Style])
            #gr = ROOT.TGraphErrors(self.nbins[i], array('d', self.x_vals[i]), array('d', self.y_vals[i]), array('d', self.x_errs[i]), array('d', self.y_errs[i]))
            #gr.SetTitle(title)
            #gr.GetXaxis().SetTitle(xtitle)
            #gr.GetYaxis().SetTitle(ytitle)

            self.graphErrs [i] = gr  

        
    def DrawPlots_ATLAS(self, title, xtitle, ytitle):
        ''' Draw Plots '''

        print '============ Graph Maker: Draw Plots ========'
        can_Fin = ROOT.TCanvas("c1","c1",850,600) 
        atlas_style()
        frame = ROOT.TH2D("frame",title, 1, 110, 300, 1, 0.4 ,0.84)
        frame.GetXaxis().SetTitle(xtitle)
        frame.GetYaxis().SetTitle(ytitle)
        frame.SetLabelSize(0.055,"X")
        frame.SetLabelSize(0.055,"Y")
        frame.SetTitleSize(0.055,"X")
        frame.SetTitleSize(0.055,"Y")

        frame.GetXaxis().ChangeLabel( 1, -1, 0.055,-1,-1,42, "30/4");
        frame.GetXaxis().ChangeLabel( 2, -1, 0.055,-1,-1,42, "20/5");
        frame.GetXaxis().ChangeLabel( 3, -1, 0.055,-1,-1,42, "9/6");
        frame.GetXaxis().ChangeLabel( 4, -1, 0.055,-1,-1,42, "29/6");
        frame.GetXaxis().ChangeLabel( 5, -1, 0.055,-1,-1,42, "19/7");
        frame.GetXaxis().ChangeLabel( 6, -1, 0.055,-1,-1,42, "8/8");
        frame.GetXaxis().ChangeLabel( 7, -1, 0.055,-1,-1,42, "28/8");
        frame.GetXaxis().ChangeLabel( 8, -1, 0.055,-1,-1,42, "18/9");
        frame.GetXaxis().ChangeLabel( 9, -1, 0.055,-1,-1,42, "8/10");
        frame.GetXaxis().ChangeLabel( 10, -1, 0.055,-1,-1,42, "28/10");
        frame.SetStats(0)
        frame.Draw()

        gStyle.SetLegendTextSize(0.045)
        legeff = ROOT.TLegend(0.756,0.2,0.93,0.5)
        legeff.SetFillColor(0)
        legeff.SetBorderSize(0)
        legeff.SetFillStyle(0)
        legeff.SetTextFont(42)
        #legeff.SetTextSize(0.055)
    
        ## Draw Option
        col = [1,2,3,4,6,7]
        MUi = [4,6,10,11,20,21]
 
        for ith in range(0,self.nGroup):

            self.graphErrs[ith].SetLineColor(col[ith])
            self.graphErrs[ith].SetLineWidth(2)
            self.graphErrs[ith].SetLineStyle(42)
            #self.graphErrs[ith].SetMarkerStyle(20)
            self.graphErrs[ith].SetMarkerColor(col[ith])
            self.graphErrs[ith].SetMarkerSize(1.0)
            self.graphErrs[ith].Draw("eP")
            
            legeff.AddEntry(self.graphErrs[ith],'L1 MU%d'%(MUi[ith]),"P")       
                
        legeff.Draw()

        logo=self.setATLAS()
        logo.Draw()
        name = 'Eff_vs_time'
        can_Fin.SaveAs("output/%s.pdf"%name)
        can_Fin.SaveAs("output/%s.C"%name)


 

        

def main():
    atlas_style()

    ## read form file ###
    Infos = readtxt("data/Final_Info_ztp_20181210.txt")

    ## info DProcess ## 
    thrInfos = {}
    for ith in range (1,7):
        thrInfos[ith]=ThrInfo(ith)

    for ith in Infos.keys():
        for day in Infos[ith]:
            print "ith %s day %s"%(ith,day)
            #print Infos[ith][day] 
            thrInfos[ith].AddRunInfo(Infos[ith][day])

    
    graph = ClassConvert(thrInfos)

    graph.MakeGraphErrors()
    graph.DrawPlots_ATLAS("Effitiency vs time","day/month in 2018"," L1 muon barrel trigger efficiency")


if __name__ == "__main__":
    main()
