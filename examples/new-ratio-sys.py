#!/usr/bin/python2.6

# ###########
# # Make Plots
# ###########

# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging

import array
# Python Paths
ROOTLIB, PATH_OnePlot_MODULE1, PATH_OnePlot_MODULE2 = "", "", ""

if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
if os.getenv("PATH_OnePlot_MODULE1"): PATH_OnePlot_MODULE1=os.getenv("PATH_OnePlot_MODULE1")
if os.getenv("PATH_OnePlot_MODULE2"): PATH_OnePlot_MODULE2=os.getenv("PATH_OnePlot_MODULE2")

sys.path.append(ROOTLIB)
sys.path.append(PATH_OnePlot_MODULE1)
sys.path.append(PATH_OnePlot_MODULE2)

# Python Math Modules
import array
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
import ROOT

# user modules
from oneplot import oneplot
from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
from module_style import atlas_style
    
# #########################
# User defined drawing function
# #########################

def createRatio(h1, h2, lineColor):
    h3 = h1.Clone("h3")
    h3.SetLineColor(lineColor)
    h3.SetMarkerStyle()
    #h3.SetTitle("")
    h3.SetMinimum(0.95)
    h3.SetMaximum(1.05)
    # Set up plot for markers and errors
    h3.Sumw2()
    h3.SetStats(0)
    h3.Divide(h2)
    h3.SetLineWidth(2)
    # Adjust y-axis settings
    y = h3.GetYaxis()
    y.SetTitle(" Systematic / Nominal ")
    #y.SetNdivisions(505)
    y.SetTitleSize(20)
    y.SetTitleFont(43)
    y.SetTitleOffset(1.55)
    y.SetLabelFont(43)
    y.SetLabelSize(15)

    # Adjust x-axis settings
    x = h3.GetXaxis()
    x.SetTitleSize(20)
    x.SetTitleFont(43)
    x.SetTitleOffset(4.0)
    x.SetLabelFont(43)
    x.SetLabelSize(15)

    return h3


def createCanvasPads():
    c = TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.3)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.2)
    pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2


def ratioplot(hists, lineColors, Legends, xtitle, outputPlot):
    
    denom_hist = hists[0]
    nom_hists   = hists[1:]
    nom_lineColors = lineColors[1:]
    max_y = GetMaxY(hists)
    min_x = GetMinX(denom_hist)
    max_x = GetMaxX(denom_hist)
    c, pad1, pad2 = createCanvasPads()
    
    ''' Draw Pad1 '''
    
    pad1.cd()
    Count = 0
    logo = TPaveText(0.15,0.79,0.41,0.85,"NDC");
    logo.AddText("ATLAS Internal")
    logo.SetBorderSize(0)
    logo.SetFillColor(10)
    logo.SetTextColor(1)
    logo.SetTextSize(0.06)
    logo.SetTextAlign(12)
    leg=TLegend(0.53, 0.3, 0.9, 0.87)
    leg.SetTextFont(42)
    leg.SetTextSize(0.023)
    leg.SetBorderSize(0)
    leg.SetFillColor(10)

    for hist,lineColor,legend in zip(hists, lineColors, Legends):
        
        hist.SetLineColor(lineColor)
        hist.SetLineWidth(1)
        #hist.SetLineWidth(1)
        hist.SetStats(0)
        hist.SetTitle("") 
        if Count == 0 :
            hist.GetYaxis().SetRangeUser(0,1.3*max_y)
            hist.GetXaxis().SetRangeUser(min_x, 1.3*max_x)
            hist.GetYaxis().SetTitle("Events")
            #to avoid clipping the bottom zero, redraw a small axis
            hist.GetYaxis().SetLabelSize(0.0)
            hist.Draw()
        else : 
            hist.GetYaxis().SetTitle("Events")
            hist.Draw("same")
        
        leg.AddEntry(hist, legend, "L")
        Count += 1

    leg.Draw()
    logo.Draw()

   
    ''' Draw axis'''
    
    axis = TGaxis(min_x, 0, min_x, 1.3*max_y, 0, 1.3*max_y, 520, "")
    axis.SetLabelFont(43)
    axis.SetLabelSize(15)
    axis.SetTitle("Events")
    axis.Draw()
    
    ''' Draw Pad2 '''

    pad2.cd()
    Count=0
    ratio_hists=[]

    for nom_hist, lineColor in zip(nom_hists, nom_lineColors):
        ratioHist = createRatio(nom_hist, denom_hist, lineColor)
        ratio_hists += [ratioHist]

    for ratio_hist, lineColor in zip(ratio_hists, nom_lineColors):
        if Count == 0 :
            ratio_hist.GetXaxis().SetRangeUser(min_x, 1.3*max_x)
            ratio_hist.Draw("HIST")
            ratio_hist.GetXaxis().SetTitle(xtitle)
        else:
            ratio_hist.Draw("HIST SAME")
            #ratioHist.Draw("PL SAME","","SAME")
        Count += 1 
    c.SaveAs(outputPlot)

def GetMinX(hist):
    c1=hist.GetBinCenter(1)
    c2=hist.GetBinCenter(2)
    min = c1 - ( c2 - c1 )/2
    return min
def GetMaxX(hist):
    maxbin = hist.GetNbinsX()
    c1=hist.GetBinCenter(maxbin)
    c2=hist.GetBinCenter(maxbin-1)
    max = c1 + ( c1 - c2 )/2
    return max

def GetMaxY(hists):
    max = 0
    for hist in hists : 
        for i in range(hist.GetNbinsX()):
            if hist.GetBinContent(i+1) > max : 
                max = hist.GetBinContent(i+1)
    return max 

if __name__ == "__main__":



    # DrawCommand = ["Digits_MM_time>>h_digit_time(40,0,200)", "...", ""]
    # ArrayNames = ["A", "B", ..]
    # ArrayXtitle = ["A", ...]
    # Ytitle = "dsds"

    # Get the histograms
    #rootfileA_ggh="../miniswap/ggh_mc16a_part.root"

    # ==================
    # ROOT FILE
    # ==================
    #FileDir="/eos/user/c/chenye/share/FinalResults/"
    
    #rootfile_data=FileDir+"data.root"
    #rootfile_data="output_Data.root"
    #rootfileE_Zmumu="/afs/cern.ch/user/c/chenye/private/Yusheng-MakePlot/OnePlot/Zmumu_mc16e_585_mu0_45_met15.root"
    #rootfile_dir="/afs/cern.ch/work/c/chenye/public/Hmm/Yusheng/mc16a.345097_ggF/"
    rootfile_dir="/afs/cern.ch/user/c/chenye/private/work/Systematic_hmumu/results/mc16ade.345097_ggF_phi/"
    #rootfile_dir="/afs/cern.ch/user/c/chenye/private/work/Systematic_hmumu/results/test/"
    rootfile_name = [
                        "output_DiMuonNtuple.root",
                        "output_DiMuonNtupleMUON_ID__1down.root",
                        "output_DiMuonNtupleMUON_ID__1up.root",
                        "output_DiMuonNtupleMUON_MS__1down.root",
                        "output_DiMuonNtupleMUON_MS__1up.root",
                        "output_DiMuonNtupleMUON_SAGITTA_RESBIAS__1down.root",
                        "output_DiMuonNtupleMUON_SAGITTA_RESBIAS__1up.root",
                        "output_DiMuonNtupleMUON_SAGITTA_RHO__1down.root",
                        "output_DiMuonNtupleMUON_SAGITTA_RHO__1up.root",
                        "output_DiMuonNtupleMUON_SCALE__1down.root",
                        "output_DiMuonNtupleMUON_SCALE__1up.root"
    ]
    hists_name    = [ 
                        "h_mu1_pt_","h_mu1_eta_",
                        "h_mu2_pt_","h_mu2_eta_",
                        "h_mumu_mass_","h_mumu_pt_", 
                        "h_met_","h_avgMu_",
                        "h_mjj_","h_drjj_","h_Nj_",
                        "h_Ljet_eta_","h_Ljet_pt_",
                        "h_Sjet_eta_","h_Sjet_pt_"                    
    ]

    Systemetic_type1 = [
                        "origin",
                        "MUON_EFF_ISO_STAT__1down",
                        "MUON_EFF_ISO_STAT__1up",
                        "MUON_EFF_ISO_SYS__1down",
                        "MUON_EFF_ISO_SYS__1up",
                        "MUON_EFF_RECO_STAT_LOWPT__1down",
                        "MUON_EFF_RECO_STAT_LOWPT__1up",
                        "MUON_EFF_RECO_STAT__1down",
                        "MUON_EFF_RECO_STAT__1up",
                        "MUON_EFF_RECO_SYS_LOWPT__1down",
                        "MUON_EFF_RECO_SYS_LOWPT__1up",
                        "MUON_EFF_RECO_SYS__1down",
                        "MUON_EFF_RECO_SYS__1up",
                        "MUON_EFF_TTVA_STAT__1down",
                        "MUON_EFF_TTVA_STAT__1up",
                        "MUON_EFF_TTVA_SYS__1down",
                        "MUON_EFF_TTVA_SYS__1up",
                        "MUON_EFF_TrigStatUncertainty__1down",
                        "MUON_EFF_TrigStatUncertainty__1up",
                        "MUON_EFF_TrigSystUncertainty__1down",
                        "MUON_EFF_TrigSystUncertainty__1up"
    ] 
    Systemetic_type2 = [
                        "MUON_ID__1down",
                        "MUON_ID__1up",
                        "MUON_MS__1down",
                        "MUON_MS__1up",
                        "MUON_SAGITTA_RESBIAS__1down",
                        "MUON_SAGITTA_RESBIAS__1up",
                        "MUON_SAGITTA_RHO__1down",
                        "MUON_SAGITTA_RHO__1up",
                        "MUON_SCALE__1down",
                        "MUON_SCALE__1up"
                    ]
    fignames =  [   "mu1_pt_sys","mu1_eta_sys",
                    "mu2_pt_sys","mu2_eta_sys",
                    "mumu_mass_sys","mumu_pt_sys", 
                    "met_sys","Mu_sys",
                    "mjj_sys","drjj_sys","Nj_sys",
                    "Ljet_eta_sys","Ljet_pt_sys",
                    "Sjet_eta_sys","Sjet_pt_sys"                    
    ]
    xtitles = ["Leading Muon pT [GeV]","Leading Muon Eta",
               "Sub-leading Muon pT [Gev]","Sub-leading Muon Eta",
               "Di-Muon Mass [GeV]","Di-Muon pT [GeV]",
               "MET [GeV]","< Mu >",
               "mjj [GeV]","DeltaR jj","Number of Jets",
               "Leading Jet Eta", "Leading Jet pT [GeV]",
               "Sub-leading Jet Eta", "Sub-leading Jet pT [GeV]",
    ]
    titles = ["Leading Muon pT ","Leading Muon Eta",
               "Sub-leading Muon pT ","Sub-leading Muon Eta",
               "Di-Muon Mass ","Di-Muon pT ",
               "MET ","< Mu >",
               "mjj ","DeltaR jj","Number of Jets",
               "Leading Jet Eta", "Leading Jet pT ",
               "Sub-leading Jet Eta", "Sub-leading Jet pT ",
    ]
    
    #fileinE_Zmumu = TFile(rootfileE_Zmumu)

    #==================================================== 
    # Setup names and legends
    #
    
    # set up plotting attributes
    #options=["HIST","PLE"]
    options=    ["HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST",
                 "HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST",
                 "HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST"]
    #opt_legends=["f","FLPE"]
    #opt_legends=["HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST",
     #            "HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST",
      #           "HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST"]

    opt_legends=["PL","PL","PL","PL","PL","PL","PL","PL","PL","PL",
                 "PL","PL","PL","PL","PL","PL","PL","PL","PL","PL",
                 "PL","PL","PL","PL","PL","PL","PL","PL","PL","PL","PL"]
    marker_types=             [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    marker_sizes=             [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    #marker_colors=line_colors=[1,2,3,4,6,7,8,9,11,12,13,14,15,24,27,30,31,32,33,34,38,39,39,40,43,44,45,46,47,49]
    marker_colors=line_colors=[1,2,3,4,5,6,7,8,9,11,12,13,14,15,30,31,32,33,34,38,39,39,40,41,42,43,44,45,46,49]
    line_sizes   =            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    #fill_colors=              [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    fill_colors  =            [1,2,3,4,5,6,7,8,9,11,12,13,14,15,30,31,32,33,34,38,39,39,40,41,42,43,44,45,46,49]
    fill_types=               [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

    #fill_types=[3002, 0]
    #-----------------------------------------------------------
    #-----------------------------------------------------------
    #leading jet
    #

    #for physics_name in physics_names : 
    Systemetic_type = Systemetic_type1 +Systemetic_type2

    for physics_name ,x_title in  zip (hists_name, xtitles): 
        AllHists = []
        files = []

        #------------------
        #  read Hist 
        #
        print"==============================================================="
        print "readHist"
        print"==============================================================="

        for filename in rootfile_name :
            file_name = rootfile_dir + filename
            print file_name
            myFile = TFile(file_name)
            files += [myFile]

        fileCount=0 
        for filename,file in zip(rootfile_name,files) :
            if fileCount == 0:
                for hist_type1 in Systemetic_type1:

                    print"------------------------------------------------"
                    print physics_name+hist_type1
                    h1 = file.Get(physics_name+hist_type1)
                    print "BinContent test : %s"% h1.GetBinContent(10)
                    AllHists += [h1]
            else : 
                print"------------------------------------------------"
                print physics_name+"origin"
                h2 = file.Get(physics_name+"origin")
                print "BinContent test : %s"%h2.GetBinContent(10)
                AllHists += [h2]
            fileCount += 1
   
        #------------------
        #  check  Hist 
        #
        print"==============================================================="
        print "CHECK"
        print"==============================================================="
        for hist, systype in zip(AllHists,Systemetic_type):
            print "%s GetNbinsX() %s  GetBinContent %s "%(systype,hist.GetNbinsX(), hist.GetBinContent(10))
        #--------------------
        # Draw in One Plot
        #
        #for hist_ZmumuE , hist_data , fig , x_title in zip(hists_origin, data_hists, fignames, xtitles):
        print"==============================================================="
        print "Draw"
        print"==============================================================="
        

        list_histo = AllHists
        names = Systemetic_type
        legends = Systemetic_type
        legends[0] = "Nominal"

        print "length : names %d legend %d opt_legend %d color %d option %d  "%(len(names),len(legends),len(opt_legends),len(marker_colors),len(options))
        # set fig name
        #dir_name="Plot-Syst-mc16ade-newcolor-bigratio/"
        dir_name="Plot-Syst-test/"
        figname=dir_name+physics_name+"total.png"
        d = os.path.join(dir_name)
        if not os.path.exists(d):
            os.makedirs(d)
        
        ratioplot(list_histo, line_colors ,legends, x_title, figname)        


        '''    
        # draw the histograms
        theone = oneplot("DEBUG")
        theone.initialize(
                        list_histo=list_histo,names=names,legends=legends, opt_legends=opt_legends,
                        options = options,
                        xtitle=x_title ,ytitle="Events",
                        figname=figname,
                        ##ratio=0.2,ratiotitle="Data/MC",ratio_denominater="origin",
                        ratio=0.2 , ratio_denominater="origin",
                        #ratio_marker_types=marker_types, ratio_marker_colors=line_colors,
                        marker_types=marker_types, marker_sizes = marker_sizes,
                        line_sizes=line_sizes, line_colors=line_colors, marker_colors=marker_colors,
                        #fill_types = fill_types, fill_colors = fill_colors
                        #stack=["Zjets", "Top", "qqZZ", "WZ", "WW"]
                        )
        theone.plot1DHistogram()
        theone.finish()
        '''
#        fileinE_Zmumu.Close()
