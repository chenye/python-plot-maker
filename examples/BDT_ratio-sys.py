#!/usr/bin/python2.6

# ###########
# # Make Plots
# ###########

# ####################
# # Libraries and Includes
# ####################

# Python System Modules
import os,sys,glob
import logging

import array
# Python Paths
ROOTLIB, PATH_OnePlot_MODULE1, PATH_OnePlot_MODULE2 = "", "", ""

if os.getenv("ROOTLIB"): ROOTLIB=os.getenv("ROOTLIB")
#if os.getenv("PATH_OnePlot_MODULE1"): PATH_OnePlot_MODULE1=os.getenv("PATH_OnePlot_MODULE1")
#if os.getenv("PATH_OnePlot_MODULE2"): PATH_OnePlot_MODULE2=os.getenv("PATH_OnePlot_MODULE2")

sys.path.append(ROOTLIB)
#sys.path.append(PATH_OnePlot_MODULE1)
#sys.path.append(PATH_OnePlot_MODULE2)

# Python Math Modules
import array
import math
from math import sqrt,fabs,sin,pow

# ROOT Modules
from ROOT import TFile,TTree,TChain,TBranch,TH1F,TH2F,TH1D,TLegend, TGaxis, TPaveText
from ROOT import TCanvas,TPad
from ROOT import TLorentzVector
from ROOT import gROOT, gDirectory,gStyle
from ROOT import TPaveText, TLegend
from ROOT import kDeepSea 
import ROOT

# user modules
#from oneplot import oneplot
#from module_syst import systPDFCT10, systEnvelope, syst1v1, systCombine
#from module_style import atlas_style

# #########################
# User defined drawing function
# #########################

def createRatio(h1, h2, lineColor, MinY, MaxY):
    h3 = h1.Clone("h3")
    h3.SetLineColor(lineColor)
    h3.SetMarkerStyle()
    #h3.SetTitle("")
    #h3.SetMinimum(MinY)
    #h3.SetMaximum(MaxY)
    # Set up plot for markers and errors
    h3.Sumw2()
    h3.SetStats(0)
    h3.Divide(h2)
    h3.SetLineWidth(2)
    # Adjust y-axis settings
    y = h3.GetYaxis()
    y.SetTitle(" Systematic / Nominal ")
    #y.SetNdivisions(505)
    y.SetTitleSize(20)
    y.SetTitleFont(43)
    y.SetTitleOffset(1.55)
    y.SetLabelFont(43)
    y.SetLabelSize(15)

    # Adjust x-axis settings
    x = h3.GetXaxis()
    x.SetTitleSize(20)
    x.SetTitleFont(43)
    x.SetTitleOffset(4.0)
    x.SetLabelFont(43)
    x.SetLabelSize(15)

    return h3


def createCanvasPads():
    c = TCanvas("c", "canvas", 800, 800)
    # Upper histogram plot is pad1
    pad1 = TPad("pad1", "pad1", 0, 0.4, 1, 1.0)
    pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = TPad("pad2", "pad2", 0, 0.05, 1, 0.39)
    pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetBottomMargin(0.2)
    pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2


def ratioplot(hists, lineColors, Legends, xtitle, outputPlot, RatioMinY, RatioMaxY, PlotInfo):

    denom_hist = hists[0]
    #nom_hists   = hists[1:]
    #nom_lineColors = lineColors[1:]
    nom_hists   = hists
    nom_lineColors = lineColors
    max_y = GetMaxY(hists)
    min_x = GetMinX(denom_hist)
    max_x = GetMaxX(denom_hist)
    c, pad1, pad2 = createCanvasPads()
    print ">>>>>>> max Y : %s"%max_y

    ''' Draw Pad1 '''

    pad1.cd()
    Count = 0
    logo = TPaveText(0.15,0.79,0.41,0.85,"NDC");
    logo.AddText("ATLAS Internal")
    logo.SetBorderSize(0)
    logo.SetFillColor(10)
    logo.SetTextColor(1)
    logo.SetTextSize(0.06)
    logo.SetTextAlign(12)

    plotInfo = TPaveText(0.15,0.7,0.41,0.75,"NDC");
    plotInfo.AddText(PlotInfo)
    plotInfo.SetBorderSize(0)
    plotInfo.SetFillColor(10)
    plotInfo.SetTextColor(1)
    plotInfo.SetTextSize(0.04)
    plotInfo.SetTextAlign(12)

    leg=TLegend(0.5, 0.35, 0.87, 0.88)
    #leg=TLegend(0.55, 0.45, 0.9, 0.9)
    leg.SetTextFont(42)
    leg.SetTextSize(0.027)
    leg.SetBorderSize(0)
    leg.SetFillColor(10)

    lineColors_new = lineColors[:len(hists)]
    Legends_new = Legends[:len(hists)]

    for hist,lineColor,legend in zip(hists, lineColors_new, Legends_new):

        hist.SetLineColor(lineColor)
        hist.SetLineWidth(1)
        #hist.SetLineWidth(1)
        hist.SetStats(0)
        hist.SetTitle("") 
        if Count == 0 :
            hist.GetYaxis().SetRangeUser(0,2.7*max_y)
            hist.GetXaxis().SetRangeUser(min_x, 1.5*max_x)
            #hist.GetXaxis().SetRangeUser(0, 1.5*max_x)
            hist.GetYaxis().SetTitle("Events")
            #to avoid clipping the bottom zero, redraw a small axis
            hist.GetYaxis().SetLabelSize(0.0)
            hist.Draw()
        else : 
            hist.GetYaxis().SetTitle("Events")
            hist.Draw("same")

        leg.AddEntry(hist, legend, "L")
        Count += 1

    leg.Draw()
    logo.Draw()
    plotInfo.Draw()


    ''' Draw axis'''
    print "min_X------------>>>>>  %s"%min_x
    axis = TGaxis(min_x, 0, min_x, 2.7*max_y, 0, 2.7*max_y, 520, "")
    #axis = TGaxis(0, 0, 0, 2.7*max_y, 0, 2.7*max_y, 520, "")
    axis.SetLabelFont(43)#43
    axis.SetLabelSize(15)#15
    axis.SetTitle("Events")
    axis.Draw()

    ''' Draw Pad2 '''

    pad2.cd()
    Count=0
    ratio_hists=[]

    for nom_hist, lineColor in zip(nom_hists, nom_lineColors):
        ratioHist = createRatio(nom_hist, denom_hist, lineColor, RatioMinY, RatioMaxY )
        ratio_hists += [ratioHist]

    ratio_max = GetMaxY(ratio_hists)
    ratio_min = GetMinY(ratio_hists)
    fluctuation_up   = math.fabs(ratio_max -1) 
    fluctuation_down = math.fabs(ratio_min -1) 

    if fluctuation_down > fluctuation_up :
        fluctuation = fluctuation_down
    else:
        fluctuation = fluctuation_up

    
    for ratio_hist, lineColor in zip(ratio_hists, nom_lineColors):
        if Count == 0 :
            ratio_hist.GetXaxis().SetRangeUser(min_x, 1.5*max_x)
            #ratio_hist.GetXaxis().SetRangeUser(0, 1.5*max_x)
            ratio_hist.GetYaxis().SetRangeUser(1-fluctuation*1.5, 1+fluctuation*1.5)
            ratio_hist.Draw("HIST")
            ratio_hist.GetXaxis().SetTitle(xtitle)
        else:
            ratio_hist.Draw("HIST SAME")
            #ratioHist.Draw("PL SAME","","SAME")
        
        Count += 1 
        c.SaveAs(outputPlot)

def GetMinX(hist):
    min = hist.GetXaxis().GetXmin();
    return min
def GetMaxX(hist):
    maxbin = hist.GetNbinsX()
    c1=hist.GetBinCenter(maxbin)
    c2=hist.GetBinCenter(maxbin-1)
    max = c1 + ( c1 - c2 )/2

    return max

def GetMaxY(hists):
    max = -1
    for hist in hists : 
        for i in range(hist.GetNbinsX()):
            if hist.GetBinContent(i+1) > max : 
                max = hist.GetBinContent(i+1)
    
    return max 


def GetMinY(hists):
    min = 1000000
    for hist in hists : 
        for i in range(hist.GetNbinsX()):
            if hist.GetBinContent(i+1) < min : 
                min = hist.GetBinContent(i+1)

    return min
if __name__ == "__main__":


    RUNNUM="340000"
    # DrawCommand = ["Digits_MM_time>>h_digit_time(40,0,200)", "...", ""]
    # ArrayNames = ["A", "B", ..]
    # ArrayXtitle = ["A", ...]
    # Ytitle = "dsds"

    # Get the histograms
    #rootfileA_ggh="../miniswap/ggh_mc16a_part.root"

    # ==================
    # Color
    # ==================
         
    # ==================
    # ROOT FILE
    # ==================
    #FileDir="/eos/user/c/chenye/share/FinalResults/"

    #rootfile_data=FileDir+"data.root"
    #rootfile_data="output_Data.root"
    #rootfileE_Zmumu="/afs/cern.ch/user/c/chenye/private/Yusheng-MakePlot/OnePlot/Zmumu_mc16e_585_mu0_45_met15.root"
    #rootfile_dir="/afs/cern.ch/work/c/chenye/public/Hmm/Yusheng/mc16a.340000_ggF/"
    rootfile_dir="/eos/user/c/chenye/www/hmm/Systematic_Results/340000/all/"
    #rootfile_dir="/afs/cern.ch/user/c/chenye/private/work/Systematic_hmumu/results/test/"
    rootfile_name = [
        "340000_output_DiMuonNtuple_BDT-1.root",
        "340000_output_DiMuonNtupleEG_RESOLUTION_ALL__1down_BDT.root",
        "340000_output_DiMuonNtupleEG_RESOLUTION_ALL__1up_BDT.root",
        "340000_output_DiMuonNtupleEG_SCALE_ALL__1down_BDT.root",
        "340000_output_DiMuonNtupleEG_SCALE_ALL__1up_BDT.root",
        "340000_output_DiMuonNtupleJET_EtaIntercalibration_NonClosure__1down_BDT.root",
        "340000_output_DiMuonNtupleJET_EtaIntercalibration_NonClosure__1up_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_1__1down_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_1__1up_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_2__1down_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_2__1up_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_3__1down_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_3__1up_BDT.root",
        "340000_output_DiMuonNtupleJET_JER_SINGLE_NP__1up_BDT.root",
        "340000_output_DiMuonNtupleMET_SoftTrk_ResoPara_BDT.root",
        "340000_output_DiMuonNtupleMET_SoftTrk_ResoPerp_BDT.root",
        "340000_output_DiMuonNtupleMET_SoftTrk_ScaleDown_BDT.root",
        "340000_output_DiMuonNtupleMET_SoftTrk_ScaleUp_BDT.root",
        "340000_output_DiMuonNtupleMUON_ID__1down_BDT.root",
        "340000_output_DiMuonNtupleMUON_ID__1up_BDT.root",
        "340000_output_DiMuonNtupleMUON_MS__1down_BDT.root",
        "340000_output_DiMuonNtupleMUON_MS__1up_BDT.root",
        "340000_output_DiMuonNtupleMUON_SCALE__1down_BDT.root",
        "340000_output_DiMuonNtupleMUON_SCALE__1up_BDT.root"

    ]
    rootfile_name_EG = [
        #"DiMuonNtuple.root",
        "340000_output_DiMuonNtuple_BDT-1.root",
        "340000_output_DiMuonNtupleEG_RESOLUTION_ALL__1down_BDT.root",
        "340000_output_DiMuonNtupleEG_RESOLUTION_ALL__1up_BDT.root",
        "340000_output_DiMuonNtupleEG_SCALE_ALL__1down_BDT.root",
        "340000_output_DiMuonNtupleEG_SCALE_ALL__1up_BDT.root"
    ]

    rootfile_name_JET = [
        "340000_output_DiMuonNtuple_BDT-1.root",
        "340000_output_DiMuonNtupleJET_EtaIntercalibration_NonClosure__1down_BDT.root",
        "340000_output_DiMuonNtupleJET_EtaIntercalibration_NonClosure__1up_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_1__1down_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_1__1up_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_2__1down_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_2__1up_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_3__1down_BDT.root",
        "340000_output_DiMuonNtupleJET_GroupedNP_3__1up_BDT.root",
        "340000_output_DiMuonNtupleJET_JER_SINGLE_NP__1up_BDT.root"
    ]
    rootfile_name_MUON = [

        "340000_output_DiMuonNtuple_BDT-1.root",
        "340000_output_DiMuonNtupleMUON_ID__1down_BDT.root",
        "340000_output_DiMuonNtupleMUON_ID__1up_BDT.root",
        "340000_output_DiMuonNtupleMUON_MS__1down_BDT.root",
        "340000_output_DiMuonNtupleMUON_MS__1up_BDT.root",
        "340000_output_DiMuonNtupleMUON_SCALE__1down_BDT.root",
        "340000_output_DiMuonNtupleMUON_SCALE__1up_BDT.root"
    ]
    rootfile_name_MET = [
        "340000_output_DiMuonNtuple_BDT-1.root",
        "340000_output_DiMuonNtupleMET_SoftTrk_ResoPara_BDT.root",
        "340000_output_DiMuonNtupleMET_SoftTrk_ResoPerp_BDT.root",
        "340000_output_DiMuonNtupleMET_SoftTrk_ScaleDown_BDT.root",
        "340000_output_DiMuonNtupleMET_SoftTrk_ScaleUp_BDT.root"

    ]


    hists_name    = [ 
        "h_ClassOut_XGB_fJVT_Higgs",
        "h_ClassOut_XGB_fJVT_VBF",
        "h_Event_XGB_fJVT_Category"
    ]

    Systemetic_type1 = [
        "origin",
        "MUON_EFF_ISO_STAT__1down",
        "MUON_EFF_ISO_STAT__1up",
        "MUON_EFF_ISO_SYS__1down",
        "MUON_EFF_ISO_SYS__1up",
        "MUON_EFF_RECO_STAT__1down",
        "MUON_EFF_RECO_STAT__1up",
        "MUON_EFF_RECO_SYS__1down",
        "MUON_EFF_RECO_SYS__1up",
        "MUON_EFF_TTVA_STAT__1down",
        "MUON_EFF_TTVA_STAT__1up",
        "MUON_EFF_TTVA_SYS__1down",
        "MUON_EFF_TTVA_SYS__1up",
        "MUON_EFF_TrigStatUncertainty__1down",
        "MUON_EFF_TrigStatUncertainty__1up",
        "MUON_EFF_TrigSystUncertainty__1down",
        "MUON_EFF_TrigSystUncertainty__1up",
        "PRW_DATASF__1down",
        "PRW_DATASF__1up",
        "FT_EFF_B_systematics__1down",
        "FT_EFF_B_systematics__1up",
        "FT_EFF_C_systematics__1down",
        "FT_EFF_C_systematics__1up",
        "FT_EFF_Light_systematics__1down",
        "FT_EFF_Light_systematics__1up",
        "FT_EFF_extrapolation__1down",
        "FT_EFF_extrapolation__1up",
        "FT_EFF_extrapolation_from_charm__1down",
        "FT_EFF_extrapolation_from_charm__1up",
        "JET_JvtEfficiency__1down",
        "JET_JvtEfficiency__1up",
        "JET_fJvtEfficiency__1down",
        "JET_fJvtEfficiency__1up"
    ]
    Systemetic_type1_EG = [
        "origin"
    ]
    Systemetic_type1_MET = [
        "origin"
    ]

    Systemetic_type1_OTHER = [
        "origin",
        "PRW_DATASF__1down",
        "PRW_DATASF__1up"
    ]

    Systemetic_type1_JET = [
        "origin",
        "FT_EFF_B_systematics__1down",
        "FT_EFF_B_systematics__1up",
        "FT_EFF_C_systematics__1down",
        "FT_EFF_C_systematics__1up",
        "FT_EFF_Light_systematics__1down",
        "FT_EFF_Light_systematics__1up",
        "FT_EFF_extrapolation__1down",
        "FT_EFF_extrapolation__1up",
        "FT_EFF_extrapolation_from_charm__1down",
        "FT_EFF_extrapolation_from_charm__1up",
        "JET_JvtEfficiency__1down",
        "JET_JvtEfficiency__1up",
        "JET_fJvtEfficiency__1down",
        "JET_fJvtEfficiency__1up"
    ]

    Systemetic_type1_MUON = [
        "origin",
        "MUON_EFF_ISO_STAT__1down",
        "MUON_EFF_ISO_STAT__1up",
        "MUON_EFF_ISO_SYS__1down",
        "MUON_EFF_ISO_SYS__1up",
        "MUON_EFF_RECO_STAT__1down",
        "MUON_EFF_RECO_STAT__1up",
        "MUON_EFF_RECO_SYS__1down",
        "MUON_EFF_RECO_SYS__1up",
        "MUON_EFF_TTVA_STAT__1down",
        "MUON_EFF_TTVA_STAT__1up",
        "MUON_EFF_TTVA_SYS__1down",
        "MUON_EFF_TTVA_SYS__1up",
        "MUON_EFF_TrigStatUncertainty__1down",
        "MUON_EFF_TrigStatUncertainty__1up",
        "MUON_EFF_TrigSystUncertainty__1down",
        "MUON_EFF_TrigSystUncertainty__1up"
    ]
    Systemetic_type2_EG = [
        "EG_RESOLUTION_ALL__1down",
        "EG_RESOLUTION_ALL__1up",
        "EG_SCALE_ALL__1down",
        "EG_SCALE_ALL__1up"

    ]
    Systemetic_type2_JET = [
        "JET_EtaIntercalibration_NonClosure__1down",
        "JET_EtaIntercalibration_NonClosure__1up",
        "JET_GroupedNP_1__1down",
        "JET_GroupedNP_1__1up",
        "JET_GroupedNP_2__1down",
        "JET_GroupedNP_2__1up",
        "JET_GroupedNP_3__1down",
        "JET_GroupedNP_3__1up",
        "JET_JER_SINGLE_NP__1up"
    ]

    Systemetic_type2_MET = [
        "MET_SoftTrk_ResoPara",
        "MET_SoftTrk_ResoPerp",
        "MET_SoftTrk_ScaleDown",
        "MET_SoftTrk_ScaleUp"
    ]

    Systemetic_type2_MUON = [
        "MUON_ID__1down",
        "MUON_ID__1up",
        "MUON_MS__1down",
        "MUON_MS__1up",
        "MUON_SCALE__1down",
        "MUON_SCALE__1up"
    ]

    Systemetic_type2 = [
        "EG_RESOLUTION_ALL__1down",
        "EG_RESOLUTION_ALL__1up",
        "EG_SCALE_ALL__1down",
        "EG_SCALE_ALL__1up",
        "JET_EtaIntercalibration_NonClosure__1down",
        "JET_EtaIntercalibration_NonClosure__1up",
        "JET_GroupedNP_1__1down",
        "JET_GroupedNP_1__1up",
        "JET_GroupedNP_2__1down",
        "JET_GroupedNP_2__1up",
        "JET_GroupedNP_3__1down",
        "JET_GroupedNP_3__1up",
        "JET_JER_SINGLE_NP__1up",
        "MET_SoftTrk_ResoPara",
        "MET_SoftTrk_ResoPerp",
        "MET_SoftTrk_ScaleDown",
        "MET_SoftTrk_ScaleUp",
        "MUON_ID__1down",
        "MUON_ID__1up",
        "MUON_MS__1down",
        "MUON_MS__1up",
        "MUON_SCALE__1down",
        "MUON_SCALE__1up"
    ]

    fignames =  [   "mu1_pt_sys","mu1_eta_sys",
                 "mu2_pt_sys","mu2_eta_sys",
                 "met_sys","Mu_sys",
                 "mjj_sys","drjj_sys","Nj_sys",
                 "Ljet_eta_sys","Ljet_pt_sys",
                 "Sjet_eta_sys","Sjet_pt_sys"                    
    ]
    xtitles = [
        "ClassOut_XGB_fJVT_Higgs",
        "ClassOut_XGB_fJVT_VBF",
        "Event_XGB_fJVT_Category",
        "Dimuon Mass (FSR)"
    ]
    titles = [
        "ClassOut_XGB_fJVT_Higgs",
        "ClassOut_XGB_fJVT_VBF",
        "Event_XGB_fJVT_Category",
        "Dimuon Mass (FSR)"
    ]

    #fileinE_Zmumu = TFile(rootfileE_Zmumu)

    #==================================================== 
    # Setup names and legends
    #

    # set up plotting attributes
    #options=["HIST","PLE"]
    options=    ["HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST",
                 "HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST",
                 "HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST"]
    #opt_legends=["f","FLPE"]
    #opt_legends=["HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST",
    #            "HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST",
    #           "HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST","HIST"]

    opt_legends=["PL","PL","PL","PL","PL","PL","PL","PL","PL","PL",
                 "PL","PL","PL","PL","PL","PL","PL","PL","PL","PL",
                 "PL","PL","PL","PL","PL","PL","PL","PL","PL","PL","PL"]
    marker_types=             [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    marker_sizes=             [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    #marker_colors=line_colors=[1,2,3,4,6,7,8,9,11,12,13,14,15,24,27,30,31,32,33,34,38,39,39,40,43,44,45,46,47,49]
    marker_colors=line_colors=[1,2,3,4,800-2,6,7,8,9,11,12,13,14,15,30,31,32,33,34,38,39,39,40,41,42,43,44,45,46,49]
    #marker_colors=line_colors=[1,416-9,416-6,416-3,416+1,600-9,600-7,600-4,600+1,400-9,400-7,400-4,400,632-9,632-7,632-4,632+1,616-3,432-3,800-3,820-3,840-3,860-3,880-3,900-3]
    #marker_colors=line_colors=[1,416-9,416-6,416-3,416+1,600-9,600-7,600-4,600+1,400-9,400-7,400-4,400,632-9,632-7,632-4,632+1,432-3,432-6,432+1,432+3,900+2,900,900-3,900-7]
    line_sizes   =            [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    #fill_colors=              [2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
    fill_colors  =            [1,2,3,4,5,6,7,8,9,11,12,13,14,15,30,31,32,33,34,38,39,39,40,41,42,43,44,45,46,49]
    fill_types=               [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

    #fill_types=[3002, 0]
    #-----------------------------------------------------------
    #-----------------------------------------------------------
    #

    #|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    #
    #                       EG
    #
    #|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    #for physics_name in physics_names : 
    Systemetic_type_EG = Systemetic_type1_EG +Systemetic_type2_EG

    for physics_name ,x_title in  zip (hists_name, xtitles): 
        AllHists = []
        files = []

        #------------------
        #  read Hist 
        #
        print"==============================================================="
        print "readHist"
        print"==============================================================="

        for filename in rootfile_name_EG :
            file_name = rootfile_dir + filename
            print file_name
            myFile = TFile(file_name)
            files += [myFile]

        fileCount=0 
        for filename,file in zip(rootfile_name_EG,files) :
            if fileCount == 0:
                for hist_type1 in Systemetic_type1_EG:

                    print"----------------------------------if--------------"
                    print physics_name+"_"+ hist_type1
                    h1 = file.Get(physics_name+"_"+ hist_type1)
                    #print "BinContent test : %s"% h1.GetBinContent(10)
                    print "BinContent test : %s  | NBinX %s "% (h1.GetBinContent(10), h1.GetNbinsX())
                    AllHists += [h1]
            else : 
                print"----------------------------------else--------------"
                print physics_name
                h2 = file.Get(physics_name)
                print "BinContent test : %s  | NBinX %s "% (h2.GetBinContent(10), h2.GetNbinsX())
                AllHists += [h2]
           
            fileCount += 1

        print"==============================================================="
        print "CHECK ", physics_name 
        print"==============================================================="
        for hist, systype in zip(AllHists,Systemetic_type_EG):
            print 
            print "%s GetNbinsX() %s  GetBinContent %s "%(systype,hist.GetNbinsX(), hist.GetBinContent(10))

        print"==============================================================="
        print "Draw"
        print"==============================================================="

        list_histo = AllHists
        names = Systemetic_type_EG
        legends = Systemetic_type_EG
        legends[0] = "Nominal"

        print "length : names %d legend %d opt_legend %d color %d option %d  "%(len(names),len(legends),len(opt_legends),len(marker_colors),len(options))
        # set fig name
        #dir_name="Plot-Syst-mc16ade-newcolor-bigratio/"
        dir_name="Plot-Syst-"+RUNNUM+"/"
        figname=dir_name+physics_name+"_EG.pdf"
        d = os.path.join(dir_name)
        if not os.path.exists(d):
            os.makedirs(d)
        
        #ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.95, 1.1, "EG Systematics")        
        print "physics_name ----- >>>  %s"%physics_name
        if physics_name == "h_Event_XGB_fJVT_Category":
            print "physics_name ----- >>>  %s pass"%physics_name
            ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.98, 1.02, "EG Systematics")        

        else:
            ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.98, 1.02, "EG Systematics")        
        #ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.97, 1.2, "EG Systematics")        
    #|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    #
    #                       JET
    #
    #|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    #for physics_name in physics_names : 
    Systemetic_type = Systemetic_type1_JET +Systemetic_type2_JET

    for physics_name ,x_title in  zip (hists_name, xtitles): 
        AllHists = []
        files = []

        #------------------
        #  read Hist 
        #
        print"==============================================================="
        print "readHist"
        print"==============================================================="

        for filename in rootfile_name_JET :
            file_name = rootfile_dir + filename
            print file_name
            myFile = TFile(file_name)
            files += [myFile]

        fileCount=0 
        for filename,file in zip(rootfile_name_JET,files) :
            if fileCount == 0:
                for hist_type1 in Systemetic_type1_JET:

                    print"----------------------------------if--------------"
                    print physics_name+"_"+ hist_type1
                    h1 = file.Get(physics_name+"_"+ hist_type1)
                    print "BinContent test : %s  | NBinX %s "% (h1.GetBinContent(10), h1.GetNbinsX())
                    print "BinContent test : %s"% h1.GetBinContent(10)
                    AllHists += [h1]
            else : 
                print"----------------------------------else--------------"
                print physics_name
                h2 = file.Get(physics_name)
                print "BinContent test : %s"%h2.GetBinContent(10)
                AllHists += [h2]
           
            fileCount += 1

        print"==============================================================="
        print "CHECK"
        print"==============================================================="
        for hist, systype in zip(AllHists,Systemetic_type):
            print 
            print "%s GetNbinsX() %s  GetBinContent %s "%(systype,hist.GetNbinsX(), hist.GetBinContent(10))

        print"==============================================================="
        print "Draw"
        print"==============================================================="

        list_histo = AllHists
        names = Systemetic_type
        legends = Systemetic_type
        legends[0] = "Nominal"

        print "length : names %d legend %d opt_legend %d color %d option %d  "%(len(names),len(legends),len(opt_legends),len(marker_colors),len(options))
        # set fig name
        #dir_name="Plot-Syst-mc16ade-newcolor-bigratio/"
        dir_name="Plot-Syst-"+RUNNUM+"/"
        figname=dir_name+physics_name+"_JET.pdf"
        d = os.path.join(dir_name)
        if not os.path.exists(d):
            os.makedirs(d)

        if physics_name == "h_Event_XGB_fJVT_Category":
            ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.8, 1.25, "Jet Systmatics")        
        else:
            ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.85, 1.25, "Jet Systmatics")        
        
        #ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.9, 1.3, "Jet Systmatics")        
        #ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.85, 1.3, "Jet Systmatics")        

    #|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    #
    #                       MET
    #
    #|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

    #for physics_name in physics_names : 
    Systemetic_type = Systemetic_type1_MET +Systemetic_type2_MET

    for physics_name ,x_title in  zip (hists_name, xtitles): 
        AllHists = []
        files = []

        #------------------
        #  read Hist 
        #
        print"==============================================================="
        print "readHist"
        print"==============================================================="

        for filename in rootfile_name_MET :
            file_name = rootfile_dir + filename
            print file_name
            myFile = TFile(file_name)
            files += [myFile]

        fileCount=0 
        for filename,file in zip(rootfile_name_MET,files) :
            if fileCount == 0:
                for hist_type1 in Systemetic_type1_MET:

                    print"----------------------------------if--------------"
                    print physics_name+"_"+ hist_type1
                    h1 = file.Get(physics_name+"_"+ hist_type1)
                    print "BinContent test : %s"% h1.GetBinContent(10)
                    print "BinContent test : %s  | NBinX %s "% (h1.GetBinContent(10), h1.GetNbinsX())
                    AllHists += [h1]
            else : 
                print"----------------------------------else--------------"
                print physics_name
                h2 = file.Get(physics_name)
                print "BinContent test : %s"%h2.GetBinContent(10)
                AllHists += [h2]
           
            fileCount += 1

        print"==============================================================="
        print "CHECK"
        print"==============================================================="
        for hist, systype in zip(AllHists,Systemetic_type):
            print 
            print "%s GetNbinsX() %s  GetBinContent %s "%(systype,hist.GetNbinsX(), hist.GetBinContent(10))

        print"==============================================================="
        print "Draw"
        print"==============================================================="

        list_histo = AllHists
        names = Systemetic_type
        legends = Systemetic_type
        legends[0] = "Nominal"

        print "length : names %d legend %d opt_legend %d color %d option %d  "%(len(names),len(legends),len(opt_legends),len(marker_colors),len(options))
        # set fig name
        #dir_name="Plot-Syst-mc16ade-newcolor-bigratio/"
        dir_name="Plot-Syst-"+RUNNUM+"/"
        figname=dir_name+physics_name+"_MET.pdf"
        d = os.path.join(dir_name)
        if not os.path.exists(d):
            os.makedirs(d)

        if physics_name =="h_Event_XGB_fJVT_Category" :
            ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.95, 1.05, "MET Systematics")        
        else:
            ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.85, 1.15, "MET Systmatics")        
        #ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.95, 1.1, "MET Systematics")        
        #ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.95, 1.05, "MET Systematics")        
        #ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.85, 1.3, "MET Systematics")        

    #|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    #
    #                       MUON
    #
    #|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
    #for physics_name in physics_names : 
    Systemetic_type = Systemetic_type1_MUON +Systemetic_type2_MUON

    for physics_name ,x_title in  zip (hists_name, xtitles): 
        AllHists = []
        files = []

        #------------------
        #  read Hist 
        #
        print"==============================================================="
        print "readHist"
        print"==============================================================="

        for filename in rootfile_name_MUON :
            file_name = rootfile_dir + filename
            print file_name
            myFile = TFile(file_name)
            files += [myFile]

        fileCount=0 
        for filename,file in zip(rootfile_name_MUON,files) :
            if fileCount == 0:
                for hist_type1 in Systemetic_type1_MUON:

                    print"----------------------------------if--------------"
                    print physics_name+"_"+ hist_type1
                    h1 = file.Get(physics_name+"_"+ hist_type1)
                    print "BinContent test : %s"% h1.GetBinContent(10)
                    print "BinContent test : %s  | NBinX %s "% (h1.GetBinContent(10), h1.GetNbinsX())
                    AllHists += [h1]
            else : 
                print"----------------------------------else--------------"
                print physics_name
                h2 = file.Get(physics_name)
                print "BinContent test : %s"%h2.GetBinContent(10)
                AllHists += [h2]
           
            fileCount += 1

        print"==============================================================="
        print "CHECK"
        print"==============================================================="
        for hist, systype in zip(AllHists,Systemetic_type):
            print 
            print "%s GetNbinsX() %s  GetBinContent %s "%(systype,hist.GetNbinsX(), hist.GetBinContent(10))

        print"==============================================================="
        print "Draw"
        print"==============================================================="

        list_histo = AllHists
        names = Systemetic_type
        legends = Systemetic_type
        legends[0] = "Nominal"

        print "length : names %d legend %d opt_legend %d color %d option %d  "%(len(names),len(legends),len(opt_legends),len(marker_colors),len(options))
        # set fig name
        #dir_name="Plot-Syst-mc16ade-newcolor-bigratio/"
        dir_name="Plot-Syst-"+RUNNUM+"/"
        figname=dir_name+physics_name+"_MUON.pdf"
        d = os.path.join(dir_name)
        if not os.path.exists(d):
            os.makedirs(d)

        if physics_name == "h_Event_XGB_fJVT_Category":
            ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.95, 1.05, "Muon Systematics")        
        else:
            ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.95, 1.05,"Muon Systematics")        
        #ratioplot(list_histo, line_colors ,legends, x_title, figname, 0.85, 1.2,"Muon Systematics")        


