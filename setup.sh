#!/bin/bash

# Set up ROOT path for python plotting scripts
# Should be a valid ROOT version with PyROOT setup
#export ROOTLIB='/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.14.04-x86_64-slc6-gcc62-opt/lib'

# Path for Python Modules
# This is set automatically
source /data/wang/softwares/root-6.26.0-opt/bin/thisroot.sh
export PATH_MyPlot=`pwd`
export PATH_MyPlot_Modules=$PATH_MyPlot"/modules/"

# Path for atlas plotting style
export PATH_AtlasStyle=`pwd`"/atlas-style/"

mkdir -p output/Resolved 
mkdir -p output/Merged 
